#include "GCamera.h"
using namespace Game;
//------------------------------------------------------------------------------------
GCamera::GCamera(void)
{
    mPosition			= XMFLOAT3(0.0f, 0.0f, -1.0f);
    mTarget				= XMFLOAT3(0.0f, 0.0f, 0.0f);
    mUp					= GMathVF(GMathFV(mPosition) + GMathFV(XMFLOAT3(0, 1, 0)));
	mUpVector			= GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	mLookAtTargetVector = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));
	this->initViewMatrix();

	mAngle			= 0.0f;
	mClientWidth	= 0;
	mClientHeight	= 0;
	mNearest		= 0.0f;
	mFarthest		= 0.0f;

	XMStoreFloat4x4(&mView, XMMatrixIdentity());
	XMStoreFloat4x4(&mProj, XMMatrixIdentity());
	XMStoreFloat4x4(&mOrtho, XMMatrixIdentity());
	XMStoreFloat4x4(&mReflectedView, XMMatrixIdentity());
}
//------------------------------------------------------------------------------------
GCamera::GCamera( const GCamera& camera )
{
	*this = camera;
}
//------------------------------------------------------------------------------------
const GCamera& GCamera::operator=( const GCamera& camera )
{
    mPosition			= camera.mPosition;
    mTarget				= camera.mTarget;
    mUp					= camera.mUp;
	mUpVector			= camera.mUpVector;
	mLookAtTargetVector = camera.mLookAtTargetVector;

	mAngle			= camera.mAngle;
	mClientWidth	= camera.mClientWidth;
	mClientHeight	= camera.mClientHeight;
	mNearest		= camera.mNearest;
	mFarthest		= camera.mFarthest;

    mView			= camera.mView;
	mReflectedView	= camera.mReflectedView;
	mProj			= camera.mProj;
	mOrtho			= camera.mOrtho;

    return *this;
}
//------------------------------------------------------------------------------------
void GCamera::initViewMatrix()
{
	XMStoreFloat4x4(&mView, XMMatrixLookAtLH(XMLoadFloat3(&mPosition), XMLoadFloat3(&mTarget), 
		XMLoadFloat3(&(this->Up()))));
}
//------------------------------------------------------------------------------------
void GCamera::InitReflectionMatrix( const XMFLOAT4 &plane_parameters )
{
	XMVECTOR plane = XMLoadFloat4(&plane_parameters);
	//mReflectedView = GMathMF( XMMatrixReflect(plane) * GMathFM(mView) );

	//XMFLOAT3 old_position = mPosition;
	//Position( XMFLOAT3( mPosition.x, mPosition.y * -1.0f, mPosition.z ) );
	//mReflectedView = mView;
	//Position( old_position );

	//XMFLOAT3 reflected_position = XMFLOAT3( mPosition.x, mPosition.y * -1.0f, mPosition.z );

	//XMVECTOR position = GMathFV( XMFLOAT3( mPosition.x, mPosition.y * -1.0f, mPosition.z ) );
	//XMVECTOR target = GMathFV( XMFLOAT3( mTarget.x, mTarget.y * -1.0f, mTarget.z ) );
	//XMVECTOR up = GMathFV( XMFLOAT3( mUp.x, mUp.y * -1.0f, mUp.z ) ) - position;
	//XMStoreFloat4x4( &mReflectedView, XMMatrixLookAtLH( position, target, up ) );

	XMMATRIX reflection_matrix = XMMatrixReflect(plane);
	XMVECTOR position = XMVector4Transform( XMLoadFloat4( 
		&XMFLOAT4( mPosition.x, mPosition.y, mPosition.z, 1.0f ) ), reflection_matrix );
	XMVECTOR target = XMVector4Transform( XMLoadFloat4( 
		&XMFLOAT4( mTarget.x, mTarget.y, mTarget.z, 1.0f ) ), reflection_matrix );
	XMVECTOR up = XMVector4Transform( XMLoadFloat4( 
		&XMFLOAT4( mUp.x, mUp.y, mUp.z, 1.0f ) ), reflection_matrix ) - position;
	XMStoreFloat4x4( &mReflectedView, XMMatrixLookAtLH( position, target, up ) );

	
	//XMVECTOR proj_point;
	//XMVECTOR plane_point_vector = GMathFV(plane_point);
	//XMVECTOR unit_plane_vector = XMVector3Normalize(GMathFV(unit_vector));
	//
	//XMVECTOR reflection_point = GMathFV(mPosition);
	//proj_point = reflection_point - 
	//	XMVector3Dot(reflection_point - plane_point_vector, unit_plane_vector) * unit_plane_vector;
	//reflection_point = reflection_point + (proj_point - reflection_point) * 2.0f;
	//XMFLOAT3 reflected_position = GMathVF(reflection_point);

	//reflection_point = GMathFV(mTarget);
	//proj_point = reflection_point - 
	//	XMVector3Dot(reflection_point - plane_point_vector, unit_plane_vector) * unit_plane_vector;
	//reflection_point = reflection_point + (proj_point - reflection_point) * 2.0f;
	//XMFLOAT3 reflected_target = GMathVF(reflection_point);

	//reflection_point = GMathFV(mUp);
	//proj_point = reflection_point - 
	//	XMVector3Dot(reflection_point - plane_point_vector, unit_plane_vector) * unit_plane_vector;
	//reflection_point = reflection_point + (proj_point - reflection_point) * 2.0f;
	//XMFLOAT3 reflected_up_vector = GMathVF(reflection_point - GMathFV(reflected_position));
}
//------------------------------------------------------------------------------------
void GCamera::InitProjMatrix( const float angle, const uint client_width, const uint client_height, 
	const float near_plane, const float far_plane )
{
	mAngle = angle;
	mClientWidth = client_width;
	mClientHeight = client_height;
	mNearest = near_plane;
	mFarthest = far_plane;
	XMStoreFloat4x4(&mProj, XMMatrixPerspectiveFovLH(angle, 
		static_cast<float>( client_width ) / static_cast<float>( client_height ), 
		near_plane, far_plane));
}
//------------------------------------------------------------------------------------
void GCamera::Move( const XMFLOAT3 &direction )
{
	mPosition = GMathVF(XMVector3Transform(GMathFV(mPosition), 
		XMMatrixTranslation(direction.x, direction.y, direction.z)));
	mTarget = GMathVF(XMVector3Transform(GMathFV(mTarget), 
		XMMatrixTranslation(direction.x, direction.y, direction.z)));
	mUp = GMathVF(XMVector3Transform(GMathFV(mUp), 
		XMMatrixTranslation(direction.x, direction.y, direction.z)));
	mUpVector = GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	mLookAtTargetVector = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));

	this->initViewMatrix();
}
//------------------------------------------------------------------------------------
void GCamera::RotateSelf( const XMFLOAT3 &axis, const float degrees )
{
	if ( XMVector3Equal( GMathFV( axis ), XMVectorZero() ) ||
		degrees == 0.0f )
		return;

	XMVECTOR look_at_target = GMathFV(mTarget) - GMathFV(mPosition);
	XMVECTOR look_at_up = GMathFV(mUp) - GMathFV(mPosition);

	// Determine basic local vectors
	// for current camera space.
	XMVECTOR z = XMVector3Normalize( look_at_target ) * axis.z;
	XMVECTOR y = XMVector3Normalize( look_at_up ) * axis.y;
	XMVECTOR x = XMVector3Normalize( 
		XMVector3Cross( look_at_up, look_at_target ) ) * axis.x;
	XMVECTOR world_rotation_axis = x + y + z;

	// Rotate camera's vectors.
	look_at_target = XMVector3Transform(look_at_target, 
		XMMatrixRotationAxis(world_rotation_axis, XMConvertToRadians(degrees)));
	look_at_up = XMVector3Transform(look_at_up, 
		XMMatrixRotationAxis(world_rotation_axis, XMConvertToRadians(degrees)));

	// Restore vectors' end points mTarget and mUp from new rotated vectors.
	mTarget = GMathVF(GMathFV(mPosition) + look_at_target);
	mUp = GMathVF(GMathFV(mPosition) + look_at_up);
	mUpVector = GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	mLookAtTargetVector = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));

	this->initViewMatrix();
}
//------------------------------------------------------------------------------------
void GCamera::Rotate( const XMFLOAT3 &axis, const float degrees)
{
	if (XMVector3Equal(GMathFV(axis), XMVectorZero()) ||
		degrees == 0.0f)
		return;

	// Rotate vectors.
	XMFLOAT3 look_at_target = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));
	XMFLOAT3 look_at_up = GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	look_at_target = GMathVF(XMVector3Transform(GMathFV(look_at_target), 
		XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(degrees))));
	look_at_up = GMathVF(XMVector3Transform(GMathFV(look_at_up), 
		XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(degrees))));

	// Restore vectors' end points mTarget and mUp from new rotated vectors.
	mTarget = GMathVF(GMathFV(mPosition) + GMathFV(look_at_target));
	mUp = GMathVF(GMathFV(mPosition) + GMathFV(look_at_up));
	mUpVector = GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	mLookAtTargetVector = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));

	this->initViewMatrix();
}
//------------------------------------------------------------------------------------
void GCamera::Target( const XMFLOAT3 &new_target )
{
	if (XMVector3Equal(GMathFV(new_target), GMathFV(mPosition)) ||
		XMVector3Equal(GMathFV(new_target), GMathFV(mTarget)))
		return;

	XMFLOAT3 old_look_at_target = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));	
	XMFLOAT3 new_look_at_target = GMathVF(GMathFV(new_target) - GMathFV(mPosition));
	float angle = XMConvertToDegrees(XMVectorGetX(
		XMVector3AngleBetweenNormals(XMVector3Normalize(GMathFV(old_look_at_target)), 
		XMVector3Normalize(GMathFV(new_look_at_target)))));
	if (angle != 0.0f && angle != 360.0f && angle != 180.0f)
	{
		XMVECTOR axis = XMVector3Cross(GMathFV(old_look_at_target), GMathFV(new_look_at_target));
		Rotate(GMathVF(axis), angle);
	}
	mTarget = new_target;
	mUpVector = GMathVF(GMathFV(mUp) - GMathFV(mPosition));
	mLookAtTargetVector = GMathVF(GMathFV(mTarget) - GMathFV(mPosition));
	this->initViewMatrix();
}
//------------------------------------------------------------------------------------
void GCamera::Position( const XMFLOAT3& new_position )
{
	XMFLOAT3 move_vector = GMathVF(GMathFV(new_position) - GMathFV(mPosition));
	XMFLOAT3 target = mTarget;
	this->Move(move_vector);
	this->Target(target);
}
//------------------------------------------------------------------------------------
void GCamera::Angle( const float angle )
{
	mAngle = angle;
	InitProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}
//------------------------------------------------------------------------------------
void GCamera::NearestPlane( const float nearest )
{
	mNearest = nearest;
	OnResize(mClientWidth, mClientHeight);
}
//------------------------------------------------------------------------------------
void GCamera::FarthestPlane( const float farthest )
{
	mFarthest = farthest;
	OnResize(mClientWidth, mClientHeight);
}
//------------------------------------------------------------------------------------
void GCamera::InitOrthoMatrix( const uint clientWidth, const uint clientHeight,
		const float nearZ, const float fartherZ )
{
	XMStoreFloat4x4( &mOrtho, XMMatrixOrthographicLH( static_cast<float>( clientWidth ), 
		static_cast<float>( clientHeight ), 0.0f, fartherZ ) );
}
//------------------------------------------------------------------------------------
void GCamera::OnResize( const uint new_width, const uint new_height )
{
	mClientWidth = new_width;
	mClientHeight = new_height;
	InitProjMatrix( mAngle, new_width, new_height, mNearest, mFarthest );
	InitOrthoMatrix( new_width, new_height, 0.0f, mFarthest );
}
//------------------------------------------------------------------------------------