#include "GTerrain.h"
using namespace Game;
//------------------------------------------------------------------------------------
GTerrain::GTerrain(ID3D11Device *device, ID3D11DeviceContext *context, 
					  float length_x, float length_z, float height, std::string height_map_filename)
{
	mpDevice = device;
	mpContext = context;
	mpVertexBuffer = nullptr;
	mpIndexBuffer = nullptr;
	mLengthX = length_x;
	mLengthZ = length_z;
	mHeight = height;
	mHeightMapFilename = height_map_filename;
	mWorld = GMathMF(XMMatrixIdentity());

	// Construct vertices of terrain from file
	GenerateVertices();

	// Create indices
	uint k = 0; // quad number
	uint quads_count = mSegmentsCountX * mSegmentsCountZ;
	mIndices.resize(quads_count * 6);
	uint quad_top_left_point_index = 0;
	for (uint i = 0; i < quads_count; ++i)
	{// for each quad
		// calculate next quad top left point index
		if (i % mSegmentsCountX == 0 && i != 0) 
		{
			quad_top_left_point_index += 2;
		}
		else if (i != 0)
		{
			quad_top_left_point_index += 1;
		}

		// upper triangle
		mIndices[k]		= quad_top_left_point_index;
		mIndices[k+1]	= quad_top_left_point_index + 1;
		mIndices[k+2]	= quad_top_left_point_index + mSegmentsCountX + 1;
		// lower triangle
		mIndices[k+3]	= mIndices[k+2];
		mIndices[k+4]	= mIndices[k+1];
		mIndices[k+5]	= mIndices[k+2] + 1;
		k += 6;
	}

	GenerateNormals();
	ConstructBuffers();
}
//------------------------------------------------------------------------------------
GTerrain::~GTerrain(void)
{
	ReleaseCOM(mpIndexBuffer);
	ReleaseCOM(mpVertexBuffer);
}
//------------------------------------------------------------------------------------
void GTerrain::draw()
{
	uint strides = GVertex::GetSize();
	uint offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &strides, &offset);
	mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	mpContext->DrawIndexed(mIndices.size(), 0, 0);
}
//------------------------------------------------------------------------------------
void GTerrain::ConstructBuffers()
{
	if (mVertices.size() <= 0)
		return;

	// Clear old buffers
	ReleaseCOM(mpIndexBuffer);
	ReleaseCOM(mpVertexBuffer);	

	// Feed vertex data
	void *vertices = GetVertexData();

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mpDevice->CreateBuffer(&verticesDesc, &verticesData, &mpVertexBuffer));
	delete[] vertices;

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mpDevice->CreateBuffer(&indicesDesc, &indicesData, &mpIndexBuffer));
}
//------------------------------------------------------------------------------------
void GTerrain::GenerateVertices()
{
	GImage image;
	image.LoadBitmap(mHeightMapFilename);

	mSegmentsCountX = image.GetWidth() - 1;
	mSegmentsCountZ = image.GetHeight() - 1;
	float width_segment_length = mLengthX/mSegmentsCountX;
	float height_segment_length = mLengthZ/mSegmentsCountZ;
	float width_texel_length = 1.0f/mSegmentsCountX;
	float height_texel_length = 1.0f/mSegmentsCountZ;
	XMFLOAT3 point(0.0f, 0.0f, 0.0f);
	XMFLOAT2 point_tex_coords(0.0f, 0.0f);
	XMFLOAT3 translate_coords(mLengthX/-2.0f, 0.0f, mLengthZ/2.0f);
	uint vertices_count = image.GetWidth() * image.GetHeight();

	mVertices.clear();
	mVertices.resize(vertices_count);
	mVertices[0].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
	mVertices[0].mPosition.y = image.AccessData()[0][0].r * mHeight;
	mVertices[0].mTextureCoordinates = point_tex_coords;
	mVertices[0].mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	int row_index = 0;
	int column_index = 0;
	for (uint i=1; i<vertices_count; ++i)
	{
		// if point is first in line
		if (i % (mSegmentsCountX + 1) == 0)
		{// reset it's coordinates
			point.x = 0.0f;
			point.z -= height_segment_length;
			point_tex_coords.x = 0;
			point_tex_coords.y += height_texel_length;
			
			row_index += 1;
			column_index = 0;
		}
		else
		{
			point.x += width_segment_length;
			point_tex_coords.x += width_texel_length;

			column_index += 1;
		}

		mVertices[i].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
		mVertices[i].mPosition.y = image.AccessData()[row_index][column_index].r * mHeight;
		mVertices[i].mTextureCoordinates = point_tex_coords;
		mVertices[i].mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);

		// TODO: Tangent, binormal
	}
}
//------------------------------------------------------------------------------------
void GTerrain::GenerateNormals()
{
	uint indices_count = mIndices.size();
	XMVECTOR vec1, vec2;
	for (uint i = 0; i < indices_count; i += 3)
	{
		vec1 = GMathFV(mVertices[mIndices[i + 1]].mPosition) - GMathFV(mVertices[mIndices[i]].mPosition);
		vec2 = GMathFV(mVertices[mIndices[i + 2]].mPosition) - GMathFV(mVertices[mIndices[i]].mPosition);
		vec1 = XMVector3Normalize(XMVector3Cross(vec1, vec2));		
		mVertices[mIndices[i]].mNormal = GMathVF(GMathFV(mVertices[mIndices[i]].mNormal) + vec1);
		mVertices[mIndices[i + 1]].mNormal = GMathVF(GMathFV(mVertices[mIndices[i + 1]].mNormal) + vec1);
		mVertices[mIndices[i + 2]].mNormal = GMathVF(GMathFV(mVertices[mIndices[i + 2]].mNormal) + vec1);
	}
	for (GVertex& vertex : mVertices)
	{
		vertex.mNormal = GMathVF(XMVector3Normalize(GMathFV(vertex.mNormal)));
	}
}
//------------------------------------------------------------------------------------
void* GTerrain::GetVertexData()
{
	uint verticesNumber = mVertices.size();
	GVertexData *vertices = new GVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}
	return static_cast<void*>(vertices);
}
//------------------------------------------------------------------------------------