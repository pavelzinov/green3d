#include "GPlayer.h"
using namespace Game;


GPlayer::GPlayer(void)
{
	mMovementVelocity = 100.0f; // meters/sec
	mTurnVelocity = 70.0f; // degrees/sec
	mTimeBufferMax = 1.5f;
	mTimeBuffer[0] = mTimeBufferMax;
	mTimeBuffer[1] = mTimeBufferMax;
	mIsMovingForward = false;
	mIsMovingBackward = false;
	mIsTurningLeft = false;
	mIsTurningRight = false;
}

void GPlayer::Initialize( float degrees, uint screen_width, uint screen_height, float near_z, float far_z )
{
	mFirstPersonLook.Position( XMFLOAT3( 0.0f, 5.0f, -100.0f ) );
	mFirstPersonLook.Target( XMFLOAT3( 0.0f, 5.0f, 0.0f ) );
	mFirstPersonLook.InitProjMatrix( XMConvertToRadians( degrees ), screen_width,
		screen_height, near_z, far_z );
	mFirstPersonLook.InitOrthoMatrix( screen_width, screen_height, near_z, far_z );
}

void GPlayer::Update(const float dSeconds)
{
	XMVECTOR look_direction = GMathFV(XMFLOAT3(mFirstPersonLook.LookAtTarget()));
	look_direction = XMVector3Normalize(look_direction);
	float delta_distance = 0.0f;
	float delta_angle = 0.0f;
	float current_speed = 0.0f;

	if (mIsMovingForward)
	{
		mFirstPersonLook.Move(GMathVF(look_direction * mMovementVelocity * dSeconds));
		mIsMovingForward = false;
	}
	else
	{
		current_speed = -mMovementVelocity / mTimeBufferMax * 
			(mTimeBufferMax - mTimeBuffer[0] + dSeconds) + mMovementVelocity;
		delta_distance = current_speed * dSeconds;
		mTimeBuffer[0] -= dSeconds;
		if (mTimeBuffer[0] > 0)
		{
			mFirstPersonLook.Move(GMathVF(look_direction * delta_distance));
		}
	}
	if (mIsMovingBackward)
	{
		mFirstPersonLook.Move(GMathVF(look_direction * -mMovementVelocity * dSeconds));
		mIsMovingBackward = false;
	}
	else
	{
		current_speed = -mMovementVelocity / mTimeBufferMax * 
			(mTimeBufferMax - mTimeBuffer[1] + dSeconds) + mMovementVelocity;
		delta_distance = current_speed * dSeconds;
		mTimeBuffer[1] -= dSeconds;
		if (mTimeBuffer[1] > 0)
		{
			mFirstPersonLook.Move(GMathVF(look_direction * -delta_distance));
		}
	}
	if (mIsTurningLeft)
	{
		mFirstPersonLook.Rotate(XMFLOAT3(0.0f, 1.0f, 0.0f), -mTurnVelocity * dSeconds);
		mIsTurningLeft = false;
	}
	if (mIsTurningRight)
	{
		mFirstPersonLook.Rotate(XMFLOAT3(0.0f, 1.0f, 0.0f), mTurnVelocity * dSeconds);
		mIsTurningRight = false;
	}
}


void GPlayer::MoveForward()
{
	if (mIsMovingBackward)
		mIsMovingBackward = false;
	mIsMovingForward = true;
	// restore time buffer
	mTimeBuffer[0] = mTimeBufferMax;
}


void GPlayer::MoveBackward()
{
	if (mIsMovingForward)
		mIsMovingForward = false;
	mIsMovingBackward = true;
	// restore time buffer
	mTimeBuffer[1] = mTimeBufferMax;
}


void GPlayer::TurnLeft()
{
	if (mIsTurningRight)
		mIsTurningRight = false;
	mIsTurningLeft = true;
}


void GPlayer::TurnRight()
{
	if (mIsTurningLeft)
		mIsTurningLeft = false;
	mIsTurningRight = true;
}


GPlayer::~GPlayer(void)
{
}
