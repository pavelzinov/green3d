#include "GShadersManager.h"
using namespace Game;
//------------------------------------------------------------------------------------
GShadersManager::GShadersManager( ID3D11Device *device, ID3D11DeviceContext *context )
{
	mpDevice = device;
	mpContext = context;

	// Create blend states.
	D3D11_BLEND_DESC blend_desc;
	ZeroMemory( &blend_desc, sizeof( blend_desc ) );
	blend_desc.AlphaToCoverageEnable = true;
	blend_desc.IndependentBlendEnable = true;
	blend_desc.RenderTarget[0].BlendEnable = TRUE;
	blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	HR( mpDevice->CreateBlendState( &blend_desc, &mpBlendStateEnableAlpha ) );

	mpBlendStateDisableAlpha = nullptr;
}
//------------------------------------------------------------------------------------
GShadersManager::~GShadersManager(void)
{
	ReleaseCOM( mpBlendStateDisableAlpha );
	ReleaseCOM( mpBlendStateEnableAlpha );
	for ( auto &shader_pass : mShaderPasses ) delete shader_pass.second;
}
//------------------------------------------------------------------------------------
void GShadersManager::AddShaderPass( const std::string &shader_id, 
	const std::string &vertex_shader_file, 
	const std::string &vertex_shader_main_routine_name )
{
	std::vector<std::string> shader_names;
	shader_names.push_back( "VS" );
	std::vector<std::string> shader_files;
	shader_files.push_back( vertex_shader_file );
	std::vector<std::string> shader_main_routine_names;
	shader_main_routine_names.push_back( vertex_shader_main_routine_name );

	AddShaderPass( shader_id, shader_names, shader_files, 
		shader_main_routine_names );
}
//------------------------------------------------------------------------------------
void GShadersManager::AddShaderPass( const std::string &shader_id, 
	const std::vector<std::string> &shader_names, 
	const std::vector<std::string> &shader_files, 
	const std::vector<std::string> &shader_main_routine_names )
{
	if ( shader_files.size() != shader_main_routine_names.size() ||
		shader_files.size() != shader_names.size() )
		return;

	mShaderPasses.append( shader_id, new GShaderPass( mpDevice, mpContext ) );

	for ( uint i = 0; i < shader_names.size(); ++i )
	{
		if ( shader_names[i] == "VS" )
		{
			mShaderPasses[shader_id]->LoadVertexShader( shader_files[i], 
				shader_main_routine_names[i] );
		}
		else if ( shader_names[i] == "GS" )
		{
			mShaderPasses[shader_id]->LoadGeometryShader( shader_files[i], 
				shader_main_routine_names[i] );
		}
		else if ( shader_names[i] == "PS" )
		{
			mShaderPasses[shader_id]->LoadPixelShader( shader_files[i], 
				shader_main_routine_names[i] );
		}
	}
}
//------------------------------------------------------------------------------------