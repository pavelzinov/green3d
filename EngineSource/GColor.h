#pragma once
#include <DirectXMath.h>
using DirectX::XMFLOAT4;

namespace Game
{

class GColor
{
public:
	GColor(void);
	~GColor(void);

	static const XMFLOAT4 WHITE;
	static const XMFLOAT4 BLACK;
	static const XMFLOAT4 RED;
	static const XMFLOAT4 GREEN;
	static const XMFLOAT4 BLUE;
	static const XMFLOAT4 YELLOW;
	static const XMFLOAT4 CYAN;
	static const XMFLOAT4 MAGENTA;
	static const XMFLOAT4 BEACH_SAND;
	static const XMFLOAT4 LIGHT_BLUE;
	static const XMFLOAT4 LIGHT_YELLOW_GREEN;
	static const XMFLOAT4 DARK_YELLOW_GREEN;
	static const XMFLOAT4 DARK_BROWN;
	static const XMFLOAT4 GREY;
};

} // namespace