#include "GParticles.h"
using namespace Game;
//------------------------------------------------------------------------------------
GParticles::GParticles( ID3D11Device *device, ID3D11DeviceContext *context, float particle_size, 
	const XMFLOAT3 &particle_point_emitter, float particle_maximum_lifetime, 
	uint particles_per_second, const std::string &texture_id )
{
	mpDevice = device;
	mpContext = context;

	mParticleSize = particle_size;
	mParticlesEmitterPosition = particle_point_emitter;
	mParticleLifetime = particle_maximum_lifetime;
	mTextureID = texture_id;
	mParticlesPerSecond = particles_per_second;
	mCapacity = 1000;
	mVertexBufferData.resize( mCapacity );

	mpVertexBuffer = nullptr;

	ConstructDynamicBuffers();
}
//------------------------------------------------------------------------------------
GParticles::~GParticles()
{
	ReleaseCOM( mpVertexBuffer );
}
//------------------------------------------------------------------------------------
void GParticles::ConstructDynamicBuffers()
{
	ReleaseCOM( mpVertexBuffer );

	// Feed vertex data
	uint verticesNumber = mParticles.size();
	mVertexBufferData.resize( mCapacity );
	uint i = 0;
	for ( const auto &particle : mParticles )
	{
		mVertexBufferData[i].mPosition = particle.mPosition;
		++i;
	}

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * mCapacity;
	verticesDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DYNAMIC;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = mVertexBufferData.data();
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR( mpDevice->CreateBuffer( &verticesDesc, &verticesData, &mpVertexBuffer ) );
}
//------------------------------------------------------------------------------------
void GParticles::UpdateBuffers()
{
	uint verticesNumber = mParticles.size();
	if ( verticesNumber <= 0 )
		return;

	uint i = 0;
	for ( const auto &particle : mParticles )
	{
		mVertexBufferData[i].mPosition = particle.mPosition;
		++i;
	}

	D3D11_MAPPED_SUBRESOURCE mappedVertices;
	mpContext->Map( mpVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedVertices );
	memcpy( mappedVertices.pData, static_cast<void*>( mVertexBufferData.data() ), 
		( verticesNumber * GVertex::GetSize() ) );
	mpContext->Unmap( mpVertexBuffer, 0 );
}
//------------------------------------------------------------------------------------
void GParticles::Update( float delta_seconds )
{
	// Remove old particles for which lifetime exceeds given one.
	mParticles.remove_if( [&]( Particle &particle ) {
		return particle.mCurrentLifeTime + delta_seconds >= particle.mLifeTime;
	});

	// Update old particles.
	for ( auto &particle : mParticles )
	{
		particle.mCurrentLifeTime += delta_seconds;
		particle.mPosition = GMathVF( GMathFV( particle.mPosition ) + 
			GMathFV( particle.mVelocity ) * delta_seconds );
	}

	// Add new particles.
	uint append_particles_count = static_cast<uint>( 
		static_cast<float>( mParticlesPerSecond ) * delta_seconds );
	if ( append_particles_count > mParticlesPerSecond ) append_particles_count = mParticlesPerSecond;
	Particle new_particle;
	new_particle.mPosition = mParticlesEmitterPosition;
	new_particle.mCurrentLifeTime = 0.0f;
	
	for ( uint i = 0; i < append_particles_count; ++i )
	{
		new_particle.mVelocity = XMFLOAT3( mRandom.Gen( 100.0f, -100.0f ), 
			mRandom.Gen( 100.0f, -100.0f ), mRandom.Gen( 100.0f, -100.0f ) );
		new_particle.mLifeTime = mRandom.Gen( mParticleLifetime );
		mParticles.push_back( new_particle );
	}

	if ( mParticles.size() > mCapacity )
	{
		// WARNING: hard coded!
		mCapacity = mParticles.size() + 1000;
		ConstructDynamicBuffers();
	}
	else
	{
		UpdateBuffers();
	}
}
//------------------------------------------------------------------------------------
void GParticles::Render() const
{
	if ( mParticles.size() <= 0 )
		return;

	uint strides = GVertex::GetSize();
	uint offset = 0;
	mpContext->IASetVertexBuffers( 0, 1, &mpVertexBuffer, &strides, &offset );
	mpContext->Draw( mParticles.size(), 0 );
}
//------------------------------------------------------------------------------------