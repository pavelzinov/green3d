#pragma once
#include "GUtility.h"
#include "GModel.h"
#include "GInput.h"
#include "GCamera.h"
#include "GLight.h"
#include "GText.h"
#include "GApplicationInformation.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GSceneManager
	{
	public:
		GSceneManager(void) {}
		~GSceneManager(void) {}

		// Initialize scene. TODO: this function must not refer to rendering elements of application
		// in future!!!
		void Initialize(GTexturesManager* textures_manager, GMaterialsManager* materials_manager, 
			const GApplicationInformation &app_info, ID3D11Device *device, ID3D11DeviceContext *context);

		// Update scene objects.
		void OnUpdate(const float delta_seconds);

		// Update scene objects when some user input occurs.
		void OnInput(const GInput& input);

		// Get all scene models.
		const std::vector<GModel>& GetModels() const { return mModels; }

		// Get all scene lights.
		const std::vector<GLight>& GetLights() const { return mLights; }

		// Get all scene cameras. The first one is the main camera and always initialized.
		const std::vector<GCamera>& GetCameras() const { return mCameras; }

	private:
		// Stores all models of the scene.
		std::vector<GModel>	mModels;
		
		// Stores all light sources in scene.
		std::vector<GLight> mLights;
		
		// Random number generator.
		GRandom mRandom;

		// Stores all of the scene cameras.
		// First camera in the vector is the main one.
		// Main camera must always be initialized.
		std::vector<GCamera> mCameras;
	}; // class GSceneManager
} // namespace
//------------------------------------------------------------------------------------