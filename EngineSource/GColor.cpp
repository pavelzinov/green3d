#include "GColor.h"
using namespace Game;
//------------------------------------------------------------------------------------
const XMFLOAT4 GColor::WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GColor::BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GColor::RED(1.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GColor::GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GColor::BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GColor::YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GColor::CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GColor::MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GColor::BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
const XMFLOAT4 GColor::LIGHT_BLUE(0.2f, 0.8f, 1.0f, 1.0f);
const XMFLOAT4 GColor::LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
const XMFLOAT4 GColor::DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
const XMFLOAT4 GColor::DARK_BROWN(0.45f, 0.39f, 0.34f, 1.0f);
const XMFLOAT4 GColor::GREY(0.4f, 0.4f, 0.4f, 1.0f);
//------------------------------------------------------------------------------------
GColor::GColor(void)
{
}
//------------------------------------------------------------------------------------
GColor::~GColor(void)
{
}
//------------------------------------------------------------------------------------