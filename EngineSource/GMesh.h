#pragma once
#include "GUtility.h"
#include "GMaterial.h"
#include "GObjFile.h"
#include "GVertex.h"
#include "GMaterialsManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// GMesh class.
	//
	// Stores a bunch of vertices with the same
	// material characteristics. Usually it forms some
	// kind of simple object with single material
	// over it.
	// 
	// This class supports loading single mesh from .OBJ
	// file. This mesh usually has its own index, based on
	// its position among other meshes in single file.
	//
	// 
	////////////////////////////////////////////////////////
	class GMesh
	{
	public:
		// Construct mesh with null buffers, and obj_file object, containing geometry and material
		// Since single OBJ file can contain lots of meshes, user have to specify primitive index
		// which starts from 0.
		GMesh(const std::string& mesh_name, const GObjFile& obj_file, const uint primitive_index, 
			const GMaterialsManager &materials, ID3D11Device *device, ID3D11DeviceContext *context);
		
		// Construct mesh from external mesh.
		GMesh(const GMesh &mesh);
		
		// Assign mesh to current one.
		const GMesh& operator=(const GMesh &mesh);
		
		// Attach vertices if material is the same.
		const GMesh operator+(const GMesh &mesh) const;
		virtual ~GMesh(void);
		
		// Load mesh from OBJ file parser.
		void LoadFromObj(const GObjFile& obj_file, const uint primitive_index, 
			const GMaterialsManager &materials);
		
		// Load from array of meshes with the same material as first mesh in a sequence.
		//void LoadFromMeshes(const std::vector<GMesh*> &meshes);
		
		// Assign material.
		void SetMaterial(const GMaterial& material);
		
		// Retrieve mesh's current material.
		const std::string& GetMaterialID() const { return mMaterialID; }
		
		// Get mesh's name.
		//const std::string& GetName() const { return mName; }
		
		// Draw mesh by single draw call.
		void draw() const;
		
		// Returns mesh's world matrix.
		const XMFLOAT4X4& GetWorld() const { return mWorld; }
		
		// Rotate mesh around given axis in local mesh's coordinate system.
		// Angle must be in degrees.
		void RotateSelf(const XMFLOAT3 &axis, float degrees);
		
		// Scale mesh coordinates along each axis.
		void Scale(float dx, float dy, float dz);
		
		// Scale mesh coordinates along each axis by the same value.
		void Scale(float delta_size);
		
		// Move mesh by vector.
		void Move(XMFLOAT3 direction);
		
		// Move mesh by individual values along each axis.
		void Move(float dir_x, float dir_y, float dir_z);

		// Returns the number of vertices that mesh consists of
		const uint GetVertexCount() const { return mVertices.size(); }

	protected:
		// Append vertices, transformed by matrix `world`.
		// This action is not reconstructing buffers
		void AppendVertices(const std::vector<GVertex> &vertices, XMFLOAT4X4 world);
		
		// Append new vertices. This action is not reconstructing buffers.
		void AppendVertices(const std::vector<GVertex> &vertices);
		
		// (Re)constructs vertex and index buffers
		virtual void ConstructBuffers();
		
		// Returns pointer to dynamic array of vertex data. Remember to call delete[] on 
		// its result once you have finished working with it.
		void* GetVertexData();
		
		// Parse .OBJ file data line by line.
		void ParseObjData(vector<std::string>& file_lines);
		
		// Calculate tangent and binormal for triangle, 
		// composed by v1, v2 and v3 vertices, passed in clockwise order.
		void CalculateTangentAndBinormal( GVertex& v1, GVertex& v2, GVertex& v3 );

		// Calculate tangent and binormal for triangle, 
		// composed by v1, v2 and v3 vertices, passed in clockwise order.
		void CalculateNormal( GVertex& v1, GVertex& v2, GVertex& v3 );

		// Construct mWorld matrix from other three.
		// mWorld = mScaling * mRotation * mTranslation
		void UpdateWorld();

		std::string				mName;			// Mesh's unique name
		std::string				mMaterialID;	// Mesh's material name
		std::vector<GVertex>	mVertices;		// Vertices array
		std::vector<uint>		mIndices;		// Indices array

		XMFLOAT4X4				mWorld;			// World matrix
		XMFLOAT4X4				mScaling;		// Scaling matrix
		XMFLOAT4X4				mTranslation;	// Translation matrix
		XMFLOAT4X4				mRotation;		// Rotation matrix

		ID3D11Device			*mDevice;		// DirectX11 device
		ID3D11DeviceContext		*mContext;		// DirectX11 device context
		ID3D11Buffer			*mVB;			// Vertex buffer
		ID3D11Buffer			*mIB;			// Index buffer
	}; // class GMesh

	// Define the smart pointer type for this class.
	typedef std::shared_ptr<GMesh> GMeshPtr;
} // namespace Game
//------------------------------------------------------------------------------------