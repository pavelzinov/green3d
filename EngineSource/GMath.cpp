#include "GMath.h"
using namespace Game;
//------------------------------------------------------------------------------------
const float GMath::INFINITY = FLT_MAX;
const float GMath::PI  = 3.14159265358979323f;
const float GMath::PIDIV2 = 1.570796326794896615f;
const float GMath::PIDIV4 = 0.7853981633974483075f;
const float GMath::EPS = 0.0001f;
uint GMath::id = 0;
//------------------------------------------------------------------------------------
GMath::GMath(void)
{
	
}
//------------------------------------------------------------------------------------
GMath::~GMath(void)
{
}
//------------------------------------------------------------------------------------
const XMFLOAT4X4 GMath::MTranspose(const XMFLOAT4X4 &matrix)
{
	return GMathMF(XMMatrixTranspose(GMathFM(matrix)));
}
//------------------------------------------------------------------------------------
//namespace Game
//{
//const XMFLOAT4X4 operator*( const XMFLOAT4X4 &matrix1, const XMFLOAT4X4 &matrix2 )
//{
//	XMFLOAT4X4 result;
//	XMStoreFloat4x4( &result, XMLoadFloat4x4( &matrix1 ) * XMLoadFloat4x4( &matrix2 ) );
//	return result;
//}
//}
//------------------------------------------------------------------------------------
//const XMFLOAT4X4 operator*( XMFLOAT4X4 matrix1, XMFLOAT4X4 matrix2 )
//{
//	XMFLOAT4X4 result;
//	XMStoreFloat4x4( &result, XMLoadFloat4x4( &matrix1 ) * XMLoadFloat4x4( &matrix2 ) );
//	return result;
//}
////------------------------------------------------------------------------------------