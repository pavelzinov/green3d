#pragma once
#include "GUtility.h"
//------------------------------------------------------------------------------------
namespace Game
{
	struct GApplicationInformation 
	{
		std::string mWindowTitle;
		uint mClientWidth;
		uint mClientHeight;
		uint mUpdatesPerSecond;
		float mMillisecondsPerUpdate;
		uint mFramesPerSecond;
		float mMillisecondsPerFrame;
		std::string mVideoCardName;
		uint mVideoCardFreeMemory;
		XMFLOAT4 mScreenClearColor;
	};
}