#include "GTexturesManager.h"
using namespace Game;
//------------------------------------------------------------------------------------
GTexturesManager::GTexturesManager( ID3D11Device* device, ID3D11DeviceContext* context,
	ID3D11RenderTargetView* window_render_target_view, 
	ID3D11DepthStencilView* window_depth_stencil_view )
{
	mpDevice = device;
	mpContext = context;	
	mpWindowRenderTargetView = window_render_target_view;
	mpWindowDepthStencilView = window_depth_stencil_view;

	mpRenderTagetView = nullptr;
	mpDepthStencilBuffer = nullptr;
	mpDepthStencilView = nullptr;
	mpSceneDepthStencilSRV = nullptr;
	mpSceneDepthStencilView = nullptr;
	mpSceneDepthStencilTexture = nullptr;

	mShaderTexturesSemantics.append( "NormalMap", "" );
	mShaderTexturesSemantics.append( "ShadowMap", "" );
	mShaderTexturesSemantics.append( "DiffuseMap", "" );
	mShaderTexturesSemantics.append( "AmbientMap", "" );
	mShaderTexturesSemantics.append( "ReflectionMap", "" );
	mShaderTexturesSemantics.append( "LightMap", "" );
	mShaderTexturesSemantics.append( "gCubeMap", "" );
	mShaderTexturesSemantics.append( "gProjectionTexture", "" );

	mRenderedTexturesRestore.reserve( 100 );

	GenerateSamplers();
	SetSamplers();

	// Create depth texture to render to.
	D3D11_TEXTURE2D_DESC TDesc;
	mShadowMapSize = 1024;

	TDesc.Width  = mShadowMapSize;
	TDesc.Height = mShadowMapSize;
	TDesc.MipLevels = 1;
	TDesc.ArraySize = 1;
	TDesc.Format = DXGI_FORMAT_R16_TYPELESS;
	TDesc.SampleDesc.Count = 1;
	TDesc.SampleDesc.Quality = 0;
	TDesc.Usage = D3D11_USAGE_DEFAULT;
	TDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	TDesc.CPUAccessFlags = 0;
	TDesc.MiscFlags = 0;
	HR( mpDevice->CreateTexture2D( &TDesc, 0, &mpSceneDepthStencilTexture ) );

	D3D11_DEPTH_STENCIL_VIEW_DESC DSVDesc;

	DSVDesc.Format = DXGI_FORMAT_D16_UNORM;
	DSVDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DSVDesc.Flags = 0;
	DSVDesc.Texture2D.MipSlice = 0;
	HR( mpDevice->CreateDepthStencilView( mpSceneDepthStencilTexture, &DSVDesc, &mpSceneDepthStencilView ) );

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;

	SRVDesc.Format = DXGI_FORMAT_R16_UNORM;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;
	SRVDesc.Texture2D.MostDetailedMip = 0;
	HR( mpDevice->CreateShaderResourceView( mpSceneDepthStencilTexture, &SRVDesc, &mpSceneDepthStencilSRV ) );
}
//------------------------------------------------------------------------------------
GTexturesManager::GTexturesManager( const GTexturesManager& manager )
{
	// TODO: fill this non-standard copy function.
}
//------------------------------------------------------------------------------------
GTexturesManager::~GTexturesManager( void )
{
	// Unbind all shader resource views.
	std::vector<ID3D11ShaderResourceView*> srvs( mShaderTexturesSemantics.size(), nullptr );
	mpContext->PSSetShaderResources( 0, srvs.size(), srvs.data() );

	ReleaseCOM( mpSceneDepthStencilSRV );
	ReleaseCOM( mpSceneDepthStencilView );
	ReleaseCOM( mpSceneDepthStencilTexture );

	ReleaseCOM( mpDepthStencilView );
	ReleaseCOM( mpDepthStencilBuffer );
	ReleaseCOM( mpRenderTagetView );
	// Release samplers
	for ( auto &item : mSamplers )
	{
		ReleaseCOM( item.second );
	}
}
//------------------------------------------------------------------------------------
std::string GTexturesManager::AddTexture( const std::string &texture_name, GTexture& texture )
{
	auto names = GetTexturesNames();
	auto found = std::find_if( mTextures.begin(), mTextures.end(), 
		[&]( const std::pair<std::string, GTexture> &texture_pair )->bool
	{
		return texture_pair.second.GetFileName() == texture.GetFileName();	
	} );
	if ( found == mTextures.end() )
	{// If .dds file is not in the dictionary, check it's key
		// If the key already exists, add "_auto" to it, and
		// append it to the dictionary.
		if ( !mTextures.contains( texture_name ) )
		{
			mTextures.append( texture_name, texture );
			return texture_name;
		}
		else
		{
			mTextures.append( texture_name + "_auto", texture );
			return texture_name + "_auto";
		}
	}
	else
	{// If .dds file already in dictionary, don't append another one.
		// Return existing key.
		return ( *found ).first;
	}
	
	return texture_name;
}
//------------------------------------------------------------------------------------
std::string GTexturesManager::AddTexture( const std::string &texture_name, const std::string &texture_file )
{
	GTexture texture( mpDevice, texture_name, texture_file );
	return AddTexture( texture_name, texture );
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromFile( const std::string &filename )
{
	GFileAscii file;
	file.Open( filename );
	auto lines = file.GetLines();
	// remove all empty strings
	lines.erase( std::remove_if( lines.begin(), lines.end(), []( const std::string &str )->bool
	{
		return str == "" || str == "\n" || str == "\t";
	} ), lines.end() );
	std::string texture_name;
	std::string texture_file;
	for ( uint i = 0; i < lines.size(); ++i )
	{
		if ( lines[i][0] != '/' && lines[i][1] != '/' && lines[i] != "\n" )
		{
			if ( lines[i].find( "." ) == std::string::npos )
			{
				texture_name = lines[i];
			}
			else
			{
				texture_file = lines[i];
				AddTexture( texture_name, texture_file );
			}
		}		
	}
	return true;
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromMtlFile( const std::string &filename )
{
	GMtlFile file;
	file.Load( filename );
	auto materials = file.GetMaterials();
	for ( GMaterial& material : materials )
	{
		if ( material.map_Ka != "" ) {
			AddTexture( material.mName + ".map_Ka", material.map_Ka );
		}
		if ( material.map_Kd != "" ) {
			AddTexture( material.mName + ".map_Kd", material.map_Kd );
		}
		if ( material.map_Ks != "" ) {
			AddTexture( material.mName + ".map_Ka", material.map_Ks );
		}
		if ( material.map_Ns != "" ) {
			AddTexture( material.mName + ".map_Ns", material.map_Ns );
		}
		if ( material.map_d != "" ) {
			AddTexture( material.mName + ".map_d", material.map_d );
		}
		if ( material.map_bump != "" ) {
			AddTexture( material.mName + ".map_bump", material.map_bump );
		}
		if ( material.disp != "" ) {
			AddTexture( material.mName + ".map_disp", material.disp );
		}
		if ( material.decal != "" ) {
			AddTexture( material.mName + ".map_decal", material.decal );
		}
	}
	return true;
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromObjFile( const GObjFile &obj_file )
{
	auto mtl_files = obj_file.GetMaterialLibraries();
	for ( const auto &mtl_file : mtl_files )
	{
		LoadAddTexturesFromMtlFile( mtl_file );
	}

	return true;
}
//------------------------------------------------------------------------------------
GTexture& GTexturesManager::GetTexture( const std::string &texture_name )
{
	return mTextures.at( texture_name );
}
//------------------------------------------------------------------------------------
GRenderTargetTexture& GTexturesManager::GetRenderedTexture( const std::string &texture_name )
{
	return mRenderTargetTextures.at( texture_name );
}
//------------------------------------------------------------------------------------
const std::string GTexturesManager::GetTextureNameByFileName( const std::string &file_name ) const
{
	for ( auto &texture : mTextures )
	{
		if ( texture.second.GetFileName() == file_name &&
			!mRenderTargetTextures.contains( texture.first ) )
		{
			return texture.first;
		}
	}
	return "";
}
//------------------------------------------------------------------------------------
std::vector<std::string> GTexturesManager::GetTexturesNames()
{
	std::vector<std::string> names;
	names.reserve( mTextures.size() );

	for ( const auto &texture : mTextures )
	{
		names.push_back( texture.first );
	}

	return names;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetTexture( const std::string &semantic_name, const std::string &texture_name )
{
	if ( semantic_name == "ShadowMap" )
		return;

	if ( !mShaderTexturesSemantics.contains( semantic_name ) )
		return;

	if ( mShaderTexturesSemantics[semantic_name] == texture_name )
		return;

	// Find slot number for texture shader resource
	int slot_number = mShaderTexturesSemantics.index( semantic_name );

	// Set corresponding shader slot to use texture with desired `texture_name`
	ID3D11ShaderResourceView * resources[] = { nullptr };
	// Unbind resource.
	// Find legit texture to bind to shader slot
	if ( mRenderTargetTextures.contains( texture_name ) )
	{// search for texture view in render textures first
		resources[0] = GetRenderedTexture( texture_name ).GetView();
	}
	else if ( mTextures.contains( texture_name ) )
	{
		resources[0] = GetTexture( texture_name ).GetView();
	}
	else
	{
		return;
	}
	
	mpContext->PSSetShaderResources( slot_number, 1, resources );
	mShaderTexturesSemantics[semantic_name] = texture_name;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetShadowMapTexture()
{
	// Find slot number for texture shader resource
	int slot_number = mShaderTexturesSemantics.index( "ShadowMap" );

	// Set corresponding shader slot to use texture with desired `texture_name`
	ID3D11ShaderResourceView * resources[1];
	resources[0] = mpSceneDepthStencilSRV;
	mpContext->PSSetShaderResources( slot_number, 1, resources );
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetTextures( const std::vector<std::pair<std::string, std::string>> &shader_slot_pairs )
{
	for ( auto &shader_slot_pair : shader_slot_pairs )
	{
		SetTexture( shader_slot_pair.first, shader_slot_pair.second );
	}
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetAllCurrentTextures()
{
	// Unbind all textures from pixel shader state
	std::vector<ID3D11ShaderResourceView*> srvs( mShaderTexturesSemantics.size(), nullptr );
	mpContext->PSSetShaderResources( 0, mShaderTexturesSemantics.size(), srvs.data() );
	srvs.clear();
	for ( const auto &semantic : mShaderTexturesSemantics )
	{
		if ( mRenderTargetTextures.contains( semantic.second ) )
		{// search for texture view in render textures first
			srvs.push_back( GetRenderedTexture( semantic.second ).GetView() );
		}
		else if ( mTextures.contains( semantic.second ) )
		{
			srvs.push_back( GetTexture( semantic.second ).GetView() );
		}
		else
		{
			srvs.push_back( nullptr );
		}
	}
	mpContext->PSSetShaderResources( 0, mShaderTexturesSemantics.size(), srvs.data() );
}
//------------------------------------------------------------------------------------
HRESULT GTexturesManager::AddRenderedTexture( const std::string &texture_name, 
	ID3D11RenderTargetView* window_render_target_view, 
	ID3D11DepthStencilView* window_depth_stencil_view, 
	const uint texture_width, const uint texture_height, 
	const uint sample_count, const uint sample_quality,
	DXGI_FORMAT texture_format )
{
	mSampleCount = sample_count;
	mSampleQuality = sample_quality;

	auto names = GetTexturesNames();
	if ( std::find( names.begin(), names.end(), texture_name ) != names.end() )
		return E_FAIL;

	GRenderTargetTexture texture( mpDevice, texture_width, texture_height, texture_format ); 
	mRenderTargetTextures.append( texture_name, texture );

	OnWindowResize( window_render_target_view, window_depth_stencil_view );
	ResizeTexture( texture_name, texture_width, texture_height );

	return S_OK;
}
//------------------------------------------------------------------------------------
void GTexturesManager::RenderToTexture( const std::string &texture_name, XMFLOAT4& clear_color )
{
	// Restore all previously unbound resources.
	std::vector<ID3D11ShaderResourceView*> resources;
	resources.push_back(nullptr);
	int slot_index = -1;
	for ( const auto &semantic_and_name : mRenderedTexturesRestore )
	{
		slot_index = mShaderTexturesSemantics.index( semantic_and_name.first );
		resources.back() = GetRenderedTexture( semantic_and_name.second ).GetView();
		mpContext->PSSetShaderResources( slot_index, 1, resources.data() );
	}

	// Unbind the texture, which we want to render into.
	// It can be bound to different in-shader texture views,
	// so iterate through all mShaderTexturesSemantic array.
	// TODO: optimize it! Now it sets single element at a time.
	mRenderedTexturesRestore.clear();
	resources.back() = nullptr;
	slot_index = -1;

	for ( const auto &semantic_pair : mShaderTexturesSemantics )
	{
		if ( semantic_pair.second == texture_name )
		{
			mRenderedTexturesRestore.append( semantic_pair.first, semantic_pair.second );
		}
	}
	
	for ( const auto &semantic_and_name : mRenderedTexturesRestore )
	{
		slot_index = mShaderTexturesSemantics.index( semantic_and_name.first );
		mpContext->PSSetShaderResources( slot_index, 1, resources.data() );
	}
	
	// Set texture as render target.
	mpContext->OMSetRenderTargets( 0, nullptr, nullptr );
	mpContext->OMSetRenderTargets( 1, &mpRenderTagetView, mpDepthStencilView );
	mpContext->ClearRenderTargetView( mpRenderTagetView, reinterpret_cast<float*>( &clear_color ) );
	mpContext->ClearDepthStencilView( mpDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0 );

	// Set view port to match the size of rendered texture.
	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>( mRenderTargetTextures[texture_name].GetWidth() );
	vp.Height = static_cast<float>( mRenderTargetTextures[texture_name].GetHeight() );
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	mpContext->RSSetViewports( 1, &vp );
}
//------------------------------------------------------------------------------------
void GTexturesManager::RenderToScreen( const uint screen_width, const uint screen_height, 
	const XMFLOAT4& clear_color )
{
	// TODO: REDESIGN!
	mpContext->OMSetRenderTargets( 0, nullptr, nullptr );
	mpContext->OMSetRenderTargets( 1, &mpWindowRenderTargetView, mpWindowDepthStencilView );
	mpContext->ClearRenderTargetView( mpWindowRenderTargetView, reinterpret_cast<const float*>( &clear_color ) );
	mpContext->ClearDepthStencilView( mpWindowDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0 );

	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>( screen_width );
	vp.Height = static_cast<float>( screen_height );
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	
	mpContext->RSSetViewports( 1, &vp );
	
	// Restore all previously unbound resources.
	SetShadowMapTexture();

	std::vector<ID3D11ShaderResourceView*> resources;
	resources.push_back(nullptr);
	int slot_index = -1;
	for ( const auto &semantic_and_name : mRenderedTexturesRestore )
	{
		slot_index = mShaderTexturesSemantics.index( semantic_and_name.first );
		resources.back() = GetRenderedTexture( semantic_and_name.second ).GetView();
		mpContext->PSSetShaderResources( slot_index, 1, resources.data() );
	}
}
//------------------------------------------------------------------------------------
void GTexturesManager::RenderToDepth()
{
	// Clear ShadowMap shader slot from depth texture.

	// Find slot number for texture shader resource
	int slot_number = mShaderTexturesSemantics.index( "ShadowMap" );

	// Set corresponding shader slot to use texture with desired `texture_name`
	ID3D11ShaderResourceView * resources[] = { nullptr };
	mpContext->PSSetShaderResources( slot_number, 1, resources );

	// Set texture as render target.
	ID3D11RenderTargetView* rtv[] = { nullptr, nullptr };
	mpContext->OMSetRenderTargets( 2, rtv, mpSceneDepthStencilView );
	mpContext->ClearDepthStencilView( mpSceneDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0 );

	// Set view port to match the size of rendered texture.
	D3D11_VIEWPORT vp;
	D3D11_TEXTURE2D_DESC desc;
	mpSceneDepthStencilTexture->GetDesc( &desc );
	vp.Width = static_cast<float>( desc.Width );
	vp.Height = static_cast<float>( desc.Height );
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	mpContext->RSSetViewports( 1, &vp );
}
//------------------------------------------------------------------------------------
void GTexturesManager::OnWindowResize( ID3D11RenderTargetView *window_render_target_view, 
	ID3D11DepthStencilView *window_depth_stencil_view )
{
	mpWindowRenderTargetView = window_render_target_view;
	mpWindowDepthStencilView = window_depth_stencil_view;
	ResizeDepthTexture( mShadowMapSize );
}
//------------------------------------------------------------------------------------
void GTexturesManager::ResizeTexture( const std::string &texture_name, const uint new_texture_width, 
	const uint new_texture_height )
{
	ReleaseCOM( mpDepthStencilView );
	ReleaseCOM( mpDepthStencilBuffer );
	ReleaseCOM( mpRenderTagetView );

	// Create the render target texture.
	this->GetRenderedTexture( texture_name ).OnResize( new_texture_width, new_texture_height );
	ID3D11Texture2D *texture = this->GetRenderedTexture( texture_name ).GetTexture2D();

	// Setup the description of the render target view.
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	// Create the render target view.
	HR( mpDevice->CreateRenderTargetView( texture, 
		&renderTargetViewDesc, &mpRenderTagetView ) );

	// Create the depth/stencil buffer and view.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc; // depth/stencil buffer is just a 2D texture,
	ZeroMemory( &depthStencilBufferDesc, sizeof( depthStencilBufferDesc ) );
	depthStencilBufferDesc.ArraySize			= 1;
	depthStencilBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags		= 0;
	depthStencilBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Width				= new_texture_width;
	depthStencilBufferDesc.Height				= new_texture_height;
	depthStencilBufferDesc.MipLevels			= 1;
	depthStencilBufferDesc.MiscFlags			= 0;
	depthStencilBufferDesc.SampleDesc.Count		= mSampleCount;
	depthStencilBufferDesc.SampleDesc.Quality	= mSampleQuality;
	depthStencilBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	HR( mpDevice->CreateTexture2D( &depthStencilBufferDesc, 0, &mpDepthStencilBuffer ) );
	HR( mpDevice->CreateDepthStencilView( mpDepthStencilBuffer, 0, &mpDepthStencilView ) );
}
//------------------------------------------------------------------------------------
void GTexturesManager::ResizeDepthTexture( const uint new_texture_size )
{
	mShadowMapSize = new_texture_size;

	ReleaseCOM( mpSceneDepthStencilSRV );
	ReleaseCOM( mpSceneDepthStencilView );
	ReleaseCOM( mpSceneDepthStencilTexture );

	// Create depth texture to render to.
	D3D11_TEXTURE2D_DESC TDesc;

	TDesc.Width  = mShadowMapSize;
	TDesc.Height = mShadowMapSize;
	TDesc.MipLevels = 1;
	TDesc.ArraySize = 1;
	TDesc.Format = DXGI_FORMAT_R16_TYPELESS;
	TDesc.SampleDesc.Count = 1;
	TDesc.SampleDesc.Quality = 0;
	TDesc.Usage = D3D11_USAGE_DEFAULT;
	TDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	TDesc.CPUAccessFlags = 0;
	TDesc.MiscFlags = 0;
	HR( mpDevice->CreateTexture2D( &TDesc, 0, &mpSceneDepthStencilTexture ) );

	D3D11_DEPTH_STENCIL_VIEW_DESC DSVDesc;

	DSVDesc.Format = DXGI_FORMAT_D16_UNORM;
	DSVDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	DSVDesc.Flags = 0;
	DSVDesc.Texture2D.MipSlice = 0;
	HR( mpDevice->CreateDepthStencilView( mpSceneDepthStencilTexture, &DSVDesc, & mpSceneDepthStencilView ) );

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;

	SRVDesc.Format =  DXGI_FORMAT_R16_UNORM;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;
	SRVDesc.Texture2D.MostDetailedMip = 0;
	HR( mpDevice->CreateShaderResourceView( mpSceneDepthStencilTexture, &SRVDesc, &mpSceneDepthStencilSRV ) );
}
//------------------------------------------------------------------------------------
void GTexturesManager::GenerateSamplers()
{
	// Generate default sampler state
	ID3D11SamplerState* default_sampler_state;
	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR( mpDevice->CreateSamplerState( &samplerDesc, &default_sampler_state ) );

	mSamplers.append( "LinearSampler", default_sampler_state );

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR( mpDevice->CreateSamplerState( &samplerDesc, &default_sampler_state ) );

	mSamplers.append( "PointSampler", default_sampler_state );

	samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	samplerDesc.BorderColor[0] = samplerDesc.BorderColor[1] = 
		samplerDesc.BorderColor[2] = samplerDesc.BorderColor[3] = 1.0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HR( mpDevice->CreateSamplerState( &samplerDesc, &default_sampler_state ) );

	mSamplers.append( "gPointCmpSampler", default_sampler_state );

	default_sampler_state = 0;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetSamplers()
{
	// Set all defined samplers.
	std::vector<ID3D11SamplerState *> samplers;
	for (const auto &sampler : mSamplers)
	{
		samplers.push_back(sampler.second);
	}
	mpContext->PSSetSamplers( 0, samplers.size(), samplers.data() );
}
//------------------------------------------------------------------------------------
bool GTexturesManager::AddSampler( const std::string &sampler_name, D3D11_SAMPLER_DESC &sampler_description )
{
	ID3D11SamplerState* temp_sampler_state;
	HR( mpDevice->CreateSamplerState( &sampler_description, &temp_sampler_state ) );
	bool result = mSamplers.append( sampler_name, temp_sampler_state );
	// return success or fail of insertion
	return result;
}