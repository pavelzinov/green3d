#pragma once
#include "GUtility.h"
#include "GInput.h"
//-----------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// Stores View and Projection matrices used by shaders
	// to translate 3D world into 2D screen surface
	// Camera can be moved and rotated. Also, user can change 
	// camera's target and position
	////////////////////////////////////////////////////////
	class GCamera
	{
	public:
		// Constructs default camera looking at 0,0,0
		// placed at 0,0,-1 with up vector 0,1,0 (note that mUp is NOT a vector - it's vector's end)
		GCamera();
		
		// Create camera, based on another one.
		GCamera( const GCamera& camera );
		
		// Copy all camera's parameters.
		const GCamera& operator=( const GCamera& camera );
		
		~GCamera(void) {}

	private:
		// Initialize camera's View matrix from mPosition, mTarget and mUp coordinates
		void initViewMatrix();

	public:
		// Reflect current view matrix by plane. The plane is defined
		// as a vector, which contains coefficients of the following
		// plane equation:
		// A * x + B * y + C * z + D = 0.
		void InitReflectionMatrix( const XMFLOAT4 &plane_parameters );

		// Initialize camera's perspective Projection matrix
		void InitProjMatrix( const float angle, const uint client_width, const uint client_height, 
			const float nearest, const float farthest );
	
		// Initialize camera's orthogonal projection
		void InitOrthoMatrix( const uint client_width, const uint client_height,
			const float near_plane, const float far_plane );

		// Resize matrices when window size changes
		void OnResize( const uint new_width, const uint new_height );

		///////////////////////////////////////////////
		/*** View matrix transformation interfaces ***/
		///////////////////////////////////////////////

		// Move camera along direction by amounts
		// specified by direction's coordinates.
		void Move( const XMFLOAT3 &direction );
	
		// Rotate camera around `axis` by `degrees`. Axis
		// defined in space relative to camera's look-at-target 
		// and look-at-up vectors. It means that look-at-target
		// vector is z, look-at-up vector is y, and x is a vector
		// along ( look-at-up x look-at-target ) cross product vector.
		void RotateSelf( const XMFLOAT3 &axis, const float degrees );

		// Rotate camera around `axis` by `degrees`. Camera's position is a	
		// pivot point of rotation, so it doesn't change.
		void Rotate( const XMFLOAT3 &axis, const float degrees );
	
		// Set camera position coordinates
		void Position( const XMFLOAT3& new_position );
	
		// Get camera position coordinates
		const XMFLOAT3& Position() const { return mPosition; }
	
		// Change camera target position
		void Target( const XMFLOAT3 &new_target );
	
		// Get camera's target position coordinates
		const XMFLOAT3& Target() const { return mTarget; }
	
		// Get camera's up vector
		const XMFLOAT3& Up() const { return mUpVector; }
	
		// Get camera's look at target vector
		const XMFLOAT3& LookAtTarget() const { return mLookAtTargetVector; }	
	
		// Returns transposed camera's View matrix	
		const XMFLOAT4X4& View() const { return mView; }

		// Returns reflected camera's view matrix
		const XMFLOAT4X4& ReflectedView() const { return mReflectedView; }

		/////////////////////////////////////////////////////
		/*** Projection matrix transformation interfaces ***/
		/////////////////////////////////////////////////////

		// Set view frustum's angle
		void Angle( const float angle );
	
		// Get view frustum's angle
		const float Angle() const { return mAngle; }

		// Set nearest culling plane distance from view frustum's projection plane
		void NearestPlane( const float nearest );
	
		// Set farthest culling plane distance from view frustum's projection plane
		void FarthestPlane( const float farthest );

		// Returns transposed camera's Projection matrix
		const XMFLOAT4X4& Proj() const { return mProj; }
	
		// Returns transposed orthogonal camera matrix
		const XMFLOAT4X4& Ortho() const { return mOrtho; }

		// Rotate and move camera according to the input.
		//void OnInput(const GInput &input) {}

	private:
		/*** Camera parameters ***/
		XMFLOAT3 mPosition;		// Camera's coordinates.
		XMFLOAT3 mTarget;		// View target's coordinates.
		XMFLOAT3 mUp;			// Camera's up vector end coordinates.
		XMFLOAT3 mUpVector;
		XMFLOAT3 mLookAtTargetVector;

		/*** Projection parameters ***/
		float	mAngle;			// Angle of view frustum.
		uint	mClientWidth;		// Window's width.
		uint	mClientHeight;	// Window's height.
		float	mNearest;			// Nearest view frustum plane.
		float	mFarthest;		// Farthest view frustum plane.

		XMFLOAT4X4  mView;		// View matrix.
		XMFLOAT4X4	mReflectedView;	// Reflected view matrix.
		XMFLOAT4X4	mProj;		// Projection matrix.
		XMFLOAT4X4	mOrtho;		// Ortho matrix for drawing without transformation.
	}; // class GCamera
} // namespace Game
//-----------------------------------------------------------------------------------