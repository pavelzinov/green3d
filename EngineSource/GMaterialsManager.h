#pragma once
#include "GUtility.h"
#include "GMaterial.h"
#include "GFileAscii.h"
#include "GString.h"
#include "GMtlFile.h"
#include "GTexturesManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GMaterialsManager
	{
	public:
		GMaterialsManager() {}
		~GMaterialsManager() {}

		// Returns constant material from the manager by its name.
		// Calling this function with empty string parameter of with
		// the material name, which doesn't exist, results in
		// exception being thrown.
		const GMaterial& GetMaterialByName( const std::string &material_name ) const
			{ return mMaterials.at( material_name ); }

		// Add explicitly constructed material. Make sure that
		// all of the texture names, used within this material correspond
		// to the actual textures in GTexturesManager.
		void AddMaterial( const GMaterial& material );

		// Append materials from .mtl file.
		bool LoadAddMaterialsFromFile( const std::string &filename, 
			const GTexturesManager *textures_manager );

		//bool LoadAddMaterialsFromFile(const std::string &filename);

		// Register materials contained within given .obj file.
		// They will be added to the current materials collection.
		bool LoadAddMaterialsFromObjFile( const GObjFile &obj_file, 
			const GTexturesManager *textures_manager );

	private:
		GFastMap<std::string, GMaterial> mMaterials;
	};
	
	// Define the smart pointer type for this class.
	typedef std::shared_ptr<GMaterialsManager> GMaterialsManagerPtr;
}
//------------------------------------------------------------------------------------