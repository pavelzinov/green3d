#include "GText.h"
using namespace Game;
//------------------------------------------------------------------------------------
GText::GText(ID3D11Device* device, ID3D11DeviceContext* context, 
	const int screen_width, const int screen_height, 
	const std::string& font_desc_filename, std::string text)
{
	mpDevice = device;
	mpContext = context;
	mVertexBuffer = nullptr;
	mIndexBuffer = nullptr;
	mCapacity = text.size();
	mText = text;
	mColor = GColor::WHITE;
	mTextureName = "";
	mFontFile = font_desc_filename;
	mRealPosition = XMINT2(0, 0);
	mWorld = GMathMF(XMMatrixTranslation(static_cast<float>(screen_width)/-2.0f, 
		static_cast<float>(screen_height)/2.0f, 0.0f));

	this->DictionaryFromFile(mFontFile);
	this->GenerateGeometry();
	this->ConstructBuffers();
}
//------------------------------------------------------------------------------------
GText::GText(const GText &text)
{
	mVertexBuffer = nullptr;
	mIndexBuffer = nullptr;

	mpDevice = text.mpDevice;
	mpContext = text.mpContext;
	mCapacity = text.mText.size();
	mText = text.mText;
	mColor = text.mColor;
	mTextureName = text.mTextureName;
	mFontFile = text.mFontFile;
	mRealPosition = text.mRealPosition;
	mWorld = text.mWorld;
	mDictionary = text.mDictionary;

	this->GenerateGeometry();
	this->ConstructBuffers();

	//*this = text;
}
//------------------------------------------------------------------------------------
const GText& GText::operator=(const GText &text)
{
	mpDevice = text.mpDevice;
	mpContext = text.mpContext;
	mCapacity = text.mText.size();
	mText = text.mText;
	mColor = text.mColor;
	mTextureName = text.mTextureName;
	mFontFile = text.mFontFile;
	mRealPosition = text.mRealPosition;
	mWorld = text.mWorld;
	mDictionary = text.mDictionary;

	this->GenerateGeometry();
	this->ConstructBuffers();

	return *this;
}
//------------------------------------------------------------------------------------
GText::~GText(void)
{
	ReleaseCOM(mIndexBuffer);
	ReleaseCOM(mVertexBuffer);
}
//------------------------------------------------------------------------------------
void GText::SetText(std::string text)
{
	// Don't do anything if the text is the same
	if (text == mText)
		return;

	mText = text;
	GenerateGeometry();
	if (text.size() > mCapacity)
	{
		mCapacity = text.size();
		ConstructBuffers();
	}
	else
	{
		UpdateBuffers();
	}
}
//------------------------------------------------------------------------------------
void GText::Position(XMINT2 position)
{
	// Update world matrix
	mWorld = GMathMF(GMathFM(mWorld) * XMMatrixTranslation(
		static_cast<float>(position.x - mRealPosition.x), 
		static_cast<float>(-(position.y - (-mRealPosition.y))), 0.f));

	// Update real position
	mRealPosition.x = position.x;
	mRealPosition.y = -static_cast<int>(position.y);	
}
//------------------------------------------------------------------------------------
void GText::SetFontFile(const std::string& font_desc_filename)
{
	mFontFile = font_desc_filename;
	DictionaryFromFile(mFontFile);
	GenerateGeometry();
	UpdateBuffers();
}
//------------------------------------------------------------------------------------
void GText::DictionaryFromFile(const std::string &font_filename)
{
	GFileAscii file;
	file.Open(font_filename);
	auto lines = file.GetLines();
	mTextureName = lines[0];
	GLetter letter;
	for (uint i = 1; i < lines.size(); ++i)
	{
		std::stringstream(lines[i]) >> letter;
		mDictionary.insert(std::pair<char, GLetter>(letter.mLetter, letter));
	}
}
//------------------------------------------------------------------------------------
void GText::GenerateGeometry()
{
	mVertices.clear();
	mIndices.clear();

	XMFLOAT2 top_left_current(0.0f, 0.0f);
	uint current_vertex_index = 0;
	GVertex temp_vertex;
	for (uint i = 0; i < mText.size(); i++)
	{
		if (mText[i] == '\n')
		{// if letter is new line symbol, reset x position and lower y-position
			top_left_current.x = 0.0f;
			top_left_current.y -= mDictionary['A'].mHeight - 1.0f;
			continue;
		}

		if (mText[i] == ' ')
		{// if letter is `space` - skip 5 pixels
			top_left_current.x += 5.0f;
			continue;
		}

		GLetter letter = mDictionary[mText[i]];
		//0 Bottom-Left
		temp_vertex.mPosition = XMFLOAT3(top_left_current.x, top_left_current.y - letter.mHeight, 0.0f);
		temp_vertex.mTextureCoordinates = XMFLOAT2(letter.mStartPosition, 1.0f);
		mVertices.push_back(temp_vertex);
		//1 Bottom-Right
		temp_vertex.mPosition = XMFLOAT3(top_left_current.x + letter.mWidth, top_left_current.y - letter.mHeight, 0.0f);
		temp_vertex.mTextureCoordinates = XMFLOAT2(letter.mEndPosition, 1.0f);
		mVertices.push_back(temp_vertex);
		//2 Top-Left
		temp_vertex.mPosition = XMFLOAT3(top_left_current.x, top_left_current.y, 0.0f);
		temp_vertex.mTextureCoordinates = XMFLOAT2(letter.mStartPosition, 0.0f);
		mVertices.push_back(temp_vertex);
		//3 Top-Right
		temp_vertex.mPosition = XMFLOAT3(top_left_current.x + letter.mWidth, top_left_current.y, 0.0f);
		temp_vertex.mTextureCoordinates = XMFLOAT2(letter.mEndPosition, 0.0f);
		mVertices.push_back(temp_vertex);

		mIndices.push_back(current_vertex_index + 0);
		mIndices.push_back(current_vertex_index + 2);
		mIndices.push_back(current_vertex_index + 3);
		mIndices.push_back(current_vertex_index + 0);
		mIndices.push_back(current_vertex_index + 3);
		mIndices.push_back(current_vertex_index + 1);

		current_vertex_index += 4;
		// Change top-left point so we can draw next letter
		top_left_current.x += letter.mWidth + 1.0f;
	}
}
//------------------------------------------------------------------------------------
void GText::ConstructBuffers()
{
	ReleaseCOM(mIndexBuffer);
	ReleaseCOM(mVertexBuffer);

	if (mVertices.size() <= 0)
		return;

	// Feed vertex data
	uint verticesNumber = mVertices.size();
	GVertexData *vertices = new GVertexData[4 * mCapacity];
	for (uint i=0; i<verticesNumber; i++)
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * 4 * mCapacity;
	verticesDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DYNAMIC;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mpDevice->CreateBuffer(&verticesDesc, &verticesData, &mVertexBuffer));
	delete[] vertices;

	uint* indices = new uint[6 * mCapacity];
	for (uint i=0; i<mIndices.size(); i++)
	{
		indices[i] = mIndices[i];
	}

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint) * 6 * mCapacity;
	indicesDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DYNAMIC;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = indices;
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mpDevice->CreateBuffer(&indicesDesc, &indicesData, &mIndexBuffer));
	delete[] indices;
}
//------------------------------------------------------------------------------------
void GText::UpdateBuffers()
{
	uint verticesNumber = mVertices.size();
	if (verticesNumber <= 0)
		return;

	GVertexData *vertices = new GVertexData[verticesNumber];
	for (uint i=0; i<verticesNumber; i++)
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}

	D3D11_MAPPED_SUBRESOURCE mappedVertices;
	mpContext->Map(mVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedVertices);
	memcpy(mappedVertices.pData, (void*)vertices, (verticesNumber*GVertex::GetSize()));
	mpContext->Unmap(mVertexBuffer, 0);
	delete[] vertices;

	D3D11_MAPPED_SUBRESOURCE mappedIndices;
	mpContext->Map(mIndexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedIndices);
	memcpy(mappedIndices.pData, mIndices.data(), (mIndices.size() * sizeof(uint)));
	mpContext->Unmap(mIndexBuffer, 0);
}
//------------------------------------------------------------------------------------
void GText::draw() const
{
	if (mVertices.size() <= 0)
		return;

	uint strides = GVertex::GetSize();
	uint offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &strides, &offset);
	mpContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	mpContext->DrawIndexed(mIndices.size(), 0, 0);
}
//------------------------------------------------------------------------------------
void GText::OnResize(const uint &new_window_width, const uint &new_window_height)
{
	mWorld = GMathMF(XMMatrixTranslation(static_cast<float>(new_window_width)/-2.0f + 
		static_cast<float>(mRealPosition.x), 
		static_cast<float>(new_window_height)/2.0f - 
		static_cast<float>(-mRealPosition.y), 0.0f));
}