#pragma once
#include "GUtility.h"
#include "GShaderPass.h"
#include "GSceneManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GSceneRenderer
	{
	public:
		GSceneRenderer() {}
		~GSceneRenderer() {}

		void Initialize( GSceneManager *scene_manager, 
			GTexturesManager *textures_manager, GMaterialsManager *materials_manager, 
			const GApplicationInformation &app_info, 
			ID3D11Device *device, ID3D11DeviceContext *context );

		void Render();

	private:
		// Stores D3D11 device.
		ID3D11Device		*mpDevice;

		// Stores D3D11 device context.
		ID3D11DeviceContext *mpContext;

		// Stores shaders, used for rendering by their unique names.
		GFastMap<std::string, GShaderPass*> mShaders;

		// Stores scene manager reference.
		GSceneManager *mpSceneManager;

		// Stores application's texture manager
		// for future use.
		GTexturesManager *mpTexturesManager;

		// Stores application's materials manager
		// for future use.
		GMaterialsManager *mpMaterialsManager;

	}; // class GSceneRenderer
} // namespace
//------------------------------------------------------------------------------------