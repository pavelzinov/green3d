#include "GMaterialsManager.h"
using namespace Game;
//------------------------------------------------------------------------------------
void GMaterialsManager::AddMaterial( const GMaterial &material )
{
	mMaterials.append( material.mName, material );
}
//-----------------------------------------------------------------------------------
bool GMaterialsManager::LoadAddMaterialsFromFile( const std::string &filename, 
	const GTexturesManager *textures_manager )
{
	// Load materials from .mtl file, by means of GMtlFile class object.
	GMtlFile mtl_file;
	mtl_file.Load( filename );
	auto materials = mtl_file.GetMaterials();
	// Clear empty materials, if any.
	materials.erase( std::remove_if( materials.begin(), materials.end(), []( const GMaterial &mat )->bool
	{
		return mat.mName == "";
	} ), materials.end() );
	// Assign concrete texture from GTexturesManager
	// to every material's texture field.
	for ( GMaterial& material : materials )
	{
		material.map_Ka = textures_manager->GetTextureNameByFileName( material.map_Ka );
		material.map_Kd = textures_manager->GetTextureNameByFileName( material.map_Kd );
		material.map_Ks = textures_manager->GetTextureNameByFileName( material.map_Ks );
		material.map_Ns = textures_manager->GetTextureNameByFileName( material.map_Ns );
		material.map_d = textures_manager->GetTextureNameByFileName( material.map_d );
		material.map_bump = textures_manager->GetTextureNameByFileName( material.map_bump );
		material.disp = textures_manager->GetTextureNameByFileName( material.disp );
		material.decal = textures_manager->GetTextureNameByFileName( material.decal );
		mMaterials.append( material.mName, material );
	}
	return true;
}
//-----------------------------------------------------------------------------------
//bool GMaterialsManager::LoadAddMaterialsFromFile( const std::string &filename )
//{
//	GMtlFile mtl_file;
//	mtl_file.Load( filename );
//	auto materials = mtl_file.GetMaterials();
//	// Clear empty materials
//	materials.erase( std::remove_if( materials.begin(), materials.end(), []( GMaterial &mat )->bool
//	{
//		return mat.mName == "";
//	} ), materials.end() );
//	// Append material from file to materials.
//	// Material can not contain textures, so clear them 
//	// before storing material in vector.
//	for ( GMaterial& material : materials )
//	{
//		// Clear all texture fields
//		material.map_Ka = "";
//		material.map_Kd = "";
//		material.map_Ks = "";
//		material.map_Ns = "";
//		material.map_d = "";
//		material.map_bump = "";
//		material.disp = "";
//		material.decal = "";
//		mMaterials.insert( std::pair<std::string, GMaterial>( material.mName, material ) );
//	}
//	return true;
//}
//-----------------------------------------------------------------------------------
bool GMaterialsManager::LoadAddMaterialsFromObjFile( const GObjFile &obj_file, 
	const GTexturesManager *textures_manager )
{
	auto mtl_files = obj_file.GetMaterialLibraries();
	for ( const auto &mtl_file : mtl_files )
	{
		LoadAddMaterialsFromFile( mtl_file, textures_manager );
	}

	return true;
}
//-----------------------------------------------------------------------------------