#pragma once
#include <float.h>
#include "DirectXMath.h"
#include "GUtility.h"
typedef unsigned int uint;
using namespace DirectX;
//------------------------------------------------------------------------------------
namespace Game
{
	class GMath
	{
	public:
		GMath(void);
		~GMath(void);

		static const float INFINITY;
		static const float PI;
		static const float PIDIV2;
		static const float PIDIV4;
		static const float EPS;

		static uint GetUniqueId() { return ++id; }
		static const XMFLOAT4X4 MTranspose(const XMFLOAT4X4 &matrix);

	private:
		static uint id;
	}; // class
} // namespace
//------------------------------------------------------------------------------------

// TODO: embed those functions in class later.

// Comprising two values by using MATH_EPS
template <class T>
inline bool GMathEqual(T x, T y)
{ 
	if (fabs(static_cast<float>(x-y)) <= GMath::EPS)
		return true;
	return false;
}

inline XMVECTOR GMathFV(const XMFLOAT3 &val)
{
	return XMLoadFloat3(&val);
}

inline XMFLOAT3 GMathVF(const XMVECTOR &vec)
{
	XMFLOAT3 val;
	XMStoreFloat3(&val, vec);
	return val;
}

inline XMFLOAT4 GMathVF4(const XMVECTOR &vec)
{
	XMFLOAT4 val;
	XMStoreFloat4(&val, vec);
	return val;
}

inline XMMATRIX GMathFM(const XMFLOAT4X4 &val)
{
	return XMLoadFloat4x4(&val);
}

inline XMFLOAT4X4 GMathMF(const XMMATRIX &matrix)
{
	XMFLOAT4X4 val;
	XMStoreFloat4x4(&val, matrix);
	return val;
}

inline const XMFLOAT4X4 GMathMul( const XMFLOAT4X4 &matrix1, const XMFLOAT4X4 &matrix2 )
{
	XMFLOAT4X4 result;
	XMStoreFloat4x4( &result, XMLoadFloat4x4( &matrix1 ) * XMLoadFloat4x4( &matrix2 ) );
	return result;
}

inline const XMFLOAT4X4 GMathMulT( const XMFLOAT4X4 &matrix1, const XMFLOAT4X4 &matrix2 )
{
	XMFLOAT4X4 result;
	XMStoreFloat4x4( &result, 
		XMMatrixTranspose( XMLoadFloat4x4( &matrix1 ) * XMLoadFloat4x4( &matrix2 ) ) );
	return result;
}

inline const XMFLOAT4X4 GMathT( const XMFLOAT4X4 &matrix )
{
	XMFLOAT4X4 result;
	XMStoreFloat4x4( &result, 
		XMMatrixTranspose( XMLoadFloat4x4( &matrix ) ) );
	return result;
}

inline float GMathRound(float& val)
{
	return (val > 0.0f) ? floor(val + 0.5f) : ceil(val - 0.5f);
}

inline float GMathFloatRound(float val)
{
	if (GMathEqual<float>(val, GMathRound(val)))
		return GMathRound(val);
	return val;
}