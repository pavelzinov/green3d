#include "GModel.h"
using namespace Game;
//------------------------------------------------------------------------------------
GModel::GModel(std::string id, ID3D11Device *device, ID3D11DeviceContext *context)
{
	mID = id;
	mpDevice = device;
	mpContext = context;
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mWorld = mRotation = mTranslation = mScaling;
}
//------------------------------------------------------------------------------------
void GModel::LoadFromObj(const GObjFile &obj_file, const GMaterialsManager &materials_manager)
{
	if (obj_file.GetGroups().size() < 0)
		return;

	for (uint i = 0; i < obj_file.GetGroups()[0].mMeshes.size(); ++i)
	{
		mMeshes.push_back(GMesh(obj_file.GetGroups()[0].mName, obj_file, i, 
			materials_manager, mpDevice, mpContext));
	}
}
//------------------------------------------------------------------------------------
void GModel::LoadFromObj(const GObjFile &obj_file, uint object_index, 
	const GMaterialsManager &materials_manager)
{
	if ( object_index < 0 || 
		object_index > ( obj_file.GetGroups().size() - 1 ) )
		return;

	int mesh_start_index = obj_file.GetFirstMeshGlobalIndex( object_index );
	for ( uint j = 0; j < obj_file.GetGroups()[object_index].mMeshes.size(); ++j )
	{
		mMeshes.push_back( GMesh( 
			obj_file.GetGroups()[object_index].mName + "_mesh_" + GString::FromInt( j ), 
			obj_file, mesh_start_index + j, materials_manager, mpDevice, mpContext ) );
		//mesh_start_index += 1;
	}
}
//------------------------------------------------------------------------------------
void GModel::MergeMeshes()
{
	std::map<std::string, GMesh> unique_meshes;
	for (const auto &mesh : mMeshes)
	{
		if (unique_meshes.find(mesh.GetMaterialID()) != unique_meshes.end())
		{
			unique_meshes.find(mesh.GetMaterialID())->second = 
				unique_meshes.at(mesh.GetMaterialID()) + mesh;
		}
		else
		{
			unique_meshes.insert(std::make_pair(mesh.GetMaterialID(), mesh));
		}
	}
	// Clear old meshes and fill
	// container with merged ones.
	mMeshes.clear();
	for (auto &mesh_pair : unique_meshes)
	{
		mMeshes.push_back(mesh_pair.second);
	}
}
//------------------------------------------------------------------------------------
void GModel::Scale(const float scale_x, const float scale_y, const float scale_z)
{
	XMStoreFloat4x4(&mScaling, XMLoadFloat4x4(&mScaling) 
		* XMMatrixScaling(scale_x, scale_y, scale_z));
	UpdateWorld();
}
//------------------------------------------------------------------------------------
void GModel::Move(const float dir_x, const float dir_y, const float dir_z)
{
	XMStoreFloat4x4(&mTranslation, XMLoadFloat4x4(&mTranslation) 
		* XMMatrixTranslation(dir_x, dir_y, dir_z));
	UpdateWorld();
}
//------------------------------------------------------------------------------------
void GModel::RotateSelf(const XMFLOAT3 &axis, float degrees)
{
	//for ( auto &mesh : mMeshes )
	//{
	//	mesh.RotateSelf( axis, degrees );
	//}
	if ( XMVector3Equal( GMathFV(axis), XMVectorZero() ) ||
		degrees == 0.0f )
		return;

	XMStoreFloat4x4( &mRotation, XMLoadFloat4x4( &mRotation ) 
		* XMMatrixRotationAxis( XMLoadFloat3( &axis ), XMConvertToRadians( degrees ) ) );

	UpdateWorld();
}
//------------------------------------------------------------------------------------
void GModel::SetMaterial(const GMaterial &material)
{
	// For each mesh set material.
	for (auto &mesh : mMeshes)
	{
		mesh.SetMaterial(material);
	}
}
//------------------------------------------------------------------------------------
void GModel::UpdateWorld()
{
	mWorld = GMathMul( GMathMul( mScaling, mRotation ), mTranslation );
}
//------------------------------------------------------------------------------------