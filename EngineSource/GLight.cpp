#include "GLight.h"
using namespace Game;

GLight::GLight(void)
{
	mPosition = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mAmbientColor = GColor::BEACH_SAND;
	mDiffuseColor = mAmbientColor;
	mSpecularColor = mAmbientColor;
	mAttenuationParameters = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mSpotPower = 0.0f;
	mRange = 0.0f;
	mType = GLIGHT_TYPE_PARALLEL;

	mCamera.Position(GMathVF(XMVector3Normalize(GMathFV(mDirection)) * -1000.0f));
	mCamera.Target(GMathVF(XMVector3Normalize(GMathFV(mDirection)) * 1000.0f));
	mCamera.InitProjMatrix( GMath::PIDIV2, 1024, 1024, 0.01f, 2000.0f );
	mCamera.InitOrthoMatrix( 1024, 1024, 0.01f, 2000.0f );
}

void GLight::SetPos(float x, float y, float z) 
{
	mPosition.x = x; mPosition.y = y; mPosition.z = z;
	//mCamera.SetPosition(XMFLOAT3(x, y, z));
}

void GLight::SetDirection(XMFLOAT3 direction)
{
	mDirection = direction;
	XMFLOAT3 test = GMathVF(XMVector3Normalize(GMathFV(mDirection)) * -1000.0f);
	mCamera.Position(GMathVF(XMVector3Normalize(GMathFV(mDirection)) * -1000.0f));
	test = GMathVF(XMVector3Normalize(GMathFV(mDirection)) * -1000.0f);
	mCamera.Target(GMathVF(XMVector3Normalize(GMathFV(mDirection)) * 1000.0f));
}
