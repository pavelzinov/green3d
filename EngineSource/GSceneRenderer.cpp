#include "GSceneRenderer.h"
using namespace Game;
//------------------------------------------------------------------------------------
void GSceneRenderer::Initialize( GSceneManager *scene_manager, 
	GTexturesManager *textures_manager, GMaterialsManager *materials_manager, 
	const GApplicationInformation &app_info, 
	ID3D11Device *device, ID3D11DeviceContext *context )
{
	mpSceneManager = scene_manager;
	mpTexturesManager = textures_manager;
	mpMaterialsManager = materials_manager;
	mpDevice = device;
	mpContext = context;


}
//------------------------------------------------------------------------------------
void GSceneRenderer::Render()
{

}
//------------------------------------------------------------------------------------