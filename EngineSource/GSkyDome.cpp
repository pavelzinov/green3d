#include "GSkyDome.h"
using namespace Game;
//-------------------------------------------------------------------------------------
GSkyDome::GSkyDome( ID3D11Device *device, ID3D11DeviceContext *context,
		 const std::string &obj_filename, const float scale, 
		 const std::string &texture_id )
{
	mpDevice = device;
	mpContext = context;
	mTextureID = texture_id;
	mpVertexBuffer = nullptr;

	GObjFile obj_file;
	obj_file.Load( obj_filename );
	obj_file.InvertTriangles();

	GVertex vertex;
	int faces_count = 0;
	if ( obj_file.GetGroups().size() > 0) faces_count = obj_file.GetGroups()[0].mMeshes[0].mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mVertex.x * scale;
			vertex.mPosition.y = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mVertex.y * scale;
			vertex.mPosition.z = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mVertex.z * scale;

			vertex.mNormal.x = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mNormal.nz;

			vertex.mTextureCoordinates.x = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mTexCoords.u;
			vertex.mTextureCoordinates.y = obj_file.GetGroups()[0].mMeshes[0].
				mFaces[i].mVertices[j].mTexCoords.v;

			mVertices.push_back(vertex);
		}
	}

	ConstructVertexBuffer();
}
//-------------------------------------------------------------------------------------
GSkyDome::GSkyDome(const GSkyDome& sky_dome)
{
	mpDevice = sky_dome.mpDevice;
	mpContext = sky_dome.mpContext;
	mTextureID = sky_dome.mTextureID;
	mVertices = sky_dome.mVertices;
	mpVertexBuffer = nullptr;

	ConstructVertexBuffer();
}
//-------------------------------------------------------------------------------------
void GSkyDome::Draw() const
{
	uint strides = GVertex::GetSize();
	uint offset = 0;
	mpContext->IASetVertexBuffers( 0, 1, &mpVertexBuffer, &strides, &offset );
	mpContext->Draw( mVertices.size(), 0 );
}
//-------------------------------------------------------------------------------------
void GSkyDome::ConstructVertexBuffer()
{
	if (mVertices.size() <= 0)
		return;

	// Clear old buffers
	ReleaseCOM(mpVertexBuffer);	

	// Feed vertex data
	uint verticesNumber = mVertices.size();
	GVertexData *vertices = new GVertexData[verticesNumber];
	for ( uint i = 0; i < verticesNumber; ++i )
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mpDevice->CreateBuffer(&verticesDesc, &verticesData, &mpVertexBuffer));
	delete[] vertices;
}
//-------------------------------------------------------------------------------------