#include "GApplication.h"
using namespace Game;
//------------------------------------------------------------------------------------
// This function forwards Windows messages to corresponding function of GApplication class
LRESULT CALLBACK InternalWindowProc ( HWND hWnd, uint msg, WPARAM wParam, LPARAM lParam )
{
	static GApplication * app = nullptr; // �������� �� ���������� ����� ���������, ���� � ���������
	switch ( msg )
	{
		case WM_CREATE:
		{// ��� �������� CreateWindow �������� this � �������� ����� ��������� CREATESTRUCT, 
			//��������� �� ������� ���������� � lParam, � ������� ��� � ������ ���������
			CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>( lParam );//reinterpret_cast<CREATESTRUCT*>( lParam );
			app = static_cast<GApplication*>( cs->lpCreateParams );
			return 0;
		}
	}

	if ( app )
		return app->msgProc( msg, wParam, lParam );
	else
		return DefWindowProc( hWnd, msg, wParam, lParam );		
}
//------------------------------------------------------------------------------------
GApplication::GApplication(HINSTANCE hInstance)
{
	mhAppInst		= hInstance;
	mhMainWnd		= 0;
	mAppPaused		= false;
	mMinimized		= false;
	mMaximized		= false;
	mResizing		= false;

    mSampleCount	= 1;
    mSampleQuality	= 0;

	mApplicationDesc.mClientWidth = 800;
	mApplicationDesc.mClientHeight = 600;
	mApplicationDesc.mScreenClearColor = GColor::BLUE;
	mApplicationDesc.mWindowTitle = "D3D11 Application";

	mpDevice					= nullptr;
	mpContext					= nullptr;
	mpSwapChain					= nullptr;
	mpDepthStencilBuffer		= nullptr;
	mpWindowRenderTargetView	= nullptr;
	mpWindowDepthStencilView	= nullptr;
	mpDepthDisabledStencil		= nullptr;
	mpDepthEnabledStencil		= nullptr;
	mpRSBackfaceCullingSolid	= nullptr;
	mpRSNoCullingSolid			= nullptr;
	mpRSWireframe				= nullptr;

	mDriverType	= D3D_DRIVER_TYPE_HARDWARE;

	mUpdatesPerSecond = 70;
	mSecondsBetweenUpdates = 1.0f / mUpdatesPerSecond;
}
//------------------------------------------------------------------------------------	
GApplication::~GApplication(void)
{
	// Set this to correctly clear all resources, since
	// D3D can't do that in full screen mode
	mpSwapChain->SetFullscreenState(false, nullptr);

	ReleaseCOM(mpDepthDisabledStencil);
	ReleaseCOM(mpWindowDepthStencilView);
	ReleaseCOM(mpDepthStencilBuffer);
	ReleaseCOM(mpWindowRenderTargetView);
	ReleaseCOM(mpDepthEnabledStencil);
	ReleaseCOM(mpRSBackfaceCullingSolid);
	ReleaseCOM(mpRSNoCullingSolid);
	ReleaseCOM(mpRSWireframe);
	ReleaseCOM(mpSwapChain);
	ReleaseCOM(mpContext);

#ifdef DEBUG
	ID3D11Debug* debug = 0;
	mpDevice->QueryInterface( IID_ID3D11Debug, reinterpret_cast<void**>( &debug ) );
	debug->ReportLiveDeviceObjects( D3D11_RLDO_DETAIL );
	ReleaseCOM( debug );
#endif // DEBUG

	ReleaseCOM(mpDevice);

	ChangeDisplaySettings(nullptr, 0);
}
//------------------------------------------------------------------------------------
HINSTANCE GApplication::GetApplicationInstance() 
{
	return mhAppInst;
}
//------------------------------------------------------------------------------------
HWND GApplication::GetWindowHandle()
{
	return mhMainWnd;
}
//------------------------------------------------------------------------------------
int GApplication::run()
{
	MSG msg = {0};

	mTimer.reset();
	mTimer.start();
	mTimer.tick();

	float delta_time = 0.0f;
	float num_skipped = 0.0f;

	while(msg.message != WM_QUIT)
	{
		// If there are Window messages then process them.
		if(PeekMessage( &msg, 0, 0, 0, PM_REMOVE ))
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		// Otherwise, do animation/game stuff.
		else
		{
			// UPS fixed. FPS max			
			delta_time = static_cast<float>(mTimer.getDeltaTimeNow());
			if (delta_time >= mSecondsBetweenUpdates &&
				!mAppPaused)
			{
				num_skipped = delta_time / mSecondsBetweenUpdates;
				mSecondsBetweenUpdates = 1.0f / (mUpdatesPerSecond - num_skipped);
				updateScene(delta_time);
				mTimer.tick();
			}
			else if (delta_time < mSecondsBetweenUpdates &&
				!mAppPaused)
			{
				drawScene();				
			}
			else if(mAppPaused)
			{
			    Sleep(100);
			}
		}
	}

	return (int)msg.wParam;
}
//------------------------------------------------------------------------------------
void GApplication::initApp()
{
	initMainWindow();
	initDirect3D();
    mInput.Initialize();
}
//------------------------------------------------------------------------------------
void GApplication::onResize()
{
	// Clear obsolete interfaces
	//ReleaseCOM(mpDepthDisabledStencil);
	ReleaseCOM(mpWindowDepthStencilView);
	ReleaseCOM(mpDepthStencilBuffer);
	ReleaseCOM(mpWindowRenderTargetView);
	//ReleaseCOM(mpDepthEnabledStencil);

	// Changing buffers size.
	HR(mpSwapChain->ResizeBuffers(2, mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight, 
		DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));
	
	// Create RenderTargetView of BackBuffer from SwapChain.
	ID3D11Texture2D *pBackBuffer = 0;
	HR(mpSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer)));
	HR(mpDevice->CreateRenderTargetView(pBackBuffer, 0, &mpWindowRenderTargetView));
	ReleaseCOM(pBackBuffer);

	// Create the depth/stencil buffer and view.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc; // depth/stencil buffer is just a 2D texture,
	SecureZeroMemory(&depthStencilBufferDesc, sizeof(depthStencilBufferDesc));
	depthStencilBufferDesc.ArraySize			= 1;
	depthStencilBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags		= 0;
	depthStencilBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Width				= mApplicationDesc.mClientWidth;
	depthStencilBufferDesc.Height				= mApplicationDesc.mClientHeight;
	depthStencilBufferDesc.MipLevels			= 1;
	depthStencilBufferDesc.MiscFlags			= 0;
	depthStencilBufferDesc.SampleDesc.Count		= mSampleCount;
	depthStencilBufferDesc.SampleDesc.Quality	= mSampleQuality;
	depthStencilBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	mpDevice->CreateTexture2D(&depthStencilBufferDesc, 0, &mpDepthStencilBuffer);
	mpDevice->CreateDepthStencilView(mpDepthStencilBuffer, 0, &mpWindowDepthStencilView);
	mpContext->OMSetRenderTargets(1, &mpWindowRenderTargetView, mpWindowDepthStencilView);

	// Set View port
	D3D11_VIEWPORT vp;
    vp.Height   = static_cast<float>(mApplicationDesc.mClientHeight);
	vp.Width	= static_cast<float>(mApplicationDesc.mClientWidth);
	vp.MaxDepth	= 1.0f;
	vp.MinDepth	= 0.0f;
	vp.TopLeftX	= 0;
	vp.TopLeftY	= 0;

	mpContext->RSSetViewports(1, &vp);
}
//------------------------------------------------------------------------------------
void GApplication::updateScene(float dSeconds)
{
#ifndef PRODUCTION_RELEASE
	// Code computes the average frames per second, and also the
	// average time it takes to render one frame.

	static int updatesCnt = 0;
	static float currentTimeBetweenUpdates = 0.0f;

	updatesCnt++;

	// Compute averages over one second period.
	if( (mTimer.getGameTime() - currentTimeBetweenUpdates) >= 1.0f )
	{
		mApplicationDesc.mUpdatesPerSecond = updatesCnt;
		mApplicationDesc.mMillisecondsPerUpdate = 1000.0f / static_cast<float>(mApplicationDesc.mUpdatesPerSecond);

		// Reset for next average.
		updatesCnt = 0;
		currentTimeBetweenUpdates  += 1.0f;
	}
#endif

	
}
//------------------------------------------------------------------------------------
void GApplication::drawScene()
{
#ifndef PRODUCTION_RELEASE
	// Code computes the average frames per second, and also the
	// average time it takes to render one frame.

	static int frameCnt = 0;
	static float currentTimeBetweenFrames = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if( (mTimer.getGameTime() - currentTimeBetweenFrames) >= 1.0f )
	{
		mApplicationDesc.mFramesPerSecond = frameCnt;
		mApplicationDesc.mMillisecondsPerFrame = 1000.0f / static_cast<float>(mApplicationDesc.mFramesPerSecond);

		// Reset for next average.
		frameCnt = 0;
		currentTimeBetweenFrames += 1.0f;
	}
#endif
	// Clear screen with mClearColor
	ClearScreen();
}
//------------------------------------------------------------------------------------
LRESULT GApplication::msgProc(uint msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
    case WM_INPUT:
        mInput.OnWindowsInput(reinterpret_cast<HRAWINPUT>(lParam), mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight);
        return 0;

	case WM_MOUSEMOVE:
		mInput.OnMouseMove(LOWORD(lParam), HIWORD(lParam));
		return 0;

	case WM_LBUTTONDOWN:
		mInput.OnMouseLeftDown( LOWORD(lParam), HIWORD(lParam) );
		return 0;
    
	case WM_KEYDOWN:
		mInput.OnKeyDown( static_cast<USHORT>(wParam) );
        if (wParam == VK_ESCAPE)
        {
            PostQuitMessage(0);
        }
        return 0;

	case WM_KEYUP:
		mInput.OnKeyUp( static_cast<USHORT>(wParam) );		
		return 0;
	
	// WM_ACTIVATE is sent when the window become active again
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE )
		{
			mAppPaused = true;
			mTimer.pause();
		}
		else
		{
			mAppPaused = false;
			mTimer.resume();
		}
		return 0;

	// WM_ENTERSIZEMOVE is sent when the user grabs the resize bars.
	case WM_ENTERSIZEMOVE:
		mAppPaused = true;
		mResizing  = true;
		mTimer.pause();
		return 0;

	// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		mAppPaused = false;
		mResizing  = false;
		mTimer.resume();
		onResize();
		return 0;

	// WM_DESTROY is sent when the window is being destroyed.
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	// Catch this message to prevent the window from becoming too small.
	case WM_GETMINMAXINFO:
		reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.x = 200;
		reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.y = 200;
		return 0;

	case WM_SIZE:
		mApplicationDesc.mClientWidth	= LOWORD(lParam);
		mApplicationDesc.mClientHeight	= HIWORD(lParam);
		if( mpDevice )
		{
			if( wParam == SIZE_MINIMIZED )
			{
				mAppPaused = true;
				mMinimized = true;
				mMaximized = false;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				mAppPaused = false;
				mMinimized = false;
				mMaximized = true;
				onResize();
			}
			else if( wParam == SIZE_RESTORED )
			{				
				// Restoring from minimized state?
				if( mMinimized )
				{
					mAppPaused = false;
					mMinimized = false;
					onResize();
				}
				// Restoring from maximized state?
				else if( mMaximized )
				{
					mAppPaused = false;
					mMaximized = false;
					onResize();
				}
				else if( mResizing )
				{
					// If user is dragging the resize bars, we do not resize 
					// the buffers here because as the user continuously 
					// drags the resize bars, a stream of WM_SIZE messages are
					// sent to the window, and it would be pointless (and slow)
					// to resize for each WM_SIZE message received from dragging
					// the resize bars.  So instead, we reset after the user is 
					// done resizing the window and releases the resize bars, which 
					// sends a WM_EXITSIZEMOVE message.
				}
				else // API call such as SetWindowPos or mSwapChain->SetFullscreenState.
				{
					onResize();
				}
			}
		}
		return 0;
	}
	return DefWindowProc(mhMainWnd, msg, wParam, lParam);
}
//------------------------------------------------------------------------------------
void GApplication::initMainWindow()
{
	GLog::Debug( "initMainWindow() called\n" );

	WNDCLASS			wc;	
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hbrBackground	= static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	wc.hCursor			= LoadCursor(0, IDC_ARROW);
	wc.hIcon			= ExtractIcon(mhAppInst, L"ApplicationIcon.ico", 0);
	wc.hInstance		= mhAppInst;
	wc.lpfnWndProc		= InternalWindowProc;
	wc.lpszClassName	= L"BaseWindowClass";
	wc.lpszMenuName		= 0;
	wc.style			= CS_HREDRAW | CS_VREDRAW;

	if (!RegisterClass(&wc))
	{
		MessageBox(mhMainWnd, L"Can't register window class", L"Error!", MB_OK);
		PostQuitMessage(0);
	}

	// Determine window's resolution
	// and set it accordingly
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);
	if (mMaximized == true)
	{
		mApplicationDesc.mClientWidth = screen_width;
		mApplicationDesc.mClientHeight = screen_height;

		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = static_cast<DWORD>(mApplicationDesc.mClientWidth);
		dmScreenSettings.dmPelsHeight = static_cast<DWORD>(mApplicationDesc.mClientHeight);
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		mhMainWnd = CreateWindowW(L"BaseWindowClass", 
			GString::ToUnicode(mApplicationDesc.mWindowTitle).c_str(), 
			WS_POPUP, 0, 0,
			mApplicationDesc.mClientWidth/* + 12*/, // 12 - is window y border thickness
			mApplicationDesc.mClientHeight/* + 34*/, // 34 - is window x border thickness
			0, 0, mhAppInst, this);
	}
	else
	{
		mhMainWnd = CreateWindowW(L"BaseWindowClass", 
			GString::ToUnicode(mApplicationDesc.mWindowTitle).c_str(), 
			WS_OVERLAPPEDWINDOW, 0, 0,
			mApplicationDesc.mClientWidth + 12, // 12 - is window y border thickness
			mApplicationDesc.mClientHeight + 34, // 34 - is window x border thickness
			0, 0, mhAppInst, this);
	}

	if (mhMainWnd==INVALID_HANDLE_VALUE)
	{
		MessageBox(mhMainWnd, L"Can't create a window", L"Error", MB_OK);
		PostQuitMessage(0);
	}

	ShowWindow(mhMainWnd, SW_SHOW);
	SetForegroundWindow(mhMainWnd);
	SetFocus(mhMainWnd);
	UpdateWindow(mhMainWnd);
}
//------------------------------------------------------------------------------------
HRESULT GApplication::initDirect3D()
{
	GLog::Debug( "initDirect3D() called\n" );

	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect( mhMainWnd, &rc );

	uint createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	uint numDriverTypes = ARRAYSIZE( driverTypes );

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	uint numFeatureLevels = ARRAYSIZE( featureLevels );

	DXGI_SWAP_CHAIN_DESC sd;
	SecureZeroMemory( &sd, sizeof(sd) );
	sd.BufferCount = 1;
	sd.BufferDesc.Width = mApplicationDesc.mClientWidth;
	sd.BufferDesc.Height = mApplicationDesc.mClientHeight;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = mhMainWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = true;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Try to create Device and Swap chain
	for( uint driverTypeIndex = 0; driverTypeIndex < numDriverTypes; ++driverTypeIndex )
	{
		mDriverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain( NULL, 
											mDriverType, 
											NULL, 
											createDeviceFlags, 
											featureLevels, 
											numFeatureLevels,
											D3D11_SDK_VERSION, 
											&sd, 
											&mpSwapChain, 
											&mpDevice, 
											&mFeatureLevel, 
											&mpContext );
		if( SUCCEEDED( hr ) )
			break;
	}

	GetVideoCardStatistics();

	// Create RenderTargetView of BackBuffer from SwapChain
	ID3D11Texture2D *pBackBuffer = 0;
	HRR(mpSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer)));
	HRR(mpDevice->CreateRenderTargetView(pBackBuffer, 0, &mpWindowRenderTargetView));
	ReleaseCOM(pBackBuffer);

	// Create the depth/stencil buffer and view.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc; // depth/stencil buffer is just a 2D texture,
	SecureZeroMemory(&depthStencilBufferDesc, sizeof(depthStencilBufferDesc));
	depthStencilBufferDesc.ArraySize			= 1;
	depthStencilBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags		= 0;
	depthStencilBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Height				= mApplicationDesc.mClientHeight;
	depthStencilBufferDesc.MipLevels			= 1;
	depthStencilBufferDesc.MiscFlags			= 0;
	depthStencilBufferDesc.SampleDesc.Count		= mSampleCount;
	depthStencilBufferDesc.SampleDesc.Quality	= mSampleQuality;
	depthStencilBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	depthStencilBufferDesc.Width				= mApplicationDesc.mClientWidth;
	HRR(mpDevice->CreateTexture2D(&depthStencilBufferDesc, 0, &mpDepthStencilBuffer));
	HRR(mpDevice->CreateDepthStencilView(mpDepthStencilBuffer, 0, &mpWindowDepthStencilView));
	mpContext->OMSetRenderTargets(1, &mpWindowRenderTargetView, mpWindowDepthStencilView);

	// Resize disabled depth state
	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
	SecureZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));
	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create the state using the device.
	HRR(mpDevice->CreateDepthStencilState(&depthDisabledStencilDesc, &mpDepthDisabledStencil));

	// Create rasterizer
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer states
	HRR(mpDevice->CreateRasterizerState(&rasterDesc, &mpRSBackfaceCullingSolid));

	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	HRR(mpDevice->CreateRasterizerState(&rasterDesc, &mpRSNoCullingSolid));

	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	HRR(mpDevice->CreateRasterizerState(&rasterDesc, &mpRSWireframe));

	// Now set the rasterizer state.
	BackfaceCullingOn();

	// Set up the description of the stencil state.
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	SecureZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create the depth stencil state.
	HRR(mpDevice->CreateDepthStencilState(&depthStencilDesc, &mpDepthEnabledStencil));
	// Set the depth stencil state.
	TurnZBufferOn();

	// Set View port
	D3D11_VIEWPORT vp;
	vp.Height   = static_cast<float>(mApplicationDesc.mClientHeight);
	vp.Width	= static_cast<float>(mApplicationDesc.mClientWidth);
	vp.MaxDepth	= 1.0f;
	vp.MinDepth	= 0.0f;
	vp.TopLeftX	= 0;
	vp.TopLeftY	= 0;
	mpContext->RSSetViewports(1, &vp);

	return S_OK;
}
//------------------------------------------------------------------------------------
void GApplication::GetVideoCardStatistics()
{
	IDXGIDevice *pDXGIDevice;
	mpDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&pDXGIDevice);
	IDXGIAdapter *pAdapter;
	pDXGIDevice->GetAdapter(&pAdapter);
	DXGI_ADAPTER_DESC adapter_desc;
	pAdapter->GetDesc(&adapter_desc);
	mApplicationDesc.mVideoCardName = GString::ToAscii(std::wstring(adapter_desc.Description));
	mApplicationDesc.mVideoCardFreeMemory = adapter_desc.DedicatedVideoMemory / 1024 / 1024;
	ReleaseCOM(pAdapter);
	ReleaseCOM(pDXGIDevice);
}
//------------------------------------------------------------------------------------
void GApplication::ClearScreen()
{
	mpContext->ClearRenderTargetView(mpWindowRenderTargetView, 
		reinterpret_cast<float*>(&mApplicationDesc.mScreenClearColor));
	mpContext->ClearDepthStencilView(mpWindowDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);
}
//-----------------------------------------------------------------------------------
int GApplication::GetEncoderClsid(WCHAR *format, CLSID *pClsid)
{
	unsigned int num = 0,  size = 0;
	Gdiplus::GetImageEncodersSize(&num, &size);
	if(size == 0) return -1;
	Gdiplus::ImageCodecInfo *pImageCodecInfo = (Gdiplus::ImageCodecInfo *)(malloc(size));
	if(pImageCodecInfo == NULL) return -1;
	Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);
	for(unsigned int j = 0; j < num; ++j)
	{
		if(wcscmp(pImageCodecInfo[j].MimeType, format) == 0){
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;
		}    
	}
	free(pImageCodecInfo);
	return -1;
}
//-----------------------------------------------------------------------------------
int GApplication::GetScreen(LPWSTR lpszFilename, ULONG uQuality) // by Napalm
{
	// TODO: uncomment all of the code below.
	//ULONG_PTR gdiplusToken;
	//Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	//auto result_startup = Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	//HWND hMyWnd = GetDesktopWindow(); // get my own window
	//RECT  r;             // the area we are going to capture 
	//int w, h;            // the width and height of the area
	//HDC dc;              // the container for the area
	//int nBPP;
	//HDC hdcCapture;
	//LPBYTE lpCapture;
	//int nCapture;
	//int iRes;
	//CLSID imageCLSID;
	//Gdiplus::Bitmap *pScreenShot;
	//HGLOBAL hMem;
	//int result = 0;

	//// get the area of my application's window      
	////GetClientRect(hMyWnd, &r);
	//GetWindowRect(hMyWnd, &r);
	//dc = GetWindowDC(hMyWnd);//   GetDC(hMyWnd) ;
	//w = r.right - r.left;
	//h = r.bottom - r.top;
	//nBPP = GetDeviceCaps(dc, BITSPIXEL);
	//hdcCapture = CreateCompatibleDC(dc);


	//// create the buffer for the screenshot
	//BITMAPINFO bmiCapture = {
	//	sizeof(BITMAPINFOHEADER), w, -h, 1, nBPP, BI_RGB, 0, 0, 0, 0, 0,
	//};

	//// create a container and take the screenshot
	//HBITMAP hbmCapture = CreateDIBSection(dc, &bmiCapture,
	//	DIB_PAL_COLORS, (LPVOID *)&lpCapture, NULL, 0);

	//// failed to take it
	//if(!hbmCapture)
	//{
	//	DeleteDC(hdcCapture);
	//	DeleteDC(dc);
	//	Gdiplus::GdiplusShutdown(gdiplusToken);
	//	printf("failed to take the screenshot. err: %d\n", GetLastError());
	//	return 0;
	//}

	//// copy the screenshot buffer
	//nCapture = SaveDC(hdcCapture);
	//SelectObject(hdcCapture, hbmCapture);
	//BitBlt(hdcCapture, 0, 0, w, h, dc, 0, 0, SRCCOPY);
	//RestoreDC(hdcCapture, nCapture);
	//DeleteDC(hdcCapture);
	//DeleteDC(dc);

	//Gdiplus::GpImage *bob;
	//IStream *ssStr;

	//// save the buffer to a file    
	//pScreenShot = new Gdiplus::Bitmap(hbmCapture, (HPALETTE)NULL);
	//Gdiplus::EncoderParameters encoderParams;
	//encoderParams.Count = 1;
	//encoderParams.Parameter[0].NumberOfValues = 1;
	//encoderParams.Parameter[0].Guid  = Gdiplus::EncoderQuality;
	//encoderParams.Parameter[0].Type  = Gdiplus::EncoderParameterValueTypeLong;
	//encoderParams.Parameter[0].Value = &uQuality;
	//GetEncoderClsid(L"image/jpeg", &imageCLSID);
	////iRes = (pScreenShot->Save(lpszFilename, &imageCLSID, &encoderParams) == Ok);
	//IStream *pStream = NULL;
	//LARGE_INTEGER liZero = {};
	//ULARGE_INTEGER pos = {};
	//STATSTG stg = {};
	//ULONG bytesRead=0;
	//HRESULT hrRet=S_OK;

	//BYTE* buffer = NULL;  // this is your buffer that will hold the jpeg bytes
	//DWORD dwBufferSize = 0;  // this is the size of that buffer;
	//
	//hrRet = CreateStreamOnHGlobal(NULL, TRUE, &pStream);
	//hrRet = pScreenShot->Save(pStream, &imageCLSID, &encoderParams) == 0 ? S_OK : E_FAIL;
	////hrRet = pScreenShot->Save(lpszFilename, &imageCLSID, &encoderParams) == 0 ? S_OK : E_FAIL;
	//hrRet = pStream->Seek(liZero, STREAM_SEEK_SET, &pos);
	//hrRet = pStream->Stat(&stg, STATFLAG_NONAME);

	//// allocate a byte buffer big enough to hold the jpeg stream in memory
	//buffer = new BYTE[stg.cbSize.LowPart];
	//hrRet = (buffer == NULL) ? E_OUTOFMEMORY : S_OK;
	//dwBufferSize = stg.cbSize.LowPart;

	//// copy the stream into memory
	//hrRet = pStream->Read(buffer, stg.cbSize.LowPart, &bytesRead);

	//// now go save "buffer" and "dwBufferSize" off somewhere.  This is the jpeg buffer
	//// don't forget to free it when you are done

	//// After success or if any of the above calls fail, don't forget to release the stream
	//if (pStream)
	//{
	//	pStream->Release();
	//}

	//delete pScreenShot;
	//DeleteObject(hbmCapture);
	//Gdiplus::GdiplusShutdown(gdiplusToken);
	//return result;
	////return iRes;
	return 0;
}
//-----------------------------------------------------------------------------------