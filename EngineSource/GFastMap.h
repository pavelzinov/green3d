#pragma once
#include <iostream>
#include <vector>
#include <unordered_map>
typedef unsigned int uint;
//------------------------------------------------------------------------------------
namespace Game
{
	// ===============================================================================
	// GFastMap class.
	// ===============================================================================
	// This class implements associative container with
	// fast element access and fast iteration through elements.
	// Do not use it's iterators in STL algorithm functions, which somehow
	// alter vector's elements positions!
	// For read-only iterating through all elements you can use:
	// for (const auto &item : fast_map_container_object)
	// If you want to find and change particular value, use:
	// for (auto &item : fast_map_container_object)
	// Data stored in vector array, so for-range loop iterates through
	// the elements of the vector.
	// Do not modify item.first.
	// Access data by using item.second. Access the key by using item.first.
	// ===============================================================================
	template <class key_type, class value_type>
	class GFastMap
	{
	public:
		// Construct the map.
		GFastMap() {}

		// Construct the map and reserver some memory for it,
		// so append() command will work a lot faster.
		GFastMap(const uint size) { reserve(size); }

		GFastMap(const GFastMap& fast_map) 
		{
			indexes = fast_map.indexes;
			data = fast_map.data;
		}

		const GFastMap& operator=(const GFastMap& fast_map)
		{
			clear();
			indexes = fast_map.indexes;
			data = fast_map.data;
		}

		// De-construct the map.
		~GFastMap() { clear(); }

		uint size() { return data.size(); }

		// Reserve the memory for the map, so
		// adding new members will be faster.
		void reserve(const uint size)
		{
			data.reserve(size);
		}

		// Add new element with the given key and value.
		bool append(const key_type &key, const value_type &value)
		{
			data.push_back(std::make_pair(key, value));
			indexes.insert(std::make_pair(key, data.size() - 1));
			return true;
		}

		// Clear all content.
		// Reserved space is still allocated.
		void clear()
		{
			data.clear();
			indexes.clear();
		}

		// Check if the element with the given key already exists in
		// the container.
		bool contains(const key_type &key) const
		{
			if (indexes.find(key) != indexes.end())
				return true;
			return false;
		}

		// Get the value by given key.
		value_type& at(const key_type &key)
		{
			return data[indexes.at(key)].second;
		}

		// Get the constant value by given key.
		const value_type& at(const key_type &key) const
		{
			return data[indexes.at(key)].second;
		}

		// Get the value by given key.
		value_type& operator[](const key_type &key)
		{
			return at(key);
		}

		// Get the value by given key.
		const value_type& operator[](const key_type &key) const
		{
			return at(key);
		}

		// Get the index of the value addressed with the given key, 
		// stored in inner vector array. Returns -1 if not found.
		int index(const key_type &key) const
		{
			return indexes.at(key);
		}

		// Get the key by the index of the vector it addresses.
		const key_type& key_by_index(const unsigned int index) const
		{
			return data[index].first;
		}

		// Constant begin() iterator for range-for loop.
		typename std::vector<std::pair<const key_type, value_type>>::const_iterator begin() const
		{
			return data.begin();
		}

		// Constant end() iterator for range-for loop.
		typename std::vector<std::pair<const key_type, value_type>>::const_iterator end() const
		{
			return data.end();
		}

		// Non-constant begin() iterator for range-for loop.
		typename std::vector<std::pair<const key_type, value_type>>::iterator begin()
		{
			return data.begin();
		}

		// Non-constant end() iterator for range-for loop.
		typename std::vector<std::pair<const key_type, value_type>>::iterator end()
		{
			return data.end();
		}

	private:
		// Unordered map containing indexes of the values
		// by the unique key.
		std::unordered_map<key_type, unsigned int> indexes;

		// Vector, containing actual data.
		std::vector<std::pair<const key_type, value_type>> data;
	}; // class GFastMap
} // namespace Game
//------------------------------------------------------------------------------------