#include "GString.h"
using namespace Game;
//--------------------------------------------------------------------------------------
std::stringstream GString::mWorkerStream = std::stringstream( "" );
//--------------------------------------------------------------------------------------
std::string GString::ToAscii(const std::wstring& wstr)
{
	if (wstr.size() == 0)
		return "";

	int input_string_length = static_cast<int>(wstr.length()) + 1;
	int required_char_length = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), 
		input_string_length, 0, 0, nullptr, nullptr);
	char* buffer = new char[required_char_length];
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), input_string_length, buffer, 
		required_char_length, nullptr, nullptr);

	std::string out(buffer);

	delete[] buffer;
	return out;

}
//--------------------------------------------------------------------------------------
std::wstring GString::ToUnicode(const std::string& str)
{
	if (str.size() == 0)
		return L"";

	int input_string_length = static_cast<int>(str.length()) + 1;
	int required_wchar_length = MultiByteToWideChar(CP_ACP, 0, str.c_str(), 
		input_string_length, 0, 0);
	wchar_t* buffer = new wchar_t[required_wchar_length];
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), input_string_length, buffer, 
		required_wchar_length);
	
	std::wstring out(buffer);

	delete[] buffer;
	return out;
}
//--------------------------------------------------------------------------------------
std::vector<std::string>& GString::Split(const std::string &str, const char delimiter,
											 std::vector<std::string> &result)
{
	// Clear inner string stream object.
	mWorkerStream.str( str );
	mWorkerStream.clear();

	//std::stringstream ss( str );
	std::string item;
	result.reserve( str.size() );
	while( std::getline( mWorkerStream, item, delimiter ) ) {
		if ( item.size() > 0 ) {
			result.push_back( item );
		}
	}
	return result;
}
//--------------------------------------------------------------------------------------
std::vector<std::string> GString::Split(const std::string &str, const char delimiter)
{
	std::vector<std::string> result;
	Split(str, delimiter, result);
	return result;
}
//--------------------------------------------------------------------------------------
int GString::ToInt(const std::string& str)
{
	return std::stoi(str);
}
//--------------------------------------------------------------------------------------
float GString::ToFloat(const std::string& str)
{
	return std::stof(str);
}
//--------------------------------------------------------------------------------------
std::string GString::FromInt(const int value)
{
	// Clear inner string stream object.
	mWorkerStream.str( "" );
	mWorkerStream.clear();

	mWorkerStream << value;
	return mWorkerStream.str();
}
//--------------------------------------------------------------------------------------
std::string GString::FromFloat(const float value)
{
	// Clear inner string stream object.
	mWorkerStream.str( "" );
	mWorkerStream.clear();

	mWorkerStream << value;
	return mWorkerStream.str();
}
//--------------------------------------------------------------------------------------
std::vector<std::string>& GString::Split(const std::string &str, const std::string delimiters,
											 std::vector<std::string> &result)
{
	std::string construction_string;
	uint string_size = str.size();
	for (uint i = 0; i < string_size; ++i)
	{
		if (delimiters.find(str[i]) == std::string::npos)
		{
			construction_string.push_back(str[i]);
		}
		else
		{
			if (construction_string.size() > 0)
			{
				result.push_back(construction_string);
				construction_string.clear();
			}			
		}
	}
	if (construction_string.size() > 0)
	{
		result.push_back(construction_string);
		construction_string.clear();
	}

	return result;
}
//--------------------------------------------------------------------------------------
std::vector<std::string> GString::Split(const std::string &str, const std::string delimiters)
{
	std::vector<std::string> result;
	Split(str, delimiters, result);
	return result;
}
//--------------------------------------------------------------------------------------