#include "GUISlider.h"
using namespace Game;
//------------------------------------------------------------------------------------

GUISlider::GUISlider(void)
{
}
//------------------------------------------------------------------------------------
GUISlider::~GUISlider(void)
{
}
//------------------------------------------------------------------------------------
void GUISlider::Initialize( const std::string &label, const XMFLOAT2 &top_left, 
	const uint indicator_width, const uint indicator_height, const XMFLOAT4 &text_color, 
	const XMFLOAT4 &background_color, const XMFLOAT4 &foreground_color, 
	const float min, const float max, const float step, const float current, 
	ID3D11Device *device, ID3D11DeviceContext *context, 
	const GApplicationInformation &app_info,
	const float near_plane, const float far_plane )
{
	mLabel = label;
	mTopLeft = top_left;
	mIndicatorWidth = indicator_width;
	mIndicatorHeight = indicator_height;
	mTextColor = text_color;
	mBackgroundColor = background_color;
	mForegroundColor = foreground_color;
	mMinimumValue = min;
	mMaximumValue = max;
	mStep = step;
	mCurrentValue = current;

	mNearPlane = near_plane;
	mFarPlane = far_plane;
	mScreenWidth = app_info.mClientWidth;
	mScreenHeight = app_info.mClientHeight;
	XMStoreFloat4x4( &mOrthogonalProjection, 
		XMMatrixOrthographicLH( static_cast<float>( mScreenWidth ), 
		static_cast<float>( mScreenHeight ), mNearPlane, mFarPlane ) );

	mUpperText = std::make_shared<GText>( GText( device, context, 
		app_info.mClientWidth, app_info.mClientHeight,
		"Data/Fonts/courier_new_12_dark_green.font", 
		mLabel + ": " + GString::FromFloat( mCurrentValue ) ) );
	mUpperText->Position( XMINT2( mTopLeft.x, mTopLeft.y - 
		mUpperText->GetSymbolHeight() - 2 ) );

	//std::string bottom_text = "Min: " + GString::FromFloat( mMinimumValue ) +
	//	" Max: " + GString::FromFloat( mMaximumValue ) +
	//	" Step: " + GString::FromFloat( mStep );
	//mBottomText = std::make_shared<GText>( GText( device, context, 
	//	app_info.mClientWidth, app_info.mClientHeight,
	//	"Data/Fonts/courier_new_12_dark_green.font", 
	//	bottom_text ) );
	//mBottomText->Position( XMINT2( mTopLeft.x, mTopLeft.y + 
	//	mIndicatorHeight + 2.0f ) );

	mBackgroundPlane = std::make_shared<GPlane>( GPlane(device, context, 
		mIndicatorWidth, 1, mIndicatorHeight, 1 ) );
	mBackgroundPlane->Move( 0.5f * mIndicatorWidth, -0.5f * mIndicatorHeight, 0.0f );
	mBackgroundPlane->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );

	GMaterial material;
	material.Clear();
	material.SetKd( XMFLOAT3( mBackgroundColor.x, mBackgroundColor.y, mBackgroundColor.z ) );
	material.SetKa( XMFLOAT3( mBackgroundColor.x, mBackgroundColor.y, mBackgroundColor.z ) );

	mBackgroundPlane->SetMaterial(material);

	mForegroundPlane = std::make_shared<GPlane>( GPlane(device, context, 
		mIndicatorWidth, 1, mIndicatorHeight, 1 ) );
	mForegroundPlane->Move( 0.5f * mIndicatorWidth, -0.5f * mIndicatorHeight, 0.0f );
	mForegroundPlane->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );

	material.Clear();
	material.SetKd( XMFLOAT3( mForegroundColor.x, mForegroundColor.y, mForegroundColor.z ) );
	material.SetKa( XMFLOAT3( mForegroundColor.x, mForegroundColor.y, mForegroundColor.z ) );

	mForegroundPlane->SetMaterial(material);

	// Change foreground width according to current value.
	mCurrentForegroundWidthScale = 1.0f;
	SetCurrentValue( mCurrentValue );
}
//------------------------------------------------------------------------------------
void GUISlider::Render( GShadersManager &shaders_manager, 
	const std::string &font_shader_id, const std::string &overlay_shader_id, 
	GTexturesManager &textures_manager )
{
	shaders_manager[font_shader_id]->ClearConstantBuffers();
	shaders_manager[font_shader_id]->SetShader();
	shaders_manager.EnableAlpha();
	shaders_manager[font_shader_id]->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	shaders_manager[font_shader_id]->SetConstant( "PerFrame", "Projection", mOrthogonalProjection );
	shaders_manager[font_shader_id]->UpdateConstantBuffer( "PerFrame" );

	shaders_manager[font_shader_id]->SetConstant( "PerObject", "FontColor", mTextColor );
	textures_manager.SetTexture( "DiffuseMap", mUpperText->GetTextureName() );
	shaders_manager[font_shader_id]->SetConstant( "PerObject", "World", mUpperText->GetWorld() );	
	shaders_manager[font_shader_id]->UpdateConstantBuffer( "PerObject" );
	mUpperText->draw();

	//shaders_manager[font_shader_id]->.SetConstant( "PerObject", "FontColor", mTextColor );
	//textures_manager.SetTexture( "DiffuseMap", mBottomText->GetTextureName() );
	//shaders_manager[font_shader_id]->.SetConstant( "PerObject", "World", GMath::MTranspose( mBottomText->GetWorld() ) );	
	//shaders_manager[font_shader_id]->.UpdateConstantBuffer( "PerObject" );
	//mBottomText.draw();

	shaders_manager[overlay_shader_id]->ClearConstantBuffers();
	shaders_manager[overlay_shader_id]->SetShader();
	shaders_manager.EnableAlpha();
	shaders_manager[overlay_shader_id]->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	shaders_manager[overlay_shader_id]->SetConstant( "PerFrame", "Projection", mOrthogonalProjection );
	shaders_manager[overlay_shader_id]->UpdateConstantBuffer( "PerFrame" );

	shaders_manager[overlay_shader_id]->SetConstant( "PerObject", "World", 
		GMathMF( GMathFM( mBackgroundPlane->GetWorld() ) * XMMatrixTranslation( 
		static_cast<float>( -0.5f * mScreenWidth ) + mTopLeft.x,	
		static_cast<float>( 0.5f * mScreenHeight ) - mTopLeft.y, 
		0.0f ) ) );
	shaders_manager[overlay_shader_id]->SetConstant( "PerObject", "UseDiffuseMap", 0);
	shaders_manager[overlay_shader_id]->UpdateConstantBuffer( "PerObject" );
	mBackgroundPlane->draw();

	shaders_manager[overlay_shader_id]->SetConstant( "PerObject", "World", 
		GMathMF( GMathFM( mForegroundPlane->GetWorld() ) * XMMatrixTranslation( 
		static_cast<float>( -0.5f * mScreenWidth ) + mTopLeft.x,	
		static_cast<float>( 0.5f * mScreenHeight ) - mTopLeft.y, 
		0.0f ) ) );
	shaders_manager[overlay_shader_id]->SetConstant( "PerObject", "UseDiffuseMap", 0);
	shaders_manager[overlay_shader_id]->UpdateConstantBuffer( "PerObject" );
	mForegroundPlane->draw();
}
//------------------------------------------------------------------------------------
void GUISlider::OnWindowResize( const uint new_width, const uint new_height )
{
	mScreenWidth = new_width;
	mScreenHeight = new_height;

	mUpperText->OnResize( mScreenWidth, mScreenHeight );
	//mBottomText->OnResize( mScreenWidth, mScreenHeight );

	XMStoreFloat4x4( &mOrthogonalProjection, 
		XMMatrixOrthographicLH( static_cast<float>( mScreenWidth ), 
		static_cast<float>( mScreenHeight ), mNearPlane, mFarPlane ) );
}
//------------------------------------------------------------------------------------
void GUISlider::OnInput( const GInput &input )
{
	// Find the relative position of mouse to the
	// indicator. Then find the number of steps
	// to take to fill the bar near mouse press
	// position.
	if ( input.IsMouseLeftDown() )
	{
		int x = input.MousePos().x;
		int y = input.MousePos().y;

		if ( y >= mTopLeft.y && y <= mTopLeft.y + mIndicatorHeight && 
			x >= mTopLeft.x && x <= mTopLeft.x + mIndicatorWidth )
		{
			float width_length = (static_cast<float>( x ) - mTopLeft.x) / 
				mIndicatorWidth * (mMaximumValue - mMinimumValue);

			float steps_to_take = static_cast<float>( static_cast<int>( width_length / mStep + 0.5f ) );

			SetCurrentValue( steps_to_take * mStep + mMinimumValue );
		}
	}
}
//------------------------------------------------------------------------------------
void GUISlider::SetCurrentValue( const float current )
{
	mCurrentValue = current;
	mUpperText->SetText( mLabel + ": " + GString::FromFloat( mCurrentValue ) );

	mForegroundPlane->Move( 0.5f * mIndicatorWidth * ( 1.0f - mCurrentForegroundWidthScale ), 0.0f, 0.0f );
	mForegroundPlane->Scale( 1.0f / mCurrentForegroundWidthScale, 1.0f, 1.0f );

	mCurrentForegroundWidthScale = ( mCurrentValue - mMinimumValue ) / 
		( mMaximumValue - mMinimumValue );

	mForegroundPlane->SetScale( mCurrentForegroundWidthScale, 1.0f, 1.0f );
	mForegroundPlane->Move( -0.5f * mIndicatorWidth * ( 1.0f - mCurrentForegroundWidthScale ), 0.0f, 0.0f );
}
//------------------------------------------------------------------------------------
void GUISlider::StepIncrease()
{
	if ( mCurrentValue != mMaximumValue )
	{
		mCurrentValue += mStep;
		if ( mCurrentValue > mMaximumValue ) mCurrentValue = mMaximumValue;
		SetCurrentValue( mCurrentValue );
	}
}
//------------------------------------------------------------------------------------
void GUISlider::StepDecrease()
{
	if ( mCurrentValue != mMinimumValue )
	{
		mCurrentValue -= mStep;
		if ( mCurrentValue < mMinimumValue ) mCurrentValue = mMinimumValue;
		SetCurrentValue( mCurrentValue );
	}
}
//------------------------------------------------------------------------------------