#include "GObjFile.h"
using namespace Game;
//--------------------------------------------------------------------------------------
bool GObjFile::Load(std::string filename)
{
	// Read .OBJ file into lines.
	std::vector<std::string> file_lines;
	std::vector<std::string> material_file_lines;
	GFileAscii file;
	file.Open(filename);
	file_lines = file.GetLines();
	if (file_lines.size() <= 0)
		return false;

	// Clear old information.
	mMaterialLibraries.clear();
	mGroups.clear();

	// Read materials.
	std::vector<std::string> string_parts;
	GMaterial current_material;
	current_material.Clear();
	current_material.mName = "";
	std::string obj_file_directory = filename.substr(0, filename.find_last_of('/') + 1);

	// Get vertices.
	VertexPosition vertex_position;	
	VertexNormal vertex_normal;
	TextureCoordinate texture_coordinate;

	// Get faces
	Face face;
	FaceVertex face_vertex;
	int normals_count = mNormals.size();
	int texcoord_count = mTexCoords.size();
	vector<string> vnt_indices;
	bool object_found = false;
	GMaterial empty_material;
	current_material = empty_material;
	std::vector<std::string> materials_seen_in_group;
	for (uint i = 0; i < file_lines.size(); ++i)
	{
		string_parts = GString::Split(file_lines[i], " \t");
		if (string_parts.size() == 0)
			continue;
		if (string_parts[0] == "mtllib")
		{
			GMtlFile mtl_file;
			// TODO: warning! .MTL loading from .OBJ file location ONLY!
			mtl_file.Load(obj_file_directory + string_parts[1]);
			auto materials = mtl_file.GetMaterials();
			if (materials.size() > 0)
			{
				mMaterialLibraries.push_back(obj_file_directory + string_parts[1]);
				mMaterials.insert(mMaterials.end(), materials.begin(), materials.end());
				int a = 10;
				a += 5;
			}
		}
		else if (string_parts[0] == "g" || string_parts[0] == "o" || string_parts[0] == "s")
		{
			object_found = true;
			Group group;
			group.mName = string_parts[1];
			mGroups.push_back(group);
		}
		else if (string_parts[0] == "usemtl")
		{
			object_found = true;
			auto material = find_if(mMaterials.begin(), mMaterials.end(), [&](const GMaterial &mat)->bool
			{
				return mat.mName == string_parts[1];
			});
			if (material != mMaterials.end()) {
				current_material = *material;
			}
			else {
				return false;
			}
		}
		else if (string_parts[0] == "v")
		{
			vertex_position.x = GString::ToFloat(string_parts[1]);
			vertex_position.y = GString::ToFloat(string_parts[2]);
			vertex_position.z = GString::ToFloat(string_parts[3]);
			mVertices.push_back(vertex_position);
		}
		else if (string_parts[0] == "vn")
		{
			vertex_normal.nx = GString::ToFloat(string_parts[1]);
			vertex_normal.ny = GString::ToFloat(string_parts[2]);
			vertex_normal.nz = GString::ToFloat(string_parts[3]);
			mNormals.push_back(vertex_normal);
			normals_count = mNormals.size();
		}
		else if (string_parts[0] == "vt")
		{
			texture_coordinate.u = GString::ToFloat(string_parts[1]);
			texture_coordinate.v = GString::ToFloat(string_parts[2]);
			mTexCoords.push_back(texture_coordinate);
			texcoord_count = mTexCoords.size();
		}
		else if (string_parts[0] == "f")
		{
			// If object has been found, create mesh with material,
			// before filling it with faces, marked by `f` symbol in file
			if (object_found)
			{
				object_found = false;
				if (current_material.mName == "")
				{// if no material used on the object, set it to empty
					Mesh mesh;
					mesh.mMaterial = empty_material;
					mGroups.back().mMeshes.push_back(mesh);
				}
				else
				{
					Mesh mesh;
					mesh.mMaterial = current_material;
					mGroups.back().mMeshes.push_back(mesh);
				}
				// reset current material for next mesh
				current_material = empty_material;
			}
			// Read model faces
			// First face vertex
			vnt_indices = GString::Split(string_parts[1], '/');			
			face_vertex.mVertex = mVertices[GString::ToInt(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[GString::ToInt(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[1]) - 1];
			}
			face.mVertices[0] = face_vertex;
			// second face vertex
			vnt_indices = GString::Split(string_parts[2], '/');			
			face_vertex.mVertex = mVertices[GString::ToInt(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[GString::ToInt(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[1]) - 1];
			}
			face.mVertices[1] = face_vertex;
			// third face vertex
			vnt_indices = GString::Split(string_parts[3], '/');			
			face_vertex.mVertex = mVertices[GString::ToInt(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[GString::ToInt(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[GString::ToInt(vnt_indices[1]) - 1];
			}
			face.mVertices[2] = face_vertex;
			// store face
			mGroups.back().mMeshes.back().mFaces.push_back(face);
		}
	}

	// Clear unneeded resources.
	mVertices.clear();
	mNormals.clear();
	mTexCoords.clear();
	mMaterials.clear();

	return true;
}
//-------------------------------------------------------------------------------------
bool GObjFile::Save(std::string filename) const
{
	// TODO: implement this!
	return true;
}
//-------------------------------------------------------------------------------------
int GObjFile::GetMeshesCount() const
{
	int count = 0;
	for (const auto& group : mGroups)
	{
		count += group.mMeshes.size();
	}
	return count;
}
//---------------------------------------------------------------------------------------
void GObjFile::GetIndexes(const uint mesh_global_index, int &group_index, int &mesh_index) const
{
	int counter = 0;
	for (uint i = 0; i < mGroups.size(); ++i)
	{
		for (uint j = 0; j < mGroups[i].mMeshes.size(); ++j)
		{
			if (counter == mesh_global_index)
			{
				group_index = i;
				mesh_index = j;
				return;
			}
			counter += 1;
		}
	}
	
	// Not found.
	group_index = -1;
	mesh_index = -1;
}
//---------------------------------------------------------------------------------------
int GObjFile::GetFirstMeshGlobalIndex(const uint group_index) const
{
	if ( group_index < 0 || 
		group_index > ( mGroups.size() - 1 ) )
		return -1;

	int meshes_count = 0;
	for ( uint i = 0; i < group_index; ++i )
		for ( uint j = 0; j < mGroups[i].mMeshes.size(); ++j )
		{
			meshes_count += 1;
		}

	return meshes_count;
}
//---------------------------------------------------------------------------------------
void GObjFile::InvertTriangles()
{
	FaceVertex temp_for_swap;
	for ( auto &group : mGroups )
	{
		for ( auto &mesh : group.mMeshes)
		{
			for ( auto &face : mesh.mFaces )
			{
				// Swap first vertex and last vertex
				// of the model's face. Invert face's
				// normal.
				temp_for_swap = face.mVertices[0];
				face.mVertices[0] = face.mVertices[2];
				face.mVertices[2] = temp_for_swap;
				face.mVertices[0].mNormal.nx *= -1.0f;
				face.mVertices[0].mNormal.ny *= -1.0f;
				face.mVertices[0].mNormal.nz *= -1.0f;
				face.mVertices[1].mNormal.nx *= -1.0f;
				face.mVertices[1].mNormal.ny *= -1.0f;
				face.mVertices[1].mNormal.nz *= -1.0f;
				face.mVertices[2].mNormal.nx *= -1.0f;
				face.mVertices[2].mNormal.ny *= -1.0f;
				face.mVertices[2].mNormal.nz *= -1.0f;
				// TODO: reconstruct tangent and binormal.
			}
		}
	}
}
//---------------------------------------------------------------------------------------