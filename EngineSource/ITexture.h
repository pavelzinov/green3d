#pragma once
#include "GUtility.h"

namespace Game
{
	class ITexture
	{
	public:
		ITexture(ID3D11Device* device) { mpDevice = device; }
		virtual ~ITexture() {}

		virtual ID3D11ShaderResourceView* GetView() const = 0;

	protected:
		ID3D11Device				*mpDevice;
	};
}