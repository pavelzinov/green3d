#pragma once

#ifndef UBUNTU
#include <windows.h>
#endif

#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <fstream>
#include "GFileAscii.h"
//---------------------------------------------------------
namespace Game
{

class GLog
{
public:
	GLog(void) {}
	~GLog(void) {}

	static void Debug(const std::string &message);
	static void MessageBox(const std::string& message);

	void WriteLine(const std::string &message);
	void SaveToFile(const std::string &filename);

private:
	GFileAscii mLog;
};

} // namespace