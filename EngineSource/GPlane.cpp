#include "GPlane.h"
using namespace Game;
//------------------------------------------------------------------------------------
GPlane::GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, uint width_segments,
				  float height, uint height_segments)
{
	mpDevice = device;
	mpContext = context;
	mpVB = nullptr;
	mpIB = nullptr;
	XMStoreFloat4x4( &mScaling, XMMatrixIdentity() );
	mRotation = mTranslation = mScaling;

	mWidth = width;
	mHeight = height;
	mWidthSegments = width_segments;
	mHeightSegments = height_segments;

	GenerateVertices();

	// Create indices
	uint faces_count = width_segments * height_segments * 2;
	// 2 triangles in every quad
	uint k = 0; // quad number
	int quad_top_left_point_count = GetVertexCount() - width_segments - 1;
	mIndices.resize( faces_count * 3 );
	for ( int i = 0; i < quad_top_left_point_count; ++i )
	{
		// adding quad to the index buffer            
		if ( ( i + 1 ) % ( width_segments + 1 ) != 0 )
		{// if point is not on the plane's right edge
			// upper triangle
			mIndices[k]		= i;
			mIndices[k+1]	= i + 1;
			mIndices[k+2]	= i + width_segments + 1;
			// lower triangle
			mIndices[k+3]	= mIndices[k+2];
			mIndices[k+4]	= mIndices[k+1];
			mIndices[k+5]	= mIndices[k+2] + 1;
			k += 6;
		} // end if
	} // end for

	GenerateNormals();
	ConstructBuffers();
}
//------------------------------------------------------------------------------------
GPlane::GPlane(const GPlane& plane)
{
	mpVB = nullptr;
	mpIB = nullptr;

	mWidth = plane.mWidth;
	mHeight = plane.mHeight;
	mWidthSegments = plane.mWidthSegments;
	mHeightSegments = plane.mHeightSegments;

	mName = plane.mName;
	mMaterialID = plane.mMaterialID;
	mVertices = plane.mVertices;
	mIndices = plane.mIndices;
	mScaling = plane.mScaling;
	mTranslation = plane.mTranslation;
	mRotation = plane.mRotation;

	mpDevice = plane.mpDevice;
	mpContext = plane.mpContext;

	// Construct new drawing buffers.
	ConstructBuffers();

	// Utilize operator=.
	//*this = plane;
}
//------------------------------------------------------------------------------------
const GPlane& GPlane::operator=(const GPlane &plane)
{
	mWidth = plane.mWidth;
	mHeight = plane.mHeight;
	mWidthSegments = plane.mWidthSegments;
	mHeightSegments = plane.mHeightSegments;

	mName = plane.mName;
	mMaterialID = plane.mMaterialID;
	mVertices = plane.mVertices;
	mIndices = plane.mIndices;
	mScaling = plane.mScaling;
	mTranslation = plane.mTranslation;
	mRotation = plane.mRotation;

	mpDevice = plane.mpDevice;
	mpContext = plane.mpContext;

	// Release old drawing buffers.
	if ( mVertices.size() <= 0 )
	{
		ReleaseCOM(mpIB);
		ReleaseCOM(mpVB);
	}
	else
	{
		// Construct new drawing buffers.
		ConstructBuffers();
	}

	return *this;
}
//------------------------------------------------------------------------------------
void GPlane::GenerateNormals()
{
	if (mIndices.size() == 0 || mVertices.size() == 0)
		return;

	XMVECTOR vec1 = GMathFV(mVertices[mIndices[0]].mPosition) - 
		GMathFV(mVertices[mIndices[1]].mPosition);
	XMVECTOR vec2 = GMathFV(mVertices[mIndices[0]].mPosition) - 
		GMathFV(mVertices[mIndices[2]].mPosition);
	// determine normal for first triangle
	XMFLOAT3 normal = GMathVF(XMVector3Normalize(XMVector3Cross(vec1, vec2)));
	// all vertices will have the normal equal to normal vector of first triangle
	for (auto it=mVertices.begin(); it != mVertices.end(); it++)
	{
		(*it).mNormal = normal;
	}
}
//------------------------------------------------------------------------------------
void GPlane::GenerateVertices()
{
	float width_segment_length = mWidth/mWidthSegments;
	float height_segment_length = mHeight/mHeightSegments;
	float width_texel_length = 1.0f/mWidthSegments;
	float height_texel_length = 1.0f/mHeightSegments;
	XMFLOAT3 point(0.0f, 0.0f, 0.0f);
	XMFLOAT2 point_tex_coords(0.0f, 0.0f);
	XMFLOAT3 translate_coords(-mWidth/2.0f, 0.0f, mHeight/2.0f);
	uint vertices_count = (mWidthSegments+1)*(mHeightSegments+1);

	mVertices.clear();
	mVertices.resize(vertices_count);
	mVertices[0].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
	mVertices[0].mTextureCoordinates = point_tex_coords;
	for (uint i=1; i<vertices_count; ++i)
	{
		// if point is first in line
		if (i % (mWidthSegments + 1) == 0)
		{// reset it's coordinates
			point.x = 0.0f;
			point.z -= height_segment_length;
			point_tex_coords.x = 0;
			point_tex_coords.y += height_texel_length;
		}
		else
		{
			point.x += width_segment_length;
			point_tex_coords.x += width_texel_length;
		}

		mVertices[i].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
		mVertices[i].mTextureCoordinates = point_tex_coords;
		mVertices[i].mDiffuseColor = GColor::DARK_YELLOW_GREEN;
		mVertices[i].mAmbientColor = GColor::DARK_YELLOW_GREEN;

		// TODO: Tangent, binormal
	}
}
//------------------------------------------------------------------------------------
void GPlane::ConstructBuffers()
{
	if (mVertices.size() <= 0)
		return;

	// Clear old buffers
	ReleaseCOM(mpIB);
	ReleaseCOM(mpVB);	

	// Feed vertex data
	void *vertices = GetVertexData();

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * GetVertexCount();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mpDevice->CreateBuffer(&verticesDesc, &verticesData, &mpVB));
	delete[] vertices;

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mpDevice->CreateBuffer(&indicesDesc, &indicesData, &mpIB));
}
//------------------------------------------------------------------------------------
void* GPlane::GetVertexData()
{
	uint verticesNumber = GetVertexCount();
	GVertexData *vertices = new GVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}
	return static_cast<void*>(vertices);
}
//-------------------------------------------------------------------------------------
const XMFLOAT4X4 GPlane::GetWorld() const
{
	return GMathMF( GMathFM( mScaling ) * GMathFM( mRotation ) 
		* GMathFM( mTranslation ) );
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::RotateSelf(const XMFLOAT3 &axis, float degrees)
{
	if ( XMVector3Equal( GMathFV(axis), XMVectorZero() ) ||
		degrees == 0.0f )
		return GetWorld();

	XMStoreFloat4x4( &mRotation, XMLoadFloat4x4( &mRotation ) 
		* XMMatrixRotationAxis( XMLoadFloat3( &axis ), XMConvertToRadians( degrees ) ) );

	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::Scale(float scale_x, float scale_y, float scale_z)
{	
	XMStoreFloat4x4(&mScaling, XMLoadFloat4x4(&mScaling) 
		* XMMatrixScaling(scale_x, scale_y, scale_z));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::Scale(float delta_size)
{
	return Scale(delta_size, delta_size, delta_size);
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::SetScale(float dx, float dy, float dz)
{
	XMStoreFloat4x4(&mScaling, XMMatrixScaling(dx, dy, dz));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::SetScale(float delta_size)
{
	return SetScale(delta_size, delta_size, delta_size);
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::Move(XMFLOAT3 direction)
{
	XMStoreFloat4x4(&mTranslation, XMLoadFloat4x4(&mTranslation) 
		* XMMatrixTranslation(direction.x, direction.y, direction.z));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GPlane::Move(float dir_x, float dir_y, float dir_z)
{
	return Move(XMFLOAT3(dir_x, dir_y, dir_z));
}
//-----------------------------------------------------------------------------------
void GPlane::draw() const
{
	uint strides = GVertex::GetSize();
	uint offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVB, &strides, &offset);
	mpContext->IASetIndexBuffer(mpIB, DXGI_FORMAT_R32_UINT, 0);
	mpContext->DrawIndexed(mIndices.size(), 0, 0);
}
//----------------------------------------------------------------------------------
void GPlane::SetMaterial(const GMaterial& material)
{
	mMaterialID = material.mName;
	for (GVertex &vertex : mVertices)
	{
		vertex.mDiffuseColor = material.GetDiffuseColor();
		vertex.mAmbientColor = material.GetAmbientColor();
	}
	// Reconstruct vertices
	ConstructBuffers();
}
//------------------------------------------------------------------------------------