#pragma once
#include <windows.h>
#include <gdiplus.h>
#include <vector>
#include <string>
#include "GString.h"
typedef unsigned int uint;
//------------------------------------------------------------------------------------
namespace Game
{
	// Class to process BMP, JPEG and PNG file data.
	// Stores each pixel value.
	// Each pixel can be accessed like this: Data[x][y],
	// where `x` corresponds to image's columns and `y` corresponds to image's
	// rows. This class also stores image's parameters such as width and height 
	// in pixels. This class utilizes GDI+
	class GImage
	{
	public:
		GImage(void) { mWidth = 0; mHeight = 0; }
		~GImage(void) {}

	private:
		// Pixel structure. For images which does not support alpha channel, `a` parameter will be equal to 255
		struct Pixel
		{
			float r, g, b, a;
		};

	public:
		// Load BMP, JPEG or PNG file
		bool LoadBitmap(std::string filename);
		// Access image's pixels array by reference
		const std::vector<std::vector<Pixel>>& AccessData() { return mData; }
		// Get image's pixels
		std::vector<std::vector<Pixel>> GetData() const { return mData; }
		// Get image's height
		const uint GetHeight() const { return mHeight; }
		// Get image's width
		const uint GetWidth() const { return mWidth; }

	private:
		// Two-dimensional array, storing pixels of the image
		std::vector<std::vector<Pixel>> mData;
		// Image's width in pixels
		uint mWidth;
		// Image's height in pixels
		uint mHeight;

	}; // class
} // namespace
//------------------------------------------------------------------------------------