#include "GShaderPass.h"
using namespace Game;
//------------------------------------------------------------------------------------
GShaderPass::GShaderPass( ID3D11Device* device, ID3D11DeviceContext* context )
{
	mTopology					= D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	mpDevice					= device;
	mpContext					= context;
	mpPixelShader				= nullptr;
	mpVertexShader				= nullptr;
	mpVertexLayout				= nullptr;
	mpGeometryShader			= nullptr;

	mEnabledVertexShader	= false;
	mEnabledGeometryShader	= false;
	mEnabledPixelShader		= false;
}
//------------------------------------------------------------------------------------
GShaderPass::~GShaderPass( void )
{
	for ( auto &buffer : mConstantBuffersResources ) ReleaseCOM( buffer );
	ReleaseCOM( mpGeometryShader );
	ReleaseCOM( mpVertexLayout );
	ReleaseCOM( mpVertexShader );
	ReleaseCOM( mpPixelShader );
}
//------------------------------------------------------------------------------------
HRESULT GShaderPass::LoadVertexShader( const std::string &filename, 
	const std::string &shader_function_name )
{
	ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile( GString::ToUnicode( filename ).c_str(), 
		shader_function_name.c_str(), "vs_5_0", &pVSBlob );
	if( FAILED( hr ) )
	{
		MessageBox( NULL, 
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK );
		return hr;
	}

	// Create the vertex shader
	hr = mpDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader );
	if( FAILED( hr ) )
	{	
		pVSBlob->Release();
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	uint numElements = ARRAYSIZE( layout );

	// Create the input layout
	hr = mpDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if( FAILED( hr ) )
		return hr;

	// Find all constant buffers in file
	GatherAllConstantBuffersInFile( filename );

	// Sort buffers by their slot number, specified in shader file
	// like this - cb0, cb1, cb2 and so on. These slots are controlled
	// by shader code programmer. Here we sort it only because of included
	// files, which may contain other constant buffers.
	ConstantBufferInfo cb_info;
	for ( int i = 0; i < static_cast<int>( mConstantBuffersInfo.size() ); ++i )
	{
		if ( mConstantBuffersInfo[i].mSlot != i )
		{
			cb_info = mConstantBuffersInfo[i];
			mConstantBuffersInfo.erase( mConstantBuffersInfo.begin() + i, mConstantBuffersInfo.begin() + i + 1 );
			mConstantBuffersInfo.insert( mConstantBuffersInfo.begin() + i, cb_info );
			i = -1;
		}
	}
	// Assign slots to every buffer according to buffer's position in array.
	for ( uint i = 0; i < mConstantBuffersInfo.size(); ++i )
		mConstantBuffersInfo[i].mSlot = i;
	// Construct buffers, containing shader constant buffers data bytes.
	// Fill bytes with 0 as default.
	for ( uint i = 0; i < mConstantBuffersInfo.size(); ++i )
	{
		mConstantBuffersData.push_back( std::vector<char>() );
		for ( uint j = 0; j < mConstantBuffersInfo[i].mMembers.size(); ++j )
		{
			if ( mConstantBuffersInfo[i].mMembers[j].mType == "matrix" ) 
			{
				mConstantBuffersInfo[i].mMembers[j].mByteOffset = mConstantBuffersData[i].size();

				for ( uint k = 0; k < 64; ++k )
					mConstantBuffersData.back().push_back( 0 );
			}
			else 
			{
				mConstantBuffersInfo[i].mMembers[j].mByteOffset = mConstantBuffersData[i].size();
				uint quantity = 1;
				std::vector<std::string> split_strings = 
					GString::Split( mConstantBuffersInfo[i].mMembers[j].mType, "floatin" );

				if ( split_strings.size() > 0 )
					quantity = std::stoi( split_strings[0] );

				for ( uint k = 0; k < quantity * 4; ++k )
					mConstantBuffersData.back().push_back( 0 );
			}
		}
	}

	// Create constant buffer resources.
	ID3D11Buffer *constant_buffer_resource = nullptr;
	D3D11_BUFFER_DESC bd;
	for ( uint i = 0; i < mConstantBuffersData.size(); ++i )
	{
		ZeroMemory( &bd, sizeof( bd ) );
		bd.Usage = D3D11_USAGE_DEFAULT;//D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = mConstantBuffersData[i].size();
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0; //D3D11_CPU_ACCESS_WRITE;
		HRR( mpDevice->CreateBuffer( &bd, NULL, &constant_buffer_resource ) );
		mConstantBuffersResources.push_back( constant_buffer_resource );
	}

	mEnabledVertexShader = true;

	return S_OK;
}
//------------------------------------------------------------------------------------
HRESULT GShaderPass::LoadPixelShader( const std::string &filename, 
	const std::string &shader_function_name )
{
	ID3DBlob* pPSBlob = NULL;
	HRESULT hr = CompileShaderFromFile( GString::ToUnicode( filename ).c_str(), 
		shader_function_name.c_str(), "ps_5_0", &pPSBlob );
	if( FAILED( hr ) )
	{
		MessageBox( NULL,
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK );
		return hr;
	}

	// Create the pixel shader
	hr = mpDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
	if( FAILED( hr ) )
		return hr;

	mEnabledPixelShader = true;

	return S_OK;
}
//------------------------------------------------------------------------------------
HRESULT GShaderPass::LoadGeometryShader( const std::string &filename, const std::string &shader_function_name )
{
	ID3DBlob* pGSBlob = NULL;
	HRESULT hr = CompileShaderFromFile( GString::ToUnicode( filename ).c_str(), 
		shader_function_name.c_str(), "gs_5_0", &pGSBlob );
	if( FAILED( hr ) )
	{
		MessageBox( NULL,
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK );
		return hr;
	}

	// Create the pixel shader
	hr = mpDevice->CreateGeometryShader( pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), NULL, &mpGeometryShader );
	pGSBlob->Release();
	if( FAILED( hr ) )
		return hr;

	mEnabledGeometryShader = true;

	return S_OK;
}
//------------------------------------------------------------------------------------
HRESULT GShaderPass::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
	LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;
	hr = D3DCompileFromFile( szFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel, 
		dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
	if( FAILED( hr ) )
	{
		if( pErrorBlob != NULL )
			OutputDebugStringA( static_cast<char*>( pErrorBlob->GetBufferPointer() ) );
		if( pErrorBlob ) pErrorBlob->Release();
		return hr;
	}
	if( pErrorBlob ) pErrorBlob->Release();

	return S_OK;
}
//------------------------------------------------------------------------------------
void GShaderPass::GatherAllConstantBuffersInFile( const std::string &filename )
{
	// Parse file lines.
	GFileAscii file;
	file.Open( filename );
	auto file_lines = file.GetLines();
	std::vector<std::string> split_strings;
	int curved_brackets_count = 0; // Stores the count of curved brackets to determine constant buffer ending.
	for ( auto line : file_lines )
	{
		split_strings.clear();
		split_strings = GString::Split( line, " \t\":;" );
		// If we get empty array after performing split operation - skip current loop step.
		if ( split_strings.size() <= 0 )
			continue;

		// If we find #include directive, go into included file to analyze its content as well.
		if ( split_strings[0] == "#include" )	
		{
			std::string include_filename = split_strings[1];
			// Determine relative location of included file.
			if ( filename.find_last_of( "\\" ) != std::string::npos ) {
				include_filename = filename.substr( 0, filename.find_last_of( "\\" ) + 1 ) + split_strings[1];
			}
			else if ( filename.find_last_of( "/" ) != std::string::npos )	{
				include_filename = filename.substr( 0, filename.find_last_of( "/" ) + 1 ) + split_strings[1];
			}
			GatherAllConstantBuffersInFile( include_filename );
		}
		else if ( split_strings[0] == "cbuffer" )	
		{
			ConstantBufferInfo cb_info;
			cb_info.mName = split_strings[1];
			cb_info.mSlot = D3D11_COMMONSHADER_CONSTANT_BUFFER_HW_SLOT_COUNT + 1;
			std::string slot_number = "";
			for ( uint i = 2; i < split_strings.size(); ++i )
			{
				slot_number += split_strings[i];
			}
			if ( slot_number.find( "register" ) != std::string::npos )
			{
				slot_number = slot_number.substr( slot_number.find_last_of( "( " ) + 1, 
					slot_number.find_last_of( " )" ) - slot_number.find_last_of( "( " ) - 1 );

				split_strings.clear();
				split_strings = GString::Split( slot_number, " \t\ncb()" );
				if ( split_strings.size() > 0 )
				{
					cb_info.mSlot = std::stoi( split_strings[0] );
				}
			}
			mConstantBuffersInfo.push_back( cb_info );
			curved_brackets_count++;
		}
		else if ( split_strings[0] == "matrix" || split_strings[0] == "float4" ||
			split_strings[0] == "float3" || split_strings[0] == "float2" ||
			split_strings[0] == "float" || split_strings[0] == "int4" ||
			split_strings[0] == "int3" || split_strings[0] == "int2" || split_strings[0] == "int" ) 
		{
			if ( curved_brackets_count <= 0 )
				continue;

			VariableInfo var_info;
			var_info.mType = split_strings[0];
			var_info.mName = split_strings[1];

			mConstantBuffersInfo.back().mMembers.push_back( var_info );
		}
		else if ( split_strings[0] == "}" && curved_brackets_count > 0 ) {
			curved_brackets_count--;
		}
	}
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const std::vector<char> &new_value_bytes, const uint bytes_size )
{
	// Find buffer with given name.
	for ( uint i = 0; i < mConstantBuffersInfo.size(); ++i ) {
		if ( mConstantBuffersInfo[i].mName == constant_buffer_name ) {
			// Find variable with given name.
			for ( uint j = 0; j < mConstantBuffersInfo[i].mMembers.size(); ++j ) {
				if ( mConstantBuffersInfo[i].mMembers[j].mName == variable_name ) {
					// Update variable's bytes starting at variable's location in data buffer.
					memcpy( &mConstantBuffersData[i][mConstantBuffersInfo[i].mMembers[j].mByteOffset],
						new_value_bytes.data(), bytes_size );
					return;
				}
			}
		}
	}
}
//------------------------------------------------------------------------------------
void GShaderPass::ClearConstantBuffer( const std::string &constant_buffer_name )
{
	for ( uint i = 0; i < mConstantBuffersInfo.size(); ++i )
	{
		if ( mConstantBuffersInfo[i].mName == constant_buffer_name )
		{
			ZeroMemory( mConstantBuffersData[i].data(), sizeof( mConstantBuffersData[i] ) );
		}
	}
}
//------------------------------------------------------------------------------------
void GShaderPass::ClearConstantBuffers()
{
	for ( uint i = 0; i < mConstantBuffersData.size(); ++i )
	{
		ZeroMemory( mConstantBuffersData[i].data(), sizeof( mConstantBuffersData[i] ) );
	}
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMFLOAT4X4 &new_value )
{
	// Transpose matrix first.
	XMFLOAT4X4 t_matrix = GMathT( new_value );
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( t_matrix ), 0 );
	memcpy( value_bytes.data(), &t_matrix, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( t_matrix ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMFLOAT4 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//-----------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMFLOAT3 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMFLOAT2 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const float &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMINT4 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMINT3 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const XMINT2 &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
	const std::string &variable_name, const int &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::SetConstant( const std::string &constant_buffer_name, 
						  const std::string &variable_name, const uint &new_value )
{
	// Form byte array, filled with `new_value` data.
	std::vector<char> value_bytes( sizeof( new_value ), 0 );
	memcpy( value_bytes.data(), &new_value, value_bytes.size() );
	SetConstant( constant_buffer_name, variable_name, value_bytes, sizeof( new_value ) );
}
//------------------------------------------------------------------------------------
void GShaderPass::UpdateConstantBuffer( const std::string &constant_buffer_name )
{
	// Find constant buffer by its name and call update on corresponding resource.
	for ( uint i = 0; i < mConstantBuffersInfo.size(); ++i ) {
		if ( mConstantBuffersInfo[i].mName == constant_buffer_name ) {
			mpContext->UpdateSubresource( mConstantBuffersResources[i], 0, nullptr, 
				mConstantBuffersData[i].data(), 0, 0 );
			return;
		}
	}
}
//------------------------------------------------------------------------------------
void GShaderPass::SetShader()
{
	mpContext->IASetInputLayout( mpVertexLayout );
	mpContext->IASetPrimitiveTopology( mTopology );

	ID3D11VertexShader *vs = nullptr;
	if ( mEnabledVertexShader ) vs = mpVertexShader;
	mpContext->VSSetShader( vs, nullptr, 0 );
	for ( uint i = 0; i < mConstantBuffersInfo.size() && vs != nullptr; ++i ) {
		mpContext->VSSetConstantBuffers( mConstantBuffersInfo[i].mSlot, 
			1, &mConstantBuffersResources[i] );
	}

	ID3D11GeometryShader *gs = nullptr;
	if ( mEnabledGeometryShader ) gs = mpGeometryShader;
	mpContext->GSSetShader( gs, nullptr, 0 );
	for ( uint i = 0; i < mConstantBuffersInfo.size() && gs != nullptr; ++i ) {
		mpContext->GSSetConstantBuffers( mConstantBuffersInfo[i].mSlot, 
			1, &mConstantBuffersResources[i] );
	}

	ID3D11PixelShader *ps = nullptr;
	if ( mEnabledPixelShader ) ps = mpPixelShader;
	mpContext->PSSetShader( ps, nullptr, 0 );
	for ( uint i = 0; i < mConstantBuffersInfo.size() && ps != nullptr; ++i ) {
		mpContext->PSSetConstantBuffers( mConstantBuffersInfo[i].mSlot, 
			1, &mConstantBuffersResources[i] );
	}
}
//------------------------------------------------------------------------------------