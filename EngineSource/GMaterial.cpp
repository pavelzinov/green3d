#include "GMaterial.h"
using namespace Game;
//------------------------------------------------------------------------------------
bool GMaterial::UsesTextureMaps() const
{
	return (map_Ka != "" || map_Kd != "" ||
		map_Ks != "" || map_Ns != "" || map_d != "" ||
		map_bump != "" || disp != "" || decal != "");
}
//------------------------------------------------------------------------------------
void GMaterial::Clear()
{
	mName = "";
	d = 1.0f;
	Ka.r = 0.0f;
	Ka.g = 0.0f;
	Ka.b = 0.0f;
	Kd.r = 0.0f;
	Kd.g = 0.0f;
	Kd.b = 0.0f;
	Ks.r = 0.0f;
	Ks.g = 0.0f;
	Ks.b = 0.0f;
	illum = 0;
	map_Ka = "";
	map_Kd = "";
	map_Ks = "";
	map_Ns = "";
	map_d = "";
	map_bump = "";
	disp = "";
	decal = "";
}
//------------------------------------------------------------------------------------
bool GMaterial::operator==(const GMaterial& material)
{
	return (mName == material.mName && d == material.d && 
		Ka.r == material.Ka.r && Ka.g == material.Ka.g && Ka.b == material.Ka.b &&
		Kd.r == material.Kd.r && Kd.g == material.Kd.g && Kd.r == material.Kd.b &&
		Ks.r == material.Ks.r && Ks.g == material.Ks.g && Ks.r == material.Ks.b &&
		illum == material.illum && map_Ka == material.map_Ka && map_Kd == material.map_Kd &&
		map_Ks == material.map_Ks && map_Ns == material.map_Ns && map_d == material.map_d &&
		map_bump == material.map_bump && disp == material.disp && decal == material.decal);
}
//------------------------------------------------------------------------------------
