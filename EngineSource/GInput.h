#pragma once
#include "GUtility.h"
#include "GTimer.h"
#include <sstream>
/////////////////////////////////////////////////////
// Class GInput
//---------------------------------------------------
// - Based on Raw Input
// - Updating states only when some input came to application from device
// - How to make this class work:
// --- Init Raw Input somewhere
// --- Place Update() in WM_INPUT messge
// --- Place UpdateMouseMove() at the end of every frame update
// - Allows:
// --- Check if mouse buttons are down, up or clicked
// --- Check keyboard for key pressed, down or up
/////////////////////////////////////////////////////
namespace Game
{
	//extern HWND gWindowHandle;
	struct MouseState 
	{
		bool LeftBtnUp;
		bool RightBtnUp;
		bool MiddleBtnUp;
		POINT Position;
	};

	class GInput
	{
	public:
		GInput(void) {}
		~GInput(void) {}

		// Initialize inner members, such as Raw Input.
		void Initialize();
		
		// MUST BE CALLED from window's message 
		// processing routine, when WM_INPUT message comes in.
		void OnWindowsInput(HRAWINPUT hRawInput, uint client_width, uint client_height);
		
		// MUST BE CALLED from window's message 
		// processing routine, when WM_MOUSEMOVE message comes in.
		// This function operates with mouse screen position.
		void OnMouseMove(uint x, uint y);

		// Call this if you want to handle WM_LBUTTONDOWN message explicitly.
		void OnMouseLeftDown(uint x, uint y);

		// Returns current mouse position in screen-space coordinates.
		// This is the last mouse coordinates in array which represents
		// mouse states.
		const POINT& MousePos() const { return mMouseStates.back().Position; }

		// MUST BE CALLED at the end of processing update.
		void Clear() { mMouseStates.clear(); mMouseStates.push_back( mCurrentMouseState ); }

		void OnKeyDown( USHORT key_code ) { mKeysDown[key_code] = true; mKeysUp[key_code] = false; }

		void OnKeyUp( USHORT key_code ) { mKeysDown[key_code] = false; mKeysUp[key_code] = true; }

		long GetAbsoluteX(void) const { return mMouseStates.back().Position.x/*mCurrentX*/; }
		
		long GetAbsoluteY(void) const { return mMouseStates.back().Position.y/*mCurrentY*/; }
		
		long GetRelativeX(void) const;
		
		long GetRelativeY(void) const;
		
		float GetMouseSensitivity() const;
		void SetMouseSensitivity(float val);
		bool IsMouseLeftClicked(void) const;
		bool IsMouseRightClicked(void) const;
		bool IsMouseLeftDown(void) const;
		bool IsMouseRightDown(void);
		bool IsMouseMoved(void);
		bool IsKeyDown(USHORT symbol);
		bool IsKeyUp(USHORT symbol);
		bool IsKeyPressed(USHORT symbol);
		//char GetLastKeyPressed() { return };
		bool IsScrollDown(void);
		bool IsScrollUp(void);

	private:
		// Inner timer
		GTimer mScrollTimer;
		// Mouse cursor position
		long mCurrentX;
		long mCurrentY;
		long mPreviousX;
		long mPreviousY;
		// Mouse states
		bool mMouseLeftButtonDown;
		bool mMouseLeftButtonUp;
		bool mMouseLeftBtnClicked;
		bool mMouseRightButtonDown;
		bool mMouseRightButtonUp;
		bool mMouseMiddleButtonDown;
		bool mMouseMiddleButtonUp;
		// Mouse scrolling
		bool mScrollDown;
		bool mScrollUp;    
		// Mouse property
		float mScrollDelay; // how long to scroll
		float mMouseSensitivity; // by how much to move mouse per input
		// Keyboard
		bool mKeysDown[1000];
		bool mKeysUp[1000];

		// Stores mouse states per input event.
		// Should be cleared after every object dispatched
		// those mouse states and done processing it.
		std::vector<MouseState>	mMouseStates;
		
		// Current cursor state. Changes per input event.
		MouseState	mCurrentMouseState;

		// Screen resolution.
		int mScreenWidth;
		int mScreenHeight;

		// Temp variables
		float mTmpDX;
		float mTmpDY;
		
		// Stores raw input data
		// per raw input window's message.
		HRAWINPUT mInput;

		// Storest cursor as an image.
		HCURSOR mNormalCursor;
	};

}