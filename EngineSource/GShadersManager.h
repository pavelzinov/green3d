#pragma once
#include "GUtility.h"
#include "GShaderPass.h"
#include "GFastMap.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GShadersManager
	{
	public:
		
		GShadersManager( ID3D11Device *device, ID3D11DeviceContext *context );
		
		~GShadersManager(void);

		// Add shader pass with single vertex shader.
		void AddShaderPass( const std::string &shader_id, 
			const std::string &vertex_shader_file, 
			const std::string &vertex_shader_main_routine_name );

		// Add shader pass with shaders, enumerated in vectors in pipeline usage order.
		// I.e. vertex shader, hull, domain, geometry, pixel. Shader names goes like this
		// VS, PS, GS and so on. Example of vectors:
		// =============================================================================
		// |shader_names	|		shader_files			|shader_main_routine_names
		// =============================================================================
		// |VS				|Data/Shaders/particles.hlsl	|VS
		// |GS				|Data/Shaders/particles.hlsl	|GeometryEntry
		// and so on...
		void AddShaderPass( const std::string &shader_id, 
			const std::vector<std::string> &shader_names, 
			const std::vector<std::string> &shader_files, 
			const std::vector<std::string> &shader_main_routine_names );

		void EnableAlpha() { mpContext->OMSetBlendState( mpBlendStateEnableAlpha, nullptr, 0xffffffff ); }

		void DisableAlpha() { mpContext->OMSetBlendState( mpBlendStateDisableAlpha, nullptr, 0xffffffff ); }

		// Access concrete shader by its id.
		GShaderPass* operator[]( const std::string& shader_id ) 
		{ return mShaderPasses.at( shader_id ); }

		// Access concrete shader by its id.
		GShaderPass* at( const std::string& shader_id ) 
		{ return mShaderPasses.at( shader_id ); }

	private:
		GFastMap<std::string, GShaderPass*> mShaderPasses;
		ID3D11Device						*mpDevice;
		ID3D11DeviceContext					*mpContext;
		ID3D11BlendState					*mpBlendStateEnableAlpha;
		ID3D11BlendState					*mpBlendStateDisableAlpha;
	}; // class GShadersManager
} // namespace Game
//------------------------------------------------------------------------------------