#pragma once
#include <string>
#include <vector>
#include <windows.h>
#include <fstream>
#include <sstream>
typedef unsigned int uint;
//------------------------------------------------------------------------------------
namespace Game
{
	class GFileAscii
	{
	public:
		GFileAscii(void) {}
		~GFileAscii(void) {}

		bool Open(const std::string filename);
		uint Size() { return mContent.size(); }
		std::string& Content() { return mContent; }
		std::vector<std::string>& GetLines() { return mLines; }
		void WriteLine(const std::string content);
		// Save content to hard drive file
		void Save(const std::string filename);
		// Return current directory in format "C:/current directory/"
		static std::string CurrentDirectory();

	private:
		struct FileNameInformation
		{
			// Example: C:/projects/home/myproject/text.txt
			std::string mShortName;				// text
			std::string mFullNameWithExtension;	// C:/projects/home/myproject/text.txt
			std::string mExtension;				// txt
			std::string mExtensionWithDot;		// .txt
			std::string mShortWithExtension;	// text.txt
			std::string mHomeFolderFull;		// C:/projects/home/myproject/
			std::string mHomeFolderShort;		// myproject
		} mNameInformation;
		std::string mContent;
		std::vector<std::string> mLines;

	
		// Parse mContent string to make mLines vector
		void ParseContent();
	}; // class
} // namespace
//------------------------------------------------------------------------------------