#pragma once
#include "GUtility.h"
#include "GVertex.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GParticles
	{
	public:
		struct Particle
		{
			XMFLOAT3 mPosition;
			XMFLOAT3 mVelocity;
			float mCurrentLifeTime;
			float mLifeTime;
		};
		GParticles( ID3D11Device *device, ID3D11DeviceContext *context, float particle_size, 
			const XMFLOAT3 &particle_point_emitter, float particle_maximum_lifetime, 
			uint particles_per_second, const std::string &texture_id );
		~GParticles();

		void Update( float delta_seconds );

		void Render() const;

		const std::string& GetTextureID() const { return mTextureID; }

		void SetParticleSize( float new_size ) { mParticleSize = new_size; }

		const float GetParticleSize() const { return mParticleSize; }

		const uint GetCount() const { return mParticles.size(); }

		const uint GetParticlesPerSecond() const { return mParticlesPerSecond; }

		void SetParticlesPerSecond( uint new_particles_per_second ) { 
			mParticlesPerSecond = new_particles_per_second; 
		}

		void SetEmitterPosition( const XMFLOAT3& new_position ) { 
			mParticlesEmitterPosition = new_position; 
		}

		const XMFLOAT3& GetEmitterPosition() const { return mParticlesEmitterPosition; }

	protected:
		void ConstructDynamicBuffers();
		void UpdateBuffers();

		std::list<Particle>		mParticles;
		std::vector<GVertexData> mVertexBufferData;
		float					mParticleSize;
		XMFLOAT3				mParticlesEmitterPosition;
		float					mParticleLifetime;
		std::string				mTextureID;
		uint					mParticlesPerSecond;
		uint					mCapacity;

		GRandom				mRandom;
		ID3D11Device		*mpDevice;
		ID3D11DeviceContext	*mpContext;
		ID3D11Buffer		*mpVertexBuffer;

	}; // class GParticles
} // namespace Game
//------------------------------------------------------------------------------------