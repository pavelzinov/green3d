#pragma once
#include "GUtility.h"
#include "GMesh.h"
#include "GVertex.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GModel
	{
	public:
		GModel(std::string id, ID3D11Device *device, ID3D11DeviceContext *context);
		~GModel(void) {}

		// Load first model found in .obj file.
		void LoadFromObj(const GObjFile &obj_file, const GMaterialsManager &materials_manager);

		// Load model with specified zero-based `object_index` from .obj file.
		// Each model consists of number of meshes with different materials each.
		// Note that object file must be registered in GTexturesManager and
		// GMaterialsManager first, so that those managers load appropriate
		// textures and materials first, before this function access them.
		// Calling this function for .obj files without materials specified
		// in connected .mtl file is perfectly acceptable.
		void LoadFromObj(const GObjFile &obj_file, uint object_index, 
			const GMaterialsManager &materials_manager);

		// Merge meshes with identical materials.
		void MergeMeshes();

		// Access all meshes forming the model.
		const std::vector<GMesh>& AccessMeshes() const { return mMeshes; }

		// Get the number of meshes which construct the model
		uint GetSize() const { return mMeshes.size(); }

		// Scale model in size.
		void Scale(const float scale_x, const float scale_y, const float scale_z);

		// Scale model in size.
		void Scale(const float scale) { Scale(scale, scale, scale); }

		// Move model by given directions and margins.
		void Move(const float dir_x, const float dir_y, const float dir_z);

		// Move model by vector.
		void Move(const XMFLOAT3 &direction) { Move(direction.x, direction.y, direction.z); }

		// Returns mesh's world matrix.
		const XMFLOAT4X4& GetWorld() const { return mWorld; }

		// Rotate mesh around given axis in local mesh's coordinate system.
		// Angle must be in degrees.
		void RotateSelf(const XMFLOAT3 &axis, float degrees);

		// Set single material to all internal meshes.
		void SetMaterial(const GMaterial &material);

		// Get model's string identifier.
		const std::string& GetID() const { return mID; }

	private:

		// Construct mWorld matrix from other three.
		// mWorld = mScaling * mRotation * mTranslation
		void UpdateWorld();

		std::string mID;
		// Contains all meshes by their material name
		std::vector<GMesh>	mMeshes;
		ID3D11Device		*mpDevice;
		ID3D11DeviceContext *mpContext;
		
		// Transformation matrices
		XMFLOAT4X4	mWorld;
		XMFLOAT4X4	mScaling;
		XMFLOAT4X4	mRotation;
		XMFLOAT4X4	mTranslation;

	}; // class GModel
	
	// Define the smart pointer type for this class.
	typedef std::shared_ptr<GModel> GModelPtr;
} // namespace
//------------------------------------------------------------------------------------