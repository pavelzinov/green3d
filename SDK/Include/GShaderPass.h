#pragma once
#include "GUtility.h"
#include "GVertex.h"
//------------------------------------------------------------------------------------
namespace Game
{
	/*
	* GShader class.
	* Responsible for loading .hlsl shader files, setting them into pipeline
	* and updating shader constant buffer variables from C++ code. Capable
	* of loading #include files in .hlsl. Also responsible for enabling/disabling
	* alpha channel.
	*
	* Usage example:
	* GShader shader = GShader( device, context );
	* shader.LoadShader( "Data/Shaders/light.hlsl" );
	* ...
	* // suppose that light.hlsl file has constant buffer named "PerObject"
	* // containing "float4 Position" variable
	* shader.SetConstant( "PerObject", XMFLOAT4( 54.0f, 34.0f, 12.0f, 0.0f ) );
	* shader.UpdateConstantBuffer( "PerObject" );
	* ...
	* shader.EnableAlpha();
	*/
	class GShaderPass
	{
	public:
		struct VariableInfo
		{
			std::string mType;
			std::string mName;
			uint mByteOffset;
		};

		struct ConstantBufferInfo
		{
			std::string mName;
			uint mSlot;
			std::vector<VariableInfo> mMembers;
		};

		GShaderPass( ID3D11Device* device, ID3D11DeviceContext* context );
		
		~GShaderPass( void );

		// Load vertex shader from .FX file
		HRESULT LoadVertexShader( const std::string &filename, const std::string &shader_function_name );

		// Load pixel shader from .FX file
		HRESULT LoadPixelShader( const std::string &filename, const std::string &shader_function_name );

		// Load geometry shader from .FX file
		HRESULT LoadGeometryShader( const std::string &filename, const std::string &shader_function_name );

		void EnableVertexShader() { mEnabledVertexShader = true; }

		void DisableVertexShader() { mEnabledVertexShader = false; }

		void EnableGeometryShader() { mEnabledGeometryShader = true; }

		void DisableGeometryShader() { mEnabledGeometryShader = false; }

		void EnablePixelShader() { mEnabledPixelShader = true; }

		void DisablePixelShader() { mEnabledPixelShader = false; }

		// Set all vertex and pixel shaders along with their parameters
		void SetShader();

		// Clear shader constant buffer with given name
		void ClearConstantBuffer( const std::string &constant_buffer_name );

		// Clear all shader's constant buffers
		void ClearConstantBuffers();

		// How to interpret input vertices
		D3D_PRIMITIVE_TOPOLOGY mTopology;

		// Set shader variables by their name and the name of their constant buffer.
		// Signatures must absolutely match. So for `matrix` shader constant we use 
		// XMFLOAT4X4 type, for int3 type we use XMINT3 type. Don't mess formats
		// because it will lead to undefined behavior!
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMFLOAT4X4 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMFLOAT4 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMFLOAT3 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMFLOAT2 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const float &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMINT4 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMINT3 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const XMINT2 &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const int &new_value );
		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const uint &new_value );


		// Update constant buffer with given name
		void UpdateConstantBuffer( const std::string &constant_buffer_name );

	private:
		// Compile shader from .fx file. Vertex and pixel shaders must be compiled independently
		HRESULT CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
			LPCSTR szShaderModel, ID3DBlob** ppBlobOut );

		// Recursively parse shader .fx file and its included files, to find and
		// store all constant buffers. Constant buffers must be described like this:
		// cbuffer Name : register( b# );
		// {
		//     matrix matr;
		//     float4 var1;
		//     float3 var2;
		//     float  padding0;
		//     ...
		// }
		// last closing bracket must be on separate line. Padding variables stubs 
		// must be declared for unused parts of 16-byte registers.
		void GatherAllConstantBuffersInFile( const std::string &filename );

		// Returns the number of bytes used for storing constant buffer data information
		const uint GetConstantBuffersSize() const { return mConstantBuffersData.size(); }

		void SetConstant( const std::string &constant_buffer_name, 
			const std::string &variable_name, const std::vector<char> &new_value_bytes, const uint bytes_size );

	private:
		ID3D11Device*			mpDevice;
		ID3D11DeviceContext*	mpContext;
		ID3D11PixelShader*		mpPixelShader;
		ID3D11VertexShader*		mpVertexShader;
		ID3D11InputLayout*		mpVertexLayout;
		ID3D11GeometryShader*	mpGeometryShader;

		bool	mEnabledVertexShader;
		bool	mEnabledGeometryShader;
		bool	mEnabledPixelShader;
		
		// Contains information about each constant buffer in shader and its members.
		// This information is constant buffer name, variables types and names.
		std::vector<ConstantBufferInfo>	mConstantBuffersInfo;
		
		// Stores raw input data for shaders.
		std::vector<std::vector<char>>	mConstantBuffersData;

		// Stores constant buffers references for use in shader.
		std::vector<ID3D11Buffer*>		mConstantBuffersResources;
	}; // class
	
	// Define the smart pointer type for this class.
	typedef std::shared_ptr<GShaderPass> GShaderPassPtr;
} // namespace