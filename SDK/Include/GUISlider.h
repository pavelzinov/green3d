#pragma once
#include "GUtility.h"
#include "GShaderPass.h"
#include "GInput.h"
#include "GPlane.h"
#include "GText.h"
#include "GApplicationInformation.h"
#include "GTexturesManager.h"
#include "GShadersManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GUISlider
	{
	public:
		GUISlider(void);
		~GUISlider(void);

		void Initialize( const std::string &label, const XMFLOAT2 &top_left, 
			const uint indicator_width, const uint indicator_height, const XMFLOAT4 &text_color, 
			const XMFLOAT4 &background_color, const XMFLOAT4 &foreground_color, 
			const float min, const float max, const float step, const float current, 
			ID3D11Device *device, ID3D11DeviceContext *context, 
			const GApplicationInformation &app_info, 
			const float near_plane, const float far_plane );
		
		void Render( GShadersManager &shaders_manager, 
			const std::string &font_shader_id, const std::string &overlay_shader_id, 
			GTexturesManager &textures_manager );
		
		void OnWindowResize( const uint new_width, const uint new_height );
		
		void OnInput( const GInput &input );

		void SetCurrentValue( const float current );

		void StepIncrease();

		void StepDecrease();

		const std::string& GetLabel() const { return mLabel; }

		const float GetCurrentValue() const { return mCurrentValue; }

		const uint GetSliderHeight() const { return mUpperText->GetSymbolHeight() + 
			/*mBottomText->GetSymbolHeight() +*/ mIndicatorHeight + 2/* + 2.0f*/; }

	private:
		std::string mLabel;
		XMFLOAT2	mTopLeft;
		uint		mIndicatorWidth;
		uint		mIndicatorHeight;
		XMFLOAT4	mTextColor;
		XMFLOAT4	mBackgroundColor;
		XMFLOAT4	mForegroundColor;
		float		mMinimumValue;
		float		mMaximumValue;
		float		mStep;
		float		mCurrentValue;

		GPlanePtr	mBackgroundPlane;
		GPlanePtr	mForegroundPlane;

		GTextPtr	mUpperText;
		//GTextPtr	mBottomText;
		XMFLOAT4X4	mOrthogonalProjection;

		float		mCurrentForegroundWidthScale;
		float		mNearPlane;
		float		mFarPlane;
		uint		mScreenWidth;
		uint		mScreenHeight;
	};

	typedef std::shared_ptr<GUISlider> GUISliderPtr;
}
//------------------------------------------------------------------------------------