#pragma once
#include "GUtility.h"
#include "GVertex.h"
#include "GMaterial.h"
//------------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// Handling construction of plane mesh, it's scaling,
	// movement, rotation and drawing
	////////////////////////////////////////////////////////
	class GPlane
	{
	public:
		GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, uint width_segments,
			float height, uint height_segments);
		
		GPlane(const GPlane& plane);

		// Assign plane to current one.
		const GPlane& operator=(const GPlane &plane);
		
		~GPlane(void) {}

		uint GetVertexCount() const { return mVertices.size(); }

		// Assign material.
		void SetMaterial(const GMaterial& material);

		// Returns mesh's world matrix.
		const XMFLOAT4X4 GetWorld() const;

		// Rotate mesh around given axis in local mesh's coordinate system.
		// Angle must be in degrees.
		XMFLOAT4X4 RotateSelf(const XMFLOAT3 &axis, float degrees);

		// Scale mesh coordinates along each axis.
		XMFLOAT4X4 Scale(float dx, float dy, float dz);

		// Scale mesh coordinates along each axis by the same value.
		XMFLOAT4X4 Scale(float delta_size);

		XMFLOAT4X4 SetScale(float dx, float dy, float dz);

		XMFLOAT4X4 SetScale(float delta_size);

		// Move mesh by vector.
		XMFLOAT4X4 Move(XMFLOAT3 direction);

		// Move mesh by individual values along each axis.
		XMFLOAT4X4 Move(float dir_x, float dir_y, float dir_z);

		// Set internal vertex and index buffers into graphical pipeline.
		void draw() const;

	protected:
		
		void GenerateVertices();
		
		void GenerateNormals();
		
		void ConstructBuffers();
		
		void* GetVertexData();

		float	mWidth;
		float	mHeight;
		uint	mWidthSegments;
		uint	mHeightSegments;

		std::string				mName;			// Mesh's unique name
		std::string				mMaterialID;	// Mesh's material name
		std::vector<GVertex>	mVertices;		// Vertices array
		std::vector<uint>		mIndices;		// Indices array

		XMFLOAT4X4				mScaling;		// Scaling matrix
		XMFLOAT4X4				mTranslation;	// Translation matrix
		XMFLOAT4X4				mRotation;		// Rotation matrix

		ID3D11Device			*mpDevice;		// DirectX11 device
		ID3D11DeviceContext		*mpContext;		// DirectX11 device context
		ID3D11Buffer			*mpVB;			// Vertex buffer
		ID3D11Buffer			*mpIB;			// Index buffer
	}; // class GPlane

	typedef std::shared_ptr<GPlane> GPlanePtr;
} // namespace Game
//------------------------------------------------------------------------------------