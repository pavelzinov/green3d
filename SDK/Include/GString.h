#pragma once
#include <iostream>
#include <vector>
#include <sstream>
#include "windows.h"
typedef unsigned int uint;
//--------------------------------------------------------------------------------------
namespace Game
{
	class GString
	{
	public:
		GString(void) {}
		~GString(void) {}

		static std::string ToAscii(const std::wstring &wstr);
		static std::wstring ToUnicode(const std::string &str);
		static std::vector<std::string>& Split(const std::string &str, const char delimiter,
			std::vector<std::string> &result);	
		static std::vector<std::string> Split(const std::string &str, const char delimiter);
		static std::vector<std::string>& Split(const std::string &str, const std::string delimiters,
			std::vector<std::string> &result);
		static std::vector<std::string> Split(const std::string &str, const std::string delimiters);
		static int ToInt(const std::string& str);
		static float ToFloat(const std::string& str);
		static std::string FromInt(const int value);
		static std::string FromFloat(const float value);

	private:
		static std::stringstream mWorkerStream;
	}; // class
} // namespace
//--------------------------------------------------------------------------------------