#pragma once
#include "GUtility.h"
#include "GObjFile.h"
#include "GVertex.h"
//-------------------------------------------------------------------------------------
namespace Game
{
	class GSkyDome
	{
	public:
		// Construct spherical sky dome from first mesh in .OBJ file.
		GSkyDome( ID3D11Device *device, ID3D11DeviceContext *context,
			const std::string &obj_filename, const float scale, 
			const std::string &texture_id );

		GSkyDome(const GSkyDome& sky_dome);

		~GSkyDome(void) { ReleaseCOM( mpVertexBuffer ); }

		// Set drawing buffers into graphics pipeline.
		void Draw() const;

		// Get texture ID with which we shall draw this
		// sky dome.
		const std::string& GetTextureID() { return mTextureID; }

	private:
		// Construct drawing buffers.
		void ConstructVertexBuffer();

		std::string mTextureID;

		ID3D11Device *mpDevice;
		
		ID3D11DeviceContext *mpContext;

		ID3D11Buffer* mpVertexBuffer;

		std::vector<GVertex> mVertices;
	}; // class GSkyDome

	typedef std::shared_ptr<GSkyDome> GSkyDomePtr;
} // namespace Game
//-------------------------------------------------------------------------------------