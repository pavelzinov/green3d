#pragma once
#include "GUtility.h"
#include "GVertex.h"
#include "GImage.h"

namespace Game
{
class GTerrain
{
public:
	GTerrain(ID3D11Device *device, ID3D11DeviceContext *context, 
		float length_x, float length_z, float height, std::string height_map_filename);
	~GTerrain(void);

public:
	// Draw mesh by single draw call
	void draw();
	XMFLOAT4X4 GetWorld() { return mWorld; }

protected:
	void GenerateVertices();
	void GenerateNormals();
	void ConstructBuffers();
	// Returns pointer to dynamic array of vertex data. Remember to call delete[] on 
	// it's result once you have finished to work with it.
	void* GetVertexData();	

private:
	ID3D11Device		*mpDevice;
	ID3D11DeviceContext *mpContext;
	ID3D11Buffer		*mpVertexBuffer;
	ID3D11Buffer		*mpIndexBuffer;

private:
	std::vector<GVertex>	mVertices;
	std::vector<uint>		mIndices;
	XMFLOAT4X4				mWorld;

private:
	float		mLengthX;
	float		mLengthZ;
	float		mHeight;
	uint		mSegmentsCountX;
	uint		mSegmentsCountZ;
	std::string mHeightMapFilename;
};

}