#pragma once
#include <windows.h>
#include <sstream>
#include "GTimer.h"
#include "GUtility.h"
#include "GInput.h"
#include <gdiplus.h>
#include "GApplicationInformation.h"
//------------------------------------------------------------------------------------
namespace Game
{
	// ===============================================================================
	// GApplication class.
	// =============================================================================== 
	// Responsible for initializing win32 window and Direct3D interfaces.
	// All demo projects must inherit from it and override initApp, onResize, 
	// updateScene and drawScene. Also responsible for resizing back buffer, when
	// window's size changes.
	// ===============================================================================
	class GApplication
	{
	public:
		GApplication( HINSTANCE hInstance );
		virtual ~GApplication(void);

		HINSTANCE	GetApplicationInstance();
		HWND		GetWindowHandle();

		// Initialize application.
		virtual void initApp();
		// Start application.
		int run();
		// Only for internal use. DONT CALL IT.
		virtual LRESULT msgProc(uint msg, WPARAM wParam, LPARAM lParam);	
		virtual void SetUPS(int updates_per_second) { 
			mUpdatesPerSecond = updates_per_second;
			mSecondsBetweenUpdates = 1.0f/static_cast<float>(mUpdatesPerSecond);
		}

	protected:
		virtual void onResize();
		virtual void updateScene(float dSeconds);
		virtual void processInput(float dSeconds) {}
		virtual void drawScene();

		void TurnZBufferOn() { mpContext->OMSetDepthStencilState(mpDepthEnabledStencil, 1); }
		void TurnZBufferOff() { mpContext->OMSetDepthStencilState(mpDepthDisabledStencil, 1); }
		void BackfaceCullingOn() { mpContext->RSSetState(mpRSBackfaceCullingSolid); }
		void BackfaceCullingNone() { mpContext->RSSetState(mpRSNoCullingSolid); }
		void WireframeOn() { mpContext->RSSetState(mpRSWireframe); }
		void WireframeOff() { mpContext->RSSetState(mpRSBackfaceCullingSolid); }
		void ClearScreen();
		int GetEncoderClsid(WCHAR *format, CLSID *pClsid);
		int GetScreen(LPWSTR lpszFilename, ULONG uQuality);

		void initMainWindow();
		HRESULT initDirect3D();
		void GetVideoCardStatistics();

		HINSTANCE	mhAppInst;  // application instance handle
		HWND		mhMainWnd;  // main window handle
		bool		mAppPaused; // is the application paused?
		bool		mMinimized; // is the application minimized?
		bool		mMaximized; // is the application maximized?
		bool		mResizing;  // are the resize bars being dragged?

		// How much world updates and input updates
		// must be done between two frames.
		int			mUpdatesPerSecond;
		float		mSecondsBetweenUpdates;

		// Keep track of the application time.
		GTimer mTimer;

        // Object which receives and interprets
		// input from the keyboard and the mouse.
        GInput mInput;

		// The D3D11 device, the swap chain for page flipping,
		// the 2D texture for the depth/stencil buffer,
		// and the render target and depth/stencil views. We
		// also store a font pointer so that we can render the
		// frame statistics to the screen.
		ID3D11Device*				mpDevice;
		ID3D11DeviceContext*		mpContext;
		IDXGISwapChain*				mpSwapChain;

		ID3D11Texture2D*			mpDepthStencilBuffer;
		ID3D11RenderTargetView*		mpWindowRenderTargetView;
		ID3D11DepthStencilView*		mpWindowDepthStencilView;

		ID3D11DepthStencilState*	mpDepthDisabledStencil;
		ID3D11DepthStencilState*	mpDepthEnabledStencil;

		ID3D11RasterizerState*		mpRSBackfaceCullingSolid;
		ID3D11RasterizerState*		mpRSNoCullingSolid;
		ID3D11RasterizerState*		mpRSWireframe;

		// The following variables are initialized in the GApplication constructor
		// to default values. However, you can override the values in the
		// derived class to pick different defaults.

		// Device type
		D3D_DRIVER_TYPE		mDriverType;
		D3D_FEATURE_LEVEL	mFeatureLevel;
		//std::string			mVideoCard;			// video card name
		//int					mVideoMemory;		// video card memory

		// Window title/caption. GApplication defaults to "D3D11 Application".
		//std::wstring mMainWndCaption;

		// Color to clear the background. GApplication defaults to blue.
		//XMFLOAT4 mClearColor;

		// Initial size of the window's client area. GApplication defaults to
		// 800x600.  Note, however, that these values change at run time
		// to reflect the current client area size as the window is resized.
		//int mClientWidth;
		//int mClientHeight;
        
        // AA
        uint mSampleCount;
        uint mSampleQuality;

		GApplicationInformation mApplicationDesc;
	}; // class
} // namespace
//------------------------------------------------------------------------------------