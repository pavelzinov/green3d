#pragma once
#include "GUtility.h"
#include "GMtlFile.h"
#include "GMaterial.h"
#include "GMath.h"
#include "GFileAscii.h"
//-------------------------------------------------------------------------------------
namespace Game
{
	class GObjFile
	{
	public:
		// Structures needed for
		// analyzing .OBJ file text data.
		struct VertexPosition
		{
			float x, y, z;
		};

		struct VertexNormal
		{
			float nx, ny, nz;
		};

		struct TextureCoordinate
		{
			float u, v;
		};

		struct FaceVertex
		{
			VertexPosition		mVertex;
			TextureCoordinate	mTexCoords;
			VertexNormal		mNormal;
		};

		struct Face
		{
			FaceVertex mVertices[3];
		};

		struct Mesh
		{
			GMaterial mMaterial;
			std::vector<Face> mFaces;
		};

		struct Group
		{
			std::string mName;
			std::vector<Mesh> mMeshes;
		};
		
		// Default empty constructor.
		GObjFile(void) {}
		
		// Default empty dctor.
		~GObjFile(void) {}
		
		// Load and parse .OBJ file data.
		bool Load(std::string filename);
		
		bool Save(std::string filename) const;
		
		// Get the number of meshes within .OBJ file.
		int GetMeshesCount() const;
		
		// Get the group index and mesh index if we have global mesh index.
		void GetIndexes(const uint mesh_global_index, int &group_index, int &mesh_index) const;
		
		// Get the global index of the first mesh in the group.
		int GetFirstMeshGlobalIndex(const uint group_index) const;
		
		// Invert triangles' winding order to counter-clockwise.
		void InvertTriangles();

		// Get all objects within single .OBJ file.
		// Note that this file must be loaded in first by using Load() method
		// of this class.
		const std::vector<Group>& GetGroups() const { return mGroups; }

		const std::vector<std::string>& GetMaterialLibraries() const { return mMaterialLibraries; }

	private:
		std::vector<VertexPosition>		mVertices;
		std::vector<VertexNormal>		mNormals;
		std::vector<TextureCoordinate>	mTexCoords;
		std::vector<GMaterial>			mMaterials;
		std::vector<std::string>		mMaterialLibraries;
		
		// This is a root node, which contains all of the information
		// about objects in .OBJ file. User can access this to get all the
		// data about 
		std::vector<Group> mGroups;
	}; // class
} // namespace
//-------------------------------------------------------------------------------------