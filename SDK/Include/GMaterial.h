#pragma once
#include "GUtility.h"
#include "GTexture.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GMaterial
	{
	public:
		GMaterial(void) { Clear(); }
		bool operator==(const GMaterial& material);
		bool UsesTextureMaps() const;
		~GMaterial(void) {}
		void SetKa(const XMFLOAT3 &value) { Ka.r = value.x; Ka.g = value.y; Ka.b = value.z; }
		void SetKa(const XMFLOAT4 &value) { Ka.r = value.x; Ka.g = value.y; Ka.b = value.z; }
		XMFLOAT4 GetAmbientColor() const { return XMFLOAT4(Ka.r, Ka.g, Ka.b, d); }
		void SetKd(const XMFLOAT3 &value) { Kd.r = value.x; Kd.g = value.y; Kd.b = value.z; }
		void SetKd(const XMFLOAT4 &value) { Kd.r = value.x; Kd.g = value.y; Kd.b = value.z; }
		XMFLOAT4 GetDiffuseColor() const { return XMFLOAT4(Kd.r, Kd.g, Kd.b, d); }
		void SetKs(const XMFLOAT3 &value) { Ks.r = value.x; Ks.g = value.y; Ks.b = value.z; }
		void SetKs(const XMFLOAT4 &value) { Ks.r = value.x; Ks.g = value.y; Ks.b = value.z; }
		XMFLOAT4 GetSpecularColor() const { return XMFLOAT4(Ks.r, Ks.g, Ks.b, 1.0f); }
		void Clear();

		std::string mName;
		struct {
			float r, g, b;
		} Ka;
		struct {
			float r, g, b;
		} Kd;
		struct {
			float r, g, b;
		} Ks;
		float d;
		int illum;
		std::string map_Ka;
		std::string map_Kd;
		std::string map_Ks;
		std::string map_Ns;
		std::string map_d;
		std::string map_bump;
		std::string disp;
		std::string decal;
	}; // class
} // namespace
//------------------------------------------------------------------------------------