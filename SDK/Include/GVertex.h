#pragma once
#include "GUtility.h"
#include <vector>
//------------------------------------------------------------------------------------
namespace Game
{
	struct GVertexData
	{
		XMFLOAT3 mPosition;
		XMFLOAT2 mTextureCoordinates;
		XMFLOAT3 mNormal;
		XMFLOAT3 mTangent;
		XMFLOAT3 mBinormal;
		XMFLOAT4 mDiffuseColor;
		XMFLOAT4 mAmbientColor;
	};

	class GVertex
	{
	public:
		// Construct vertex and fill every 
		GVertex(void) { Clear(); }
		
		virtual ~GVertex(void) {}
		
		void Clear() {
			mPosition = XMFLOAT3(0.0f, 0.0f, 0.0f);
			mTextureCoordinates = XMFLOAT2(0.0f, 0.0f);;
			mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);;
			mTangent = XMFLOAT3(0.0f, 0.0f, 0.0f);;
			mBinormal = XMFLOAT3(0.0f, 0.0f, 0.0f);;
			mDiffuseColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
			mAmbientColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		}

		GVertexData GetData() const {
			GVertexData result;
			
			result.mPosition = mPosition;
			result.mTextureCoordinates = mTextureCoordinates;
			result.mNormal = mNormal;
			result.mTangent = mTangent;
			result.mBinormal = mBinormal;
			result.mDiffuseColor = mDiffuseColor;
			result.mAmbientColor = mAmbientColor;

			return result;
		}

		// Get vertex data component size in bytes
		static uint GetSize() { return sizeof(GVertexData); }

	public:
		XMFLOAT3 mPosition;
		XMFLOAT2 mTextureCoordinates;
		XMFLOAT3 mNormal;
		XMFLOAT3 mTangent;
		XMFLOAT3 mBinormal;
		XMFLOAT4 mDiffuseColor;
		XMFLOAT4 mAmbientColor;
	};
}
//------------------------------------------------------------------------------------