#pragma once
#include <windows.h>
// DirectX includes
#include <d3d11.h>
#include <directxmath.h>
#include <d3dcompiler.h>
// STL includes
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <unordered_map>
// STL streams
#include <sstream>
#include <fstream>
#include <iostream>
#include <istream>
// Memory
#include <memory>
// Include Game
#include "GLog.h"
#include "GMath.h"
#include "GColor.h"
#include "GString.h"
#include "GFileAscii.h"
#include "GTimer.h"
#include "GRandom.h"
#include "GFastMap.h"

using namespace Game;
using namespace DirectX;
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;

typedef unsigned int uint;
typedef unsigned char uchar;

//// Releasing COM-object
//inline void ReleaseCOM(IUnknown *x) 
//{ 
//	if(x != nullptr)
//	{
//		x->Release();
//		x = nullptr;
//		int a = 10;
//		a += 10;
//	}
//}

// Release COM object.
#define ReleaseCOM(x)\
{\
	if(x != nullptr)\
	{\
		x->Release();\
		x = nullptr;\
	}\
}

// Safe pointer release
template <class any_type>
inline void PtrRelease(any_type *x) 
{ 
	if(x != nullptr)
	{
		delete x; 
		x = nullptr;
	} 
}

// Output DX11 debug information
#if defined(DEBUG) | defined(_DEBUG)
inline void HR(HRESULT hresult)
{
	LPTSTR errorText = NULL;
	FormatMessage(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		|FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		|FORMAT_MESSAGE_IGNORE_INSERTS,  
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		hresult,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&errorText,  // output 
		0, // minimum size for output buffer
		NULL);   // arguments - see note 

	if ( NULL != errorText )
	{
		OutputDebugString(errorText);
		// release memory allocated by FormatMessage()
		LocalFree(errorText);
		errorText = NULL;
	}
}
#else
inline void HR(HRESULT hr)
{
	return;
}
#endif

// Return HRESULT statement
#define HRR(hr) { if (FAILED(hr)) return hr; }

// Output Debug message
#if defined(DEBUG) | defined(_DEBUG)
inline void Log(std::string &str)
{
	OutputDebugStringA(str.c_str());
}
#else
inline void Log(std::string &str)
{
	return;
}
#endif