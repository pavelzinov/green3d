#pragma once
#include "GUtility.h"
#include "GFileAscii.h"
#include "GMaterial.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GMtlFile
	{
	public:
		GMtlFile(void) {}
		~GMtlFile(void) {}
		void Load(std::string filename);
		const std::vector<GMaterial>& GetMaterials() const { return mMaterials; }
	private:
		std::vector<GMaterial> mMaterials;
	};
}
//------------------------------------------------------------------------------------