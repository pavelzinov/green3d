/* ������ 2013(�)
* ��������� �������� ������� ������ DirectX 11 �� ����� BasicFiles
* ��������� ������������ ������ ����������� ��� �������, � ����� ��� ��� �������� ������.
* ����� ��������� ��������� ��������� ����� � ����� BasicFiles. ��� ����� ����������,
* ����� �������������� ������ ���� ������������ ������� �����, �������������� �� GApplication � 
* ����� �������� ����� ������ �� ��������� � ��������� �������. ����� �������� �������� ������� � BasicFiles, ����� �����
* � ����������� ������ ������� ����� OriginalFiles, ���� ����������� ������������ ������ �� �������� �����
* ����������� ����������� � ����� BasicFiles (��� ����� ����� ������� � ���� �����������, ��� ��� ��������������
* ������ �� ���������� � ����������� ������ ����� BasicFiles. ����� ����� �������, ��� ��������� �������� ������ �����.
* � ������� ��� ���� �� ��������.
*/
#include <iostream>
#include "DXAppManager.h"
#include <vector>

int main()
{
	cout << "DX11ProjectCreator is responsible for empty DX11 project creation. This empty project"
		<< " has basic rendering class inherited from Game::GApplication window class. It renders"
		<< "few high-polygonal moving and rotating cubes." << endl;

	DXAppManager manager;
	while (true)
	{
		cout << "0 - create project from BasicFiles folder\n";
		cout << "1 - copy project to BasicFiles\n";
		cout << "2 - exit\n";
		cout << "Choose action: ";
		int choise = -1;
		cin >> choise;

		switch (choise)
		{
		case 0:
			{
				string project_name = "";
				cout << "Input desired project name: ";
				cin >> project_name;

				string main_class = "";
				cout << "Input desired main class name: ";
				cin >> main_class;

				cout << "Processing..." << endl;

				manager.CreateNewProject(project_name, main_class);

				cout << "\nDone!" << endl;
			}
			break;
		case 1:
			{
				cout << "Processing..." << endl;

				manager.BasicFilesFromProject();

				cout << "\nDone!" << endl;
			}
			break;
		case 2:
			exit(0);
			break;
		default:
			break;
		}
	}	

    system("pause");
    return 0;
}