#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 DiffuseColor : COLOR0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT) 0;
    output.Pos.xyz = vs_input.Pos;
    output.Pos.w = 1.0f;
	output.Pos.z = saturate( vs_input.Pos.z );
    output.DiffuseColor = vs_input.DiffuseColor;
    
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
    return ps_input.DiffuseColor;
}
