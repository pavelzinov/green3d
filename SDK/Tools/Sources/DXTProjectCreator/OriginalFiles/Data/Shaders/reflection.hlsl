#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerFrame : register(b0)
{
	matrix	View;
	matrix	Projection;
	matrix	ReflectedView;
}

cbuffer PerObject : register(b1)
{
	matrix	World;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 PosH : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float4 ReflectionPosition : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
	// For correct matrix multiplication.
	vs_input.Pos.w = 1.0f;

    VS_OUT output = (VS_OUT)0;
	output.PosH = mul( vs_input.Pos, World );
	output.PosH = mul( output.PosH, View );
	output.PosH = mul( output.PosH, Projection );
	output.TexC = vs_input.TexC;

	// Create the reflection projection world matrix.
	matrix reflectProjectWorld = mul( ReflectedView, Projection );
	reflectProjectWorld = mul( World, reflectProjectWorld );

	output.ReflectionPosition = mul( vs_input.Pos, reflectProjectWorld );
	
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// Sample diffuse texture pixel.
	float4 model_color = DiffuseMap.Sample( LinearSampler, ps_input.TexC );
	
	// Calculate the projected reflection texture coordinates.
	float2 reflection_texture_coordinates;
	reflection_texture_coordinates.x = ps_input.ReflectionPosition.x / 
		ps_input.ReflectionPosition.w * 0.5f + 0.5f;
	reflection_texture_coordinates.y = -ps_input.ReflectionPosition.y / 
		ps_input.ReflectionPosition.w * 0.5f + 0.5f;
	
	// Sample reflection texture.
	float4 reflection_color = ReflectionMap.Sample( LinearSampler, reflection_texture_coordinates );

	// Blend two colors together.
	model_color = lerp( model_color, reflection_color, 0.15f );

	return model_color;
}