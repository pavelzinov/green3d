#include "common_header.hlsl"
#include "light_helper.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register(b0)
{
	matrix	World;
	matrix	View;
	matrix	Projection;
	// Light description
	float4	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float4	AttenuationParameters_SpotPower;	// x - a0, y - a1, z - a2, w - spot_power
	float4	Range;								// x - range
	int4	LightType;							// x - type: 0 - parallel, 1 - point, 2 - spotlight
	float4	CameraPosition;
	// This is for shadow generation
	matrix	LightView;
	matrix	LightProjection;
	// This flag determines which texture map is defined
	int4	TexturesUsageFlags;	// x - 0/1 use diffuse map, y - normal map, z - shadow map
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);	
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.PosW = mul(float4(vs_input.Pos.xyz, 1.0f), World);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
	output.DiffuseColor = vs_input.DiffuseColor;
	output.AmbientColor = vs_input.AmbientColor;
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// vertex position in light's coordinate system
	float4 PosL = mul(ps_input.PosW, LightView);
	PosL = mul(PosL, LightProjection);

	float bias = 0.01f;

	float2 projected_coordinates;
	projected_coordinates.x = PosL.x / PosL.w / 2.0f + 0.5f;
	projected_coordinates.y = -PosL.y / PosL.w / 2.0f + 0.5f;

	float4 model_color = ps_input.DiffuseColor;
	if (TexturesUsageFlags.x == USE_DIFFUSE_MAP)
	{
		model_color = DiffuseMap.Sample(LinearSampler, ps_input.TexC);		
	}

	// Check if projected vertex coordinates are in range of [0, 1] 
	// so they are projected to the screen texture, not outside it.
	// Else, return ambient color
	if (saturate(projected_coordinates.x) != projected_coordinates.x ||
		saturate(projected_coordinates.y) != projected_coordinates.y)
	{
		model_color = model_color * saturate(dot(ps_input.Normal, -Direction.xyz));
		model_color = model_color * AmbientColor;
		model_color.a = 1.0f;
		return model_color; // was model_color * AmbientColor
	}

	float depthL = PosL.z / PosL.w - bias;
	float pixel_current_depth = depthL - 1.0f;
	if (TexturesUsageFlags.z == USE_SHADOW_MAP)
	{
		pixel_current_depth = ShadowMap.Sample(LinearSampler, projected_coordinates);
	}

	// Check if pixel's current depth from light's point of view
	// is greater than depth of current vertex in light's point of view.
	// If so, the vertex should be shadowed, thus returning ambient light
	// else light calculations involved
	if (depthL > pixel_current_depth)
	{
		model_color = model_color * saturate(dot(ps_input.Normal, -Direction.xyz));
		model_color = model_color * AmbientColor;
		model_color.a = 1.0f;
		return model_color;
	}

	SurfaceInfo surface_info = {ps_input.PosW.xyz, ps_input.Normal, model_color, model_color};

	Light light;
	light.pos = Position.xyz;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters_SpotPower.xyz;
	light.spotPower = AttenuationParameters_SpotPower.w;
	light.range = Range.x;

	if (LightType.x == 0)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType.x == 1)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
			
	}
	else if (LightType.x == 2)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}	

    return model_color;
}