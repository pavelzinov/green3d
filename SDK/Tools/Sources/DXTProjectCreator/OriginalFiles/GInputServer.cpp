#include "GInputServer.h"
using namespace Game;
//------------------------------------------------------------------------------------
GInputServer::GInputServer(void)
{
}
//------------------------------------------------------------------------------------
GInputServer::~GInputServer(void)
{
	GLog::Debug( "Destroying server...\n" );
	// ��������� ��� ������
	for (int i=0; i<mThreads.size(); i++)
	{
		TerminateThread(mThreads[i], 0);
	}
	// ������� ��� ������
	for (int i=0; i<mSockets.size(); i++)
	{
		closesocket(mSockets[i]);
	}
	GLog::Debug( "Server destroyed.\n" );
}
//------------------------------------------------------------------------------------
bool GInputServer::Initialization(int port)
{
	GLog::Debug( "Server initialization started...\n" );
	char buff[1024];
	if (WSAStartup(0x0202, (WSADATA *)&buff[0]))
	{
		Error("WSAStartup failed");
		return false;
	}

	mTest = 10;

	sockaddr_in sain;
	sain.sin_family = AF_INET;
	sain.sin_port = htons(port);
	sain.sin_addr.s_addr = INADDR_ANY;
	mServerSocket = socket(AF_INET, SOCK_STREAM, 0);

	if(mServerSocket == INVALID_SOCKET)
	{
		Error("Create socket failed with error");
		return false;
	}

	if(bind(mServerSocket,(sockaddr *)&sain, sizeof(sain)) == SOCKET_ERROR)
	{
		Error("Bind socket failed");
		closesocket(mServerSocket);
		WSACleanup();
		return false;
	}

	if(listen(mServerSocket, 6) == SOCKET_ERROR)
	{
		Error("Listen socket failed");
		closesocket(mServerSocket);
		WSACleanup();
		return false;
	}
	//mLog.printWithTime("Server initialized");
	return true;
}
//------------------------------------------------------------------------------------
void GInputServer::Run()
{
	GLog::Debug( "Server started\n" );
	HANDLE hThread;
	SOCKET client_sock;
	sockaddr_in client_addr;
	int client_addr_size = sizeof(client_addr);

	while(1)
	{
		client_sock = accept(mServerSocket, (sockaddr *)&client_addr, &client_addr_size);
		if(client_sock != INVALID_SOCKET)
		{
			//mLog.printWithTime("Client connected");
			mSockets.push_back(client_sock);
			// ������� ����� ����� ��� ����������������� ������� 
			hThread = CreateThread( 
				NULL,				// ��� security ��������� 
				0,					// ������ ����� �� ��������� 
				(LPTHREAD_START_ROUTINE) WorkingWithClient,		// ������� ��� ����������
				this,//(LPVOID) client_sock,     // ��������� ������� (�����)
				0,					// �� ����������� ������ ����������
				0);					// ���� ��������� ID ������
			if (hThread == NULL)		// ���� ����� �� ��� ������
			{
				Error("CreateThread failed");
				mSockets.erase(mSockets.end() - 1);
			}
			else 
			{
				// ������� ����� � ��� id � �������
				mThreads.push_back(hThread);
			}
		}
		else
		{
			//mLog.printWithTime("Socket has been closed");
			closesocket(client_sock);
		}
	}
}
//------------------------------------------------------------------------------------
DWORD WINAPI GInputServer::WorkingWithClient(LPVOID object)
{
	return ((GInputServer*)object)->WorkingWithClient();
}
//------------------------------------------------------------------------------------
DWORD GInputServer::WorkingWithClient()
{
	// ��������� ������ �������
	SOCKET client = mSockets[mSockets.size() - 1];

	long int req;//, id, count;
	//int i;

	// ������ �������
	do
	{
		req = recvInt(client);
		if (req > 0)
		{
			switch(req)
			{

			case TEST:
				//mLog.printWithTime("TEST called");
				sendInt(client, 258);
				break;

			case IMAGE:
				//mLog.printWithTime("IMAGE called");
				sendFile(client, "assets/sphere.jpg");
				break;

			}
		}
	}
	while (req != CLOSE_SOCKET);

	// ������� ����� � ����� �� ������
	for (int i=0; i<mSockets.size(); i++)
	{
		if (mSockets[i] == client)
		{
			mSockets.erase(mSockets.begin() + i);
			mThreads.erase(mThreads.begin() + i);
			i = mSockets.size();
		}
	}

	// ��������� �����
	closesocket(client);

	//mLog.printWithTime("Working with client is over");
	return S_OK;
}
//------------------------------------------------------------------------------------
std::string GInputServer::GetError()
{
	return mError;
}
//------------------------------------------------------------------------------------
void GInputServer::Error(const std::string &msg)
{
	GLog::Debug( msg + "\n" );
	mError = msg;
}
//------------------------------------------------------------------------------------
int GInputServer::recvInt(SOCKET &client)
{
	int result = -1;
	recv(client, (char*)&result, sizeof(int), 0);
	result = ntohl(result);
	return result;
}
//------------------------------------------------------------------------------------
void GInputServer::sendInt(SOCKET &client, int value)
{
	value = htonl(value);
	send(client, (char*)&value, 4, 0);
}
//------------------------------------------------------------------------------------
void GInputServer::sendFile(SOCKET &client, const std::string &filename)
{
	std::ifstream file (filename, std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		std::ifstream::pos_type size = file.tellg();
		char * memblock = new char [size];
		file.seekg (0, std::ios::beg);
		file.read (memblock, size);
		file.close();
		sendInt(client, size);
		send(client, memblock, size, 0);
		delete[] memblock;
	}
}
//------------------------------------------------------------------------------------