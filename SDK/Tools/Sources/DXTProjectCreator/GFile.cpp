#include "GFile.h"

GFile::GFile (std::string _fullName)
{
	fullName = _fullName;
	shortWithExtension = fullName.substr(fullName.find_last_of('\\') + 1, 
		fullName.size() - fullName.find_last_of('\\') + 1);
	homeDirectory = fullName.substr(0, fullName.find_last_of('\\') + 1);
	shortName = shortWithExtension.substr(0, shortWithExtension.find_last_of('.'));
	extension = shortWithExtension.substr(shortWithExtension.find_last_of('.') + 1, 
		shortWithExtension.size() - shortWithExtension.find_last_of('.'));
	extensionWithDot = std::string(".") + extension;
}

GFile::GFile (const GFile& file_info)
{
	shortName = file_info.shortName;
	fullName = file_info.fullName;
	extension = file_info.extension;
	extensionWithDot = file_info.extensionWithDot;
	shortWithExtension = file_info.shortWithExtension;
	homeDirectory = file_info.homeDirectory;
}

GFile& GFile::operator=(const GFile& file_info)
{
	shortName = file_info.shortName;
	fullName = file_info.fullName;
	extension = file_info.extension;
	extensionWithDot = file_info.extensionWithDot;
	shortWithExtension = file_info.shortWithExtension;
	homeDirectory = file_info.homeDirectory;
	return *this;
}

void GFile::FromFullName(std::string _fullName)
{
	fullName = _fullName;
	shortWithExtension = fullName.substr(fullName.find_last_of('\\') + 1, 
		fullName.size() - fullName.find_last_of('\\') + 1);
	homeDirectory = fullName.substr(0, fullName.find_last_of('\\') + 1);
	shortName = shortWithExtension.substr(0, shortWithExtension.find_last_of('.'));
	extension = shortWithExtension.substr(shortWithExtension.find_last_of('.') + 1, 
		shortWithExtension.size() - shortWithExtension.find_last_of('.'));
	extensionWithDot = std::string(".") + extension;
}

string GFile::CurrDir()
{
	char buffer[255] = {0};
	GetCurrentDirectoryA(255, buffer); // ������� ���������� ���� ��� ���������� �����

	return string(buffer) + "\\";
}

vector<GFile> GFile::GetFilesInfo(std::string directory)
{
	// Correct input
	if (directory.back() != '\\')
		directory += "\\";
	if (directory.find(":") == std::string::npos)
		directory = CurrDir() + directory;

	// ������ ��� ����� � ����� directory
	vector<GFile> file_infos;
	WIN32_FIND_DATAA file_data;
	HANDLE hFileFind = FindFirstFileA((directory + "*.*").c_str(), &file_data);
	if (hFileFind == INVALID_HANDLE_VALUE)
	{
		cout << directory + " directory is empty. Closing program.\n";
		system("pause");
		exit(0);
	}
	do
	{
		// ��������� ����� . � .. ������� ������������ � ������ ����������
		if (string(file_data.cFileName) != "." && string(file_data.cFileName) != "..")
		{
			file_infos.push_back(GFile(directory + file_data.cFileName));
		}
	}
	while (FindNextFileA(hFileFind, &file_data) != 0);
	FindClose(hFileFind);

	return file_infos;
}