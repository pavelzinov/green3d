#pragma once
#include <iostream>
#include <windows.h>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

class GFile
{
public:
	GFile () {}
	~GFile () {}
	GFile (std::string _fullName);
	GFile (const GFile& file_info);
	GFile& operator=(const GFile& file_info);
	void FromFullName(std::string _fullName);

	std::string shortName;           // example
	std::string fullName;            // C:/directory/example.txt
	std::string extension;           // txt 
	std::string extensionWithDot;    // .txt
	std::string shortWithExtension;  // example.txt
	std::string homeDirectory;       // C:/directory/

	static string CurrDir();
	static vector<GFile> GetFilesInfo(std::string directory);
};