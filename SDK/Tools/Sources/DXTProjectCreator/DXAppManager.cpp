#include "DXAppManager.h"

DXAppManager::DXAppManager()
{	
	mProjectName = "";
	mMainClass = "";
	mStubProjectName = "her8813HNA74NMrI64732";
	mStubMainClass = "aW9q4X1g5yoM79VDbU04";
}

DXAppManager::~DXAppManager(void)
{
}

void DXAppManager::CreateNewProject(string project_name, string main_class)
{
	mProjectName = project_name;
	mMainClass = main_class;

	// Create all new directories
	std::vector<std::string> old_folders;	
	GetDirectoriesListing(old_folders, CurrDir() + "BasicFiles");
	old_folders.insert(old_folders.begin(), CurrDir() + "BasicFiles");
	std::vector<std::string> new_folders(old_folders);
	for (auto it = new_folders.begin(); it != new_folders.end(); it++)
	{
		int position = (*it).find("BasicFiles");
		int length = std::string("BasicFiles").size();
		(*it).replace(position, length, mProjectName);
		CreateFolder((*it));
	}
	
	// Copy all files in new directories
	for (unsigned int i = 0; i < old_folders.size(); ++i)
	{
		CopyFiles(old_folders[i], new_folders[i]);
	}

	// Rename new files and their content
	for (unsigned int i = 0; i < new_folders.size(); ++i)
	{
		RenameFiles(new_folders[i], mStubProjectName, mProjectName);
		RenameFiles(new_folders[i], mStubProjectName + ".vcxproj", mProjectName + ".vcxproj");
		RenameFiles(new_folders[i], mStubMainClass, mMainClass);
		RenameContent(new_folders[i], mStubProjectName, mProjectName);
		RenameContent(new_folders[i], mStubMainClass, mMainClass);
	}
}

void DXAppManager::BasicFilesFromProject()
{
	mProjectName = GetProjectName("OriginalFiles");
	cout << "DEBUG: mProjectName = " << mProjectName << endl;
	mMainClass = GetMainClass("OriginalFiles");
	cout << "DEBUG: mMainClass = " << mMainClass << endl;

	// Create all new directories
	std::vector<std::string> old_folders;	
	GetDirectoriesListing(old_folders, CurrDir() + "OriginalFiles");
	old_folders.insert(old_folders.begin(), CurrDir() + "OriginalFiles");
	std::vector<std::string> new_folders(old_folders);
	for (auto it = new_folders.begin(); it != new_folders.end(); it++)
	{
		int position = (*it).find("OriginalFiles");
		int length = std::string("OriginalFiles").size();
		(*it).replace(position, length, "BasicFiles");
		CreateFolder((*it));
	}

	// Copy all files in new directories
	for (unsigned int i = 0; i < old_folders.size(); ++i)
	{
		CopyFiles(old_folders[i], new_folders[i]);
	}

	// Rename new files and their content
	for (unsigned int i = 0; i < new_folders.size(); ++i)
	{
		RenameFiles(new_folders[i], mProjectName, mStubProjectName);
		RenameFiles(new_folders[i], mProjectName + ".vcxproj", mStubProjectName + ".vcxproj");
		RenameFiles(new_folders[i], mMainClass, mStubMainClass);
		RenameContent(new_folders[i], mProjectName, mStubProjectName);
		RenameContent(new_folders[i], mMainClass, mStubMainClass);
	}
}

void DXAppManager::CreateFolder(string folder_name)
{
	if (CreateDirectory(folder_name.c_str(), nullptr) == FALSE)
	{
		auto error = GetLastError();
		cout << "Error: can't create directory" << endl;
		system("pause");
		exit(0);
	}
}

void DXAppManager::CopyFiles(string from_dir, string to_dir)
{
	// Correct input values:
	if (from_dir != "") 
		from_dir += "\\";
	if (to_dir != "") 
		to_dir += "\\";

	if (from_dir.find(":") == std::string::npos)
		from_dir = CurrDir() + from_dir;
	if (from_dir.find(":") == std::string::npos)
		to_dir = CurrDir() + to_dir;

	// Find all files in folder from_dir
    WIN32_FIND_DATAA file_data;
	HANDLE hFileFind = FindFirstFileA((from_dir + "*.*").c_str(), &file_data);
    if (hFileFind == INVALID_HANDLE_VALUE)
    {
        cout << from_dir + " directory is empty. Closing program.\n";
        system("pause");
        exit(0);
    }
    do
    {
		// ��������� ����� . � .. ������� ������������ � ������ ����������
        if (string(file_data.cFileName) != "." && string(file_data.cFileName) != "..")
        {
			CopyFile((from_dir + file_data.cFileName).c_str(),
				(to_dir + file_data.cFileName).c_str(), FALSE);
        }
    }
    while (FindNextFileA(hFileFind, &file_data) != 0);
	FindClose(hFileFind);
}

void DXAppManager::RenameFiles(string from_dir, string old_name, string new_name)
{
	if (from_dir != "")
		from_dir += "\\";
	if (from_dir.find(":") == std::string::npos)
		from_dir = CurrDir() + from_dir;

	vector<GFile> file_infos = GetFilesInfo(from_dir);
	for (int i=0; i<file_infos.size(); i++)
	{
		if (file_infos[i].shortName == old_name)
		{
			CopyFile(file_infos[i].fullName.c_str(), 
				(file_infos[i].homeDirectory + new_name + file_infos[i].extensionWithDot).c_str(), FALSE);
			DeleteFile(file_infos[i].fullName.c_str());
		}
	}
}

void DXAppManager::RenameContent(string directory, string old_content, string new_content)
{
	vector<GFile> file_infos = GetFilesInfo(directory);
	for (int i=0; i<file_infos.size(); i++)
	{
		while (RenameStringInFile(file_infos[i].fullName, old_content, new_content));
	}
}

vector<GFile> DXAppManager::GetFilesInfo(string directory)
{
	// Correct input
	if (directory == "")
		directory = CurrDir();
	if (directory.back() != '\\')
		directory += "\\";
	if (directory.find(":") == std::string::npos)
		directory = CurrDir() + directory;

	// ������ ��� ����� � ����� directory
	vector<GFile> file_infos;
    WIN32_FIND_DATAA file_data;
	HANDLE hFileFind = FindFirstFileA((directory + "*.*").c_str(), &file_data);
    if (hFileFind == INVALID_HANDLE_VALUE)
    {
        cout << directory + " directory is empty. Closing program.\n";
        system("pause");
        exit(0);
    }
    do
    {
		// ��������� ����� . � .. ������� ������������ � ������ ����������
        if (string(file_data.cFileName) != "." && string(file_data.cFileName) != "..")
        {
			file_infos.push_back(GFile(directory + file_data.cFileName));
        }
    }
    while (FindNextFileA(hFileFind, &file_data) != 0);
	FindClose(hFileFind);

	return file_infos;
}

bool DXAppManager::RenameStringInFile(string filename, string replace, string replacer)
{
	std::ifstream file_to_string(filename);
	std::string file_string((std::istreambuf_iterator<char>(file_to_string)), 
		std::istreambuf_iterator<char>());
	file_to_string.close();

	int position = file_string.find(replace);
	if (position != string::npos)
	{
		file_string.insert(position, replacer);
		position = file_string.find(replace);
		file_string.erase(position, replace.size());

		std::ofstream out_file;
		out_file.open(filename);
		out_file << file_string;
		out_file.close();
	}
	else
	{
		return false;
	}
	return true;
}

vector<int> DXAppManager::FindInFile(string filename, string value)
{
	vector<int> result;

	std::ifstream file_to_string(filename);
	std::string file_string((std::istreambuf_iterator<char>(file_to_string)), 
		std::istreambuf_iterator<char>());
	file_to_string.close();

	int position = file_string.find(value);	
	while (position != string::npos)
	{
		result.push_back(position);
		position = file_string.find(value, position + 1);
	}

	return result;
}

// ���������� ������ ���� ������� ����� �� ������ � �����
string DXAppManager::CurrDir()
{
    char buffer[255] = {0};
    GetCurrentDirectoryA(255, buffer); // ������� ���������� ���� ��� ���������� �����

    return string(buffer) + "\\";
}

string DXAppManager::GetProjectName(string dir)
{
	// validate input
	if (dir == "")
		dir = CurrDir();
	if (dir.back() != '\\')
		dir += "\\";
	if (dir.find(":") == std::string::npos)
		dir = CurrDir() + dir;

	// Search for file with vcxproj extension in dir folder and its subfolders
	vector<GFile> files_info;
	vector<std::string> directories;
	GetDirectoriesListing(directories, dir);
	directories.insert(directories.begin(), dir);
	for (auto it = directories.begin(); it != directories.end(); it++)
	{
		files_info = GetFilesInfo(*it);
		for (unsigned int i = 0; i < files_info.size(); ++i)
		{
			if (files_info[i].extension == "vcxproj")
				return files_info[i].shortName;
		}
	}

	return "";
}

string DXAppManager::GetMainClass(string dir)
{
	// validate input
	if (dir == "")
		dir = CurrDir();
	if (dir.back() != '\\')
		dir += "\\";
	if (dir.find(":") == std::string::npos)
		dir = CurrDir() + dir;

	// Search for file with `#include "GApplication.h"` string inside in `dir` folder and its subfolders
	vector<GFile> files_info;
	vector<std::string> directories;
	GetDirectoriesListing(directories, dir);
	directories.insert(directories.begin(), dir);
	for (auto it = directories.begin(); it != directories.end(); it++)
	{
		files_info = GetFilesInfo(*it);
		for (unsigned int i = 0; i < files_info.size(); ++i)
		{
			if (FindInFile(files_info[i].fullName, "#include \"GApplication.h\"").size() > 0
				&& files_info[i].shortName != "GApplication")
				return files_info[i].shortName;
		}
	}

	return "";
}

// This recursively gets all the filenames that match the filter in given directory
void DXAppManager::GetFileListing(std::vector<std::string>& listing, std::string directory, 
					std::string fileFilter, bool recursively)
{
	// If we are going to recurse over all the subdirectories, first of all
	// get all the files that are in this directory that match the filter
	if (recursively)
		GetFileListing(listing, directory, fileFilter, false);

	directory += "\\";

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	// Setup the filter according to whether we are getting the directories
	// or just the files
	std::string filter = directory + (recursively ? "*" : fileFilter);

	// Find the first file in the directory.
	hFind = FindFirstFile(filter.c_str(), &FindFileData);

	if (hFind == INVALID_HANDLE_VALUE)
	{
		DWORD dwError = GetLastError();
		if (dwError!=ERROR_FILE_NOT_FOUND)
		{
			std::cout << "Invalid file handle for filter "<<filter<<". Error is " << GetLastError() << std::endl;
		}
	}
	else
	{
		// Add the first file found to the list
		if (!recursively)
		{
			listing.push_back(directory + std::string(FindFileData.cFileName));
		}

		// List all the other files in the directory.
		while (FindNextFile(hFind, &FindFileData) != 0)
		{
			if (!recursively)
			{
				listing.push_back(directory + std::string(FindFileData.cFileName));
			}
			else
			{
				// If we found a directory then recurse into it
				if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)>0 && FindFileData.cFileName[0]!='.')
				{
					GetFileListing(listing, directory + std::string(FindFileData.cFileName), fileFilter);
				}
			}
		}

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES)
		{
			std::cout << "FindNextFile error. Error is "<< dwError << std::endl;
		}
	}
}

// This recursively gets all the directories inside given directory
void DXAppManager::GetDirectoriesListing(std::vector<std::string>& listing, std::string directory, 
						   std::string fileFilter, bool recursively)
{
	// If we are going to recurse over all the subdirectories, first of all
	// get all the files that are in this directory that match the filter
	if (recursively)
		GetDirectoriesListing(listing, directory, fileFilter, false);

	directory += "\\";

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	// Setup the filter according to whether we are getting the directories
	// or just the files
	std::string filter = directory + (recursively ? "*" : fileFilter);

	// Find the first file in the directory.
	hFind = FindFirstFile(filter.c_str(), &FindFileData);

	if (hFind == INVALID_HANDLE_VALUE)
	{
		DWORD dwError = GetLastError();
		if (dwError!=ERROR_FILE_NOT_FOUND)
		{
			std::cout << "Invalid file handle for filter "<<filter<<". Error is " << GetLastError() << std::endl;
		}
	}
	else
	{
		// Add the first file found to the list
		if (!recursively && FindFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY &&
			std::string(FindFileData.cFileName) != "." && std::string(FindFileData.cFileName) != "..")
		{
			listing.push_back(directory + std::string(FindFileData.cFileName));
		}

		// List all the other files in the directory.
		while (FindNextFile(hFind, &FindFileData) != 0)
		{
			if (!recursively && FindFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY &&
				std::string(FindFileData.cFileName) != "." && std::string(FindFileData.cFileName) != "..")
			{
				listing.push_back(directory + std::string(FindFileData.cFileName));
			}
			else
			{
				// If we found a directory then recurse into it
				if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)>0 && FindFileData.cFileName[0]!='.')
				{
					GetDirectoriesListing(listing, directory + std::string(FindFileData.cFileName), fileFilter);
				}
			}
		}

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES)
		{
			std::cout << "FindNextFile error. Error is "<< dwError << std::endl;
		}
	}
}