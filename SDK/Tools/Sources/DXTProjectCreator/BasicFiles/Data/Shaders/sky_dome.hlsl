#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerFrame : register(b0)
{
	matrix	View;
	matrix	Projection;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 PosH : SV_POSITION;
	float3 TexC : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.PosH = mul(float4(vs_input.Pos.xyz, 1.0f), View);
    output.PosH = mul(output.PosH, Projection);
	// Set z = w so that z/w = 1 (i.e., skydome always on far plane).
	output.PosH = output.PosH.xyww;
	// Set texture lookup vector to current vertex position vector.
	output.TexC = vs_input.Pos.xyz;
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	return gCubeMap.Sample( LinearSampler, ps_input.TexC );
}