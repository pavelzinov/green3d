#include "common_header.hlsl"
#include "light_helper.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix World;
}

cbuffer PerObjectGroup : register(b1)
{
	// Those flags determines which texture map is defined
	int		UseDiffuseMap;
	int		UseNormalMap;
	int		UseShadowMap;
	int		UseReflectionMap;
}

cbuffer PerFrame : register(b2)
{
	matrix	View;
	matrix	Projection;
	float	gShadowMapSize;
	float	gShadowBias;
	float	gSceneScale;
	float	gLightSize;
	float	gSearchSamples;
	float	gFilterSamples;
	float2	_padding0;
}

cbuffer Light : register(b3)
{
	// Light description	
	int		LightType;	// 0 - parallel, 1 - point, 2 - spotlight
	float3	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float3	AttenuationParameters; // x - a0, y - a1, z - a2
	float	SpotPower;
	float3	CameraPosition;
	float	Range;
	// This is for shadow generation
	matrix	LightView;
	matrix	LightProjection;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);	
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.PosW = mul(float4(vs_input.Pos.xyz, 1.0f), World);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
	output.DiffuseColor = vs_input.DiffuseColor;
	output.AmbientColor = vs_input.AmbientColor;
    
    return output;
}

float OffsetLookup(float2 projTexC, float2 offset)
{
	return ShadowMap.Sample( LinearSampler, projTexC + offset ).r;
}

// -------------------------------------
// STEP 1: Search for potential blockers
// -------------------------------------
float FindBlocker(float2 uv, float4 LP, float bias,
	float searchWidth, float numSamples)
{
	// divide filter width by number of samples to use
	float stepSize = 2 * searchWidth / numSamples;

	// compute starting point uv coordinates for search
	uv = uv - float2(searchWidth, searchWidth);

	// reset sum to zero
	float blockerSum = 0;
	float receiver = LP.z;
	float blockerCount = 0;
	float foundBlocker = 0;

	// iterate through search region and add up depth values
	for (int i=0; i<numSamples; i++) {
		for (int j=0; j<numSamples; j++) {
			float shadMapDepth = ShadowMap.Sample( PointSampler, uv +
				float2(i*stepSize,j*stepSize)).x;
			
			// Found a blocker.
			if (shadMapDepth < receiver) {
				blockerSum += shadMapDepth;
				blockerCount++;
				foundBlocker = 1;
			}
		}
	}

	float result;
		
	if (foundBlocker == 0) {
		// set it to a unique number so we can check
		// later to see if there was no blocker
		result = 999;
	}
	else {
		// return average depth of the blockers
		result = blockerSum / blockerCount;
	}
		
	return result;
}

// ------------------------------------------------
// STEP 2: Estimate penumbra based on
// blocker estimate, receiver depth, and light size
// ------------------------------------------------
float EstimatePenumbra(float4 LP, float Blocker, float LightSize)
{
    // receiver depth
    float receiver = LP.z;
    // estimate penumbra using parallel planes approximation
    float penumbra = (receiver - Blocker) * LightSize / Blocker;
    return penumbra;
}

// ----------------------------------------------------
// Step 3: Percentage-closer filter implementation with
// variable filter width and number of samples.
// This assumes a square filter with the same number of
// horizontal and vertical samples.
// ----------------------------------------------------
float PCF(float2 uv, float4 LP, float bias, float filterWidth, float numSamples)
{
	// compute step size for iterating through the kernel
	float stepSize = 2 * filterWidth / numSamples;

	// compute uv coordinates for upper-left corner of the kernel
	uv = uv - float2(filterWidth,filterWidth);

	float sum = 0;  // sum of successful depth tests

	// now iterate through the kernel and filter
	for (int i=0; i<numSamples; i++) {
		for (int j=0; j<numSamples; j++) {
			// get depth at current texel of the shadow map
			float shadMapDepth = 0;
            
			shadMapDepth = ShadowMap.Sample( PointSampler, uv +
				float2(i*stepSize,j*stepSize)).x;

			// test if the depth in the shadow map is closer than
			// the eye-view point
			float shad = LP.z < shadMapDepth;

			// accumulate result
			sum += shad;
		}
	}
       
	// return average of the samples
	return sum / (numSamples*numSamples);
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// vertex position in light's coordinate system
	float4 PosL = mul(ps_input.PosW, LightView);
	PosL = mul(PosL, LightProjection);
	// Factor in shadow bias here.
	PosL.z -= gShadowBias;

	// Projected coordinates in texture space.
	float2 projected_coordinates;
	projected_coordinates.x = 0.5f * PosL.x / PosL.w + 0.5f;
	projected_coordinates.y = -0.5f * PosL.y / PosL.w + 0.5f;

	// Calculate model diffuse color.
	float4 model_color = ps_input.DiffuseColor;
	if (UseDiffuseMap == USE_DIFFUSE_MAP)
	{
		model_color = DiffuseMap.Sample(LinearSampler, ps_input.TexC);
	}

	// Calculate model's normal.
	float3 bump_normal = ps_input.Normal;
	if (UseNormalMap == USE_NORMAL_MAP)
	{
		float4 bump_map = NormalMap.Sample(LinearSampler, ps_input.TexC);
		// Expand the range of the normal value from (0, +1) to (-1, +1).
		bump_map = (bump_map * 2.0f) - 1.0f;
	
		bump_normal = bump_map.z * ps_input.Normal + 
			bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
		bump_normal = normalize(bump_normal);
	}

	// ---------------------------------------------------------
	// Step 1: Find blocker estimate
	//float searchSamples = 4;   // how many samples to use for blocker search. Initial - 6
	float zReceiver = PosL.z;
	//float bias = 0.01f;
	float SceneScale = 1.0f;
	float LightSize = 0.05f;
	float searchWidth = gSceneScale * (zReceiver - 1.0) / zReceiver;
	float blocker = FindBlocker(projected_coordinates, PosL, gShadowBias,
		gSceneScale * gLightSize / PosL.z, gSearchSamples);
   
	//return (blocker*0.3);  // uncomment to visualize blockers
   
	// ---------------------------------------------------------
	// Step 2: Estimate penumbra using parallel planes approximation
	float penumbra;  
	penumbra = EstimatePenumbra(PosL, blocker, gLightSize);

	//return penumbra*32;  // uncomment to visualize penumbrae

	// ---------------------------------------------------------
	// Step 3: Compute percentage-closer filter
	// based on penumbra estimate
	//float samples = 6;	// reduce this for higher performance. Initial - 8

	// Now do a penumbra-based percentage-closer filter
	float shadowFactor;

	shadowFactor = PCF(projected_coordinates, PosL, gShadowBias, penumbra, gFilterSamples);
   
	// If no blocker was found, just return 1.0
	// since the point isn't occluded   
	if (blocker > 998)
   		shadowFactor = 1.0;

	float4 reflection_color = model_color;
	if (UseReflectionMap == USE_REFLECTION_MAP)
	{
		reflection_color = reflection_color * ReflectionMap.Sample(LinearSampler, ps_input.TexC);
	}

	SurfaceInfo surface_info = {ps_input.PosW.xyz, bump_normal, model_color, reflection_color};

	Light light;
	light.pos = Position;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters;
	light.spotPower = SpotPower;
	light.range = Range;

	if (LightType == LIGHT_TYPE_POINT)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz) * shadowFactor, model_color.a);
	}
	else if (LightType == LIGHT_TYPE_PARALLEL)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz) * shadowFactor, model_color.a);
	}
	else if (LightType == LIGHT_TYPE_SPOT)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz) * shadowFactor, model_color.a);
	}

    return model_color;
}