#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix	World;
	int		UseDiffuseMap;
	int3	_padding0;
}

cbuffer PerFrame : register(b1)
{
	matrix View;
	matrix Projection;
}

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float4 DiffuseColor : COLOR0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	// make sure z is in [0, 1] range, otherwise the part of UI figure will be cut
	// do this only for orthogonal projection!
	output.Pos.z = saturate(output.Pos.z);
    output.TexC = vs_input.TexC;
	output.DiffuseColor = vs_input.DiffuseColor;

    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	if (UseDiffuseMap == USE_DIFFUSE_MAP)
		return DiffuseMap.Sample(LinearSampler, ps_input.TexC);

	return ps_input.DiffuseColor;
}
