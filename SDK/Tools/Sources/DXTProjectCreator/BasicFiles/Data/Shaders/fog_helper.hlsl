//--------------------------------------------------------------------------------------
// Fog helpers
//-------------------------------------------------------------------------------------
#define LINEAR_FOG 0
#define EXPONENTIAL_FOG 1
#define EXPONENTIAL_THICK_FOG 2

float4 Fog(int FogType, float4 FogColor, float4 Color, float FogStart, 
	float FogEnd, float ViewpointDistance, float FogDensity)
{
	float fog_factor;
	if (FogType == LINEAR_FOG)
	{
		fog_factor = (FogEnd - ViewpointDistance) / (FogEnd - FogStart);
	}
	else if (FogType == EXPONENTIAL_FOG)
	{
		fog_factor = pow(1.0f/2.71828f, ViewpointDistance * FogDensity);
	}
	else if (FogType == EXPONENTIAL_THICK_FOG)
	{
		fog_factor = pow(1.0f/2.71828f, 
			(ViewpointDistance * FogDensity * ViewpointDistance * FogDensity));
	}

	return fog_factor * TextureColor + (1.0 - FogFactor) * FogColor
}