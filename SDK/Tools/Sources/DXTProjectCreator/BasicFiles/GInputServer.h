#pragma once
#include "GUtility.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GInputServer
	{
	public:
		GInputServer(void);
		~GInputServer(void);

	public:
		// Initialize input socket server on the given port.
		bool Initialization(int port);
		
		// Start server.
		void Run();

		// Return error message.
		std::string GetError();

	private:
		//Logger mLog;						// ������� ��������� �� �����
		// Stores error message.
		std::string mError;					// ��������� �� ������

	private:
		void Error(const std::string &msg);		// ������� ������

	private:
		int mTest;
		SOCKET mServerSocket;
		std::vector<SOCKET> mSockets;
		std::vector<HANDLE> mThreads;
		int mPort;							// ����

	private:
		static DWORD WINAPI WorkingWithClient(LPVOID object);	// ������� ��������� ������� �������
		DWORD WorkingWithClient();

	private:
		static const int STOP = 548;
		static const int CLOSE_SOCKET = 8735;
		static const int TEST = 657;
		static const int IMAGE = 435;

	private:
		int recvInt(SOCKET &client);
		void sendInt(SOCKET &client, int value);
		void sendFile(SOCKET &client, const std::string &filename);
	}; // class GInputServer
} // namespace Game
//------------------------------------------------------------------------------------