#include "aW9q4X1g5yoM79VDbU04.h"
#include "DDSTextureLoader.h"
//------------------------------------------------------------------------------------
aW9q4X1g5yoM79VDbU04::aW9q4X1g5yoM79VDbU04( HINSTANCE hInstance ) : GApplication( hInstance )
{
    // Init background color.
    mApplicationDesc.mScreenClearColor = GColor::LIGHT_BLUE;

    mDistance = 200.0f;

	//mPlayer.Initialize( 60.0f, mApplicationDesc.mClientWidth,  mApplicationDesc.mClientHeight, 0.01f, 1000.0f );

	mCamera.Position( XMFLOAT3( 0.0f, mDistance, -mDistance ) );
	mCamera.Target( XMFLOAT3( 0.0f, 10.0f, 0.0f ) );
	mCamera.InitProjMatrix( GMath::PIDIV4, mApplicationDesc.mClientWidth,
		mApplicationDesc.mClientHeight, 1.0f, 1000.0f );
	mCamera.InitOrthoMatrix( mApplicationDesc.mClientWidth , mApplicationDesc.mClientHeight, 
		1.0f, 1000.0f );
}
//------------------------------------------------------------------------------------
void aW9q4X1g5yoM79VDbU04::initApp()
{
	GApplication::initApp();

	SetUPS(  100  );

	// Load shaders
	mpFontShader = new GShader( mpDevice, mpContext );
	mpFontShader->LoadVertexShader( "Data/Shaders/font.hlsl", "VS" );
	mpFontShader->LoadPixelShader( "Data/Shaders/font.hlsl", "PS" );
	mpOverlayShader = new GShader( mpDevice, mpContext );
	mpOverlayShader->LoadVertexShader( "Data/Shaders/overlay.hlsl", "VS" );
	mpOverlayShader->LoadPixelShader( "Data/Shaders/overlay.hlsl", "PS" );
	mpDepthShader = new GShader( mpDevice, mpContext );
	mpDepthShader->LoadVertexShader( "Data/Shaders/depth.hlsl", "VS" );
	mpTexturedShader = new GShader( mpDevice, mpContext );
	mpTexturedShader->LoadVertexShader( "Data/Shaders/contact_hardening_shadows.hlsl", "VS" );
	mpTexturedShader->LoadPixelShader( "Data/Shaders/contact_hardening_shadows.hlsl", "PS" );
	mpCubeShader = new GShader( mpDevice, mpContext );
	mpCubeShader->LoadVertexShader( "Data/Shaders/sky_dome.hlsl", "VS" );
	mpCubeShader->LoadPixelShader( "Data/Shaders/sky_dome.hlsl", "PS" );
	mpReflectionShader = new GShader( mpDevice, mpContext );
	mpReflectionShader->LoadVertexShader( "Data/Shaders/reflection.hlsl", "VS" );
	mpReflectionShader->LoadPixelShader( "Data/Shaders/reflection.hlsl", "PS" );

	// Load textures
	mpTexturesManager = new GTexturesManager( mpDevice, mpContext,
		mpWindowRenderTargetView, mpWindowDepthStencilView );
	mpTexturesManager->LoadAddTexturesFromFile( "Data/Textures/scene_textures.textures" );
	mpTexturesManager->LoadAddTexturesFromMtlFile( "Data/Materials/scene_materials.mtl" );
	// Shadow map is 192 MBytes in size.
	mpTexturesManager->AddRenderedTexture( "SceneReflection", 
		mpWindowRenderTargetView, mpWindowDepthStencilView, 
		mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight, 
		mSampleCount, mSampleQuality );

	// Load materials
	mpMaterialsManager = new GMaterialsManager();
	mpMaterialsManager->LoadAddMaterialsFromFile( "Data/Materials/scene_materials.mtl", 
		*mpTexturesManager );
	GMaterial material;
	material.mName = "ReflectiveFloor";
	material.map_Kd = "Water";
	material.map_Ka = "Water";
	material.map_Ks = "SceneReflection";
	// add material, where diffuse and ambient maps use DepthTexture texture
	mpMaterialsManager->AddMaterial( material );

	// Load light
	mLight.mType = GLIGHT_TYPE_PARALLEL;
	mLight.SetDirection( XMFLOAT3( 1.0f, 0.0f, 0.0f ) );
	mLight.SetPos( 0.0f, 1000.0f, 0.0f );
	mLight.mDiffuseColor = GColor::WHITE;
	mLight.mAmbientColor = XMFLOAT4( 0.7f, 0.7f, 0.7f, 1.0f );
	mLight.mSpecularColor = GColor::WHITE;
	mLight.mAttenuationParameters = XMFLOAT3( 0.0f, 0.01f, 0.0f );
	mLight.mSpotPower = 3.0f;
	mLight.mRange = 100.0f;

	// Load text
	mText.push_back( new GText( mpDevice, mpContext, 
		mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight,
		"Data/Fonts/courier_new_12_dark_green.font", 
		"" ) );
	mText.back()->Position( XMINT2( 4, 4 ) );
	mText.back()->SetColor( GColor::WHITE );

	// Load models
	GObjFile obj_file;

	obj_file.Load( "Data/Models/lp670.obj" );
	mpTexturesManager->LoadAddTexturesFromObjFile( obj_file );
	mpMaterialsManager->LoadAddMaterialsFromObjFile( obj_file, *mpTexturesManager );
	for ( uint i = 0; i < obj_file.mGroups.size(); ++i )
	{
		mModels.push_back( new GModel( obj_file.mGroups[i].mName, 
			mpDevice, mpContext ) );
		mModels.back()->LoadFromObj( obj_file, i, *mpMaterialsManager );
		mModels.back()->MergeMeshes();
		mModels.back()->Scale( 20.0f );
		mModels.back()->RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), 90.0f );
	}

	//obj_file.Load( "Data/Models/multitextured.obj" );
	//mpTexturesManager->LoadAddTexturesFromObjFile( obj_file );
	//mpMaterialsManager->LoadAddMaterialsFromObjFile( obj_file, *mpTexturesManager );
	//mModels.push_back( new GModel( "MultitexturedCube0", mpDevice, mpContext ) );
	//mModels.back()->LoadFromObj( obj_file, 0, *mpMaterialsManager );
	//mModels.back()->MergeMeshes();
	//mModels.back()->Scale( 2.0f );

	//mModels.push_back( new GModel( "MultitexturedTeapot0", mpDevice, mpContext ) );
	//mModels.back()->LoadFromObj( obj_file, 1, *mpMaterialsManager );
	//mModels.back()->MergeMeshes();
	//mModels.back()->Scale( 2.0f );
	//mModels.back()->Move( 20.0f, 0.0f, -100.0f );

	// Load sky dome.
	mSkyDome = new GSkyDome( mpDevice, mpContext, "Data/Models/sky_dome.obj",
		100.0f, "Sky Dome Texture" );

	obj_file.Load( "Data/Models/plane_l100.obj" );
	mFloor = new GModel( "ReflectiveFloor", mpDevice, mpContext );
	mFloor->LoadFromObj( obj_file, 0, *mpMaterialsManager );
	mFloor->MergeMeshes();
	mFloor->Scale( 5.0f );
	mFloor->SetMaterial( mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ) );
	mFloor->Move( XMFLOAT3( -30.0f, -10.0f, 0.0f ) );

	//obj_file.Load( "Data/Models/plane_l100.obj" );
	//mDebugWindow = new GMesh( "DebugWindow", obj_file, 0, 
	//	*mpMaterialsManager, mpDevice, mpContext );
	//mDebugWindow->SetMaterial( mpMaterialsManager->GetMaterialByName( "DepthTexture" ) );
	//mDebugWindow->Scale( 2.0f *  
	//	mApplicationDesc.mClientWidth / mApplicationDesc.mClientHeight, 2.0f, 2.0f );
	//mDebugWindow->Move( 100.0f, -1.0f * ( mApplicationDesc.mClientHeight - 100.0f ), 0.0f );
	//mDebugWindow->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );

	obj_file.Load( "Data/Models/plane_l100.obj" );
	mCursor = new GMesh( "Cursor", obj_file, 0, 
		*mpMaterialsManager, mpDevice, mpContext );
	mCursor->SetMaterial( mpMaterialsManager->GetMaterialByName( "Cursor" ) );
	mCursor->Scale( 0.32f, 1.0f, 0.32f );
	mCursor->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );
	mCursor->Move( 16.0f, -16.0f, 0.0f );

	mSliders.append( "gShadowMapSize", new GUISlider() );
	mSliders["gShadowMapSize"]->Initialize( "gShadowMapSize", XMFLOAT2( 4.0f, 90.0f ), 200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1024.0f, 4096.0f, 1024.0f, 1024.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	mSliders.append( "gSunWidth", new GUISlider() );
	mSliders["gSunWidth"]->Initialize( "gSunWidth", XMFLOAT2( 4.0f, 150.0f ), 200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1.0f, 100.0f, 1.0f, 2.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gSceneScale", std::make_shared<GUISlider>( GUISlider() ) );
	//mSliders["gSceneScale"]->Initialize( "gSceneScale", XMFLOAT2( 4.0f, 210.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 0.1f, 100.0f, 0.1f, 1.0f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gLightSize", std::make_shared<GUISlider>( GUISlider() ) );
	//mSliders["gLightSize"]->Initialize( "gLightSize", XMFLOAT2( 4.0f, 270.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 0.01f, 0.1f, 0.001f, 0.05f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gSearchSamples", std::make_shared<GUISlider>( GUISlider() ) );
	//mSliders["gSearchSamples"]->Initialize( "gSearchSamples", XMFLOAT2( 4.0f, 330.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1.0f, 32.0f, 1.0f, 6.0f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gFilterSamples", std::make_shared<GUISlider>( GUISlider() ) );
	//mSliders["gFilterSamples"]->Initialize( "gFilterSamples", XMFLOAT2( 4.0f, 390.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1.0f, 32.0f, 1.0f, 8.0f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	// Call this at the end on initApp()
	onResize();
}
//------------------------------------------------------------------------------------
void aW9q4X1g5yoM79VDbU04::onResize()
{
    GApplication::onResize();
	mCamera.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	//mPlayer.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	// Update text position when resizing window
	mText[0]->OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	// Resize render texture
	mpTexturesManager->OnWindowResize( mpWindowRenderTargetView, mpWindowDepthStencilView );
	//mpTexturesManager->ResizeTexture( "SceneReflection", );
	mLight.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );

	for (auto &slider : mSliders)
	{
		slider.second->OnWindowResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	}
}
//------------------------------------------------------------------------------------
void aW9q4X1g5yoM79VDbU04::updateScene( float dSeconds )
{
    GApplication::updateScene( dSeconds );
	processInput( dSeconds );

	for ( auto &model : mModels )
	{		
		model->RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), 0.3f );
	}

	//mPlayer.Update( dSeconds );

	std::ostringstream stream;
	stream << "Video card: " << mApplicationDesc.mVideoCardName << 
		", " << mApplicationDesc.mVideoCardFreeMemory << " MB" <<
		"\nFPS: " << mApplicationDesc.mFramesPerSecond <<
		"\nMilliseconds per frame: " << mApplicationDesc.mMillisecondsPerFrame <<
		"\nMouse X: " << mInput.MousePos().x << " Y: " << mInput.MousePos().y;
	mText[0]->SetText( stream.str() );

	//static float milliseconds_per_capture_screen = 0.0f;
	//if ( mInput.IsKeyPressed( '5' ) )
	//{
	//	milliseconds_per_capture_screen = mTimer.getGameTime();
	//	GApplication::GetScreen( L"image1.jpg", 75 );
	//	milliseconds_per_capture_screen = ( mTimer.getGameTime() - milliseconds_per_capture_screen ) * 1000.0f;
	//}

	//stream << "\nMs per screen capture:" << milliseconds_per_capture_screen;
}
//------------------------------------------------------------------------------------
void aW9q4X1g5yoM79VDbU04::processInput( float dSeconds )
{
	GApplication::processInput( dSeconds );
	
	// Light controls:
	if ( mInput.IsKeyDown( VK_LEFT ) )
	{
		mLight.SetDirection( GMathVF( XMVector3Rotate( GMathFV( mLight.GetDirection() ), 
			XMQuaternionRotationAxis( GMathFV( XMFLOAT3( 0.0f, 0.0f, -1.0f ) ), -0.01f ) ) ) );
		mLight.SetPos( mLight.GetPos().x - 3.0f, mLight.GetPos().y, mLight.GetPos().z );
	}
	if ( mInput.IsKeyDown( VK_RIGHT ) )
	{
		mLight.SetDirection( GMathVF( XMVector3Rotate( GMathFV( mLight.GetDirection() ), 
			XMQuaternionRotationAxis( GMathFV( XMFLOAT3( 0.0f, 0.0f, -1.0f ) ), 0.01f ) ) ) );
		mLight.SetPos( mLight.GetPos().x + 3.0f, mLight.GetPos().y, mLight.GetPos().z );
	}
	if ( mInput.IsKeyDown( 'A' ) ) mCamera.RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), -1.0f );
	if ( mInput.IsKeyDown( 'D' ) ) mCamera.RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), +1.0f );
	if ( mInput.IsKeyDown( 'W' ) ) mCamera.RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -1.0f );
	if ( mInput.IsKeyDown( 'S' ) ) mCamera.RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), +1.0f );

	// Control shader parameters through sliders:
	for (auto &slider : mSliders)
	{
		slider.second->OnInput( mInput );
	}

	// Change shadow map size based on corresponding slider current value.
	if ( mSliders["gShadowMapSize"]->GetCurrentValue() != 
		mpTexturesManager->GetShadowMapSize() )
	{
		mpTexturesManager->ResizeDepthTexture(  
			static_cast<uint>( mSliders["gShadowMapSize"]->GetCurrentValue() ) );
	}

	// Clear input when done processing it.
	mInput.Clear();
}
//------------------------------------------------------------------------------------
void aW9q4X1g5yoM79VDbU04::drawScene()
{
	GApplication::drawScene();

	//------------------------------------------------------------------------------------
	//mpTexturesManager->RenderToTexture( "DepthTexture", const_cast<XMFLOAT4&>( GColor::WHITE ) );
	mpTexturesManager->RenderToDepth();

    // Render scene's z-buffer into depth texture.
	GApplication::TurnZBufferOn();
	mpDepthShader->ClearConstantBuffers();
	mpDepthShader->SetShader();
	mpDepthShader->DisableAlpha();
	mpDepthShader->SetConstant( "PerFrame", "View", 
		mLight.mCamera.View() );
	mpDepthShader->SetConstant( "PerFrame", "Projection", 
		mLight.mCamera.Ortho() );
	mpDepthShader->UpdateConstantBuffer( "PerFrame" );

	for ( const auto &model : mModels)
	{
		for ( const auto &mesh : model->AccessMeshes() )
		{
			mpDepthShader->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpDepthShader->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
		for ( const auto &mesh : model->AccessMeshes() )
		{
			mpDepthShader->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpDepthShader->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
	}

	mpTexturesManager->RenderToScreen( mApplicationDesc.mClientWidth, 
		mApplicationDesc.mClientHeight, mApplicationDesc.mScreenClearColor );
	GApplication::ClearScreen();
	GApplication::TurnZBufferOn();

	mpTexturesManager->RenderToTexture( "SceneReflection", mApplicationDesc.mScreenClearColor );
	
	// Render sky dome.
	mpCubeShader->ClearConstantBuffers();
	mpCubeShader->SetShader();
	mpCubeShader->EnableAlpha();

	mpCubeShader->SetConstant( "PerFrame", "View", mCamera.ReflectedView() );
	mpCubeShader->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	mpCubeShader->UpdateConstantBuffer( "PerFrame" );

	mpTexturesManager->SetTexture( "gCubeMap", mSkyDome->GetTextureID() );
	mSkyDome->Draw();
	//------------------------------------------------------------------------------------

	mpTexturedShader->ClearConstantBuffers();
	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();

	mpTexturedShader->SetConstant( "PerFrame", "View", mCamera.ReflectedView() );
	mpTexturedShader->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	float one_div_sm_size = 1.0f / mSliders["gShadowMapSize"]->GetCurrentValue();	
	mpTexturedShader->SetConstant( "PerFrame", "gShadowMapDimensions", 
		XMFLOAT4( mSliders["gShadowMapSize"]->GetCurrentValue(), 
		mSliders["gShadowMapSize"]->GetCurrentValue(), one_div_sm_size, one_div_sm_size ) );
	mpTexturedShader->SetConstant( "PerFrame", "gSunWidth", 
		mSliders["gSunWidth"]->GetCurrentValue() );

	mpTexturedShader->UpdateConstantBuffer( "PerFrame" );

	mpTexturedShader->SetConstant( "Light", "CameraPosition", mCamera.Position() );
	mpTexturedShader->SetConstant( "Light", "Direction", mLight.GetDirection() );
	mpTexturedShader->SetConstant( "Light", "Position", mLight.GetPos() );
	mpTexturedShader->SetConstant( "Light", "DiffuseColor", mLight.mDiffuseColor );
	mpTexturedShader->SetConstant( "Light", "AmbientColor", mLight.mAmbientColor );
	mpTexturedShader->SetConstant( "Light", "SpecularColor", mLight.mSpecularColor );
	mpTexturedShader->SetConstant( "Light", "AttenuationParameters", 
		mLight.mAttenuationParameters );
	mpTexturedShader->SetConstant( "Light", "SpotPower", mLight.mSpotPower );
	mpTexturedShader->SetConstant( "Light", "Range", mLight.mRange );
	mpTexturedShader->SetConstant( "Light", "LightType", mLight.mType );
	mpTexturedShader->SetConstant( "Light", "LightView", 
		mLight.mCamera.View() );
	mpTexturedShader->SetConstant( "Light", "LightProjection", 
		mLight.mCamera.Ortho() );
	mpTexturedShader->UpdateConstantBuffer( "Light" );

	mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 1 );
	mpTexturedShader->UpdateConstantBuffer( "PerObjectGroup" );

	mpTexturesManager->SetTexture( "NormalMap", "Brick Tiled Normals" );
	mpTexturesManager->SetShadowMapTexture();
	//mpTexturesManager->SetTexture( "ShadowMap", "DepthTexture" );
	mpTexturesManager->SetTexture( "ReflectionMap", "Brick Reflections" );

	for ( const auto &model : mModels)
	{
		for ( const auto &mesh : model->AccessMeshes() )
		{

			if ( mesh.GetMaterialID() == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );

				mpTexturesManager->SetTexture( "DiffuseMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd );
				mpTexturesManager->SetTexture( "AmbientMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Ka );
			}

			if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_bump == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
			}
			else
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
			}

			mpTexturedShader->UpdateConstantBuffer( "PerObjectGroup" );
			mpTexturedShader->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpTexturedShader->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
	}

	mpTexturesManager->RenderToScreen( mApplicationDesc.mClientWidth, 
		mApplicationDesc.mClientHeight, mApplicationDesc.mScreenClearColor );
	GApplication::ClearScreen();
	GApplication::TurnZBufferOn();

	// Render sky dome.
	mpCubeShader->ClearConstantBuffers();
	mpCubeShader->SetShader();
	mpCubeShader->EnableAlpha();

	mpCubeShader->SetConstant( "PerFrame", "View", mCamera.View() );
	mpCubeShader->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	mpCubeShader->UpdateConstantBuffer( "PerFrame" );

	mpTexturesManager->SetTexture( "gCubeMap", mSkyDome->GetTextureID() );
	mSkyDome->Draw();
	//------------------------------------------------------------------------------------

	mpTexturedShader->ClearConstantBuffers();
	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();

	mpTexturedShader->SetConstant( "PerFrame", "View", mCamera.View() );
	mpTexturedShader->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	one_div_sm_size = 1.0f / mSliders["gShadowMapSize"]->GetCurrentValue();	
	mpTexturedShader->SetConstant( "PerFrame", "gShadowMapDimensions", 
		XMFLOAT4( mSliders["gShadowMapSize"]->GetCurrentValue(), 
		mSliders["gShadowMapSize"]->GetCurrentValue(), one_div_sm_size, one_div_sm_size ) );
	mpTexturedShader->SetConstant( "PerFrame", "gSunWidth", 
		mSliders["gSunWidth"]->GetCurrentValue() );

	mpTexturedShader->UpdateConstantBuffer( "PerFrame" );

	mpTexturedShader->SetConstant( "Light", "CameraPosition", mCamera.Position() );
	mpTexturedShader->SetConstant( "Light", "Direction", mLight.GetDirection() );
	mpTexturedShader->SetConstant( "Light", "Position", mLight.GetPos() );
	mpTexturedShader->SetConstant( "Light", "DiffuseColor", mLight.mDiffuseColor );
	mpTexturedShader->SetConstant( "Light", "AmbientColor", mLight.mAmbientColor );
	mpTexturedShader->SetConstant( "Light", "SpecularColor", mLight.mSpecularColor );
	mpTexturedShader->SetConstant( "Light", "AttenuationParameters", 
		mLight.mAttenuationParameters );
	mpTexturedShader->SetConstant( "Light", "SpotPower", mLight.mSpotPower );
	mpTexturedShader->SetConstant( "Light", "Range", mLight.mRange );
	mpTexturedShader->SetConstant( "Light", "LightType", mLight.mType );
	mpTexturedShader->SetConstant( "Light", "LightView", 
		mLight.mCamera.View() );
	mpTexturedShader->SetConstant( "Light", "LightProjection", 
		mLight.mCamera.Ortho() );
	mpTexturedShader->UpdateConstantBuffer( "Light" );

	mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 1 );
	mpTexturedShader->UpdateConstantBuffer( "PerObjectGroup" );

	mpTexturesManager->SetTexture( "NormalMap", "Brick Tiled Normals" );
	mpTexturesManager->SetShadowMapTexture();
	//mpTexturesManager->SetTexture( "ShadowMap", "DepthTexture" );
	mpTexturesManager->SetTexture( "ReflectionMap", "Brick Reflections" );

	for ( const auto &model : mModels)
	{
		for ( const auto &mesh : model->AccessMeshes() )
		{

			if ( mesh.GetMaterialID() == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );

				mpTexturesManager->SetTexture( "DiffuseMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd );
				mpTexturesManager->SetTexture( "AmbientMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Ka );
			}

			if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_bump == "" )
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
			}
			else
			{
				mpTexturedShader->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
			}

			mpTexturedShader->UpdateConstantBuffer( "PerObjectGroup" );
			mpTexturedShader->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpTexturedShader->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
	}

	//------------------------------------------------------------------------------------
	// Render floor.
	mpReflectionShader->ClearConstantBuffers();
	mpReflectionShader->SetShader();
	mpReflectionShader->DisableAlpha();
	mpReflectionShader->SetConstant( "PerFrame", "View", 
		mCamera.View() );
	mpReflectionShader->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	// Reflect by Y plane.
	mCamera.InitReflectionMatrix( XMFLOAT4( 0.0f, -10.0f, 0.0f, 0.0f ) );
	mpReflectionShader->SetConstant( "PerFrame", "ReflectedView", 
		mCamera.ReflectedView() );
	mpReflectionShader->UpdateConstantBuffer( "PerFrame" );
	mpTexturesManager->SetTexture( "DiffuseMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Kd );
	mpTexturesManager->SetTexture( "AmbientMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Ka );
	mpTexturesManager->SetTexture( "ReflectionMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Ks );
	for ( const auto &mesh : mFloor->AccessMeshes() )
	{
		mpReflectionShader->SetConstant( "PerObject", "World", 
			GMathMul( mesh.GetWorld(), mFloor->GetWorld() ) );
		mpReflectionShader->UpdateConstantBuffer( "PerObject" );
		mesh.draw();
	}
	

	//------------------------------------------------------------------------------------
	// Render 2D Text.
	GApplication::TurnZBufferOff();
	mpFontShader->ClearConstantBuffers();
	mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	mpFontShader->SetConstant( "PerFrame", "Projection", mCamera.Ortho() );
	mpFontShader->UpdateConstantBuffer( "PerFrame" );

	for ( const auto &text : mText )
	{
		mpTexturesManager->SetTexture( "DiffuseMap", text->GetTextureName() );
		mpFontShader->SetConstant( "PerObject", "World", text->GetWorld() );
		mpFontShader->SetConstant( "PerObject", "FontColor", text->GetColor() );
		mpFontShader->UpdateConstantBuffer( "PerObject" );
		text->draw();
	}

	// Draw 2d overlay window.
	//mpOverlayShader->ClearConstantBuffers();
	//mpOverlayShader->SetShader();
	//mpOverlayShader->EnableAlpha();
	//mpOverlayShader->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	//mpOverlayShader->SetConstant( "PerFrame", "Projection", mCamera.Ortho() );
	//mpOverlayShader->UpdateConstantBuffer( "PerFrame" );
	//
	//mpOverlayShader->SetConstant( "PerObject", "World", 
	//	GMathMF( GMathFM( mDebugWindow->GetWorld() ) * XMMatrixTranslation( 
	//	static_cast<float>( -0.5f * mApplicationDesc.mClientWidth ) + 0,	
	//	static_cast<float>( 0.5f * mApplicationDesc.mClientHeight ) - 0, 
	//	0.0f ) ) );
	//mpOverlayShader->SetConstant( "PerObject", "UseDiffuseMap", 1);
	//mpOverlayShader->UpdateConstantBuffer( "PerObject" );
	//mpTexturesManager->SetTexture( "DiffuseMap", 
	//	mpMaterialsManager->GetMaterialByName( mDebugWindow->GetMaterialID() ).map_Kd );
	//mDebugWindow->draw();

	for (auto &slider : mSliders)
	{
		slider.second->Render( *mpFontShader, *mpOverlayShader, *mpTexturesManager );
	}

	// Draw cursor.
	mpOverlayShader->ClearConstantBuffers();
	mpOverlayShader->SetShader();
	mpOverlayShader->EnableAlpha();
	mpOverlayShader->SetConstant( "PerObject", "World", 
		GMathMF( GMathFM( mCursor->GetWorld() ) * XMMatrixTranslation( 
		static_cast<float>( -0.5f * mApplicationDesc.mClientWidth ) + mInput.MousePos().x,	
		static_cast<float>( 0.5f * mApplicationDesc.mClientHeight ) - mInput.MousePos().y, 
		0.0f ) ) );
	mpOverlayShader->SetConstant( "PerObject", "UseDiffuseMap", 1);
	mpOverlayShader->UpdateConstantBuffer( "PerObject" );	
	mpTexturesManager->SetTexture( "DiffuseMap",
		mpMaterialsManager->GetMaterialByName( mCursor->GetMaterialID() ).map_Kd );
	mCursor->draw();

    mpSwapChain->Present( 0, 0 );
}
//------------------------------------------------------------------------------------
aW9q4X1g5yoM79VDbU04::~aW9q4X1g5yoM79VDbU04( void )
{
	delete mSkyDome;
	for ( auto &slider : mSliders ) delete slider.second;
	delete mFloor;
	for ( auto &model : mModels ) delete model;
	for ( auto &text : mText ) delete text;
	delete mCursor;
	//delete mDebugWindow;
	
	delete mpReflectionShader;
	delete mpCubeShader;
	delete mpTexturedShader;
	delete mpDepthShader;
	delete mpOverlayShader;
	delete mpFontShader;

	delete mpMaterialsManager;
	delete mpTexturesManager;

	if(  mpContext  )
	{
		mpContext->Flush();
		mpContext->ClearState();
	}
}
//------------------------------------------------------------------------------------