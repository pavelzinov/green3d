#pragma once
#include <iostream>
#include <Windows.h>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include "GFile.h"

using namespace std;

class DXAppManager
{
public:
    DXAppManager();
    ~DXAppManager(void);

public:
	// ������� ����� ������
	void CreateNewProject(string project_name, string main_class);
	// Create stub project from working DX11 project and copy it's files into BasicFiles directory
	// so that all basic projects will be based upon it.
	void BasicFilesFromProject();

private:
	// ������� ����� � ������ �������
	void CreateFolder(string folder_name);
	// �������� ��� ����� �� ����� BasicFiles � ����� �������, � ����� ��������������� ����� ������-��������
	void CopyFiles(string from_dir, string to_dir);
	// Rename all files with old_name (name does not include extension!) to new_name in given directory from_dir
	void RenameFiles(string from_dir, string old_name, string new_name);
	// Replace all strings equal to old_content in all files from directory with new_content string 
	void RenameContent(string directory, string old_content, string new_content);
	// Get info about all files in given directory
	vector<GFile> GetFilesInfo(string directory);
	// Find all string positions in file
	vector<int> FindInFile(string filename, string value);
	// Get project name based on project files laying in directory dir
	string GetProjectName(string dir);
	// Get main class based on project files which has #include "GApplication.h" line
	string GetMainClass(string dir);
	// This recursively gets all the filenames that match the filter in given directory
	void GetFileListing(std::vector<std::string>& listing, std::string directory, 
		std::string fileFilter, bool recursively=true);
	// This recursively gets all the directories inside given directory
	void GetDirectoriesListing(std::vector<std::string>& listing, std::string directory, 
		std::string fileFilter="*", bool recursively=true);

private:
	string mProjectName;
	string mMainClass;
	string mStubProjectName;	// �� ��������� her8813HNA74NMrI64732
	string mStubMainClass;		// �� ��������� aW9q4X1g5yoM79VDbU04
	vector<GFile> mResultFileInfos;

private:    
    string CurrDir();
	bool RenameStringInFile(string filename, string replace, string replacer);
    
};