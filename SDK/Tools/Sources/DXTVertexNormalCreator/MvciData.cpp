#include "MvciData.h"

void MvciData::LoadFromFile(std::string filename)
{
	std::string line;	
	std::ifstream model;
	model.open(filename);	
	std::vector<std::string> file_bytes;

	if (model.is_open())
	{
		while (model.good())
		{
			getline(model, line);
			file_bytes.push_back(line);
		}
		model.close();

		// delete last empty character
		if (file_bytes[file_bytes.size() - 1] == "")
			file_bytes.pop_back();
	}
	else
	{
		return;
	}

	FromFileData(file_bytes);
}

void MvciData::FromFileData(std::vector<std::string> &file_data)
{
	mVertexType = file_data[0];

	mVerticesCount = 0;
	std::stringstream line_as_stream;	

	line_as_stream << file_data[1];
	line_as_stream >> mVerticesCount;
	line_as_stream.clear();

	float x, y, z, r, g, b, a;
	Vertex vertex;

	// reading vertices
	mVertices.clear();
	mVertices.reserve(mVerticesCount);
	auto index_data_start = mVerticesCount + 2;
	for (unsigned int i=2; i < index_data_start; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> x >> y >> z >> r >> g >> b >> a;
		vertex.x = x;
		vertex.y = y;
		vertex.z = z;
		vertex.r = r;
		vertex.g = g;
		vertex.b = b;
		vertex.a = a;
		mVertices.push_back(vertex);
		line_as_stream.clear();
	}

	// reading indices
	uint32_t i1 = 0, i2 = 0, i3 = 0;
	TriangleIndices triangle;
	uint32_t file_size = file_data.size();
	for (auto i=index_data_start; i<file_size; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> i1 >> i2 >> i3;
		triangle.first = i1 - 1;
		triangle.second = i2 - 1;
		triangle.third = i3 - 1;
		mTriangles.push_back(triangle);
		line_as_stream.clear();
	}
}

std::vector<XMFLOAT3> MvciData::ConstructNormals()
{
	// constructing normals
	std::vector<std::vector<XMFLOAT3>> normals;
	normals.resize(mVerticesCount);
	// for each triangle
	uint32_t triangles_count = mTriangles.size();
	XMVECTOR vec1, vec2, normal;
	for (uint32_t i=0; i<triangles_count; ++i)
	{
		vec1 = GMathFV(XMFLOAT3(mVertices[mTriangles[i].second].x, 
			mVertices[mTriangles[i].second].y, mVertices[mTriangles[i].second].z)) - 
			GMathFV(XMFLOAT3(mVertices[mTriangles[i].first].x, 
			mVertices[mTriangles[i].first].y, mVertices[mTriangles[i].first].z));
		vec2 = GMathFV(XMFLOAT3(mVertices[mTriangles[i].third].x, 
			mVertices[mTriangles[i].third].y, mVertices[mTriangles[i].third].z)) - 
			GMathFV(XMFLOAT3(mVertices[mTriangles[i].first].x, 
			mVertices[mTriangles[i].first].y, mVertices[mTriangles[i].first].z));
		// determine normal for first triangle
		normal = XMVector3Normalize(XMVector3Cross(vec1, vec2));
		// add triangle's normal to all it's vertices's current normals
		normals[mTriangles[i].first].push_back(GMathVF(normal));
		normals[mTriangles[i].second].push_back(GMathVF(normal));
		normals[mTriangles[i].third].push_back(GMathVF(normal));
	}

	// make sure vertex's normals don't duplicate
	for (uint32_t i = 0; i < normals.size(); ++i)
	{
		for (uint32_t j = 0; j < normals[i].size(); ++j)
		{
			for (uint32_t k = j + 1; k < normals[i].size(); ++k)
			{
				if (normals[i][k].x == normals[i][j].x &&
					normals[i][k].y == normals[i][j].y &&
					normals[i][k].z == normals[i][j].z)
				{
					normals[i].erase(normals[i].begin() + k);
					k = j + 1;
				}
			}
		}
	}

	// summarize each vertex's normals to get single value per vertex
	normal = XMVectorZero();
	std::vector<XMFLOAT3> result_normals;
	for (uint32_t i = 0; i < mVerticesCount; ++i)
	{
		// combine all normals for single vertex
		normal = XMVectorZero();
		for (auto it = normals[i].begin(); it != normals[i].end(); it++)
		{
			normal = normal + GMathFV(*it);
		}
		// normalize result and assign it to vertex's normal
		result_normals.push_back(GMathVF(XMVector3Normalize(normal)));
	}

	return result_normals;
}

// .tvni file structure:
// --------------------
// [vertices count]
// [vertex1 x] [vertex1 y] [vertex1 z] [vertex1 normal x] [vertex1 normal y] [vertex1 normal z]
// [vertex2 x] [vertex2 y] [vertex2 z] [vertex2 normal x] [vertex2 normal y] [vertex2 normal z]
// ...
// [triangle index1] [triangle index2] [triangle index3]
// ...
// --------------------
// triangle index starts from 1
void MvciData::ToTvni(std::string filename)
{
	std::ofstream file(filename, std::ios::trunc);
	std::vector<XMFLOAT3> normals = ConstructNormals();

	file << mVerticesCount << std::endl;
	for (int i = 0; i < mVertices.size(); ++i)
	{
		file << mVertices[i].x << " " << mVertices[i].y << " " << mVertices[i].z <<
			" " << normals[i].x << " " << normals[i].y << " " << normals[i].z << std::endl;
	}
	for (int i = 0; i < mTriangles.size(); ++i)
	{
		file << mTriangles[i].first << " " << mTriangles[i].second << " " << 
			mTriangles[i].third << std::endl;
	}
	file.close();
}