#pragma once
#include <iostream>
#include <windows.h>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include "GUtility.h"

class MvciData
{
private:
	struct Vertex
	{
		float x, y, z, r, g, b, a;
	};
	struct NewVertex
	{
		float x, y, z, nx, ny, nz;
	};
	struct TriangleIndices
	{
		UINT first, second, third;
	};

public:
	MvciData() {}
	~MvciData() {}

	void LoadFromFile(std::string filename);
	void FromFileData(std::vector<std::string> &file_data);

	std::vector<XMFLOAT3> ConstructNormals();
	// Convert loaded mvci data to tvni data
	void ToTvni(std::string filename);

	// mvci file structure
	std::string mVertexType;
	UINT mVerticesCount;
	std::vector<Vertex> mVertices;
	std::vector<TriangleIndices> mTriangles;
};