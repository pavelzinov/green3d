#include "GFile.h"
#include "MvciData.h"
using namespace std;

int main()
{
	vector<GFile> files_info = GFile::GetFilesInfo(GFile::CurrDir());

	std::sort(files_info.begin(), files_info.end(), [] (GFile & a, GFile & b) -> bool {
		return a.extension < b.extension;
	});

	// read mvci file
	for (auto it = files_info.begin(); it != files_info.end(); it++)
	{
		if (it->extension == "mvci")
		{
			cout << "Found .mvci file " << it->shortWithExtension << endl;
			cout << "Processing...";
			MvciData converter;
			converter.LoadFromFile(it->fullName);
			converter.ToTvni(it->homeDirectory + it->shortName + ".tvni");
			cout << "Done!" << endl;
		}
	}

	return 0;
}