﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// File layout:
// [letter] [height] [texture_coordinate_start] [texture_coordinate_end]\n
// ----
// [letter] [height] [texture_coordinate_start] [texture_coordinate_end]\n
//

namespace DXTFontParser
{
    struct LetterInfo
    {
        public LetterInfo(int start, int end, int width, int height,
            float start_relative, float end_relative)
        {
            this.start = start;
            this.end = end;
            this.width = width;
            this.height = height;
            this.start_relative = start_relative;
            this.end_relative = end_relative;
        }
        public int start;
        public int end;
        public int width;
        public int height;
        public float start_relative;
        public float end_relative;
    }

    public partial class FontParser : Form
    {
        public FontParser()
        {
            InitializeComponent();
        }

        private string filename = "";
        private string alphabet = "";
        private Dictionary<char, LetterInfo> mapped_alphabet = new Dictionary<char, LetterInfo>();

        private void ChooseFileButtonClicked(object sender, EventArgs e)
        {
            DialogResult result = openImageDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                filename = openImageDialog.FileName;
                DisplayBox.Image = Image.FromFile(filename);
            }
        }

        private void CreateFontClicked(object sender, EventArgs e)
        {
            // Get user-defined alphabet
            alphabet = dictionaryPattern.Text;
            for (int i = 0; i < alphabet.Length; i++)
            {
                if (alphabet[i] == ' ')
                {
                    alphabet = alphabet.Remove(i, 1);
                    i = -1;
                }
            }

            // Analise image file
            Bitmap data = new Bitmap(filename);
            int width = data.Width;
            int height = data.Height;

            int letter_start = -1;
            int letter_end = -1;
            int current_letter = 0;
            for (int i = 0; i < width; i++)
            {
                bool is_empty_column = true;
                for (int j = 0; j < height; j++)
                {
                    if (data.GetPixel(i, j).A != 0)
                    {
                        is_empty_column = false;
                        if (letter_start == -1)
                        {
                            letter_start = i;
                        }
                    }
                }
                if (is_empty_column == true)
                {
                    if (letter_start != -1)
                    {
                        letter_end = i - 1;
                        mapped_alphabet[alphabet[current_letter]] = new LetterInfo(letter_start,
                            letter_end, letter_end - letter_start + 1, height, (float)letter_start / (float)width,
                            ((float)letter_end + 1.0f) / (float)width);

                        letter_start = -1;
                        current_letter += 1;
                    }
                }
            }

            if (saveFontDescriptionDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // make all ',' of floats to save as '.' in file
                System.Globalization.CultureInfo customCulture =
                    (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                customCulture.NumberFormat.NumberDecimalSeparator = ".";
                System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

                StreamWriter fileStream = new StreamWriter(saveFontDescriptionDialog.FileName);
                for (int i = 0; i < mapped_alphabet.Count; i++)
                {
                    fileStream.WriteLine(alphabet[i] +
                        " " + mapped_alphabet[alphabet[i]].width +
                        " " + mapped_alphabet[alphabet[i]].height +
                        " " + mapped_alphabet[alphabet[i]].start_relative +
                        " " + mapped_alphabet[alphabet[i]].end_relative);
                }
                fileStream.Close();
            }
        }

        private void ChooseFntFileClicked(object sender, EventArgs e)
        {

        }

        private void GenerateFromFntClicked(object sender, EventArgs e)
        {

        }
    }
}
