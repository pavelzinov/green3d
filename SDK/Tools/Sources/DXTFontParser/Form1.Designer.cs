﻿namespace DXTFontParser
{
    partial class FontParser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FontParser));
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFontDescriptionDialog = new System.Windows.Forms.SaveFileDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.descriptionFromImage = new System.Windows.Forms.TabPage();
            this.ChooseFileButton = new System.Windows.Forms.Button();
            this.CreateFontButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DisplayBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dictionaryPattern = new System.Windows.Forms.TextBox();
            this.descriptionFromFnt = new System.Windows.Forms.TabPage();
            this.generateFromFnt = new System.Windows.Forms.Button();
            this.chooseFntFile = new System.Windows.Forms.Button();
            this.openFntFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl.SuspendLayout();
            this.descriptionFromImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayBox)).BeginInit();
            this.descriptionFromFnt.SuspendLayout();
            this.SuspendLayout();
            // 
            // openImageDialog
            // 
            this.openImageDialog.Filter = "PNG files|*png|BMP files|*.bmp";
            this.openImageDialog.RestoreDirectory = true;
            // 
            // saveFontDescriptionDialog
            // 
            this.saveFontDescriptionDialog.FileName = "font";
            this.saveFontDescriptionDialog.Filter = "Font Description|*.fntdesc";
            this.saveFontDescriptionDialog.RestoreDirectory = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.descriptionFromImage);
            this.tabControl.Controls.Add(this.descriptionFromFnt);
            this.tabControl.Location = new System.Drawing.Point(12, 13);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(805, 361);
            this.tabControl.TabIndex = 6;
            // 
            // descriptionFromImage
            // 
            this.descriptionFromImage.Controls.Add(this.ChooseFileButton);
            this.descriptionFromImage.Controls.Add(this.CreateFontButton);
            this.descriptionFromImage.Controls.Add(this.label2);
            this.descriptionFromImage.Controls.Add(this.DisplayBox);
            this.descriptionFromImage.Controls.Add(this.label1);
            this.descriptionFromImage.Controls.Add(this.dictionaryPattern);
            this.descriptionFromImage.Location = new System.Drawing.Point(4, 22);
            this.descriptionFromImage.Name = "descriptionFromImage";
            this.descriptionFromImage.Padding = new System.Windows.Forms.Padding(3);
            this.descriptionFromImage.Size = new System.Drawing.Size(797, 335);
            this.descriptionFromImage.TabIndex = 0;
            this.descriptionFromImage.Text = "Создать описание из картинки";
            this.descriptionFromImage.UseVisualStyleBackColor = true;
            // 
            // ChooseFileButton
            // 
            this.ChooseFileButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ChooseFileButton.Location = new System.Drawing.Point(6, 6);
            this.ChooseFileButton.Name = "ChooseFileButton";
            this.ChooseFileButton.Size = new System.Drawing.Size(775, 23);
            this.ChooseFileButton.TabIndex = 6;
            this.ChooseFileButton.Text = "Choose file...";
            this.ChooseFileButton.UseVisualStyleBackColor = true;
            this.ChooseFileButton.Click += new System.EventHandler(this.ChooseFileButtonClicked);
            // 
            // CreateFontButton
            // 
            this.CreateFontButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreateFontButton.Location = new System.Drawing.Point(6, 290);
            this.CreateFontButton.Name = "CreateFontButton";
            this.CreateFontButton.Size = new System.Drawing.Size(775, 23);
            this.CreateFontButton.TabIndex = 8;
            this.CreateFontButton.Text = "Create font description";
            this.CreateFontButton.UseVisualStyleBackColor = true;
            this.CreateFontButton.Click += new System.EventHandler(this.CreateFontClicked);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(355, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Font image:";
            // 
            // DisplayBox
            // 
            this.DisplayBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DisplayBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DisplayBox.Location = new System.Drawing.Point(6, 107);
            this.DisplayBox.Name = "DisplayBox";
            this.DisplayBox.Size = new System.Drawing.Size(775, 177);
            this.DisplayBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.DisplayBox.TabIndex = 7;
            this.DisplayBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(333, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Font analysis pattern:";
            // 
            // dictionaryPattern
            // 
            this.dictionaryPattern.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dictionaryPattern.Location = new System.Drawing.Point(6, 57);
            this.dictionaryPattern.Name = "dictionaryPattern";
            this.dictionaryPattern.Size = new System.Drawing.Size(775, 22);
            this.dictionaryPattern.TabIndex = 9;
            this.dictionaryPattern.Text = ": , . ! @ # ~ % $ ( ) - + = / * ? < > \\ A B C D E F G H I J K L M N O P Q R S T U" +
    " V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z 0 1 2 3 4 5 6 7 8" +
    " 9";
            // 
            // descriptionFromFnt
            // 
            this.descriptionFromFnt.Controls.Add(this.generateFromFnt);
            this.descriptionFromFnt.Controls.Add(this.chooseFntFile);
            this.descriptionFromFnt.Location = new System.Drawing.Point(4, 22);
            this.descriptionFromFnt.Name = "descriptionFromFnt";
            this.descriptionFromFnt.Padding = new System.Windows.Forms.Padding(3);
            this.descriptionFromFnt.Size = new System.Drawing.Size(797, 335);
            this.descriptionFromFnt.TabIndex = 1;
            this.descriptionFromFnt.Text = "Описание из файла .fnt";
            this.descriptionFromFnt.UseVisualStyleBackColor = true;
            // 
            // generateFromFnt
            // 
            this.generateFromFnt.Location = new System.Drawing.Point(6, 35);
            this.generateFromFnt.Name = "generateFromFnt";
            this.generateFromFnt.Size = new System.Drawing.Size(785, 23);
            this.generateFromFnt.TabIndex = 1;
            this.generateFromFnt.Text = "Generate .fntdesc file";
            this.generateFromFnt.UseVisualStyleBackColor = true;
            this.generateFromFnt.Click += new System.EventHandler(this.GenerateFromFntClicked);
            // 
            // chooseFntFile
            // 
            this.chooseFntFile.Location = new System.Drawing.Point(6, 6);
            this.chooseFntFile.Name = "chooseFntFile";
            this.chooseFntFile.Size = new System.Drawing.Size(785, 23);
            this.chooseFntFile.TabIndex = 0;
            this.chooseFntFile.Text = "Choose .fnt file";
            this.chooseFntFile.UseVisualStyleBackColor = true;
            this.chooseFntFile.Click += new System.EventHandler(this.ChooseFntFileClicked);
            // 
            // openFntFileDialog
            // 
            this.openFntFileDialog.FileName = "font";
            this.openFntFileDialog.Filter = "Font file|*.fnt";
            this.openFntFileDialog.RestoreDirectory = true;
            // 
            // FontParser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 390);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(827, 309);
            this.Name = "FontParser";
            this.Text = "Font image parser";
            this.tabControl.ResumeLayout(false);
            this.descriptionFromImage.ResumeLayout(false);
            this.descriptionFromImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayBox)).EndInit();
            this.descriptionFromFnt.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.SaveFileDialog saveFontDescriptionDialog;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage descriptionFromImage;
        private System.Windows.Forms.Button ChooseFileButton;
        private System.Windows.Forms.Button CreateFontButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox DisplayBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dictionaryPattern;
        private System.Windows.Forms.TabPage descriptionFromFnt;
        private System.Windows.Forms.Button generateFromFnt;
        private System.Windows.Forms.Button chooseFntFile;
        private System.Windows.Forms.OpenFileDialog openFntFileDialog;
    }
}

