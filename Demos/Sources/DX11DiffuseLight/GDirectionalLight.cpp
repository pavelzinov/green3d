#include "GDirectionalLight.h"
using namespace Game;

GDirectionalLight::GDirectionalLight(void)
{
	mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mColor = GCOLOR_BEACH_SAND;
}

GDirectionalLight::GDirectionalLight(XMFLOAT3 direction, XMFLOAT4 color)
{
	mDirection = direction;
	mColor = color;
}