#pragma once
#include "GVertex.h"

namespace Game
{

// aligned textured vertex data storage
struct GTexturedVertexData
{
	XMFLOAT3 pos;
	XMFLOAT3 normal;
	XMFLOAT2 texC;
};

class GTexturedVertex :	public GVertex
{
public:
	GTexturedVertex(const GTexturedVertex &vertex);
	GTexturedVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT2 tex_coords);
	~GTexturedVertex(void) {}

	static uint32_t getDataSize() { return sizeof(GTexturedVertexData); }

public:
	XMFLOAT2 mTexturedCoordinates;
};

}