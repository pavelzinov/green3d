#include "DiffuseLight.h"
#include "DDSTextureLoader.h"

DiffuseLight::DiffuseLight(HINSTANCE hInstance) : GApplication(hInstance)
{
    // init background color
    GApplication::mClearColor = GCOLOR_BEACH_SAND;

    mDistance = 100.0f;

	mCamera.SetPosition(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 100.0f);	
}

void DiffuseLight::initApp()
{ 
	GApplication::initApp();

	GObjFile file;
	file.Load("Data/Models/sphere.obj");

	// Load shaders
	mpColoredShader = new GShader<GColoredVertex, GSHADER_TYPE_DIR_LIGHT>(md3dDevice, md3dContext);
	mpColoredShader->LoadShader("Data/Shaders/colored_dir_light.fx");
	mpFontShader = new GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpFontShader->LoadShader("Data/Shaders/textured.fx");
	mpTexturedShader = new GShader<GTexturedVertex, GSHADER_TYPE_DIR_LIGHT>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("Data/Shaders/tex_dir_light.fx");

	// Load materials
	mpMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mpMaterialsManager->AddMaterial("Space", "Data/Textures/space.dds");
	mpMaterialsManager->AddMaterial("Water", "Data/Textures/water.dds");
	mpMaterialsManager->AddMaterial("SkyBackground", "Data/Textures/sky.dds");
	mpMaterialsManager->AddMaterial("VS2012", "Data/Textures/visual_studio_2012.dds");
	mpMaterialsManager->AddMaterial("Courier New 12pt Dark Green", "Data/Fonts/courier_new_12_dark_green.dds");
	mpMaterialsManager->AddMaterial("Segoe UI Semilight 10pt Yellow", "Data/Fonts/segoe_semilight_10_yellow.dds");	

	// Load light
	mDirLight.mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mDirLight.mColor = GCOLOR_WHITE;

	// Load text
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		""));
	mText.back()->SetPosition(XMUINT2(4, 4), mClientWidth, mClientHeight);
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		"Use RIGHT and LEFT arrow keys to rotate light's direction"));
	mText.back()->SetPosition(XMUINT2(4, mClientHeight - 
		2 * mText.back()->GetSymbolHeight()), mClientWidth, mClientHeight);

	// Load sprites
	mSprites.push_back(new GPlane<GTexturedVertex>(md3dDevice, md3dContext, 512.0f, 1, 512.0f, 1));
	mSprites.back()->SetMaterialName("Space");
	mSprites.back()->Scale(0.4f);
	for (int i=1; i<100; i++)
	{
		mSprites.push_back(new GPlane<GTexturedVertex>(
			dynamic_cast<GPlane<GTexturedVertex>&>(*mSprites[0])));
		mSprites[i]->RotateY(mRandom.Gen(360.0f, 0.1f));
		mSprites[i]->Move(mRandom.Gen(100.0f, -100.0f), 0.0f, mRandom.Gen(100.0f, -100.0f));
		if (i%2 != 0)
		{
			mSprites[i]->SetMaterialName("Water");
		}
	}
	mSprites.push_back(new GPlane<GTexturedVertex>(md3dDevice, md3dContext, 1280.0f, 1, 1024.0f, 1));
	mSprites.back()->SetMaterialName("VS2012");
	mSprites.back()->Scale(0.4f);

	mSky = new GBillboard<GTexturedVertex>(md3dDevice, md3dContext, 2000.0f, 2000.0f,
		mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());
	mSky->Move(0.0f, 0.0f, 500.0f);
	mSky->SetMaterialName("SkyBackground");

	mColoredModelsAssembled = new GMesh<GColoredVertex>(md3dDevice, md3dContext);
	mColoredModelsAssembled->LoadFromObj(file);//LoadFromObj(file);
	//mColoredModelsAssembled->Scale(10.0f);
	mLightVector = new GMesh<GColoredVertex>(md3dDevice, md3dContext);
	mLightVector->LoadFromFile("Data/Models/arrow.mvci");
}

void DiffuseLight::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void DiffuseLight::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "UPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mCamera.GetPosition().x << " " << 
		mCamera.GetPosition().y << " " << mCamera.GetPosition().z << 
		"\nCamera target position: " << mCamera.GetTarget().x << " " <<
		mCamera.GetTarget().y << " " << mCamera.GetTarget().z <<
		"\nCamera Up vector: " << mCamera.GetUp().x << " " <<
		mCamera.GetUp().y << " " << mCamera.GetUp().z <<
		"\nLight direction: " << mDirLight.mDirection.x << " " <<
		mDirLight.mDirection.y << " " << mDirLight.mDirection.z;
	mText[0]->SetText(stream.str());

	// Camera controls:
	if (mInput.IsKeyDown('W'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('S'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('D'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('A'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown(VK_SPACE))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('X'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}

	// Update billboards position based on new camera:
	mSky->UpdatePosition(mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());

	// Light controls:
	if (mInput.IsKeyDown(VK_LEFT))
	{
		mDirLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mDirLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), -0.1f)));
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(-0.1f));
	}
	if (mInput.IsKeyDown(VK_RIGHT))
	{
		mDirLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mDirLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), 0.1f)));
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(0.1f));
	}		

	GApplication::mInput.UpdateMouseMove();
}

void DiffuseLight::drawScene()
{
    GApplication::drawScene();
	GApplication::TurnZBufferOn();

    // Reset shader matrices for this frame
	mpColoredShader->ClearConstantBuffer();
	mpFontShader->ClearConstantBuffer();
	mpTexturedShader->ClearConstantBuffer();

	////////////////////////////////////////
	// Draw everything colored
	////////////////////////////////////////	
	mpColoredShader->SetShader();
	mpColoredShader->DisableAlpha();
	mpColoredShader->mPOConstantBuffer.View = mCamera.GetView();
	mpColoredShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	mpColoredShader->mPOConstantBuffer.LightDirection = mDirLight.mDirection;
	mpColoredShader->mPOConstantBuffer.LightColor = mDirLight.mColor;

	mpColoredShader->mPOConstantBuffer.World = mColoredModelsAssembled->GetWorld();
	mpColoredShader->UpdateConstantBuffer();
	mColoredModelsAssembled->draw();

	mpColoredShader->mPOConstantBuffer.World = mLightVector->GetWorld();
	mpColoredShader->UpdateConstantBuffer();
	mLightVector->draw();

	////////////////////////////////////////
	// Draw everything textured
	////////////////////////////////////////

	//////////////////////
	// Draw 3D
	//mpTexturedShader->SetShader();
	//mpTexturedShader->DisableAlpha();
	//mpTexturedShader->mPOConstantBuffer.View = mCamera.GetView();
	//mpTexturedShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	//mpTexturedShader->mPOConstantBuffer.LightDirection = mDirLight.mDirection;
	//mpTexturedShader->mPOConstantBuffer.LightColor = mDirLight.mColor;

	//for (auto it=mSprites.begin(); it!=mSprites.end(); it++)
	//{
	//	// Try to set material if it's not active
	//	mpMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
	//	// Draw primitives with current material
	//	mpTexturedShader->mPOConstantBuffer.World = (*it)->GetWorld();
	//	mpTexturedShader->UpdateConstantBuffer();
	//	(*it)->draw();
	//}
	//
	//mpMaterialsManager->SetCurrentMaterial(mSky->GetMaterialName());
	//mpTexturedShader->mPOConstantBuffer.World = mSky->GetWorld();
	//mpTexturedShader->UpdateConstantBuffer();
	//mSky->draw();
	mpFontShader->SetShader();
	mpFontShader->DisableAlpha();
	mpFontShader->mPOConstantBuffer.View = mCamera.GetView();
	mpFontShader->mPOConstantBuffer.Projection = mCamera.GetProj();

	//for (auto it=mSprites.begin(); it!=mSprites.end(); it++)
	//{
	//	// Try to set material if it's not active
	//	mpMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
	//	// Draw primitives with current material
	//	mpFontShader->mPOConstantBuffer.World = (*it)->GetWorld();
	//	mpFontShader->UpdateConstantBuffer();
	//	(*it)->draw();
	//}

	//mpMaterialsManager->SetCurrentMaterial(mSky->GetMaterialName());
	//mpFontShader->mPOConstantBuffer.World = mSky->GetWorld();
	//mpFontShader->UpdateConstantBuffer();
	//mSky->draw();

	//////////////////////
	// Draw 2D
	GApplication::TurnZBufferOff();
	//mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->mPOConstantBuffer.View = GMathMF(XMMatrixIdentity());
	mpFontShader->mPOConstantBuffer.Projection = mCamera.GetOrtho();
	
	for (auto it = mText.begin(); it != mText.end(); it++)
	{
		mpMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
		mpFontShader->mPOConstantBuffer.World = (*it)->GetWorld();
		mpFontShader->UpdateConstantBuffer();
		(*it)->draw();
	}

    mSwapChain->Present(0, 0);
}

DiffuseLight::~DiffuseLight(void)
{
	delete mLightVector;
	delete mColoredModelsAssembled;

	std::for_each(mText.begin(), mText.end(), [](GText* item) { delete item; });
	std::for_each(mSprites.begin(), mSprites.end(), [](GMesh<GTexturedVertex>* item) { delete item; });

	delete mSky;

	delete mpMaterialsManager;
		
	delete mpTexturedShader;
	delete mpFontShader;
	delete mpColoredShader;

    if( md3dContext )
        md3dContext->ClearState();
}