#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GColoredVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GMaterialsManager.h"
#include "GBillboard.h"
#include "GShader.h"
#include "GText.h"
#include "GDirectionalLight.h"
#include "GObjFile.h"

using namespace Game;

// Class DiffuseLight.
class DiffuseLight : public GApplication
{
    // Public constructor and destructor
public:
    DiffuseLight(HINSTANCE hInstance);
    ~DiffuseLight(void);

public:
    void initApp();

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
	GMaterialsManager*			mpMaterialsManager;
	GShader<GColoredVertex, GSHADER_TYPE_DIR_LIGHT>*	mpColoredShader;
	GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>*		mpFontShader;
	GShader<GTexturedVertex, GSHADER_TYPE_DIR_LIGHT>*	mpTexturedShader;

private:
    GCamera	mCamera;	// main camera
    float	mDistance;	// camera position

	GRandom	mRandom; // random generator

	std::vector<GMesh<GTexturedVertex>*>mSprites;	
	GMesh<GColoredVertex>*				mColoredModelsAssembled;
	GBillboard<GTexturedVertex>*		mSky;
	std::vector<GText*>					mText;
	GDirectionalLight					mDirLight;
	GMesh<GColoredVertex>*				mLightVector;
};