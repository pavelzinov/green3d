#pragma once
#include "GUtility.h"

namespace Game
{

const uint32_t GSHADER_TYPE_SIMPLE = 0;
const uint32_t GSHADER_TYPE_DIR_LIGHT = 1;

template <uint32_t shader_type>
struct GShaderConstantBuffer
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
};

template <>
struct GShaderConstantBuffer<GSHADER_TYPE_SIMPLE>
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
};

template <>
struct GShaderConstantBuffer<GSHADER_TYPE_DIR_LIGHT>
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
	XMFLOAT3 LightDirection;
	float chunk;
	XMFLOAT4 LightColor;
};

}