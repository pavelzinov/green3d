#include "GObjFile.h"
using namespace Game;


bool GObjFile::Load(std::string filename)
{
	vector<string> file_lines;
	file_lines = GetFileLines(filename, file_lines);
	if (file_lines.size() == 0)
		return false;

	// Get vertices properties
	vector<string> string_parts;
	VertexPosition vertex_position;	
	VertexNormal vertex_normal;
	TextureCoordinate texture_coordinate;
	for (int i = 0; i < file_lines.size(); ++i)
	{
		string_parts = SplitString(file_lines[i], ' ');
		if (string_parts.size() == 0)
			continue;
		if (string_parts[0] == "v")
		{
			vertex_position.x = FloatFromString(string_parts[1]);
			vertex_position.y = FloatFromString(string_parts[2]);
			vertex_position.z = FloatFromString(string_parts[3]);
			mVertices.push_back(vertex_position);
		}
		else if (string_parts[0] == "vn")
		{
			vertex_normal.nx = FloatFromString(string_parts[1]);
			vertex_normal.ny = FloatFromString(string_parts[2]);
			vertex_normal.nz = FloatFromString(string_parts[3]);
			mNormals.push_back(vertex_normal);
		}
		else if (string_parts[0] == "vt")
		{
			texture_coordinate.u = FloatFromString(string_parts[1]);
			texture_coordinate.v = FloatFromString(string_parts[2]);
			mTexCoords.push_back(texture_coordinate);
		}
	}

	if (mVertices.size() == 0)
		return false;

	// Get faces description
	Face face;
	FaceVertex face_vertex;
	int normals_count = mNormals.size();
	int texcoord_count = mTexCoords.size();
	vector<string> vnt_indices;
	for (int i = 0; i < file_lines.size(); ++i)
	{
		string_parts = SplitString(file_lines[i], ' ');
		if (string_parts.size() == 0)
			continue;
		if (string_parts[0] == "f")
		{
			// first face vertex
			vnt_indices = SplitString(string_parts[1], '/');			
			face_vertex.mVertex = mVertices[IntFromString(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[IntFromString(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[1]) - 1];
			}			
			face.mVertices[0] = face_vertex;
			// second face vertex
			vnt_indices = SplitString(string_parts[2], '/');			
			face_vertex.mVertex = mVertices[IntFromString(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[IntFromString(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[1]) - 1];
			}
			face.mVertices[1] = face_vertex;
			// third face vertex
			vnt_indices = SplitString(string_parts[3], '/');			
			face_vertex.mVertex = mVertices[IntFromString(vnt_indices[0]) - 1];
			if (texcoord_count != 0)
			{
				face_vertex.mTexCoords = mTexCoords[IntFromString(vnt_indices[1]) - 1];
			}
			if (texcoord_count != 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[2]) - 1];
			}
			if (texcoord_count == 0 && normals_count != 0)
			{
				face_vertex.mNormal = mNormals[IntFromString(vnt_indices[1]) - 1];
			}
			face.mVertices[2] = face_vertex;
			// store face
			mFaces.push_back(face);
		}
	}

	return true;
}

bool GObjFile::Save(std::string filename)
{
	return true;
}

std::vector<std::string>& GObjFile::GetFileLines(std::string filename, std::vector<std::string>& file_lines)
{
	string line;
	ifstream model;
	model.open(filename);

	if (model.is_open())
	{
		while (model.good())
		{
			getline(model, line);
			file_lines.push_back(line);
		}
		model.close();

		// delete last empty character
		if (file_lines.back() == "")
			file_lines.pop_back();
	}
	return file_lines;
}