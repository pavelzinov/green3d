#pragma once
#include "GUtility.h"

////////////////////////////////////////////////////////
// Class GCamera
////////////////////////////////////////////////////////
namespace Game
{

class GCamera
{
public:

    GCamera(void);
    GCamera& operator=(const GCamera& camera);
    ~GCamera(void);

	void initViewMatrix(XMFLOAT3 pos, XMFLOAT3 target, XMFLOAT3 up);
	void initProjMatrix(const float angle, const float clientWidth, const float clientHeight, 
								const float nearest, const float farthest);

	// Camera parameters
    void SetPosition(XMFLOAT3 pos);
    void SetPosition(float x, float y, float z);
    XMFLOAT3 GetPosition();
    float GetPositionX();
    float GetPositionY();
    float GetPositionZ();

    void SetTarget(XMFLOAT3 target);
    void SetTarget(float x, float y, float z);
    XMFLOAT3 GetTarget();
    float GetTargetX();
    float GetTargetY();
    float GetTargetZ();

    void SetUp(XMFLOAT3 up);
    void SetUp(float x, float y, float z);
    XMFLOAT3 GetUp();
    float GetUpX();
    float GetUpY();
    float GetUpZ();

    XMMATRIX GetView();

	// Projection parameters
	void SetAngle(float angle);
	float GetAngle();

	void SetClientWidth(float clientWidth);
	void SetClientHeight(float clientHeigth);

	void SetNearestPlane(float nearest);
	void SetFarthestPlane(float farthest);

	XMMATRIX GetProj();

private:
    // Camera parameters
    XMFLOAT3 mPosition;
    XMFLOAT3 mTarget;
    XMFLOAT3 mUp;

	// Projection parameters
	float mAngle;
	float mClientWidth;
	float mClientHeight;
	float mNearest;
	float mFarthest;

    // View matrix
    XMMATRIX  mView;

	// Projective matrix
	XMMATRIX	mProj;
};

}