#pragma once

namespace Game
{

class GRandom
{
public:
    GRandom(void);
    ~GRandom(void);

public:
    double Generate();
    double GenerateInterval(int start = 0, int end = 1);
    int GenerateInteger(int start = 0, int end = 1);
};

} // namespace Game