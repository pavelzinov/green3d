#pragma once
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

#include "GUtility.h"
#include "GTriangle.h"

namespace Game
{

//TODO: create class members
template<class T>
class GModel
{
public:
	GModel(ID3D11Device *device, ID3D11DeviceContext *context);
	GModel(const GModel<T> &model);
	~GModel(void);

public:
	void addTriangle(GTriangle<T> triangle);
	void copyTriangles(std::vector<GTriangle<T> > triangles);
	void constructBuffers();
	void loadFromFile(std::string filename);

public:
	void drawTriangles();
	void draw();

private:
	std::vector<GTriangle<T> >	mTriangles;
	std::vector<T>				mVertices;
	std::vector<UINT>			mIndices;

	ID3D11Device		*mDevice;
	ID3D11DeviceContext	*mContext;
	ID3D11Buffer		*mVB;
	ID3D11Buffer		*mIB;
};

template<class T>
GModel<T>::GModel(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mDevice = device;
	mContext = context;
	mVB = nullptr;
	mIB = nullptr;
}

template<class T>
GModel<T>::GModel(const GModel<T> &model)
{
	this->mDevice = model.mDevice;
	this->mVertices = model.mVertices;
	this->mIndices = model.mIndices;
	this->constructBuffers();
}

template<class T>
GModel<T>::~GModel()
{
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);
}

template<class T>
void GModel<T>::addTriangle(GTriangle<T> triangle)
{
	mTriangles.push_back(triangle);
	mVertices.push_back(triangle.getVertices()[0]);
	mVertices.push_back(triangle.getVertices()[1]);
	mVertices.push_back(triangle.getVertices()[2]);
}

template<class T>
void GModel<T>::copyTriangles(std::vector<GTriangle<T> > triangles)
{
	mTriangles = triangles;
	for (int i=0; i<mTriangles.size(); i++)
	{
		mVertices.push_back(mTriangles.getVertices()[0]);
		mVertices.push_back(mTriangles.getVertices()[1]);
		mVertices.push_back(mTriangles.getVertices()[2]);
	}

	this->constructBuffers();
}

template<class T>
void GModel<T>::drawTriangles()
{
	for (int i=0; i<mTriangles.size(); i++)
	{
		mTriangles[i].draw();
	}
}

template<class T>
void GModel<T>::draw()
{
	UINT strides = T::getDataSize();
	UINT offset = 0;
	mContext->IASetVertexBuffers(0, 1, &mVB, &strides, &offset);
	mContext->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);
	mContext->DrawIndexed(mIndices.size(), 0, 0);
}

}