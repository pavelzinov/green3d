#pragma once
#include "GUtility.h"
#include <vector>

namespace Game
{

class GVertex
{
public:
	GVertex(void);
	GVertex(XMFLOAT3 position);
	virtual ~GVertex(void);

	static UINT getDataSize();

	XMFLOAT3 pos;
};

}

