#include "DXEmpty.h"

DXEmpty::DXEmpty(HINSTANCE hInstance) : GApplication(hInstance)
{
	// entry point in shader file system
	mShaderFileName = L"EmptyShader.fx";

    // init background (space) color
    GApplication::mClearColor = WHITE;

    // init transformation matrixes
	mWorld = XMMatrixIdentity();
	mView = XMMatrixIdentity();
	mProj = XMMatrixIdentity();
	mWVP = XMMatrixIdentity();

	mdx = mdy = mdz = 0.0f;
	mdMove = 60.0f;
    mdAngle = 240.0f;        // in degrees
	mAngle = 90.0f;
    mDistance = 100.0f;

	// Build the static view matrix.
	mCamera.initViewMatrix(XMFLOAT3(0.0f, 0.0f, -mDistance), 
		XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
	mCamera.initProjMatrix(0.25f * MATH_PI, mClientWidth, mClientHeight, 1.0f, 2000.0f);
}

void DXEmpty::initApp()
{
    GApplication::initApp();

	CreateShaders();
}

void DXEmpty::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(0.25f*MATH_PI, mClientWidth, mClientHeight, 1.0f, 500.0f);
}

void DXEmpty::updateScene(float dt)
{
    GApplication::updateScene(dt);


	GApplication::mInput.UpdateMouseMove();
}

void DXEmpty::drawScene()
{
    GApplication::drawScene();

    // set shader variables from cpp code

    mSwapChain->Present(0, 0);
}

HRESULT DXEmpty::CreateShaders()
{
	// Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile( mShaderFileName.c_str(), "VS", "vs_4_0", &pVSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the vertex shader
	hr = md3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader );
	if( FAILED( hr ) )
	{	
		pVSBlob->Release();
        return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
	hr = md3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if( FAILED( hr ) )
        return hr;

    // Set the input layout
    md3dContext->IASetInputLayout( mpVertexLayout );

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( mShaderFileName.c_str(), "PS", "ps_4_0", &pPSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the pixel shader
	hr = md3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
    if( FAILED( hr ) )
        return hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(mpShaderConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    hr = md3dDevice->CreateBuffer( &bd, NULL, &mpShaderConstantBuffer );
    if( FAILED( hr ) )
        return hr;

	return S_OK;
}

void DXEmpty::buildVertexLayouts()
{
}

HRESULT DXEmpty::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

DXEmpty::~DXEmpty(void)
{
    if( md3dContext )
        md3dContext->ClearState();

    ReleaseCOM(mpVertexLayout);
}