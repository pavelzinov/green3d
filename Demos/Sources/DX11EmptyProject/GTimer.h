#pragma once
#include <Windows.h>

namespace Game
{
	//***********************************************
	// ����� GTimer ����� ��� �����������:
	// 1) �������� ��������� ���������� ������� ����� ����� ���������, ��� ������� ������� ������������ ����� tick().
	// 2) ��������� �������, ���������� � ������� ������� �������� start(), � ����� ����������� ����� �������
	//	� ������� pause() � ������������� � ������� resume(). ����� ������������� �� start() ��� ����� ������� �����.
	// 3) ��� ��� ����������� ��������� � �� ������ ���� �� �����.
	//***********************************************
	class GTimer
	{
	public:

		GTimer();

		double getDeltaTime();		// ����� ����� �������� tick() �������
		double getDeltaTimeNow();	// ����� ����� �������� ������ ������� � ��������� ������� ������� tick()
		double getGameTime();		// ����� �� ������� ������� start() �� ������ ������ �������

		void start();				// ������/������������� �������� ������� ����
		void pause();				// ����� �������� ������� ����
		void resume();				// ������������� ������ �������
		void reset();				// ���������� ��� ���������� ������� � ��������� ���������
		void tick();				// ���������� ���������� ���������� ������� � 
										//������������ � ������� ��������� ������

		~GTimer();
	private:
		
		double mSecsPerCnt;		// ���������� ������� �� 1 ���� ��� �������� ����������
		double mDeltaTime;		// ����� ����� ����� ���������� �������� ������� tick()
		double mGameTime;		// ����������, �������� ����� � ������ ������� ������� ������� ����������� start()

		__int64 mStartCnt;		// ���������� ������ �� ����� �������/������������� ������� �������� start()
		__int64 mPauseCnt;		// ���������� ������ ��� ������ �����
		__int64 mResumeCnt;		// ���������� ������ ��� �������������

		__int64 mTickPrevCnt;
		__int64 mTickCurrCnt;

		__int64 mTmpCnt;		// ��� �������� ��������� �������� ��������� ��������
		bool isPaused;			// ��� �������� ��������� �������� ������� ����
	};

}