#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GSimpleVertex.h"
#include "GModel.h"

using namespace Game;

// Class DXEmpty.
// Does: Empty DX11 Windows 8 SDK project
class DXEmpty : public GApplication
{
    // Public constructor and destructor
public:
    DXEmpty(HINSTANCE hInstance);
    ~DXEmpty(void);

public:
    void initApp();
private:
    void onResize();
    void updateScene(float dt);
    void drawScene();

private:
    // Functions for working with shaders
    HRESULT CreateShaders(); 
    void buildVertexLayouts();
	std::wstring mShaderFileName; 
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

private:
    ID3D11InputLayout*	mpVertexLayout;
	ID3D11PixelShader*	mpPixelShader;
	ID3D11VertexShader*	mpVertexShader;
	ID3D11Buffer*		mpShaderConstantBuffer;

private:
    // Projection matrixes
    XMMATRIX	mWorld;
    XMMATRIX	mView;
    XMMATRIX	mProj;
    XMMATRIX	mWVP;

    // Camera
    GCamera	mCamera;
	float	mdx;
	float	mdy;
	float	mdz;
	float	mdMove;       // �������� �������� ���������������� ������ �� ����
    float	mdAngle;      // ������� �������� �������� ������
	float	mAngle;
    float	mDistance;    // ������ �������� ������

	// Random generator
	GRandom	mRandom;
};

