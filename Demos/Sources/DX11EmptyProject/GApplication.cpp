#include "GApplication.h"

namespace Game
{
	LRESULT CALLBACK MainWndProc (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static GApplication * app = 0; // �������� �� ���������� ����� ���������, ���� � ���������
		switch (msg)
		{
			case WM_CREATE:
			{// ��� �������� CreateWindow �������� this � �������� ����� ��������� CREATESTRUCT, 
				//��������� �� ������� ���������� � lParam, � ������� ��� � ������ ���������
				CREATESTRUCT* cs = (CREATESTRUCT*) lParam;
				app = (GApplication*) cs->lpCreateParams;
				return 0;
			}
		}

		if ( app )
			return app->msgProc(msg, wParam, lParam);
		else
			return DefWindowProc(hWnd, msg, wParam, lParam);		
	}

	GApplication::GApplication(HINSTANCE hInstance)
	{
		mhAppInst	= hInstance;
		mhMainWnd	= 0;
		mAppPaused	= false;
		mMinimized	= false;
		mMaximized	= false;
		mResizing	= false;

        mSampleCount = 1;
        mSampleQuality = 0;

		mFrameStats = L"";

		md3dDevice			= 0;
		mSwapChain			= 0;
		mDepthStencilBuffer = 0;
		mRenderTargetView	= 0;
		mDepthStencilView	= 0;

		mMainWndCaption = L"D3D11 Application";
		md3dDriverType	= D3D_DRIVER_TYPE_HARDWARE;
		mClearColor		= BLUE;
		mClientWidth	= 800;
		mClientHeight	= 600;
	}
	
	GApplication::~GApplication(void)
	{
		ReleaseCOM(mRenderTargetView);
		ReleaseCOM(mDepthStencilView);
		ReleaseCOM(mSwapChain);
		ReleaseCOM(mDepthStencilBuffer);
		ReleaseCOM(md3dContext);	
		ReleaseCOM(md3dDevice);		
	}

	HINSTANCE GApplication::getAppInst() 
	{
		return mhAppInst;
	}

	HWND GApplication::getMainHwnd()
	{
		return mhMainWnd;
	}

	int GApplication::run()
	{
		MSG msg = {0};

		mTimer.reset();
		mTimer.start();

		while(msg.message != WM_QUIT)
		{
			// If there are Window messages then process them.
			if(PeekMessage( &msg, 0, 0, 0, PM_REMOVE ))
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
			// Otherwise, do animation/game stuff.
			else
			{
				mTimer.tick();
				if( !mAppPaused )
				{
					updateScene(mTimer.getDeltaTime());
				}
				else
				{
					Sleep(50);
				}
				drawScene();
			}
		}

		return (int)msg.wParam;
	}

	void GApplication::initApp()
	{
		initMainWindow();
		initDirect3D();
        mInput.InitGameInput();
	}

	void GApplication::onResize()
	{
		// ������� ������ ����������
		ReleaseCOM(mRenderTargetView);
		ReleaseCOM(mDepthStencilView);
		ReleaseCOM(mDepthStencilBuffer);

		// Changing buffers size
		HR(mSwapChain->ResizeBuffers(1, mClientWidth, mClientHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0));
		// ������� Rendertargetview �� backbuffer, ������ �� swapchain
		ID3D11Texture2D *pBackBuffer = 0;
		HR(mSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer)));
		HR(md3dDevice->CreateRenderTargetView(pBackBuffer, 0, &mRenderTargetView));
		ReleaseCOM(pBackBuffer);

		// Create the depth/stencil buffer and view.
		D3D11_TEXTURE2D_DESC depthStencilBufferDesc; // depth/stencil buffer is just a 2D texture, 
		//so describe it's parameters for incoming creation
		depthStencilBufferDesc.ArraySize			= 1;
		depthStencilBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
		depthStencilBufferDesc.CPUAccessFlags		= 0;
		depthStencilBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilBufferDesc.Height				= mClientHeight;
		depthStencilBufferDesc.MipLevels			= 1;
		depthStencilBufferDesc.MiscFlags			= 0;
		depthStencilBufferDesc.SampleDesc.Count		= mSampleCount;
		depthStencilBufferDesc.SampleDesc.Quality	= mSampleQuality;
		depthStencilBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
		depthStencilBufferDesc.Width				= mClientWidth;

		md3dDevice->CreateTexture2D(&depthStencilBufferDesc, 0, &mDepthStencilBuffer); // creating 2D texture
																					// using previous description
		md3dDevice->CreateDepthStencilView(mDepthStencilBuffer, 0, &mDepthStencilView);

		md3dContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);

		//Set View port
		D3D11_VIEWPORT vp;
        vp.Height   = static_cast<float>(mClientHeight);
		vp.Width	= static_cast<float>(mClientWidth);
		vp.MaxDepth	= 1.0f;
		vp.MinDepth	= 0.0f;
		vp.TopLeftX	= 0;
		vp.TopLeftY	= 0;

		md3dContext->RSSetViewports(1, &vp);
	}

	void GApplication::updateScene(float dt)
	{
#if defined(DEBUG) || defined(_DEBUG) // work frame stats only in debug mode
		// Code computes the average frames per second, and also the
		// average time it takes to render one frame.

		static int frameCnt = 0; // static expand those variables on whole program lifetime instead of destructing 
		//after function has been done
		static float t_base = 0.0f;

		frameCnt++;

		// Compute averages over one second period.
		if( (mTimer.getGameTime() - t_base) >= 1.0f )
		{
			float fps = (float)frameCnt; // fps = frameCnt / 1
			float mspf = 1000.0f / fps;

			std::wostringstream outs;
			outs.precision(6);
			outs << L"FPS: " << fps << L"\n"
				 << "Milliseconds (per frame): " << mspf;

			// Save the stats in a string for output.
			mFrameStats = outs.str();

			// Reset for next average.
			frameCnt = 0;
			t_base  += 1.0f;
		}
#endif
	}
	
	void GApplication::drawScene()
	{
		float color[4] = {mClearColor.x, mClearColor.y, mClearColor.z, mClearColor.w };
		md3dContext->ClearRenderTargetView(mRenderTargetView, color);
		md3dContext->ClearDepthStencilView(mDepthStencilView, D3D10_CLEAR_DEPTH|D3D10_CLEAR_STENCIL, 1.0f, 0);
	}

	LRESULT GApplication::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
        case WM_INPUT:
            mInput.Update((HRAWINPUT) lParam);
            return 0;
        case WM_KEYDOWN:
            if (wParam == VK_ESCAPE)
            {
                PostQuitMessage(0);
            }
            return 0;

		// WM_ACTIVATE is sent when the window become active again
		case WM_ACTIVATE:
			if( LOWORD(wParam) == WA_INACTIVE )
			{
				mAppPaused = true;
				mTimer.pause();
			}
			else
			{
				mAppPaused = false;
				mTimer.resume();
			}
			return 0;

		// WM_ENTERSIZEMOVE is sent when the user grabs the resize bars.
		case WM_ENTERSIZEMOVE:
			mAppPaused = true;
			mResizing  = true;
			mTimer.pause();
			return 0;

		// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
		// Here we reset everything based on the new window dimensions.
		case WM_EXITSIZEMOVE:
			mAppPaused = false;
			mResizing  = false;
			mTimer.resume();
			onResize();
			return 0;

		// WM_DESTROY is sent when the window is being destroyed.
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;

		// Catch this message to prevent the window from becoming too small.
		case WM_GETMINMAXINFO:
			((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
			((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;
			return 0;

		case WM_SIZE:
			mClientWidth	= LOWORD(lParam);
			mClientHeight	= HIWORD(lParam);
			if( md3dDevice )
			{
				if( wParam == SIZE_MINIMIZED )
				{
					mAppPaused = true;
					mMinimized = true;
					mMaximized = false;
				}
				else if( wParam == SIZE_MAXIMIZED )
				{
					mAppPaused = false;
					mMinimized = false;
					mMaximized = true;
					onResize();
				}
				else if( wParam == SIZE_RESTORED )
				{				
					// Restoring from minimized state?
					if( mMinimized )
					{
						mAppPaused = false;
						mMinimized = false;
						onResize();
					}
					// Restoring from maximized state?
					else if( mMaximized )
					{
						mAppPaused = false;
						mMaximized = false;
						onResize();
					}
					else if( mResizing )
					{
						// If user is dragging the resize bars, we do not resize 
						// the buffers here because as the user continuously 
						// drags the resize bars, a stream of WM_SIZE messages are
						// sent to the window, and it would be pointless (and slow)
						// to resize for each WM_SIZE message received from dragging
						// the resize bars.  So instead, we reset after the user is 
						// done resizing the window and releases the resize bars, which 
						// sends a WM_EXITSIZEMOVE message.
					}
					else // API call such as SetWindowPos or mSwapChain->SetFullscreenState.
					{
						onResize();
					}
				}
			}
			return 0;
		}
		return DefWindowProc(mhMainWnd, msg, wParam, lParam);
	}

	void GApplication::initMainWindow()
	{
		WNDCLASS			wc;		
		wc.cbClsExtra		= 0;
		wc.cbWndExtra		= 0;
		wc.hbrBackground	= (HBRUSH)GetStockObject(WHITE_BRUSH);
		wc.hCursor			= LoadCursor(0, IDC_ARROW);
		wc.hIcon			= ExtractIcon(mhAppInst, L"ApplicationIcon.ico", 0);
		wc.hInstance		= mhAppInst;
		wc.lpfnWndProc		= MainWndProc;
		wc.lpszClassName	= L"BaseWindowClass";
		wc.lpszMenuName		= 0;
		wc.style			= CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClass(&wc))
		{
			MessageBox(mhMainWnd, L"Can't register window class", L"Error!", MB_OK);
			PostQuitMessage(0);
		}

		mhMainWnd = CreateWindowW(L"BaseWindowClass",
			mMainWndCaption.c_str(),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			mClientWidth + 16,
			mClientHeight + 38,
			0,
			0,
			mhAppInst,
			this
			);

		if (mhMainWnd==INVALID_HANDLE_VALUE)
		{
			MessageBox(mhMainWnd, L"Can't create a window", L"Error", MB_OK);
			PostQuitMessage(0);
		}

		ShowWindow(mhMainWnd, SW_SHOW);
		UpdateWindow(mhMainWnd);
	}

	HRESULT GApplication::initDirect3D()
	{
		HRESULT hr = S_OK;

		RECT rc;
		GetClientRect( mhMainWnd, &rc );
		UINT width = rc.right - rc.left;
		UINT height = rc.bottom - rc.top;

		UINT createDeviceFlags = 0;
#ifdef _DEBUG
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
		D3D_DRIVER_TYPE driverTypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,
		};
		UINT numDriverTypes = ARRAYSIZE( driverTypes );

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		UINT numFeatureLevels = ARRAYSIZE( featureLevels );

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory( &sd, sizeof(sd) );
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = mhMainWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
		{
			md3dDriverType = driverTypes[driverTypeIndex];
			hr = D3D11CreateDeviceAndSwapChain( NULL, 
												md3dDriverType, 
												NULL, 
												createDeviceFlags, 
												featureLevels, 
												numFeatureLevels,
												D3D11_SDK_VERSION, 
												&sd, 
												&mSwapChain, 
												&md3dDevice, 
												&md3dFeatureLevel, 
												&md3dContext );
			if( SUCCEEDED( hr ) )
				break;
		}

		onResize();

		return S_OK;
	}
}
