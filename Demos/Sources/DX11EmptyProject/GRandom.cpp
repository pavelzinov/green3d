#include "GRandom.h"
#include <ctime>
#include <cstdlib>

namespace Game
{

GRandom::GRandom(void)
{
    srand(time(NULL));
}


GRandom::~GRandom(void)
{
}

double GRandom::Generate()
{// 0 - 1
    return ((double) rand())/((double) RAND_MAX);
}

double GRandom::GenerateInterval(int start, int end)
{
    if (end <= start)
    {
        return -1.0f;
    }
    else
    {
        return (start + (end-start)*Generate());
    }
}

int GRandom::GenerateInteger(int start, int end)
{
    if (end <= start)
    {
        return -1;
    }
    else
    {
        return (rand()%(end-start+1)+start);
    }
}

} // namespace Game
