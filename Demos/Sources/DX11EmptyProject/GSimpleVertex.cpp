#include "GSimpleVertex.h"
using namespace Game;

GSimpleVertex::GSimpleVertex(const GSimpleVertex &vertex)
{
	this->pos = vertex.pos;
	this->color = vertex.color;
}

GSimpleVertex::GSimpleVertex(XMFLOAT3 position, XMFLOAT4 color) : GVertex(position)
{
	this->color = color;
}

GSimpleVertex::~GSimpleVertex(void)
{

}

UINT GSimpleVertex::getDataSize()
{
	return sizeof(GSimpleVertexData);
}
