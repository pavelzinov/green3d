#pragma once
#include "GModel.h"
using namespace Game;

template <>
void GModel<GSimpleVertex>::constructBuffers()
{
	UINT verticesNumber = mVertices.size();
	GSimpleVertexData *vertices = new GSimpleVertexData[verticesNumber];
	for (int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].pos;
		vertices[i].color = mVertices[i].color;
	}

	// Vertex Buffer
	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GSimpleVertex::getDataSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));

	delete[] vertices;

	// Indices
	//mIndices.clear();
	//for (int i=0; i<verticesNumber; i++)
	//{
	//	mIndices.push_back(i);
	//}

	// Index Buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(UINT) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}

template <>
void GModel<GSimpleVertex>::loadFromFile(std::string filename)
{
	std::string line;
	std::stringstream line_as_stream;
	std::ifstream model;
	model.open(filename);
	float x, y, z, r, g, b, a;
	std::vector<std::string> file;

	if (model.is_open())
	{
		while (model.good())
		{
			getline(model, line);
			file.push_back(line);
		}
		model.close();
	}
	else
	{
		return;
	}

	std::string vertex_type = file[0];
	int vertex_count = 0;
	line_as_stream << file[1];
	line_as_stream >> vertex_count;
	line_as_stream.clear();

	if (vertex_type == "GSimpleVertex")
	{
		// reading vertices
		mVertices.clear();
		for (int i=2; i < vertex_count + 2; i++)
		{
			line_as_stream << file[i];
			line_as_stream >> x >> y >> z >> r >> g >> b >> a;
			mVertices.push_back(GSimpleVertex(XMFLOAT3(x, y, z), 
				XMFLOAT4(r, g, b, a)));
			line_as_stream.clear();
		}

		// reading indices
		mIndices.clear();
		UINT i1 = 0, i2 = 0, i3 = 0;
		for (int i=vertex_count+2; i<file.size(); i++)
		{
			line_as_stream << file[i];
			line_as_stream >> i1 >> i2 >> i3;
			mIndices.push_back(i1 - 1);
			mIndices.push_back(i2 - 1);
			mIndices.push_back(i3 - 1);
			line_as_stream.clear();
		}
	}
	
	this->constructBuffers();	
}