#pragma once
#include "GUtility.h"
#include <gdiplus.h>

namespace Game
{
// Class to process BMP, JPEG and PNG file data.
// Stores each pixel value.
// Each pixel can be accessed like this: Data[x][y],
// where `x` corresponds to image's columns and `y` corresponds to image's
// rows. This class also stores image's parameters such as width and height 
// in pixels. This class utilizes GDI+
class GImage
{
public:
	GImage(void) { mWidth = 0; mHeight = 0; }
	~GImage(void) {}

private:
	// Pixel structure. For images which does not support alpha channel, `a` parameter will be equal to 255
	struct Pixel
	{
		float r, g, b, a;
	};

public:
	// Load BMP, JPEG or PNG file
	bool LoadBitmap(std::string filename);
	// Access image's pixels array by reference
	const std::vector<std::vector<Pixel> >& AccessData() { return mData; }
	// Get image's pixels
	std::vector<std::vector<Pixel> > GetData() { return mData; }
	// Get image's height
	const uint32_t GetHeight() { return mHeight; }
	// Get image's width
	const uint32_t GetWidth() { return mWidth; }

private:
	// Pixel storing two-dimensional array
	std::vector<std::vector<Pixel> > mData;
	// Image's width
	uint32_t mWidth;
	// Image's height
	uint32_t mHeight;

};

}