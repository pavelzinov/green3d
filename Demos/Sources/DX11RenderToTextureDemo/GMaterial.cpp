#include "GMaterial.h"
using namespace Game;

GMaterial::GMaterial(ID3D11Device *device, std::string material_name, std::string texture_file_name)
{
	mMaterialName = material_name;
	mTextureFileName = texture_file_name;
	mpDevice = device;
	mpTexture = nullptr;
	mpTextureResource = nullptr;
	mpTextureView = nullptr;
	mpSamplerState = nullptr;

	LoadFromFile(mTextureFileName);

	mTextureWidth = 0;
	mTextureHeight = 0;
}

GMaterial::GMaterial(ID3D11Device *device, std::string material_name, uint32_t texture_width, uint32_t texture_height)
{
	mMaterialName = material_name;
	mTextureFileName = "";
	mpDevice = device;
	mpTexture = nullptr;
	mpTextureResource = nullptr;
	mpTextureView = nullptr;
	mpSamplerState = nullptr;

	OnResize(texture_width, texture_height);

	mTextureWidth = texture_width;
	mTextureHeight = texture_height;
}

GMaterial::GMaterial(const GMaterial& material)
{
	mpTexture = nullptr;
	mpTextureResource = nullptr;
	mpTextureView = nullptr;
	mpSamplerState = nullptr;

	mpDevice = material.mpDevice;
	mMaterialName = material.mMaterialName;
	mTextureFileName = material.mTextureFileName;
	if (mTextureFileName != "")
	{
		LoadFromFile(mTextureFileName);
	}
	else if (mTextureWidth == 0 && mTextureHeight == 0)
	{
		OnResize(material.mTextureWidth, material.mTextureHeight);		
	}
	mTextureWidth = material.mTextureWidth;
	mTextureHeight = material.mTextureHeight;	
}

GMaterial::~GMaterial(void)
{
	ReleaseCOM(mpTexture);
	ReleaseCOM(mpTextureResource);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpSamplerState);
}

GMaterial& GMaterial::operator=(const GMaterial& material)
{
	ReleaseCOM(mpTextureResource);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpTexture);
	ReleaseCOM(mpSamplerState);

	mpDevice = material.mpDevice;
	mMaterialName = material.mMaterialName;
	mTextureFileName = material.mTextureFileName;
	if (mTextureFileName != "")
	{
		LoadFromFile(mTextureFileName);
	}
	else if (mTextureWidth == 0 && mTextureHeight == 0)
	{
		OnResize(material.mTextureWidth, material.mTextureHeight);		
	}
	mTextureWidth = material.mTextureWidth;
	mTextureHeight = material.mTextureHeight;	

	return *this;
}

HRESULT GMaterial::LoadFromFile(std::string textureFileName)
{
	if (textureFileName == "")
		return E_FAIL;

	mTextureFileName = textureFileName;
	mTextureWidth = 0;
	mTextureHeight = 0;

	ReleaseCOM(mpTexture);
	ReleaseCOM(mpTextureResource);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpSamplerState);

	HRR(CreateDDSTextureFromFile(mpDevice, StringToWstring(mTextureFileName).c_str(), 
		&mpTextureResource, &mpTextureView));

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRR(mpDevice->CreateSamplerState(&samplerDesc, &mpSamplerState));

	return S_OK;
}

void GMaterial::OnResize(uint32_t texture_width, uint32_t texture_height)
{
	if (texture_width == 0 || texture_height == 0)
		return;

	ReleaseCOM(mpTexture);
	ReleaseCOM(mpTextureResource);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpSamplerState);

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = texture_width;
	textureDesc.Height = texture_height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	// Create the render target texture.
	HR(mpDevice->CreateTexture2D(&textureDesc, NULL, &mpTexture));

	// Setup the description of the shader resource view.
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	HR(mpDevice->CreateShaderResourceView(mpTexture, 
		&shaderResourceViewDesc, &mpTextureView));

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR(mpDevice->CreateSamplerState(&samplerDesc, &mpSamplerState));
}
