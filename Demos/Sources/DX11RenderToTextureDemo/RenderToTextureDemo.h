#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GColoredVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GMaterialsManager.h"
#include "GBillboard.h"
#include "GShader.h"
#include "GText.h"
#include "GLight.h"
#include "GObjFile.h"
#include "GImage.h"

using namespace Game;

// Class RenderToTextureDemo.
class RenderToTextureDemo : public GApplication
{
    // Public constructor and destructor
public:
    RenderToTextureDemo(HINSTANCE hInstance);
    ~RenderToTextureDemo(void);

public:
    void initApp();

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
	GMaterialsManager*									mpMaterialsManager;
	GShader<GColoredVertex, GSHADER_TYPE_LIGHT>*		mpColoredShader;
	GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>*		mpFontShader;
	GShader<GTexturedVertex, GSHADER_TYPE_LIGHT>*		mpTexturedShader;
	GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>*		mpLightMappedShader;

private:
    GCamera	mCamera;	// main camera
    float	mDistance;	// camera position
	GRandom	mRandom;	// random generator

	std::vector<GMesh<GTexturedVertex>*>	mTexturedModels;	
	std::vector<GMesh<GColoredVertex>*>		mColoredModels;
	
	std::vector<GText*>						mText;

	GMesh<GColoredVertex>*					mLightVector;
	GLight						mLight;
	
};