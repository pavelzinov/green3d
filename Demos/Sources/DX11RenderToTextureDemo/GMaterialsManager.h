#pragma once
#include "GUtility.h"
#include "GMaterial.h"

namespace Game
{
////////////////////////////////////////////////////////
// Manager designed for storing all game materials in
// single place, calling them when needed through special
// string identifiers. Those identifiers are created by
// this manager when material is added to collection.
// Meshes and other primitives are storing those
// identifiers instead of Material itself.
////////////////////////////////////////////////////////
// Additionaly, materials manager is responsible for
// rendering to texture, instead of screen. To enable this functionality,
// call InitRenderingToTexture - this will create texture and render target views.
// Don't forget to call OnResize, when window change it's size, so render target texture
// have to be resized. Call RenderToTexture to enable rendering to texture and call
// RenderToScreen to restore default rendering mechanism. Texture, that has been
// made by rendering to texture can be set into shader via SetCurrentMaterial("RenderTexture0")
////////////////////////////////////////////////////////
class GMaterialsManager
{
public:
	GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context);
	~GMaterialsManager(void);

public:
	bool AddMaterial(GMaterial& material);
	bool AddMaterial(std::string material_name, std::string texture_file);
	GMaterial& GetMaterial(std::string material_name) { return mMaterials.at(material_name); }
	std::vector<std::string> GetMaterialNames();
	void SetCurrentMaterial(std::string material_name);
	void SetCurrentMaterials(std::vector<std::string> material_names);
	
	HRESULT AddMaterial(std::string material_name, ID3D11RenderTargetView* window_render_target_view, 
		ID3D11DepthStencilView* depth_stencil_view, uint32_t texture_width, uint32_t texture_height);
	void RenderToTexture(XMFLOAT4& clear_color);
	void RenderToScreen();
	void OnResize(std::string material_name, ID3D11RenderTargetView* window_render_target_view, 
		ID3D11DepthStencilView* depth_stencil_view, uint32_t texture_width, uint32_t texture_height);

private:
	std::map<std::string, GMaterial>	mMaterials;
	std::string							mCurrentMaterialName;
	std::vector<std::string>			mCurrentMaterialNames;

private:
	ID3D11Device*	mpDevice;
	ID3D11DeviceContext* mpContext;

	ID3D11RenderTargetView*		mpWindowRenderTargetView;
	ID3D11DepthStencilView*		mpDepthStencilView;
	ID3D11RenderTargetView*		mpRenderTagetView;
};

}

