#pragma once
#include "GPlane.h"

namespace Game
{
template <class T>
class GBillboard : public GPlane<T>
{
public:
	GBillboard(ID3D11Device *device, ID3D11DeviceContext *context, float width, float height, 
		XMFLOAT3 camera_position, XMFLOAT3 camera_target, XMFLOAT3 camera_up);
	GBillboard(GBillboard& billboard);
	~GBillboard(void) {}

public:
	// Update billboard position based on new camera properties
	void UpdatePosition(XMFLOAT3 new_camera_position, XMFLOAT3 new_camera_target, 
		XMFLOAT3 new_camera_up);

protected:
	XMFLOAT3	mNormal;
	XMFLOAT3	mUp;
};

template <class T>
GBillboard<T>::GBillboard(ID3D11Device *device, ID3D11DeviceContext *context, float width, float height, 
		XMFLOAT3 camera_position, XMFLOAT3 camera_target, XMFLOAT3 camera_up) : GPlane<T>(device, context, width, 1, height, 1)
{
	mNormal = XMFLOAT3(0.0f, 1.0f, 0.0f); // initially plane is placed on xz surface
	mUp = XMFLOAT3(0.0f, 0.0f, 1.0f);

	// rotate to align normal and up
	XMFLOAT3 look_at_camera = GMathVF(GMathFV(camera_position) - GMathFV(camera_target));
	float angle = XMConvertToDegrees(XMVectorGetX(
		XMVector3AngleBetweenNormals(XMVector3Normalize(GMathFV(mNormal)), 
		XMVector3Normalize(GMathFV(look_at_camera)))));
	angle = GMathFloatRound(angle);
	if (angle == 0.0f || angle == 360.0f)
	{
		mNormal = GMathVF(XMVector3Normalize(GMathFV(look_at_camera)));
	}
	else if (angle == 180.0f)
	{
		mNormal = GMathVF(XMVector3Normalize(XMVectorZero() - GMathFV(look_at_camera)));
	}
	else
	{
		XMFLOAT3 axis = GMathVF(XMVector3Normalize(XMVector3Cross(GMathFV(mNormal), 
			GMathFV(look_at_camera))));
		this->Rotate(axis, angle);
		mNormal = GMathVF(XMVector3Normalize(XMVector3TransformNormal(GMathFV(mNormal), 
			XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(angle)))));
		mUp = GMathVF(XMVector3Normalize(XMVector3TransformNormal(GMathFV(mUp), 
			XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(angle)))));
	}
}

template <class T>
GBillboard<T>::GBillboard(GBillboard& billboard) : GMesh<T>(dynamic_cast<GMesh&>(billboard))
{
	mNormal = billboard.mNormal;
	mUp		= billboard.mUp;
}

template <class T>
void GBillboard<T>::UpdatePosition(XMFLOAT3 camera_position, 
								   XMFLOAT3 camera_target, XMFLOAT3 camera_up)
{
	// rotate to align normal and up
	XMFLOAT3 look_at_camera = GMathVF(GMathFV(camera_position) - GMathFV(camera_target));
	float angle = XMConvertToDegrees(XMVectorGetX(
		XMVector3AngleBetweenNormals(XMVector3Normalize(GMathFV(mNormal)), 
		XMVector3Normalize(GMathFV(look_at_camera)))));
	angle = GMathFloatRound(angle);
	if (angle == 0.0f || angle == 360.0f)
	{
		mNormal = GMathVF(XMVector3Normalize(GMathFV(look_at_camera)));
	}
	else if (angle == 180.0f)
	{
		mNormal = GMathVF(XMVector3Normalize(XMVectorZero() - GMathFV(look_at_camera)));
	}
	else
	{
		XMFLOAT3 axis = GMathVF(XMVector3Normalize(XMVector3Cross(GMathFV(mNormal), 
			GMathFV(look_at_camera))));
		this->Rotate(axis, angle);
		mNormal = GMathVF(XMVector3Normalize(XMVector3TransformNormal(GMathFV(mNormal), 
			XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(angle)))));
		mUp = GMathVF(XMVector3Normalize(XMVector3TransformNormal(GMathFV(mUp), 
			XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(angle)))));
	}
}

}