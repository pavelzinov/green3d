#include "GMaterialsManager.h"
using namespace Game;

GMaterialsManager::GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mpDevice = device;
	mpContext = context;
	mCurrentMaterialName = "";

	mpRenderTagetView = nullptr;
	mpWindowRenderTargetView = nullptr;
	mpDepthStencilView = nullptr;
}

GMaterialsManager::~GMaterialsManager(void)
{
	ReleaseCOM(mpRenderTagetView);
}

bool GMaterialsManager::AddMaterial(GMaterial& material)
{
	auto names = GetMaterialNames();
	if (std::find(names.begin(), names.end(), material.GetMaterialName()) != names.end())
		return false;

	mMaterials.insert(std::pair<std::string, GMaterial>(material.GetMaterialName(), material));
	return true;
}

bool GMaterialsManager::AddMaterial(std::string material_name, std::string texture_file)
{
	auto names = GetMaterialNames();
	if (std::find(names.begin(), names.end(), material_name) != names.end())
		return false;

	GMaterial material(mpDevice, material_name, texture_file); 
	mMaterials.insert(std::pair<std::string, GMaterial>(material_name, material));
	return true;
}

std::vector<std::string> GMaterialsManager::GetMaterialNames()
{
	std::vector<std::string> names;

	for (auto it = mMaterials.begin(); it != mMaterials.end(); it++)
	{
		names.push_back(it->first);
	}

	return names;
}

void GMaterialsManager::SetCurrentMaterial(std::string material_name)
{
	if (material_name != mCurrentMaterialName)
	{
		ID3D11ShaderResourceView * resources[1];
		resources[0] = GetMaterial(material_name).GetTextureView();
		mpContext->PSSetShaderResources(0, ARRAYSIZE(resources), resources);
		ID3D11SamplerState * samplers[1];
		samplers[0] = GetMaterial(material_name).GetSampler();
		mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);

		mCurrentMaterialNames.clear();
	}
}

void GMaterialsManager::SetCurrentMaterials(std::vector<std::string> material_names)
{
	// sort input materials for future comparison
	std::sort(material_names.begin(), material_names.end(), [](std::string& first, std::string& second) {
		return first < second;
	});
	// compare with current materials
	if (material_names != mCurrentMaterialNames)
	{
		// copy materials to current
		mCurrentMaterialNames.clear();
		mCurrentMaterialNames.resize(material_names.size());
		std::copy(material_names.begin(), material_names.end(), mCurrentMaterialNames.begin());

		// get shader resources for each current material
		std::vector<ID3D11ShaderResourceView*> resources;
		for (auto material_name : mCurrentMaterialNames)
		{
			resources.push_back(GetMaterial(material_name).GetTextureView());
		}
		// set shader resources
		mpContext->PSSetShaderResources(0, resources.size(), &resources[0]);
		// WARNING: this is only for shaders with single sampler for multiple 2d textures!!!!
		ID3D11SamplerState * samplers[1];
		samplers[0] = GetMaterial(mCurrentMaterialNames[0]).GetSampler();
		mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);

		mCurrentMaterialName = "";
	}
}

HRESULT GMaterialsManager::AddMaterial(std::string material_name, 
	ID3D11RenderTargetView* window_render_target_view, ID3D11DepthStencilView* depth_stencil_view, 
	uint32_t texture_width, uint32_t texture_height)
{
	auto names = GetMaterialNames();
	if (std::find(names.begin(), names.end(), material_name) != names.end())
		return E_FAIL;

	GMaterial material(mpDevice, material_name, texture_width, texture_height); 
	mMaterials.insert(std::pair<std::string, GMaterial>(material_name, material));

	OnResize(material_name, window_render_target_view, depth_stencil_view, texture_width, 
		texture_height);

	return S_OK;
}

void GMaterialsManager::RenderToTexture(XMFLOAT4& clear_color)
{
	mpContext->OMSetRenderTargets(1, &mpRenderTagetView, mpDepthStencilView);
	mpContext->ClearRenderTargetView(mpRenderTagetView, reinterpret_cast<float*>(&clear_color));
	mpContext->ClearDepthStencilView(mpDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void GMaterialsManager::RenderToScreen()
{
	mpContext->OMSetRenderTargets(1, &mpWindowRenderTargetView, mpDepthStencilView);
}

void GMaterialsManager::OnResize(std::string material_name, 
	ID3D11RenderTargetView* window_render_target_view, ID3D11DepthStencilView* depth_stencil_view, 
	uint32_t texture_width, uint32_t texture_height)
{
	mpWindowRenderTargetView = window_render_target_view;
	mpDepthStencilView = depth_stencil_view;

	ReleaseCOM(mpRenderTagetView);

	// Create the render target texture.
	this->GetMaterial(material_name).OnResize(texture_width, texture_height);
	ID3D11Texture2D *texture = this->GetMaterial(material_name).GetTexture2D();

	// Setup the description of the render target view.
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	// Create the render target view.
	HR(mpDevice->CreateRenderTargetView(texture, 
		&renderTargetViewDesc, &mpRenderTagetView));
}