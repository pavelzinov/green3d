#include "GLight.h"
using namespace Game;

GLight::GLight(void)
{
	mPosition = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mAmbientColor = GCOLOR_BEACH_SAND;
	mDiffuseColor = mAmbientColor;
	mSpecularColor = mAmbientColor;
	mAttenuationParameters = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mSpotPower = 0.0f;
	mRange = 0.0f;
	mType = GLIGHT_TYPE_PARALLEL;
}