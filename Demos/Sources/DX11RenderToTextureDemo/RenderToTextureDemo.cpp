#include "RenderToTextureDemo.h"
#include "DDSTextureLoader.h"

RenderToTextureDemo::RenderToTextureDemo(HINSTANCE hInstance) : GApplication(hInstance)
{
    // init background color
    GApplication::mClearColor = GCOLOR_BEACH_SAND;

    mDistance = 100.0f;

	mCamera.SetPosition(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.SetTarget(XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 100.0f);	
}

void RenderToTextureDemo::initApp()
{ 
	GApplication::initApp();

	// Load shaders
	mpColoredShader = new GShader<GColoredVertex, GSHADER_TYPE_LIGHT>(md3dDevice, md3dContext);
	mpColoredShader->LoadShader("Data/Shaders/colored_light.fx");
	mpFontShader = new GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpFontShader->LoadShader("Data/Shaders/textured.fx");
	mpTexturedShader = new GShader<GTexturedVertex, GSHADER_TYPE_LIGHT>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("Data/Shaders/tex_bump_light.fx");
	mpLightMappedShader = new GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpLightMappedShader->LoadShader("Data/Shaders/tex_light_mapping.fx");

	// Load materials
	mpMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mpMaterialsManager->AddMaterial("Space", "Data/Textures/space.dds");
	mpMaterialsManager->AddMaterial("Water", "Data/Textures/water.dds");
	mpMaterialsManager->AddMaterial("VS2012", "Data/Textures/visual_studio_2012.dds");
	mpMaterialsManager->AddMaterial("Courier New 12pt Dark Green", "Data/Fonts/courier_new_12_dark_green.dds");
	mpMaterialsManager->AddMaterial("Segoe UI Semilight 10pt Yellow", "Data/Fonts/segoe_semilight_10_yellow.dds");
	mpMaterialsManager->AddMaterial("Brick", "Data/Textures/brick.dds");
	mpMaterialsManager->AddMaterial("Brick Normal Map", "Data/Textures/brick_bump.dds");
	mpMaterialsManager->AddMaterial("Circle Light Map", "Data/Textures/brick_light_map.dds");
	mpMaterialsManager->AddMaterial("RenderedTexture0", mpWindowRenderTargetView, mDepthStencilView, 
		mClientWidth, mClientHeight);

	// Load light
	mLight.mType = GLIGHT_TYPE_SPOT;

	mLight.mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mLight.mPosition = XMFLOAT3(-mDistance/2.0f, 100.0f, -mDistance/2.0f);
	mLight.mDiffuseColor = GCOLOR_WHITE;
	mLight.mAmbientColor = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	mLight.mSpecularColor = GCOLOR_BLACK;
	mLight.mAttenuationParameters = XMFLOAT3(0.0f, 0.01f, 0.0f);
	mLight.mSpotPower = 8.0f;
	mLight.mRange = 300.0f;
	
	// Load light's representation
	mLightVector = new GMesh<GColoredVertex>(md3dDevice, md3dContext);
	mLightVector->LoadFromObj("Data/Models/arrow.obj");

	// Load text
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		""));
	mText.back()->SetPosition(XMUINT2(4, 4), mClientWidth, mClientHeight);
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		"Use RIGHT and LEFT arrow keys to rotate light's direction"));
	mText.back()->SetPosition(XMUINT2(4, mClientHeight - 
		2 * mText.back()->GetSymbolHeight()), mClientWidth, mClientHeight);

	// Load models
	mColoredModels.push_back(new GMesh<GColoredVertex>(md3dDevice, md3dContext));
	mColoredModels.back()->LoadFromObj("Data/Models/cube.obj");

	mTexturedModels.push_back(new GMesh<GTexturedVertex>(md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/plane.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Scale(3.0f);

	mTexturedModels.push_back(new GMesh<GTexturedVertex>(md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/cube.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Move(50.0f, 0.0f, 0.0f);

	mTexturedModels.push_back(new GMesh<GTexturedVertex>(md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/plane.obj");
	mTexturedModels.back()->SetMaterialName("RenderedTexture0");
	mTexturedModels.back()->Scale(3.0f);
	mTexturedModels.back()->RotateX(-90.0f);
	//mTexturedModels.back()->Move(0.0f, 0.0f, -50.0f);
}

void RenderToTextureDemo::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 100.0f);
	// Update text when resizing window
	mText[0]->SetPosition(XMUINT2(mText[0]->GetPosition().x, mText[0]->GetPosition().y), 
		mClientWidth, mClientHeight);
	mText[1]->SetPosition(XMUINT2(4, mClientHeight - 
		2 * mText[1]->GetSymbolHeight()), mClientWidth, mClientHeight);
	// Resize render texture
	mpMaterialsManager->OnResize("RenderedTexture0", mpWindowRenderTargetView, mDepthStencilView, 
		mClientWidth, mClientHeight);
}

void RenderToTextureDemo::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "UPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mCamera.GetPosition().x << " " << 
		mCamera.GetPosition().y << " " << mCamera.GetPosition().z << 
		"\nCamera target position: " << mCamera.GetTarget().x << " " <<
		mCamera.GetTarget().y << " " << mCamera.GetTarget().z <<
		"\nCamera Up vector: " << mCamera.GetUp().x << " " <<
		mCamera.GetUp().y << " " << mCamera.GetUp().z <<
		"\nLight direction: " << mLight.mDirection.x << " " <<
		mLight.mDirection.y << " " << mLight.mDirection.z <<
		"\nMouse X: " << mInput.GetAbsoluteX() << " Y: " << mInput.GetAbsoluteY();
	mText[0]->SetText(stream.str());

	// Camera controls:
	if (mInput.IsKeyDown('W'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('S'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('D'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('A'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown(VK_SPACE))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('X'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y -= 2.0f;
		mCamera.SetPosition(new_camera_position);
	}

	// Update billboards position based on new camera:
	//mSky->UpdatePosition(mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());

	// Light controls:
	if (mInput.IsKeyDown(VK_LEFT))
	{
		mLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), -0.1f)));
		mLight.mPosition.x -= 3.0f;
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(-0.1f));
	}
	if (mInput.IsKeyDown(VK_RIGHT))
	{
		mLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), 0.1f)));
		mLight.mPosition.x += 3.0f;
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(0.1f));
	}

	GApplication::mInput.UpdateMouseMove();
}

void RenderToTextureDemo::drawScene()
{
	GApplication::drawScene();

	//mpMaterialsManager->RenderToTexture(const_cast<XMFLOAT4&>(GCOLOR_LIGHT_YELLOW_GREEN));

	//GApplication::TurnZBufferOn();

 //   // Reset shader matrices for this frame
	//mpColoredShader->ClearConstantBuffer();
	//mpFontShader->ClearConstantBuffer();
	//mpTexturedShader->ClearConstantBuffer();
	//mpLightMappedShader->ClearConstantBuffer();

	//mpColoredShader->SetShader();
	//mpColoredShader->DisableAlpha();

	//mpColoredShader->mPOConstantBuffer.View = mCamera.GetView();
	//mpColoredShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	//mpColoredShader->mPOConstantBuffer.Light.Position = mLight.mPosition;
	//mpColoredShader->mPOConstantBuffer.Light.DiffuseColor = mLight.mDiffuseColor;
	//mpColoredShader->mPOConstantBuffer.Light.AmbientColor = mLight.mAmbientColor;
	//mpColoredShader->mPOConstantBuffer.Light.AttenuationParameters = mLight.mAttenuationParameters;
	//mpColoredShader->mPOConstantBuffer.Light.Range = mLight.mRange;
	//mpColoredShader->mPOConstantBuffer.LightType = mLight.mType;

	//for (auto model : mColoredModels)
	//{
	//	mpColoredShader->mPOConstantBuffer.World = model->GetWorld();
	//	mpColoredShader->UpdateConstantBuffer();
	//	model->draw();
	//}
	//mpColoredShader->mPOConstantBuffer.World = mLightVector->GetWorld();
	//mpColoredShader->UpdateConstantBuffer();
	//mLightVector->draw();

	//mpTexturedShader->SetShader();
	//mpTexturedShader->EnableAlpha();

	//mpTexturedShader->mPOConstantBuffer.View = mCamera.GetView();
	//mpTexturedShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	//mpTexturedShader->mPOConstantBuffer.Light.Position = mLight.mPosition;
	//mpTexturedShader->mPOConstantBuffer.Light.DiffuseColor = mLight.mDiffuseColor;
	//mpTexturedShader->mPOConstantBuffer.Light.AmbientColor = mLight.mAmbientColor;
	//mpTexturedShader->mPOConstantBuffer.Light.AttenuationParameters = mLight.mAttenuationParameters;
	//mpTexturedShader->mPOConstantBuffer.Light.Range = mLight.mRange;
	//mpTexturedShader->mPOConstantBuffer.LightType = mLight.mType;

	//std::vector<std::string> materials;
	//materials.push_back("Brick"); materials.push_back("Brick Normal Map");
	//mpMaterialsManager->SetCurrentMaterials(materials);
	//for (uint32_t i = 0; i < 2; ++i)
	//{
	//	mpTexturedShader->mPOConstantBuffer.World = mTexturedModels[i]->GetWorld();
	//	mpTexturedShader->UpdateConstantBuffer();
	//	mTexturedModels[i]->draw();
	//}

	//mpMaterialsManager->RenderToScreen();
	GApplication::ClearScreen();

	GApplication::TurnZBufferOn();

	// Reset shader matrices for this frame
	mpColoredShader->ClearConstantBuffer();
	mpFontShader->ClearConstantBuffer();
	mpTexturedShader->ClearConstantBuffer();
	mpLightMappedShader->ClearConstantBuffer();

	mpColoredShader->SetShader();
	mpColoredShader->DisableAlpha();

	mpColoredShader->mPOConstantBuffer.View = mCamera.GetView();
	mpColoredShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	mpColoredShader->mPOConstantBuffer.CameraPosition = mCamera.GetPosition();

	mpColoredShader->mPOConstantBuffer.Light.Direction = mLight.mDirection;
	mpColoredShader->mPOConstantBuffer.Light.Position = mLight.mPosition;
	mpColoredShader->mPOConstantBuffer.Light.DiffuseColor = mLight.mDiffuseColor;
	mpColoredShader->mPOConstantBuffer.Light.AmbientColor = mLight.mAmbientColor;
	mpColoredShader->mPOConstantBuffer.Light.SpecularColor = mLight.mSpecularColor;
	mpColoredShader->mPOConstantBuffer.Light.AttenuationParameters = mLight.mAttenuationParameters;
	mpColoredShader->mPOConstantBuffer.Light.SpotPower = mLight.mSpotPower;
	mpColoredShader->mPOConstantBuffer.Light.Range = mLight.mRange;
	mpColoredShader->mPOConstantBuffer.LightType = mLight.mType;

	for (auto model : mColoredModels)
	{
		mpColoredShader->mPOConstantBuffer.World = model->GetWorld();
		mpColoredShader->UpdateConstantBuffer();
		model->draw();
	}
	mpColoredShader->mPOConstantBuffer.World = mLightVector->GetWorld();
	mpColoredShader->UpdateConstantBuffer();
	mLightVector->draw();

	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();

	mpTexturedShader->mPOConstantBuffer.View = mCamera.GetView();
	mpTexturedShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	mpTexturedShader->mPOConstantBuffer.CameraPosition = mCamera.GetPosition();

	mpTexturedShader->mPOConstantBuffer.Light.Direction = mLight.mDirection;
	mpTexturedShader->mPOConstantBuffer.Light.Position = mLight.mPosition;
	mpTexturedShader->mPOConstantBuffer.Light.DiffuseColor = mLight.mDiffuseColor;
	mpTexturedShader->mPOConstantBuffer.Light.AmbientColor = mLight.mAmbientColor;
	mpTexturedShader->mPOConstantBuffer.Light.SpecularColor = mLight.mSpecularColor;
	mpTexturedShader->mPOConstantBuffer.Light.AttenuationParameters = mLight.mAttenuationParameters;
	mpTexturedShader->mPOConstantBuffer.Light.SpotPower = mLight.mSpotPower;
	mpTexturedShader->mPOConstantBuffer.Light.Range = mLight.mRange;
	mpTexturedShader->mPOConstantBuffer.LightType = mLight.mType;

	std::vector<std::string> materials;
	materials.push_back("Brick"); materials.push_back("Brick Normal Map");
	mpMaterialsManager->SetCurrentMaterials(materials);
	for (uint32_t i = 0; i < 2; ++i)
	{
		mpTexturedShader->mPOConstantBuffer.World = mTexturedModels[i]->GetWorld();
		mpTexturedShader->UpdateConstantBuffer();
		mTexturedModels[i]->draw();
	}

	// Draw 2D Text	
	mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->mPOConstantBuffer.View = GMathMF(XMMatrixIdentity());
	mpFontShader->mPOConstantBuffer.Projection = mCamera.GetOrtho();

	mpMaterialsManager->SetCurrentMaterial(mTexturedModels[2]->GetMaterialName());
	mpFontShader->mPOConstantBuffer.World = mTexturedModels[2]->GetWorld();
	mpFontShader->UpdateConstantBuffer();
	mTexturedModels[2]->draw();

	
	for (auto text : mText)
	{
		mpMaterialsManager->SetCurrentMaterial(text->GetMaterialName());
		mpFontShader->mPOConstantBuffer.World = text->GetWorld();
		mpFontShader->UpdateConstantBuffer();
		text->draw();
	}

    mSwapChain->Present(0, 0);
}

RenderToTextureDemo::~RenderToTextureDemo(void)
{
	delete mLightVector;

	std::for_each(mText.begin(), mText.end(), [](GText* item) { delete item; });
	std::for_each(mColoredModels.begin(), mColoredModels.end(), 
		[](GMesh<GColoredVertex>* item) { delete item; });
	std::for_each(mTexturedModels.begin(), mTexturedModels.end(), 
		[](GMesh<GTexturedVertex>* item) { delete item; });

	delete mpMaterialsManager;
	
	delete mpLightMappedShader;
	delete mpTexturedShader;
	delete mpFontShader;
	delete mpColoredShader;

    if( md3dContext )
        md3dContext->ClearState();
}