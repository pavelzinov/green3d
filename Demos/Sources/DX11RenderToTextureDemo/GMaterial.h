#pragma once
#include "GUtility.h"
#include "DDSTextureLoader.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores View and Projection matrices used by shaders
// to translate 3D world into 2D screen surface
// Camera can be moved
////////////////////////////////////////////////////////
class GMaterial
{
public:
	GMaterial(ID3D11Device *device, std::string material_name, std::string texture_file_name);
	GMaterial(ID3D11Device *device, std::string material_name, uint32_t texture_width, uint32_t texture_height);
	GMaterial(const GMaterial& material);
	~GMaterial(void);
	GMaterial& operator=(const GMaterial& material);

public:
	HRESULT LoadFromFile(std::string textureFileName);
	void OnResize(uint32_t texture_width, uint32_t texture_height);
	ID3D11ShaderResourceView* GetTextureView() const { return mpTextureView; }
	ID3D11Texture2D* GetTexture2D() const { return mpTexture; }
	ID3D11SamplerState* GetSampler() const { return mpSamplerState; }
	std::string GetTextureFileName() const { return mTextureFileName.substr(0, mTextureFileName.find('.')); }
	std::string GetMaterialName() const { return mMaterialName; }

private:
	std::string					mMaterialName;
	std::string					mTextureFileName;

	ID3D11Device*				mpDevice;
	ID3D11Texture2D*			mpTexture;
	ID3D11Resource*				mpTextureResource;
	ID3D11ShaderResourceView*   mpTextureView;
	ID3D11SamplerState*			mpSamplerState;

	uint32_t					mTextureWidth;
	uint32_t					mTextureHeight;


};

}