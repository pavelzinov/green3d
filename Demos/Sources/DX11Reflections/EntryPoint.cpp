#include "ReflectionDemo.h"

//DWORD ServerThread()
//{
//	GInputServer server;
//
//	if(server.Initialization(8293) == false)
//	{
//		//GLog::Debug( server->GetError() );
//		system( "pause" );
//		return 0;
//	}
//
//	server.Run();
//}

// Main function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow)
{
    // Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
	#define _CRTDBG_MAP_ALLOC
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	ReflectionDemo app(hInstance);
	app.initApp();

	//HANDLE hThread = CreateThread( 
	//	NULL,				// ��� security ��������� 
	//	0,					// ������ ����� �� ��������� 
	//	(LPTHREAD_START_ROUTINE) ServerThread,		// ������� ��� ����������
	//	0,//(LPVOID) client_sock,     // ��������� ������� (�����)
	//	0,					// �� ����������� ������ ����������
	//	0);					// ���� ��������� ID ������
	//if (hThread == NULL)		// ���� ����� �� ��� ������
	//{
	//	GLog::Debug("CreateThread failed");
	//}

	//TerminateThread( hThread, 0 );

	return app.run();
}