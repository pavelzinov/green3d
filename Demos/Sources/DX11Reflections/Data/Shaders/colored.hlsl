//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
}

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float4 Normal : NORMAL;
	float4 Color : COLOR;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 Normal : NORMAL;
    float4 Color : COLOR0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.Normal = mul(float4(vs_input.Normal.xyz, 0.0f), (float3x3)World);
	output.Normal = normalize(output.Normal);
    output.Color = vs_input.Color;
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
    return ps_input.Color;
}
