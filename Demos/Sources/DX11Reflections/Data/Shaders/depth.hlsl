//#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix	World;
}

cbuffer PerFrame : register(b1)
{
	matrix	View;
	matrix	Projection;
}

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);

    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
//float4 PS(VS_OUT ps_input) : SV_Target
//{
//    float depth = ps_input.Pos.z / ps_input.Pos.w;
//
//    return float4(depth, depth, depth, 1.0f);
//}