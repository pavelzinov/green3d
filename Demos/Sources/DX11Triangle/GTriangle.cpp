#pragma once
#include "GTriangle.h"
using namespace Game;

template<>
void GTriangle<GSimpleVertex>::setVertices(GSimpleVertex vertex1, 
	GSimpleVertex vertex2, GSimpleVertex vertex3)
{
	// Vertices
	mVertices.push_back(vertex1);
	mVertices.push_back(vertex2);
	mVertices.push_back(vertex3);

	// Normal
	XMStoreFloat3(&mNormal, XMVector3Cross(XMLoadFloat3(&vertex2.pos)-XMLoadFloat3(&vertex1.pos),
		XMLoadFloat3(&vertex3.pos)-XMLoadFloat3(&vertex2.pos)));
	XMStoreFloat3(&mNormal, XMVector3Normalize(XMLoadFloat3(&mNormal)));

	GSimpleVertexData vertices[3];
	for (int i=0; i<3; i++)
	{
		vertices[i].pos = mVertices[i].pos;
		vertices[i].color = mVertices[i].color;
	}

	// Vertex Buffer
	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GSimpleVertex::getSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

 	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));

	// Indices
	UINT indices[3] = {0, 1, 2};

	// Index Buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(UINT) * 3;
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = indices;
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}