#pragma once
#include <windows.h>
#include <sstream>
#include "GTimer.h"
#include "GUtility.h"
#include "GInput.h"

namespace Game
{
	class GApplication
	{
	public:
		GApplication(HINSTANCE hInstance);
		virtual ~GApplication(void);

		HINSTANCE	getAppInst();
		HWND		getMainHwnd();

		int run();

		// ������ �����.  ����������� ���������� ������ �������������� ������ ������
		// ��� ����������� ��������� ���������� ����������

		virtual void initApp();
		virtual void onResize(); // reset projection/etc.
		virtual void updateScene(float dt);
		virtual void drawScene();
		virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

	protected:
		void initMainWindow();
		HRESULT initDirect3D();

	protected:
		HINSTANCE mhAppInst;  // application instance handle
		HWND      mhMainWnd;  // main window handle
		bool      mAppPaused; // is the application paused?
		bool      mMinimized; // is the application minimized?
		bool      mMaximized; // is the application maximized?
		bool      mResizing;  // are the resize bars being dragged?

		// Used to keep track of the "delta-time" and game time
		GTimer mTimer;

        // Object which controls input from keyboard and mouse
        GInput mInput;

		// A string to store the frame statistics for output. We display
		// the average frames per second and the average time it takes
		// to render one frame.
		std::wstring mFrameStats;

		// The D3D10 device, the swap chain for page flipping,
		// the 2D texture for the depth/stencil buffer,
		// and the render target and depth/stencil views. We
		// also store a font pointer so that we can render the
		// frame statistics to the screen.
		ID3D11Device*			md3dDevice;
		ID3D11DeviceContext*	md3dContext;
		IDXGISwapChain*			mSwapChain;
		ID3D11Texture2D*		mDepthStencilBuffer;
		ID3D11RenderTargetView* mRenderTargetView;
		ID3D11DepthStencilView* mDepthStencilView;

		// The following variables are initialized in the GApplication constructor
		// to default values. However, you can override the values in the
		// derived class to pick different defaults.

		// Device type
		D3D_DRIVER_TYPE md3dDriverType;
		D3D_FEATURE_LEVEL md3dFeatureLevel;

		// Window title/caption. Default is "D3D11 Application".
		std::wstring mMainWndCaption;

		// Color to clear the background. Defaults to blue.
		XMFLOAT4 mClearColor;

		// Initial size of the window's client area. GApplication defaults to
		// 800x600.  Note, however, that these values change at run time
		// to reflect the current client area size as the window is resized.
		int mClientWidth;
		int mClientHeight;
        
        // Antialiasing
        UINT mSampleCount;
        UINT mSampleQuality;
	};
}