#include "GVertex.h"
using namespace Game;

GVertex::GVertex(void)
{
	this->pos = XMFLOAT3(0, 0, 0);
}

GVertex::GVertex(XMFLOAT3 position)
{
	this->pos = position;
}

GVertex::~GVertex(void)
{
}

UINT GVertex::getSize()
{
	return sizeof(XMFLOAT3);
}
