#pragma once
#include "GVertex.h"

namespace Game
{

// aligned vertex data storage
struct GSimpleVertexData
{
	XMFLOAT3 pos;
	XMFLOAT4 color;
};

class GSimpleVertex : public GVertex
{
public:
	GSimpleVertex(const GSimpleVertex &vertex);
	GSimpleVertex(XMFLOAT3 position, XMFLOAT4 color);
	~GSimpleVertex(void);

	static UINT getSize();

	XMFLOAT4 color;
};

}

