#include "GCamera.h"

namespace Game
{

GCamera::GCamera(void)
{
    mPosition	= XMFLOAT3(0,0,0);
    mTarget		= XMFLOAT3(0,0,0);
    mUp			= XMFLOAT3(0,0,0);
	mAngle		= 0.0f;
	mClientWidth	= 0.0f;
	mClientHeight	= 0.0f;
	mNearest	= 0.0f;
	mFarthest	= 0.0f;
	mView = XMMatrixIdentity();
	mProj = XMMatrixIdentity();
}

GCamera& GCamera::operator=(const GCamera& camera)
{
    mPosition   = camera.mPosition;
    mTarget     = camera.mTarget;
    mUp         = camera.mUp;

	mAngle		= camera.mAngle;
	mClientWidth	= camera.mClientWidth;
	mClientHeight	= camera.mClientHeight;
	mNearest	= camera.mNearest;
	mFarthest	= camera.mFarthest;

    mView       = camera.mView;
	mProj		= camera.mProj;
    return *this;
}

GCamera::~GCamera(void)
{
}

void GCamera::initViewMatrix(XMFLOAT3 pos, XMFLOAT3 target, 
								XMFLOAT3 up)
{
	mPosition = pos;
	mTarget = target;
	mUp = up;
	mView = XMMatrixLookAtLH(XMLoadFloat3(&mPosition), XMLoadFloat3(&mTarget), XMLoadFloat3(&mUp));
}

void GCamera::initProjMatrix(const float angle, const float clientWidth, const float clientHeight, 
								const float nearest, const float farthest)
{
	mAngle = angle;
	mClientWidth = clientWidth;
	mClientHeight = clientHeight;
	mNearest = nearest;
	mFarthest = farthest;
	mProj = XMMatrixPerspectiveFovLH(angle, clientWidth/clientHeight, 
		nearest, farthest);
}

// Set camera position
void GCamera::SetPosition(XMFLOAT3 pos)
{
    mPosition = pos;
	initViewMatrix(mPosition, mTarget, mUp);
}

void GCamera::SetPosition(float x, float y, float z)
{
    mPosition.x = x;
    mPosition.y = y;
    mPosition.z = z;
	initViewMatrix(mPosition, mTarget, mUp);
}

// Get camera position
XMFLOAT3 GCamera::GetPosition()
{
    return mPosition;
}

float GCamera::GetPositionX()
{
    return mPosition.x;
}

float GCamera::GetPositionY()
{
    return mPosition.y;
}

float GCamera::GetPositionZ()
{
    return mPosition.z;
}

// Set camera target
void GCamera::SetTarget(XMFLOAT3 target)
{
    mTarget = target;
	initViewMatrix(mPosition, mTarget, mUp);
}

void GCamera::SetTarget(float x, float y, float z)
{
    mTarget.x = x;
    mTarget.y = y;
    mTarget.z = z;
	initViewMatrix(mPosition, mTarget, mUp);
}

// Get camera target
XMFLOAT3 GCamera::GetTarget()
{
    return mTarget;
}

float GCamera::GetTargetX()
{
    return mTarget.x;
}

float GCamera::GetTargetY()
{
    return mTarget.y;
}

float GCamera::GetTargetZ()
{
    return mTarget.z;
}

// Set camera look up
void GCamera::SetUp(XMFLOAT3 up)
{
    mUp = up;
	initViewMatrix(mPosition, mTarget, mUp);
}

void GCamera::SetUp(float x, float y, float z)
{
    mUp.x = x;
    mUp.y = y;
    mUp.z = z;
	initViewMatrix(mPosition, mTarget, mUp);
}

// Get camera look up
XMFLOAT3 GCamera::GetUp()
{
    return mUp;
}

float GCamera::GetUpX()
{
    return mUp.x;
}

float GCamera::GetUpY()
{
    return mUp.y;
}

float GCamera::GetUpZ()
{
    return mUp.z;
}

// Get camera view matrix
XMMATRIX GCamera::GetView()
{
	return XMMatrixTranspose(mView);
}

void GCamera::SetAngle(float angle)
{
	mAngle = angle;
	initProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}

float GCamera::GetAngle()
{
	return mAngle;
}

void GCamera::SetClientWidth(float clientWidth)
{
	mClientWidth = clientWidth;
	initProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}

void GCamera::SetClientHeight(float clientHeigth)
{
	mClientHeight = clientHeigth;
	initProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}

void GCamera::SetNearestPlane(float nearest)
{
	mNearest = nearest;
	initProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}

void GCamera::SetFarthestPlane(float farthest)
{
	mFarthest = farthest;
	initProjMatrix(mAngle, mClientWidth, mClientHeight, mNearest, mFarthest);
}

XMMATRIX GCamera::GetProj()
{
	return XMMatrixTranspose(mProj);
}

}
