#include "GInput.h"
//    Mouse flags data.mouse.usButtonFlags:
//    RI_MOUSE_LEFT_BUTTON_DOWN
//    RI_MOUSE_LEFT_BUTTON_UP
//    RI_MOUSE_MIDDLE_BUTTON_DOWN
//    RI_MOUSE_MIDDLE_BUTTON_UP
//    RI_MOUSE_RIGHT_BUTTON_DOWN
//    RI_MOUSE_RIGHT_BUTTON_UP
//    RI_MOUSE_BUTTON_1_DOWN
//    RI_MOUSE_BUTTON_1_UP
//    .....all the way to RI_MOUSE_BUTTON_5_UP
//    RI_MOUSE_WHEEL - if you get this message then you should also read the usButtonData value to get the change in mouse wheel position
namespace Game
{

GInput::GInput(void)
{
    // Get starting mouse position
    POINT temp;
    GetCursorPos(&temp);
    mCurrentX = temp.x;
    mCurrentY = temp.y;
    mPreviousX = temp.x;
    mPreviousY = temp.y;
    
    // Init mouse buttons states
    mMouseLeftButtonDown = false;
    mMouseLeftButtonUp = false;
    mMouseRightButtonDown = false;
    mMouseRightButtonUp = false;
    mMouseMiddleButtonDown = false;
    mMouseMiddleButtonUp = false;
    mMouseSensitivity = 1.0f;
    
    // Init scroll wheel
    mScrollDelay = 0.5f; // half - second scroll will be down/up
    mScrollTimer.start(); // start timer for scrolling delay
    
    // Init keyboard buffers
    for (UINT i=0; i<65536; i++)
    {
        mKeysDown[i] = false;
        mKeysUp[i] = false;
    }

    // Get screen resolution so we will not allow cursor to go out of screen when getting raw input
    mScreenWidth = GetSystemMetrics(SM_CXFULLSCREEN);
    mScreenHeight = GetSystemMetrics(SM_CYFULLSCREEN);

    mTmpDX = 0;
    mTmpDY = 0;
}

void GInput::InitGameInput()
{
    RAWINPUTDEVICE Rid[2];

    // Keyboard
    Rid[0].usUsagePage = 1;
    Rid[0].usUsage = 6;
    Rid[0].dwFlags = 0;
    Rid[0].hwndTarget=nullptr;

    // Mouse
    Rid[1].usUsagePage = 1;
    Rid[1].usUsage = 2;
    Rid[1].dwFlags = 0;
    Rid[1].hwndTarget=nullptr;

    // Register mouse and keyboard
    if (RegisterRawInputDevices(Rid, 2, sizeof(RAWINPUTDEVICE)) == FALSE)
    {
        // TODO: Input Error Here
    }
}

GInput::~GInput(void)
{
}

void GInput::Update(HRAWINPUT hRawInput)
{
    UINT bufferSize = 0;
    // Read buffer size
    GetRawInputData(hRawInput, RID_INPUT, nullptr, &bufferSize, sizeof (RAWINPUTHEADER));

    // Create a buffer of the correct size
    BYTE *buffer = new BYTE[bufferSize];

    // Call the function again, this time with the buffer to get the data
    GetRawInputData(hRawInput, RID_INPUT, (LPVOID)buffer, &bufferSize, sizeof (RAWINPUTHEADER));

    RAWINPUT *raw = (RAWINPUT*) buffer;
    if (raw->header.dwType == RIM_TYPEMOUSE)
    {// Read the mouse data
        mTmpDX = mMouseSensitivity * (raw->data.mouse.lLastX);
        mTmpDY = mMouseSensitivity * (raw->data.mouse.lLastY);
        // Cursor
        // X-coordinate
        if ((mCurrentX+mTmpDX) < mScreenWidth && (mCurrentX+mTmpDX) > 0)
        {
            mPreviousX = mCurrentX;
            mCurrentX += mTmpDX; // this show relative coordinates
        }
        else if ((mCurrentX+mTmpDX) >= mScreenWidth)
        {
            mPreviousX = mCurrentX;
            mCurrentX = mScreenWidth-1;
        }
        else if ((mCurrentX+mTmpDX) >= 0)
        {
            mPreviousX = mCurrentX;
            mCurrentX = 0;
        }
        // Y-coordinate
        if ((mCurrentY+mTmpDY) < mScreenHeight && (mCurrentY+mTmpDY) > 0) 
        {
            mPreviousY = mCurrentY;
            mCurrentY += mTmpDY;
        }
        else if ((mCurrentY+mTmpDY) >= mScreenHeight)
        {
            mPreviousY = mCurrentY;
            mCurrentY = mScreenHeight-1;
        }
        else if ((mCurrentY+mTmpDY) >= 0)
        {
            mPreviousY = mCurrentY;
            mCurrentY = 0;
        }
        // Mouse buttons
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN) && mMouseLeftButtonDown!=true)
        {
            mMouseLeftButtonDown = true;
            mMouseLeftButtonUp = false;
        }
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP) && mMouseLeftButtonUp!=true)
        {
            mMouseLeftButtonUp = true;
            mMouseLeftButtonDown = false;
        }
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN) && mMouseRightButtonDown!=true)
        {
            mMouseRightButtonDown = true;
            mMouseRightButtonUp = false;
        }
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP) && mMouseRightButtonUp!=true)
        {
            mMouseRightButtonUp = true;
            mMouseRightButtonDown = false;
        }
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN) && mMouseMiddleButtonDown!=true)
        {
            mMouseMiddleButtonDown = true;
            mMouseMiddleButtonUp = false;
        }
        if ((raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP) && mMouseMiddleButtonUp!=true)
        {
            mMouseMiddleButtonUp = true;
            mMouseMiddleButtonDown = false;
        }
        // Mouse wheel
        if (raw->data.mouse.usButtonFlags & RI_MOUSE_WHEEL)
        {
            SHORT temp = raw->data.mouse.usButtonData;
            if (temp != 0)
            {
                if (temp < 0)
                {
                    mScrollDown = true;
                    mScrollUp = false;
                    mScrollTimer.reset();
                    mScrollTimer.start();
                }
                else
                {
                    mScrollUp = true;
                    mScrollDown = false;
                    mScrollTimer.reset();
                    mScrollTimer.start();
                }
            }
        }
    }
    // Keyboard input
    if (raw->header.dwType == RIM_TYPEKEYBOARD)
    {
        if (raw->data.keyboard.Message == WM_KEYDOWN)
        {
            mKeysDown[raw->data.keyboard.VKey] = true;
        }
        if (raw->data.keyboard.Message == WM_KEYUP)
        {
            mKeysUp[raw->data.keyboard.VKey] = true;
            mKeysDown[raw->data.keyboard.VKey] = false;
        }
    }

    delete [] buffer;
}

void GInput::UpdateMouseMove()
{
    mPreviousX = mCurrentX;
    mPreviousY = mCurrentY;
}

long GInput::GetAbsoluteX(void)
{
    return mCurrentX;
}

long GInput::GetAbsoluteY(void)
{
    return mCurrentY;
}

long GInput::GetRelativeX(void)
{
//#if defined (DEBUG) || defined (_DEBUG)
//    std::wstringstream str;
//    str << mCurrentX << L", " << mCurrentY << L" | " << mPreviousX-mCurrentX << L", " << mPreviousY-mCurrentY << std::endl;
//    OutputDebugString(str.str().c_str());
//#endif
    return mPreviousX-mCurrentX;
}

long GInput::GetRelativeY(void)
{
    return mPreviousY-mCurrentY;
}

float GInput::GetMouseSensitivity()
{
    return mMouseSensitivity;
}

void GInput::SetMouseSensitivity(float val)
{
    mMouseSensitivity = val;
}

bool GInput::IsMouseLeftClicked(void)
{
    bool tmp = mMouseLeftButtonUp;
    mMouseLeftButtonUp = false;
    return tmp;
}

bool GInput::IsMouseRightClicked(void)
{
    bool tmp = mMouseRightButtonUp;
    mMouseRightButtonUp = false;
    return tmp;
}

bool GInput::IsMouseLeftDown(void)
{
//#if defined (DEBUG) || defined (_DEBUG)
//    std::wstringstream str;
//    str << mMouseLeftButtonDown << std::endl;
//    str << L"----------------------" << std::endl;
//    OutputDebugString(str.str().c_str());
//#endif
    return mMouseLeftButtonDown;
}

bool GInput::IsMouseRightDown(void)
{
    return mMouseRightButtonDown;
}

bool GInput::IsKeyDown(USHORT symbol)
{
    bool tmp = mKeysDown[symbol];
    //mKeysDown[symbol] = false;
    return tmp;
}

bool GInput::IsKeyUp(USHORT symbol)
{
    bool tmp = mKeysUp[symbol];
    //mKeysUp[symbol] = false;
    return tmp;
}

bool GInput::IsKeyPressed(USHORT symbol)
{
    bool tmp = mKeysDown[symbol];
    mKeysDown[symbol] = false;
    return tmp;
}

bool GInput::IsScrollDown(void)
{
    bool tmp = mScrollDown;
    if (mScrollTimer.getGameTime() >= mScrollDelay) mScrollDown = false;
    return tmp;
}

bool GInput::IsScrollUp(void)
{
    bool tmp = mScrollUp;
    if (mScrollTimer.getGameTime() >= mScrollDelay) mScrollUp = false;
    return tmp;
}


}
