#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GSimpleVertex.h"
#include "GModel.h"

using namespace Game;

struct ConstantBuffer
{
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
};

// Class DX11Triangle
// Does: Empty DX11 Windows 8 SDK project
class DX11Triangle : public GApplication
{
    // Public constructor and destructor
public:
    DX11Triangle(HINSTANCE hInstance);
    ~DX11Triangle(void);

public:
    void initApp();
private:
    void onResize();
    void updateScene(float dt);
    void drawScene();

private:
    // Functions for working with shaders
    HRESULT CreateShaders(); 
	void SetShaders();
	std::wstring mShaderFileName; 
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

private:
    ID3D11InputLayout*	mpVertexLayout;
	ID3D11PixelShader*	mpPixelShader;
	ID3D11VertexShader*	mpVertexShader;
	ID3D11Buffer*		mpShaderConstantBuffer;

private:
    // Projection matrixes
    XMMATRIX	mWorld;
    XMMATRIX	mView;
    XMMATRIX	mProj;

    // Camera
    GCamera	mCamera;
	float	mdx;
	float	mdy;
	float	mdz;
	float	mdMove;       // �������� �������� ���������������� ������ �� ����
    float	mdAngle;      // ������� �������� �������� ������
	float	mAngle;
    float	mDistance;    // ������ �������� ������

	// Random generator
	GRandom	mRandom;

	GTriangle<GSimpleVertex> *Tri;
};

