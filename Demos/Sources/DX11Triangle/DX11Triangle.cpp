#include "DX11Triangle.h"

DX11Triangle::DX11Triangle(HINSTANCE hInstance) : GApplication(hInstance)
{
	// entry point in shader file system
	mShaderFileName = L"EmptyShader.fx";

    // init background (space) color
    GApplication::mClearColor = BLUE;

    // init transformation matrixes
	mWorld = XMMatrixIdentity();
	mView = XMMatrixIdentity();
	mProj = XMMatrixIdentity();

	mdx = mdy = mdz = 0.0f;
	mdMove = 60.0f;
    mdAngle = 240.0f;        // in degrees
	mAngle = 90.0f;
    mDistance = 5.0f;

	// Build the static view matrix.
	mCamera.initViewMatrix(XMFLOAT3(0.0f, 1.0f, -mDistance), 
		XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
	mCamera.initProjMatrix(MATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 100.0f);
}

void DX11Triangle::initApp()
{
    GApplication::initApp();

	Tri = new GTriangle<GSimpleVertex>(GSimpleVertex(XMFLOAT3(-5.0f, 0.0f, 0.0f), XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f)),
		GSimpleVertex(XMFLOAT3(0.0f, 5.0f, 0.0f), XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f)),
		GSimpleVertex(XMFLOAT3(5.0f, 0.0f, 0.0f), XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f)),
		md3dDevice,
		md3dContext);

	CreateShaders();
}

void DX11Triangle::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(MATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 100.0f); 
}

void DX11Triangle::updateScene(float dt)
{
    GApplication::updateScene(dt);


	GApplication::mInput.UpdateMouseMove();
}

void DX11Triangle::drawScene()
{
    GApplication::drawScene();

    // set shader variables from cpp code
	Tri->draw();

    mSwapChain->Present(0, 0);
}

HRESULT DX11Triangle::CreateShaders()
{
	// Compile the vertex shader
    ID3DBlob* pVSBlob = nullptr;
	HRESULT hr = CompileShaderFromFile( mShaderFileName.c_str(), "VS", "vs_4_0", &pVSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( nullptr,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the vertex shader
	hr = md3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, 
		&mpVertexShader );
	if( FAILED( hr ) )
	{	
		pVSBlob->Release();
        return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
	hr = md3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if( FAILED( hr ) )
        return hr;

    // Set the input layout
    md3dContext->IASetInputLayout( mpVertexLayout );

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
    hr = CompileShaderFromFile( mShaderFileName.c_str(), "PS", "ps_4_0", &pPSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( nullptr,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the pixel shader
	hr = md3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), 
		pPSBlob->GetBufferSize(), nullptr, &mpPixelShader );
	pPSBlob->Release();
    if( FAILED( hr ) )
        return hr;

    // Set primitive topology
	md3dContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	mpShaderConstantBuffer = nullptr;
    hr = md3dDevice->CreateBuffer( &bd, nullptr, &mpShaderConstantBuffer );
    if( FAILED( hr ) )
        return hr;

	SetShaders();

	return S_OK;
}

void DX11Triangle::SetShaders()
{
	ConstantBuffer cb;
	ZeroMemory(&cb, sizeof(cb));
	cb.World = XMMatrixTranspose(mWorld);
	cb.View = mCamera.GetView();
	cb.Projection = mCamera.GetProj();
	md3dContext->UpdateSubresource( mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0 );

	md3dContext->VSSetShader(mpVertexShader, nullptr, 0);
	md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);
	md3dContext->PSSetShader(mpPixelShader, nullptr, 0);
}

HRESULT DX11Triangle::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
											LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, nullptr, nullptr, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != nullptr )
            OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

DX11Triangle::~DX11Triangle(void)
{
	delete Tri;

    if( md3dContext )
        md3dContext->ClearState();

    ReleaseCOM(mpVertexLayout);
}