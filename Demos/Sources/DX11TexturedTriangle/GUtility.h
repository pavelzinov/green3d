#pragma once
#include <windows.h>

#include <d3d11.h>
#include <directxmath.h>
#include <d3dcompiler.h>

#include <vector>
#include <map>
#include <algorithm>

#include <sstream>
#include <fstream>
#include <iostream>


using namespace DirectX;

// Releasing COM-object
inline void ReleaseCOM(IUnknown *x) 
{ 
	if(x != nullptr)
	{
		x->Release(); 
		x=nullptr;
	} 
}

// Safe pointer release
template <class any_type>
inline void PtrRelease(any_type *x) 
{ 
	if(x != nullptr)
	{
		delete x; 
		x=nullptr;
	} 
}

// ������ ��� ������ ��������� �������� DX11
#if defined(DEBUG) | defined(_DEBUG)
inline void HR(HRESULT hresult)
{
	LPTSTR errorText = NULL;
	FormatMessage(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		|FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		|FORMAT_MESSAGE_IGNORE_INSERTS,  
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		hresult,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&errorText,  // output 
		0, // minimum size for output buffer
		NULL);   // arguments - see note 

	if ( NULL != errorText )
	{
		OutputDebugString(errorText);
		// release memory allocated by FormatMessage()
		LocalFree(errorText);
		errorText = NULL;
	}
}
#endif

// DX11 Colors
const XMFLOAT4 GCOLOR_WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_RED(1.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
const XMFLOAT4 GCOLOR_LIGHT_BLUE(0.2f, 0.8f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
const XMFLOAT4 GCOLOR_DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
const XMFLOAT4 GCOLOR_DARKBROWN(0.45f, 0.39f, 0.34f, 1.0f);

// Math constants
const float GMATH_INFINITY = FLT_MAX;
const float GMATH_PI  = 3.14159265358979323f;
const float GMATH_PIDIV2 = 1.570796326794896615f;
const float GMATH_EPS = 0.0001f;

// Comparising two values by using MATH_EPS
template <class T>
inline bool GMath_IsEqual(T &x, T &y)
{ 
	if (fabs(static_cast<float>(x-y)) <= MATH_EPS)
		return true;
	return false;
}

inline std::wstring StringToWstring(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}