#include "GMaterialsManager.h"
using namespace Game;

GMaterialsManager::GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mpDevice = device;
	mpContext = context;
	mCurrentMaterialName = "";
}

GMaterialsManager::~GMaterialsManager(void)
{
}

void GMaterialsManager::AddMaterial(std::string material_name, GMaterial& material)
{
	mMaterials.insert(std::pair<std::string, GMaterial>(material_name, material));
}

void GMaterialsManager::AddMaterial(std::string material_name, std::string texture_file)
{
	mMaterials.insert(std::pair<std::string, GMaterial>(material_name, 
		GMaterial(mpDevice, texture_file)));
}

GMaterial& GMaterialsManager::GetMaterial(std::string material_name)
{
	return mMaterials.at(material_name);
}

std::vector<std::string> GMaterialsManager::GetMaterialNames()
{
	std::vector<std::string> names;

	for (auto it = mMaterials.begin(); it != mMaterials.end(); it++)
	{
		names.push_back(it->first);
	}

	return names;
}

void GMaterialsManager::SetCurrentMaterial(std::string material_name)
{
	if (material_name != mCurrentMaterialName)
	{
		ID3D11ShaderResourceView * resources[1];
		resources[0] = GetMaterial(material_name).GetTexture();
		mpContext->PSSetShaderResources(0, ARRAYSIZE(resources), resources);
		ID3D11SamplerState * samplers[1];
		samplers[0] = GetMaterial(material_name).GetSampler();
		mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);
	}
}
