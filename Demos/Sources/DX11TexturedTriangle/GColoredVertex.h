#pragma once
#include "GVertex.h"

namespace Game
{

// aligned colored vertex data storage
struct GColoredVertexData
{
	XMFLOAT3 pos;
	XMFLOAT4 color;
};

// colored vertex
// contains position and color of vertex and
// some operations over it
class GColoredVertex : public GVertex
{
public:
	GColoredVertex(const GColoredVertex &vertex);
	GColoredVertex(XMFLOAT3 position, XMFLOAT4 color);
	~GColoredVertex(void);

	void SetData(float x, float y, float z, float r, float g, float b, float a);
	void SetData(XMFLOAT3 position, XMFLOAT4 color);

	static UINT getDataSize();

	XMFLOAT4 mColor;
};

}

