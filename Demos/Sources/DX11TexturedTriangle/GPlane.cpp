#include "GPlane.h"
using namespace Game;

template<>
void GPlane<GColoredVertex>::CreateVertices()
{
	UINT i = 0, j = 0; // counters
	UINT vertices_count = (mWidthSegments+1)*(mHeightSegments+1);
	UINT faces_count = mWidthSegments*mHeightSegments*2;		
	// Creating vertex buffer points	
	mVertices.resize(vertices_count, GColoredVertex(XMFLOAT3(0.0f, 0.0f, 0.0f), 
		XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)));
	XMFLOAT3 point(-0.5f*mWidth, 0.0f, -0.5f*mHeight);
	UINT k = 0; // vertices counter
	for (i=0; i<mHeightSegments+1; i++)
	{
		for (j=0; j<mWidthSegments+1; j++)
		{
			mVertices[k].mPosition = point;
			mVertices[k].mColor = GCOLOR_GREEN;
			k++;
			if (j == mWidthSegments) // if last element in the row
			{
				point.x = -0.5f*mWidth;
			}
			else // if not last element of the row
			{
				point.x += mWidth/mWidthSegments;
			}
		} // end column
		point.z += mHeight/mHeightSegments;
	} // end row
}

template<>
void GPlane<GTexturedVertex>::CreateVertices()
{
	UINT i = 0, j = 0; // counters
	UINT vertices_count = (mWidthSegments+1)*(mHeightSegments+1);
	UINT faces_count = mWidthSegments*mHeightSegments*2;		
	// Creating vertex buffer points	
	mVertices.resize(vertices_count, GTexturedVertex(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f)));
	XMFLOAT3 point(-0.5f*mWidth, 0.0f, -0.5f*mHeight);
	XMFLOAT2 point_tex_coords(0.0f, 1.0f);
	UINT k = 0; // vertices counter
	for (i=0; i<mHeightSegments+1; i++)
	{
		for (j=0; j<mWidthSegments+1; j++)
		{
			mVertices[k].mPosition = point;
			mVertices[k].mTexturedCoordinates = point_tex_coords;
			k++;
			if (j == mWidthSegments) // if last element in the row
			{
				point.x = -0.5f*mWidth;
                point_tex_coords.x = 0.0f;
			}
			else // if not last element of the row
			{
				point.x += mWidth/mWidthSegments;
				point_tex_coords.x += 1.0f/mWidthSegments;
			}
		} // end column
		point.z += mHeight/mHeightSegments;
		point_tex_coords.y -= 1.0f/mHeightSegments;
	} // end row
}
