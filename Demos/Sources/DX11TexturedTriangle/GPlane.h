#pragma once
#include "GMesh.h"

namespace Game
{
////////////////////////////////////////////////////////
// Handling construction of plane mesh, it's scaling,
// movement, rotation and drawing
////////////////////////////////////////////////////////
template <class T>
class GPlane : public GMesh<T>
{
public:
	GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, UINT width_segments,
		float height, UINT height_segments);
	GPlane(GPlane& plane);
	~GPlane(void) {}

private:
	void CreateVertices();

private:
	float	mWidth;
	float	mHeight;
	UINT	mWidthSegments;
	UINT	mHeightSegments;
};

template <class T>
GPlane<T>::GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, UINT width_segments,
		float height, UINT height_segments) : GMesh(device, context)
{
	mWidth = width;
	mHeight = height;
	mWidthSegments = width_segments;
	mHeightSegments = height_segments;

	CreateVertices();

	UINT faces_count = width_segments*height_segments*2;
	mIndices.resize(faces_count*3);
	UINT * indices = new UINT[faces_count*3];
    // Inserting 2 triangles data in every quad
	UINT k = 0; // quad number
	int limit = GetVertexCount()-width_segments-1;
	for (int i=0; i<limit; i++)
	{
		// adding quad to the index buffer            
        if ((i+1)%(width_segments+1)!=0)
        {// if point is not on the plane's edge
			// upper triangle
			indices[k]		= i;
			indices[k+1]	= i + width_segments + 1;
			indices[k+2]	= indices[k + 1] + 1;
			// lower triangle
			indices[k+3]	= i;
			indices[k+4]	= indices[k + 1] + 1;
			indices[k+5]	= i + 1;
            k += 6;
        } // end if		
	} // end for
	mIndices.reserve(faces_count*3);	
	for (UINT i=0; i<(faces_count*3); i++)
	{
		mIndices.push_back(indices[i]);
	}
	delete[] indices;

	ConstructBuffers();
}

template <class T>
GPlane<T>::GPlane(GPlane<T>& plane) : GMesh<T>(dynamic_cast<GMesh&>(plane))
{

}

}