#pragma once
#include "GUtility.h"
#include "GColoredVertex.h"
#include "GTexturedVertex.h"

namespace Game
{

template <class T>
class GTriangle
{
public:
	GTriangle(T vertex1, T vertex2, T vertex3, ID3D11Device *device, ID3D11DeviceContext *context);
	~GTriangle(void);

public:
	std::vector<T> getVertices();
	void setVertices(T vertex1, T vertex2, T vertex3);

public:
	void draw();

private:
	std::vector<T> mVertices;
	XMFLOAT3 mNormal;

	// rendering properties
	ID3D11Device		*mDevice;
	ID3D11DeviceContext	*mContext;
	ID3D11Buffer		*mVB;
	ID3D11Buffer		*mIB;
};

// Definitions of class methods
template<class T>
GTriangle<T>::GTriangle(T vertex1, T vertex2, T vertex3, ID3D11Device *device, ID3D11DeviceContext *context)
{
	mVB = NULL;
	mIB = NULL;
	mDevice = device;
	mContext = context;
	setVertices(vertex1, vertex2, vertex3);
}

template<class T>
GTriangle<T>::~GTriangle(void)
{
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);
}

template<class T>
std::vector<T> GTriangle<T>::getVertices()
{
	return mVertices;
}

template<class T>
void GTriangle<T>::draw()
{
	UINT strides = T::getDataSize();
	UINT offset = 0;
	mContext->IASetVertexBuffers(0, 1, &mVB, &strides, &offset);
	mContext->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);
	mContext->DrawIndexed(3, 0, 0);
}

}