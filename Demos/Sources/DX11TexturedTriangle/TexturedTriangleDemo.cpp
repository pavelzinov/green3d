#include "TexturedTriangleDemo.h"
#include "DDSTextureLoader.h"

TexturedTriangleDemo::TexturedTriangleDemo(HINSTANCE hInstance) : GApplication(hInstance)
{
	// entry point in shader file system
	mShaderFileName = L"TexturedShader.fx";

    // init background color
    GApplication::mClearColor = GCOLOR_BLUE;

	mdx = mdy = mdz = 0.0f;
	mdMove = 60.0f;
    mdAngle = 240.0f;
	mAngle = 90.0f;
    mDistance = 100.0f;

	mpVertexLayout = nullptr;
	mpPixelShader = nullptr;
	mpVertexShader = nullptr;
	mpShaderConstantBuffer = nullptr;

	mCamera.initViewMatrix(XMFLOAT3(0.0f, mDistance, 0.0f), 
		XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void TexturedTriangleDemo::initApp()
{
    GApplication::initApp();

	mMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mMaterialsManager->AddMaterial("Space", "space.dds");
	mMaterialsManager->AddMaterial("Water", "water.dds");
	//GTimer timer;
	//timer.tick();
	//for (int i=0; i<1000; i++)
	//{
	//	mMaterialsManager->AddMaterial("Space", "space.dds");
	//}
	//timer.tick();
	//float time = timer.getDeltaTime();
	mSprites.push_back(new GPlane<GTexturedVertex>(md3dDevice, md3dContext, 100.0f, 10, 100.0f, 10));
	mSprites[0]->SetMaterialName("Space");
	mSprites[0]->Scale(0.3f);
	for (int i=1; i<1000; i++)
	{
		mSprites.push_back(new GPlane<GTexturedVertex>(
			dynamic_cast<GPlane<GTexturedVertex>&>(*mSprites[0])));
		mSprites[i]->RotateY(mRandom.Gen(360.0f));
		mSprites[i]->Move(mRandom.Gen(100.0f, -100.0f), 0.0f, mRandom.Gen(100.0f, -100.0f));
		if (i%2 != 0)
		{
			mSprites[i]->SetMaterialName("Water");
		}
	}

	CreateShaders();
	SetShaders();
}

void TexturedTriangleDemo::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void TexturedTriangleDemo::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);
	

	GApplication::mInput.UpdateMouseMove();
}

void TexturedTriangleDemo::drawScene()
{
    GApplication::drawScene();

    // Set shader matrices from app code	
	ConstantBuffer cb;
	ZeroMemory(&cb, sizeof(cb));
	cb.View = mCamera.GetView();
	cb.Projection = mCamera.GetProj();

	// Sort primitives by material
	std::sort(mSprites.begin(), mSprites.end(), [](GMesh<GTexturedVertex>* a, GMesh<GTexturedVertex>* b)->bool
	{
		return a->mMaterialName < b->mMaterialName;
	});

	for (auto it=mSprites.begin(); it!=mSprites.end(); it++)
	{
		// Try to set material if it's not active
		mMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
		// Draw primitives with current material
		cb.World = (*it)->GetWorld();
		md3dContext->UpdateSubresource(mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0);
		(*it)->draw();
	}

    mSwapChain->Present(0, 0);
}

HRESULT TexturedTriangleDemo::CreateShaders()
{
	// Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile(mShaderFileName.c_str(), "VS", "vs_4_0", &pVSBlob);
    if(FAILED(hr))
    {
        MessageBox(NULL, 
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK);
        return hr;
    }

	// Create the vertex shader
	hr = md3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader);
	if(FAILED(hr))
	{	
		pVSBlob->Release();
        return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
	hr = md3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if(FAILED(hr))
        return hr;

    

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( mShaderFileName.c_str(), "PS", "ps_4_0", &pPSBlob );
    if(FAILED(hr))
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the pixel shader
	hr = md3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
    if(FAILED(hr))
        return hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    hr = md3dDevice->CreateBuffer( &bd, NULL, &mpShaderConstantBuffer );
    if(FAILED(hr))
        return hr;	
	
	return S_OK;
}

void TexturedTriangleDemo::SetShaders()
{	
	// Set the input layout
    md3dContext->IASetInputLayout( mpVertexLayout );
	md3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	md3dContext->VSSetShader(mpVertexShader, nullptr, 0);
	md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);
	
	md3dContext->PSSetShader(mpPixelShader, nullptr, 0);
}

HRESULT TexturedTriangleDemo::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
										   LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA(static_cast<char*>(pErrorBlob->GetBufferPointer()));
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

TexturedTriangleDemo::~TexturedTriangleDemo(void)
{
	for (auto it=mSprites.begin(); it<mSprites.end(); it++)
	{
		delete *it;
	}

    if( md3dContext )
        md3dContext->ClearState();

	delete mMaterialsManager;

	ReleaseCOM(mpShaderConstantBuffer);
	ReleaseCOM(mpVertexShader);
	ReleaseCOM(mpPixelShader);
    ReleaseCOM(mpVertexLayout);
}