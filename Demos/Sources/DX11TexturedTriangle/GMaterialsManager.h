#pragma once
#include "GUtility.h"
#include "GMaterial.h"

namespace Game
{
////////////////////////////////////////////////////////
// Manager designed for storing all game materials in
// single place, calling them when needed through special
// string identifiers. Those identifiers are created by
// this manager when material is added to collection.
// Meshes and other primitives are storing those
// identifiers instead of Material itself.
////////////////////////////////////////////////////////
class GMaterialsManager
{
public:
	GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context);
	~GMaterialsManager(void);

public:
	void AddMaterial(std::string material_name, GMaterial& material);
	void AddMaterial(std::string material_name, std::string texture_file);
	GMaterial& GetMaterial(std::string material_name);
	std::vector<std::string> GetMaterialNames();

	void SetCurrentMaterial(std::string material_name);

private:
	std::map<std::string, GMaterial>	mMaterials;
	std::string							mCurrentMaterialName;

private:
	ID3D11Device*	mpDevice;
	ID3D11DeviceContext* mpContext;
};

}

