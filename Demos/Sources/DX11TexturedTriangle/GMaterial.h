#pragma once
#include "GUtility.h"
#include "DDSTextureLoader.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores View and Projection matrices used by shaders
// to translate 3D world into 2D screen surface
// Camera can be moved
////////////////////////////////////////////////////////
class GMaterial
{
public:
	GMaterial(ID3D11Device *device, std::string textureFileName);
	GMaterial(const GMaterial& material);
	~GMaterial(void);
	GMaterial& operator=(const GMaterial& material);

public:
	void LoadFromFile(std::string textureFileName);
	ID3D11ShaderResourceView* GetTexture();
	ID3D11SamplerState* GetSampler();

private:
	ID3D11Device*				mpDevice;
	ID3D11Resource*				mpTexture;
	ID3D11ShaderResourceView*   mpTextureView;
	ID3D11SamplerState*			mpSamplerState;

private:
	std::string					mTextureFileName;

protected:
	HRESULT CreateTextureView();
};

}