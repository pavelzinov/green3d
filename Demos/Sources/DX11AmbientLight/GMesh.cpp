#pragma once
#include "GMesh.h"
using namespace Game;

template <>
void* GMesh<GColoredVertex>::GetVertexData()
{
	uint32_t verticesNumber = GetVertexCount();
	GColoredVertexData *vertices = new GColoredVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].normal = mVertices[i].mNormal;
		vertices[i].color = mVertices[i].mColor;
	}
	return static_cast<void*>(vertices);
}

template <>
void* GMesh<GTexturedVertex>::GetVertexData()
{
	uint32_t verticesNumber = GetVertexCount();
	GTexturedVertexData *vertices = new GTexturedVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].normal = mVertices[i].mNormal;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
	}
	return static_cast<void*>(vertices);
}

template <>
void GMesh<GColoredVertex>::FromFileData(std::vector<std::string> &file_data)
{
	std::string vertex_type = file_data[0];
	if (vertex_type != "GColoredVertex") return;

	unsigned int vertex_count = 0;
	std::stringstream line_as_stream;	

	line_as_stream << file_data[1];
	line_as_stream >> vertex_count;
	line_as_stream.clear();

	float x, y, z, r, g, b, a;
	GColoredVertex vertex(XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT4(0, 0, 0, 0));
	
	// reading vertices
	mVertices.clear();
	mVertices.reserve(vertex_count);
	auto index_data_start = vertex_count + 2;
	for (unsigned int i=2; i < index_data_start; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> x >> y >> z >> r >> g >> b >> a;
		vertex.mPosition.x = x;
		vertex.mPosition.y = y;
		vertex.mPosition.z = z;
		vertex.mColor.x = r;
		vertex.mColor.y = g;
		vertex.mColor.z = b;
		vertex.mColor.w = a;
		mVertices.push_back(vertex);
		line_as_stream.clear();
	}

	// reading indices
	mIndices.clear();
	mIndices.reserve((file_data.size() - vertex_count - 2) * 3);
	uint32_t i1 = 0, i2 = 0, i3 = 0;
	uint32_t file_size = file_data.size();
	for (auto i=index_data_start; i<file_size; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> i1 >> i2 >> i3;
		mIndices.push_back(i1 - 1);
		mIndices.push_back(i2 - 1);
		mIndices.push_back(i3 - 1);
		line_as_stream.clear();
	}

	// constructing normals
	std::vector<std::vector<XMFLOAT3>> normals;
	normals.resize(vertex_count);
	// for each triangle
	uint32_t indices_count = mIndices.size();
	XMVECTOR vec1, vec2, normal;
	for (uint32_t i=0; i<indices_count; i+=3)
	{
		vec1 = GMathFV(mVertices[mIndices[i + 1]].mPosition) - 
			GMathFV(mVertices[mIndices[i]].mPosition);
		vec2 = GMathFV(mVertices[mIndices[i + 2]].mPosition) - 
			GMathFV(mVertices[mIndices[i]].mPosition);
		// determine normal for first triangle
		normal = XMVector3Normalize(XMVector3Cross(vec1, vec2));
		// add triangle's normal to all it's vertices's current normals
		normals[mIndices[i]].push_back(GMathVF(normal));
		normals[mIndices[i + 1]].push_back(GMathVF(normal));
		normals[mIndices[i + 2]].push_back(GMathVF(normal));
	}

	// make sure vertex's normals don't duplicate
	for (uint32_t i = 0; i < normals.size(); ++i)
	{
		for (uint32_t j = 0; j < normals[i].size(); ++j)
		{
			for (uint32_t k = j + 1; k < normals[i].size(); ++k)
			{
				if (normals[i][k].x == normals[i][j].x &&
					normals[i][k].y == normals[i][j].y &&
					normals[i][k].z == normals[i][j].z)
				{
					normals[i].erase(normals[i].begin() + k);
					k = j + 1;
				}
			}
		}
	}
	
	// summarize each vertex's normals to get single value per vertex
	normal = XMVectorZero();
	for (uint32_t i = 0; i < vertex_count; ++i)
	{
		// combine all normals for single vertex
		normal = XMVectorZero();
		for (auto it = normals[i].begin(); it != normals[i].end(); it++)
		{
			normal = normal + GMathFV(*it);
		}
		// normalize result and assign it to vertex's normal
		mVertices[i].mNormal = GMathVF(XMVector3Normalize(normal));
	}

	this->ConstructBuffers();
}

template<>
void GMesh<GColoredVertex>::LoadFromObj(const GObjFile& obj_file)
{
	GColoredVertex vertex(XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), GCOLOR_CYAN);
	int faces_count = obj_file.mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.mFaces[i].mVertices[j].mVertex.x;
			vertex.mPosition.y = obj_file.mFaces[i].mVertices[j].mVertex.y;
			vertex.mPosition.z = obj_file.mFaces[i].mVertices[j].mVertex.z;

			vertex.mNormal.x = obj_file.mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.mFaces[i].mVertices[j].mNormal.nz;

			mVertices.push_back(vertex);
		}
	}

	uint32_t indices_count = faces_count * 3;
	mIndices.resize(indices_count);
	for (uint32_t i=0; i<indices_count; ++i)
	{
		mIndices[i] = i;
	}

	this->ConstructBuffers();
}

template<>
void GMesh<GTexturedVertex>::LoadFromObj(const GObjFile& obj_file)
{
	GTexturedVertex vertex(XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0), XMFLOAT2(0, 0));
	int faces_count = obj_file.mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.mFaces[i].mVertices[j].mVertex.x;
			vertex.mPosition.y = obj_file.mFaces[i].mVertices[j].mVertex.y;
			vertex.mPosition.z = obj_file.mFaces[i].mVertices[j].mVertex.z;

			vertex.mNormal.x = obj_file.mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.mFaces[i].mVertices[j].mNormal.nz;

			vertex.mTexturedCoordinates.x = obj_file.mFaces[i].mVertices[j].mTexCoords.u;
			vertex.mTexturedCoordinates.y = obj_file.mFaces[i].mVertices[j].mTexCoords.v;

			mVertices.push_back(vertex);
		}
	}

	uint32_t indices_count = faces_count * 3;
	mIndices.resize(indices_count);
	for (uint32_t i=0; i<indices_count; ++i)
	{
		mIndices[i] = i;
	}

	this->ConstructBuffers();
}