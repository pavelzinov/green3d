#include "GColoredVertex.h"
using namespace Game;

GColoredVertex::GColoredVertex(const GColoredVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mNormal = vertex.mNormal;
	this->mColor = vertex.mColor;	
}

GColoredVertex::GColoredVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT4 color) 
	: GVertex(position, normal)
{
	this->mColor = color;
}

uint32_t GColoredVertex::getDataSize()
{
	return sizeof(GColoredVertexData);
}
