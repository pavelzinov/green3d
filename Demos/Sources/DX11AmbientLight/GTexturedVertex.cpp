#include "GTexturedVertex.h"
using namespace Game;

GTexturedVertex::GTexturedVertex(const GTexturedVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mNormal = vertex.mNormal;
	this->mTexturedCoordinates = vertex.mTexturedCoordinates;
}

GTexturedVertex::GTexturedVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT2 tex_coords)
	: GVertex(position, normal)
{
	this->mTexturedCoordinates = tex_coords;
}
