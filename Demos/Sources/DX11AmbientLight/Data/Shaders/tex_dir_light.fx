//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 LightDirection;
	float4 LightColor;
}
Texture2D ShaderTexture;
SamplerState SampleType;

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 Pos : SV_POSITION;
	float4 Normal : NORMAL;
    float4 TexC : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS( float4 Pos : POSITION, float4 Normal : NORMAL, float4 TexC : TEXCOORD0 )
{
    VS_OUTPUT output = (VS_OUTPUT)0;
    output.Pos = mul( Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	output.Normal = normalize(mul(float4(Normal.xyz, 0.0f), World));
    output.TexC = TexC;
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VS_OUTPUT input ) : SV_Target
{
	float4 model_color = ShaderTexture.Sample(SampleType, input.TexC);
	float4 light_direction = -LightDirection;
	float light_intensity = max(dot(input.Normal, light_direction), 0.0f);
	model_color.xyz = model_color.xyz * 0.2f + model_color.xyz * light_intensity;
    return model_color;
}