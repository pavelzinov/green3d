#include "GMaterialsManager.h"
using namespace Game;

GMaterialsManager::GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mpDevice = device;
	mpContext = context;
	mCurrentMaterialName = "";
}

GMaterialsManager::~GMaterialsManager(void)
{
}

bool GMaterialsManager::AddMaterial(GMaterial& material)
{
	auto names = GetMaterialNames();
	if (std::find(names.begin(), names.end(), material.GetMaterialName()) != names.end())
		return false;

	mMaterials.insert(std::pair<std::string, GMaterial>(material.GetMaterialName(), material));
	return true;
}

bool GMaterialsManager::AddMaterial(std::string material_name, std::string texture_file)
{
	auto names = GetMaterialNames();
	if (std::find(names.begin(), names.end(), material_name) != names.end())
		return false;

	mMaterials.insert(std::pair<std::string, GMaterial>(material_name, 
		GMaterial(mpDevice, material_name, texture_file)));
	return true;
}

std::vector<std::string> GMaterialsManager::GetMaterialNames()
{
	std::vector<std::string> names;

	for (auto it = mMaterials.begin(); it != mMaterials.end(); it++)
	{
		names.push_back(it->first);
	}

	return names;
}

void GMaterialsManager::SetCurrentMaterial(std::string material_name)
{
	if (material_name != mCurrentMaterialName)
	{
		ID3D11ShaderResourceView * resources[1];
		resources[0] = GetMaterial(material_name).GetTexture();
		mpContext->PSSetShaderResources(0, ARRAYSIZE(resources), resources);
		ID3D11SamplerState * samplers[1];
		samplers[0] = GetMaterial(material_name).GetSampler();
		mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);

		mCurrentMaterialNames.clear();
	}
}

void GMaterialsManager::SetCurrentMaterials(std::vector<std::string> material_names)
{
	// sort input materials for future comparison
	std::sort(material_names.begin(), material_names.end(), [](std::string& first, std::string& second) {
		return first < second;
	});
	// compare with current materials
	if (material_names != mCurrentMaterialNames)
	{
		// copy materials to current
		mCurrentMaterialNames.clear();
		mCurrentMaterialNames.resize(material_names.size());
		std::copy(material_names.begin(), material_names.end(), mCurrentMaterialNames.begin());

		// get shader resources for each current material
		std::vector<ID3D11ShaderResourceView*> resources;
		for (auto material_name : mCurrentMaterialNames)
		{
			resources.push_back(GetMaterial(material_name).GetTexture());
		}
		// set shader resources
		mpContext->PSSetShaderResources(0, resources.size(), &resources[0]);
		// WARNING: this is only for shaders with single sampler for multiple 2d textures!!!!
		ID3D11SamplerState * samplers[1];
		samplers[0] = GetMaterial(mCurrentMaterialNames[0]).GetSampler();
		mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);

		mCurrentMaterialName = "";
	}
}
