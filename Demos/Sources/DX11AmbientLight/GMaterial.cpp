#include "GMaterial.h"
using namespace Game;

GMaterial::GMaterial(ID3D11Device *device, std::string material_name, std::string texture_file_name)
{
	mMaterialName = material_name;
	mTextureFileName = texture_file_name;
	mpDevice = device;
	mpTexture = nullptr;
	mpTextureView = nullptr;
	mpSamplerState = nullptr;

	CreateTextureView();
}

GMaterial::GMaterial(const GMaterial& material)
{
	this->mpDevice = material.mpDevice;
	this->mMaterialName = material.mMaterialName;
	this->mTextureFileName = material.mTextureFileName;
	this->CreateTextureView();
}

GMaterial::~GMaterial(void)
{
	ReleaseCOM(mpTexture);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpSamplerState);
}

HRESULT GMaterial::CreateTextureView()
{
	HRR(CreateDDSTextureFromFile(mpDevice, StringToWstring(mTextureFileName).c_str(), &mpTexture, &mpTextureView));

	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRR(mpDevice->CreateSamplerState(&samplerDesc, &mpSamplerState));

	return S_OK;
}

GMaterial& GMaterial::operator=(const GMaterial& material)
{
	ReleaseCOM(mpTexture);
	ReleaseCOM(mpTextureView);
	ReleaseCOM(mpSamplerState);

	this->mpDevice = material.mpDevice;
	this->mMaterialName = material.mMaterialName;
	this->mTextureFileName = material.mTextureFileName;
	this->CreateTextureView();

	return *this;
}

void GMaterial::LoadFromFile(std::string textureFileName)
{
	this->mTextureFileName = textureFileName;
	CreateTextureView();
}
