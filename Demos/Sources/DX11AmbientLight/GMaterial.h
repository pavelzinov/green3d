#pragma once
#include "GUtility.h"
#include "DDSTextureLoader.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores View and Projection matrices used by shaders
// to translate 3D world into 2D screen surface
// Camera can be moved
////////////////////////////////////////////////////////
class GMaterial
{
public:
	GMaterial(ID3D11Device *device, std::string material_name, std::string texture_file_name);
	GMaterial(const GMaterial& material);
	~GMaterial(void);
	GMaterial& operator=(const GMaterial& material);

public:
	void LoadFromFile(std::string textureFileName);
	ID3D11ShaderResourceView* GetTexture() const { return mpTextureView; }
	ID3D11SamplerState* GetSampler() const { return mpSamplerState; }
	std::string GetTextureFileName() const { return mTextureFileName.substr(0, mTextureFileName.find('.')); }
	std::string GetMaterialName() const { return mMaterialName; }

private:
	ID3D11Device*				mpDevice;
	ID3D11Resource*				mpTexture;
	ID3D11ShaderResourceView*   mpTextureView;
	ID3D11SamplerState*			mpSamplerState;

private:
	std::string					mMaterialName;
	std::string					mTextureFileName;	

protected:
	HRESULT CreateTextureView();
};

}