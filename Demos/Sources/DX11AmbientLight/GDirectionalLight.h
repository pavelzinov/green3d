#pragma once
#include "GUtility.h"

namespace Game
{

class GDirectionalLight
{
public:
	GDirectionalLight(void);
	GDirectionalLight(XMFLOAT3 direction, XMFLOAT4 color);
	~GDirectionalLight(void) {}

public:
	XMFLOAT3 mDirection;
	XMFLOAT4 mColor;
};

}