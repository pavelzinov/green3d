#include "GVertex.h"
using namespace Game;

GVertex::GVertex(void)
{
	this->mPosition = XMFLOAT3(0, 0, 0);
}

GVertex::GVertex(XMFLOAT3 position)
{
	this->mPosition = position;
}

GVertex::~GVertex(void)
{
}

void GVertex::SetData(float x, float y, float z)
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;
}

void GVertex::SetData(XMFLOAT3 position)
{
	this->mPosition = position;
}

UINT GVertex::getDataSize()
{
	return sizeof(XMFLOAT3);
}
