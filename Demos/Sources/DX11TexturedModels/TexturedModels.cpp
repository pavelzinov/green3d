#include "TexturedModels.h"
#include "DDSTextureLoader.h"

TexturedModels::TexturedModels(HINSTANCE hInstance) : GApplication(hInstance)
{
	// entry point in shader file system
	mShaderFileName = L"TexturedShader.fx";

    // init background color
    GApplication::mClearColor = GCOLOR_WHITE;

    mDistance = 100.0f;

	mpShaderConstantBuffer = nullptr;

	mCamera.SetPosition(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 100.0f);	
}

void TexturedModels::initApp()
{ 
	GApplication::initApp();

	mpTexturedShader = new GShader<GTexturedVertex>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("TexturedShader.fx");

	mpMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mpMaterialsManager->AddMaterial("Space", "space.dds");
	mpMaterialsManager->AddMaterial("Water", "water.dds");
	mpMaterialsManager->AddMaterial("SkyBackground", "sky.dds");
	mpMaterialsManager->AddMaterial("Courier New 12pt Dark Green", "courier_new_12_dark_green.dds");
	mpMaterialsManager->AddMaterial("Segoe UI Semilight 10pt Yellow", "segoe_semilight_10_yellow.dds");
	mpMaterialsManager->AddMaterial("VS2012", "visual_studio_2012.dds");

	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		""));
	(*(mText.end()-1))->SetPosition(XMUINT2(4, 4), mClientWidth, mClientHeight);

	mSprites.push_back(new GPlane<GTexturedVertex>(md3dDevice, md3dContext, 512.0f, 1, 512.0f, 1));
	(*(mSprites.end()-1))->SetMaterialName("Space");
	(*(mSprites.end()-1))->Scale(0.4f);
	for (int i=1; i<10; i++)
	{
		mSprites.push_back(new GPlane<GTexturedVertex>(
			dynamic_cast<GPlane<GTexturedVertex>&>(*mSprites[0])));
		mSprites[i]->RotateY(mRandom.Gen(360.0f));
		mSprites[i]->Move(mRandom.Gen(100.0f, -100.0f), 0.0f, mRandom.Gen(100.0f, -100.0f));
		if (i%2 != 0)
		{
			mSprites[i]->SetMaterialName("Water");
		}
	}
	mSprites.push_back(new GPlane<GTexturedVertex>(md3dDevice, md3dContext, 1280.0f, 1, 1024.0f, 1));
	(*(mSprites.end()-1))->SetMaterialName("VS2012");
	(*(mSprites.end()-1))->Scale(0.4f);

	mSky = new GBillboard<GTexturedVertex>(md3dDevice, md3dContext, 2000.0f, 2000.0f,
		mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());
	mSky->Move(0.0f, 0.0f, 500.0f);
	mSky->SetMaterialName("SkyBackground");

	mpTexturedShader->SetShader();
	
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    HR(md3dDevice->CreateBuffer(&bd, NULL, &mpShaderConstantBuffer));
	md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);
}

void TexturedModels::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void TexturedModels::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "UPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mCamera.GetPosition().x << " " << 
		mCamera.GetPosition().y << " " << mCamera.GetPosition().z << 
		"\nCamera target position: " << mCamera.GetTarget().x << " " <<
		mCamera.GetTarget().y << " " << mCamera.GetTarget().z <<
		"\nCamera Up vector: " << mCamera.GetUp().x << " " <<
		mCamera.GetUp().y << " " << mCamera.GetUp().z;
	mText[0]->SetText(stream.str());

	if (mInput.IsKeyDown('W'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('S'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('D'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('A'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyPressed('1'))
	{
		for (auto it=mText.begin(); it != mText.end(); it++)
			(*it)->SetMaterial(mpMaterialsManager->GetMaterial("Segoe UI Semilight 10pt Yellow"));
	}
	if (mInput.IsKeyPressed('2'))
	{
		for (auto it=mText.begin(); it != mText.end(); it++)
			(*it)->SetMaterial(mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"));
	}

	mSky->UpdatePosition(mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());	

	GApplication::mInput.UpdateMouseMove();
}

void TexturedModels::drawScene()
{
    GApplication::drawScene();

    // Set shader matrices from app code
	ConstantBuffer cb;
	ZeroMemory(&cb, sizeof(cb));
	//////////////////////
	// Draw 3D
	GApplication::TurnZBufferOn();
	mpTexturedShader->DisableAlpha();
	cb.View = mCamera.GetView();
	cb.Projection = mCamera.GetProj();	

	// Sort primitives by material
	//std::sort(mSprites.begin(), mSprites.end(), [](GMesh<GTexturedVertex>* a, GMesh<GTexturedVertex>* b)
	//{
	//	return a->mMaterialName < b->mMaterialName;
	//});

	//for (auto it=mSprites.begin(); it!=mSprites.end(); it++)
	//{
	//	// Try to set material if it's not active
	//	mpMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
	//	// Draw primitives with current material
	//	cb.World = (*it)->GetWorld();
	//	md3dContext->UpdateSubresource(mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0);
	//	(*it)->draw();
	//}
	//
	//mpMaterialsManager->SetCurrentMaterial(mSky->GetMaterialName());
	//cb.World = mSky->GetWorld();
	//md3dContext->UpdateSubresource(mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0);
	//mSky->draw();

	//////////////////////
	// Draw 2D
	GApplication::TurnZBufferOff();
	mpTexturedShader->EnableAlpha();
	cb.View = GMathMF(XMMatrixIdentity());
	cb.Projection = mCamera.GetOrtho();
	
	for (auto it=mText.begin(); it!=mText.end(); it++)
	{
		mpMaterialsManager->SetCurrentMaterial((*it)->GetMaterialName());
		cb.World = (*it)->GetWorld();
		md3dContext->UpdateSubresource(mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0);
		(*it)->draw();
	}

    mSwapChain->Present(0, 0);
}

HRESULT TexturedModels::CreateShaders()
{
	//// Compile the vertex shader
 //   ID3DBlob* pVSBlob = NULL;
	//HRESULT hr = CompileShaderFromFile(mShaderFileName.c_str(), "VS", "vs_4_0", &pVSBlob);
 //   if(FAILED(hr))
 //   {
 //       MessageBox(NULL, 
	//		L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
	//		L"Error", 
	//		MB_OK);
 //       return hr;
 //   }

	//// Create the vertex shader
	//hr = md3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
	//	&mpVertexShader);
	//if(FAILED(hr))
	//{	
	//	pVSBlob->Release();
 //       return hr;
	//}

 //   // Define the input layout
 //   D3D11_INPUT_ELEMENT_DESC layout[] =
 //   {
 //       { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
 //       { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//};
	//UINT numElements = ARRAYSIZE( layout );

 //   // Create the input layout
	//hr = md3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
 //                                         pVSBlob->GetBufferSize(), &mpVertexLayout );
	//pVSBlob->Release();
	//if(FAILED(hr))
 //       return hr;    

	//// Compile the pixel shader
	//ID3DBlob* pPSBlob = NULL;
 //   hr = CompileShaderFromFile( mShaderFileName.c_str(), "PS", "ps_4_0", &pPSBlob );
 //   if(FAILED(hr))
 //   {
 //       MessageBox( NULL,
 //                   L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
	//				L"Error", 
	//				MB_OK );
 //       return hr;
 //   }

	//// Create the pixel shader
	//hr = md3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	//pPSBlob->Release();
 //   if(FAILED(hr))
 //       return hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    HR(md3dDevice->CreateBuffer(&bd, NULL, &mpShaderConstantBuffer));
	md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);

	// create blend state
	//D3D11_BLEND_DESC blend_desc;
	//ZeroMemory(&blend_desc, sizeof(blend_desc));
	//blend_desc.AlphaToCoverageEnable = true;
	//blend_desc.IndependentBlendEnable = true;
	//blend_desc.RenderTarget[0].BlendEnable = TRUE;
	//blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	//blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	//blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	//blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	//blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	//blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	//blend_desc.RenderTarget[0].RenderTargetWriteMask = 0x0f; 
	//md3dDevice->CreateBlendState(&blend_desc, &mpBlendState);
	
	return S_OK;
}

void TexturedModels::SetShaders()
{	
	// Set the input layout
 //   md3dContext->IASetInputLayout( mpVertexLayout );
	//md3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//md3dContext->VSSetShader(mpVertexShader, nullptr, 0);
	//md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);
	//
	//md3dContext->PSSetShader(mpPixelShader, nullptr, 0);

	//// set alpha-blending for texture opacity
	//md3dContext->OMSetBlendState(mpBlendState, nullptr, 0xffffffff);
}

HRESULT TexturedModels::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
										   LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA(static_cast<char*>(pErrorBlob->GetBufferPointer()));
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

TexturedModels::~TexturedModels(void)
{
	for (auto it=mText.begin(); it<mText.end(); it++)
	{
		delete *it;
	}
	delete mSky;
	for (auto it=mSprites.begin(); it<mSprites.end(); it++)
	{
		delete *it;
	}
	delete mpMaterialsManager;
	delete mpTexturedShader;

    if( md3dContext )
        md3dContext->ClearState();

	ReleaseCOM(mpShaderConstantBuffer);
}