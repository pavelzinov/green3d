#include "GTexturedVertex.h"
using namespace Game;

GTexturedVertex::GTexturedVertex(const GTexturedVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mTexturedCoordinates = vertex.mTexturedCoordinates;
}

GTexturedVertex::GTexturedVertex(XMFLOAT3 position, XMFLOAT2 texturedCoordinates)
{
	this->mPosition = position;
	this->mTexturedCoordinates = texturedCoordinates;
}

GTexturedVertex::~GTexturedVertex(void)
{
}

void GTexturedVertex::SetData(float x, float y, float z, float u, float v)
{
	this->mPosition.x = x;
	this->mPosition.y = y;
	this->mPosition.z = z;
	this->mTexturedCoordinates.x = u;
	this->mTexturedCoordinates.y = v;
}

void GTexturedVertex::SetData(XMFLOAT3 position, XMFLOAT2 texturedCoordinates)
{
	SetData(position.x, position.y, position.z, texturedCoordinates.x, texturedCoordinates.y);
}

UINT GTexturedVertex::getDataSize()
{
	return sizeof(GTexturedVertexData);
}
