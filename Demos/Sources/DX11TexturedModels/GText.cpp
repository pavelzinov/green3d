#include "GText.h"
using namespace Game;


GText::GText(ID3D11Device* device, ID3D11DeviceContext* context, 
			 int screen_width, int screen_height, 
			 const GMaterial& material, std::string text)
{
	mpDevice = device;
	mpContext = context;
	mVertexBuffer = nullptr;
	mIndexBuffer = nullptr;
	mCapacity = text.size();
	mText = text;
	mRealPosition = XMINT2(0, 0);
	mWorld = GMathMF(XMMatrixIdentity());
	mMaterialName = material.GetMaterialName();

	this->SetPosition(XMUINT2(0, 0), screen_width, screen_height);

	this->DictionaryFromFile(material.GetTextureFileName() + ".fntdesc");
	this->GenerateGeometry();
	this->ConstructBuffers();
}


GText::~GText(void)
{
	ReleaseCOM(mIndexBuffer);
	ReleaseCOM(mVertexBuffer);
}

void GText::SetText(std::string text)
{
	if (text == mText)
		return;

	this->mText = text;
	this->GenerateGeometry();
	if (text.size() > mCapacity)
	{
		mCapacity = text.size();
		this->ConstructBuffers();
	}
	else
	{
		this->UpdateBuffers();
	}
}

void GText::SetPosition(XMUINT2 position, int screen_width, int screen_height)
{
	mRealPosition.x = position.x;
	mRealPosition.y = -static_cast<int>(position.y);
	mWorld = GMathMF(XMMatrixTranslation(static_cast<float>(-screen_width)/2.0f + static_cast<float>(position.x), 
		static_cast<float>(screen_height)/2.0f - static_cast<float>(position.y), 0.0f));
}

void GText::SetMaterial(const GMaterial& material) 
{
	if (mMaterialName == material.GetMaterialName())
		return;

	mMaterialName = material.GetMaterialName();
	DictionaryFromFile(material.GetTextureFileName() + ".fntdesc");
	GenerateGeometry();
	UpdateBuffers();
}

void GText::DictionaryFromFile(std::string font_filename)
{
	std::ifstream file_data;
	file_data.open(font_filename, std::ios::out);
	GLetter letter;

	mDictionary.clear();

	if (file_data.is_open())
	{
		while (file_data >> letter)
		{			
			mDictionary.push_back(letter);
		}
		file_data.close();
	}
}

void GText::GenerateGeometry()
{
	mVertices.clear();
	mIndices.clear();

	XMFLOAT2 top_left_current(0.0f, 0.0f);
	uint32_t current_vertex_index = 0;
	for (uint32_t i = 0; i < mText.size(); i++)
	{
		if (mText[i] == '\n')
		{// if letter is new line symbol, reset x position and lower y-position
			top_left_current.x = 0.0f;
			top_left_current.y -= mDictionary[0].mHeight - 1.0f;
			continue;
		}

		if (mText[i] == ' ')
		{// if letter is `space` - skip 5 pixels
			top_left_current.x += 5.0f;
			continue;
		}

		GLetter letter = GetLetter(mText[i]);
		//0 Bottom-Left
		mVertices.push_back(GTexturedVertex(
			XMFLOAT3(top_left_current.x, top_left_current.y - letter.mHeight, 0.0f), 
			XMFLOAT2(letter.mStartPosition, 1.0f)));
		//1 Bottom-Right
		mVertices.push_back(GTexturedVertex(
			XMFLOAT3(top_left_current.x + letter.mWidth, top_left_current.y - letter.mHeight, 0.0f), 
			XMFLOAT2(letter.mEndPosition, 1.0f)));
		//2 Top-Left
		mVertices.push_back(GTexturedVertex(
			XMFLOAT3(top_left_current.x, top_left_current.y, 0.0f), 
			XMFLOAT2(letter.mStartPosition, 0.0f)));
		//3 Top-Right
		mVertices.push_back(GTexturedVertex(
			XMFLOAT3(top_left_current.x + letter.mWidth, top_left_current.y, 0.0f), 
			XMFLOAT2(letter.mEndPosition, 0.0f)));

		mIndices.push_back(current_vertex_index + 0);
		mIndices.push_back(current_vertex_index + 2);
		mIndices.push_back(current_vertex_index + 3);
		mIndices.push_back(current_vertex_index + 0);
		mIndices.push_back(current_vertex_index + 3);
		mIndices.push_back(current_vertex_index + 1);

		current_vertex_index += 4;
		// Change top-left point so we can draw next letter
		top_left_current.x += letter.mWidth + 1.0f;
	}
}

void GText::ConstructBuffers()
{
	ReleaseCOM(mIndexBuffer);
	ReleaseCOM(mVertexBuffer);

	if (mVertices.size() <= 0)
		return;

	// Feed vertex data
	uint32_t verticesNumber = mVertices.size();
	GTexturedVertexData *vertices = new GTexturedVertexData[4 * mCapacity];
	for (uint32_t i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
	}

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GTexturedVertex::getDataSize() * 4 * mCapacity;
	verticesDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DYNAMIC;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mpDevice->CreateBuffer(&verticesDesc, &verticesData, &mVertexBuffer));
	delete[] vertices;

	uint32_t* indices = new uint32_t[6 * mCapacity];
	for (uint32_t i=0; i<mIndices.size(); i++)
	{
		indices[i] = mIndices[i];
	}

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint32_t) * 6 * mCapacity;
	indicesDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DYNAMIC;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = indices;
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mpDevice->CreateBuffer(&indicesDesc, &indicesData, &mIndexBuffer));
	delete[] indices;
}

void GText::UpdateBuffers()
{
	uint32_t verticesNumber = mVertices.size();
	if (verticesNumber <= 0)
		return;

	GTexturedVertexData *vertices = new GTexturedVertexData[verticesNumber];
	for (uint32_t i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
	}

	D3D11_MAPPED_SUBRESOURCE mappedVertices;
	mpContext->Map(mVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedVertices);
	memcpy(mappedVertices.pData, (void*)vertices, (verticesNumber*GTexturedVertex::getDataSize()));
	mpContext->Unmap(mVertexBuffer, 0);
	delete[] vertices;

	D3D11_MAPPED_SUBRESOURCE mappedIndices;
	mpContext->Map(mIndexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedIndices);
	memcpy(mappedIndices.pData, (void*)&mIndices[0], (mIndices.size()*sizeof(uint32_t)));
	mpContext->Unmap(mIndexBuffer, 0);
}

GLetter GText::GetLetter(char letter)
{
	for (auto it = mDictionary.begin(); it != mDictionary.end(); it++)
	{
		if (it->mLetter == letter)
			return *it;
	}
	return GLetter('\0', 0, 0, 0.0f, 0.0f);
}

void GText::draw()
{
	if (mVertices.size() <= 0)
		return;

	uint32_t strides = GTexturedVertex::getDataSize();
	uint32_t offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &strides, &offset);
	mpContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	mpContext->DrawIndexed(mIndices.size(), 0, 0);
}
