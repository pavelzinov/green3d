#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GColoredVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GMaterialsManager.h"
#include "GBillboard.h"
#include "GShader.h"
#include "GText.h"

using namespace Game;

struct ConstantBuffer
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
};

// Class TexturedModels.
class TexturedModels : public GApplication
{
    // Public constructor and destructor
public:
    TexturedModels(HINSTANCE hInstance);
    ~TexturedModels(void);

public:
    void initApp();

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
    // Create input layout and load vertex and pixel shaders
    HRESULT CreateShaders(); 
	// Set vertex and pixel shaders before drawing
    void SetShaders();
	// Shader file entry point. Must be .fx file
	std::wstring mShaderFileName;
	// Compile shaders from file
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, 
		ID3DBlob** ppBlobOut);

private:
	ID3D11Buffer*		mpShaderConstantBuffer; // variable to access constant buffer in shaders

	GMaterialsManager*			mpMaterialsManager;
	GShader<GTexturedVertex>*	mpTexturedShader;

private:
    GCamera	mCamera;	// main camera
    float	mDistance;	// camera position

	GRandom	mRandom; // random generator

	std::vector<GMesh<GTexturedVertex>*>mSprites;
	GBillboard<GTexturedVertex>*		mSky;
	std::vector<GText*>					mText;
};