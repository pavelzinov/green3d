#include "GLetter.h"
namespace Game
{

GLetter::GLetter(char letter, uint32_t width, uint32_t height, float start, float end)
{
	mLetter = letter;
	mWidth = width;
	mHeight = height;
	mStartPosition = start;
	mEndPosition = end;
}

std::ifstream& operator>>(std::ifstream& ifs, GLetter& letter)
{
	ifs >> letter.mLetter;
	ifs >> letter.mWidth;
	ifs >> letter.mHeight;
	ifs >> letter.mStartPosition;
	ifs >> letter.mEndPosition;
	return ifs;
}

}