#pragma once
#include "GUtility.h"
#include "GTexturedVertex.h"

namespace Game
{

class GLetter
{
public:
	GLetter() {}
	GLetter(char letter, uint32_t width, uint32_t height, float start, float end);
	~GLetter(void) {}

public:
	char		mLetter;
	uint32_t	mWidth;
	uint32_t	mHeight;
	float		mStartPosition;
	float		mEndPosition;

private:
	friend std::ifstream& operator>>(std::ifstream& ifs, GLetter& letter);
};
std::ifstream& operator>>(std::ifstream& ifs, GLetter& letter);

}