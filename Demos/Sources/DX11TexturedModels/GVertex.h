#pragma once
#include "GUtility.h"
#include <vector>

namespace Game
{
/////////////////////////////////////////////////
// Basic class for storing vertex data. Basic 
// vertex has only position in 3D space.
/////////////////////////////////////////////////
class GVertex
{
public:
	// Construct vertex with 0, 0, 0 position
	GVertex(void);
	// Construct vertex with given XMFLOAT3 position
	GVertex(XMFLOAT3 position);
	virtual ~GVertex(void);

	// Set vertex data. In the case on basic vertex class it is vertex position. 
	// Here it is set per-axis.
	void SetData(float x, float y, float z);
	// Set vertex data. In the case of basic vertex class it is vertex position.
	// Here it is set by XMFLOAT3 vector.
	void SetData(XMFLOAT3 position);

	// Get vertex data component size in bytes. Useful when creating vertex buffers.
	static UINT getDataSize();

	XMFLOAT3 mPosition;
};

}

