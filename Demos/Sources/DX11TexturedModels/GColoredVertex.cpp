#include "GColoredVertex.h"
using namespace Game;

GColoredVertex::GColoredVertex(const GColoredVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mColor = vertex.mColor;
}

GColoredVertex::GColoredVertex(XMFLOAT3 position, XMFLOAT4 color) : GVertex(position)
{
	this->mColor = color;
}

GColoredVertex::~GColoredVertex(void)
{

}

void GColoredVertex::SetData(float x, float y, float z, float r, float g, float b, float a)
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;

	mColor.x = r;
	mColor.y = g;
	mColor.z = b;
	mColor.w = a;	
}

void GColoredVertex::SetData(XMFLOAT3 position, XMFLOAT4 color)
{
	mPosition = position;
	this->mColor = color;
}

UINT GColoredVertex::getDataSize()
{
	return sizeof(GColoredVertexData);
}
