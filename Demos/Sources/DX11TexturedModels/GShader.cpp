#include "GShader.h"
using namespace Game;

template <>
HRESULT GShader<GTexturedVertex>::LoadShader(std::string filename)
{
	// Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile(StringToWstring(filename).c_str(), "VS", "vs_4_0", &pVSBlob);
    if(FAILED(hr))
    {
        MessageBox(NULL, 
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK);
        return hr;
    }

	// Create the vertex shader
	hr = mpDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader);
	if(FAILED(hr))
	{	
		pVSBlob->Release();
        return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
	hr = mpDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if(FAILED(hr))
        return hr;    

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile(StringToWstring(filename).c_str(), "PS", "ps_4_0", &pPSBlob );
    if(FAILED(hr))
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the pixel shader
	hr = mpDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
    if(FAILED(hr))
        return hr;

	// create blend state
	D3D11_BLEND_DESC blend_desc;
	ZeroMemory(&blend_desc, sizeof(blend_desc));
	blend_desc.AlphaToCoverageEnable = true;
	blend_desc.IndependentBlendEnable = true;
	blend_desc.RenderTarget[0].BlendEnable = TRUE;
	blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	HRR(mpDevice->CreateBlendState(&blend_desc, &mpBlendStateEnableAlpha));
	mpCurrentBlendState = mpBlendStateEnableAlpha;

	mpBlendStateDisableAlpha = nullptr;
	//blend_desc.RenderTarget[0].BlendEnable = false;
	//HRR(mpDevice->CreateBlendState(&blend_desc, &mpBlendStateDisableAlpha));

	return S_OK;
}

template <>
void GShader<GTexturedVertex>::SetShader()
{
	mpContext->IASetInputLayout(mpVertexLayout);
	mpContext->IASetPrimitiveTopology(mTopology);

	mpContext->VSSetShader(mpVertexShader, nullptr, 0);
	
	mpContext->PSSetShader(mpPixelShader, nullptr, 0);

	// set alpha-blending for texture opacity
	mpContext->OMSetBlendState(mpCurrentBlendState, nullptr, 0xffffffff);
}