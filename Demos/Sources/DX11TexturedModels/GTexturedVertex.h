#pragma once
#include "GVertex.h"

namespace Game
{

// aligned textured vertex data storage
struct GTexturedVertexData
{
	XMFLOAT3 pos;
	XMFLOAT2 texC;
};

class GTexturedVertex :	public GVertex
{
public:
	GTexturedVertex(const GTexturedVertex &vertex);
	GTexturedVertex(XMFLOAT3 position, XMFLOAT2 texturedCoordinates);
	~GTexturedVertex(void);

	void SetData(float x, float y, float z, float u, float v);
	void SetData(XMFLOAT3 position, XMFLOAT2 texturedCoordinates);

	static UINT getDataSize();

	XMFLOAT2 mTexturedCoordinates;
};

}