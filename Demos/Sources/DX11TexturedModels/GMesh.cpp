#pragma once
#include "GMesh.h"
using namespace Game;

template <>
void* GMesh<GColoredVertex>::GetVertexData()
{
	UINT verticesNumber = GetVertexCount();
	GColoredVertexData *vertices = new GColoredVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].color = mVertices[i].mColor;
	}
	return static_cast<void*>(vertices);
}

template <>
void* GMesh<GTexturedVertex>::GetVertexData()
{
	UINT verticesNumber = GetVertexCount();
	GTexturedVertexData *vertices = new GTexturedVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
	}
	return static_cast<void*>(vertices);
}

template <>
void GMesh<GColoredVertex>::FromFileData(std::vector<std::string> &file_data)
{
	std::string vertex_type = file_data[0];
	if (vertex_type != "GColoredVertex") return;

	unsigned int vertex_count = 0;
	std::stringstream line_as_stream;	

	line_as_stream << file_data[1];
	line_as_stream >> vertex_count;
	line_as_stream.clear();

	float x, y, z, r, g, b, a;
	GColoredVertex vertex(XMFLOAT3(0, 0, 0), XMFLOAT4(0, 0, 0, 0));
	
	// reading vertices
	mVertices.clear();
	mVertices.reserve(vertex_count);
	auto index_data_start = vertex_count + 2;
	for (unsigned int i=2; i < index_data_start; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> x >> y >> z >> r >> g >> b >> a;
		vertex.SetData(x, y, z, r, g, b, a);
		mVertices.push_back(vertex);
		line_as_stream.clear();
	}

	// reading indices
	mIndices.clear();
	mIndices.reserve((file_data.size() - vertex_count - 2) * 3);
	UINT i1 = 0, i2 = 0, i3 = 0;
	UINT file_size = file_data.size();
	for (auto i=index_data_start; i<file_size; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> i1 >> i2 >> i3;
		mIndices.push_back(i1 - 1);
		mIndices.push_back(i2 - 1);
		mIndices.push_back(i3 - 1);
		line_as_stream.clear();
	}

	this->ConstructBuffers();
}