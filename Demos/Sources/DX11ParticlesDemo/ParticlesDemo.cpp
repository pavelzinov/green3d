#include "ParticlesDemo.h"
#include "DDSTextureLoader.h"
//------------------------------------------------------------------------------------
ParticlesDemo::ParticlesDemo( HINSTANCE hInstance ) : GApplication( hInstance )
{
    // Init background color.
    mApplicationDesc.mScreenClearColor = GColor::LIGHT_BLUE;

    mDistance = 300.0f;

	//mPlayer.Initialize( 60.0f, mApplicationDesc.mClientWidth,  mApplicationDesc.mClientHeight, 0.01f, 1000.0f );

	mCamera.Position( XMFLOAT3( 0.0f, mDistance, -mDistance ) );
	mCamera.Target( XMFLOAT3( 0.0f, 10.0f, 0.0f ) );
	mCamera.InitProjMatrix( GMath::PIDIV4, mApplicationDesc.mClientWidth,
		mApplicationDesc.mClientHeight, 1.0f, 1000.0f );
	mCamera.InitOrthoMatrix( mApplicationDesc.mClientWidth , mApplicationDesc.mClientHeight, 
		1.0f, 1000.0f );
}
//------------------------------------------------------------------------------------
void ParticlesDemo::initApp()
{
	GApplication::initApp();

	SetUPS(  100  );

	// Load shaders.	
	mpShadersManager = new GShadersManager( mpDevice, mpContext );
	std::vector<std::string> shader_names;
	std::vector<std::string> shader_files;
	std::vector<std::string> shader_main_routine_names;

	shader_names.push_back( "VS" );
	shader_files.clear();
	shader_files.push_back( "Data/Shaders/depth.hlsl" );
	shader_main_routine_names.push_back( "VS" );
	mpShadersManager->AddShaderPass( "DepthShader", shader_names, shader_files, shader_main_routine_names );

	shader_names.push_back( "PS" );
	shader_files.clear();
	shader_files.push_back( "Data/Shaders/font.hlsl" );
	shader_files.push_back( "Data/Shaders/font.hlsl" );
	shader_main_routine_names.push_back( "PS" );
	mpShadersManager->AddShaderPass( "FontShader", shader_names, shader_files, shader_main_routine_names );

	shader_files.clear();
	shader_files.push_back( "Data/Shaders/overlay.hlsl" );
	shader_files.push_back( "Data/Shaders/overlay.hlsl" );
	mpShadersManager->AddShaderPass( "OverlayShader", shader_names, shader_files, 
		shader_main_routine_names );

	shader_files.clear();
	shader_files.push_back( "Data/Shaders/contact_hardening_shadows.hlsl" );
	shader_files.push_back( "Data/Shaders/contact_hardening_shadows.hlsl" );
	mpShadersManager->AddShaderPass( "ShadowMultitextureShader", shader_names, shader_files, 
		shader_main_routine_names );

	shader_files.clear();
	shader_files.push_back( "Data/Shaders/projected_light_mapping.hlsl" );
	shader_files.push_back( "Data/Shaders/projected_light_mapping.hlsl" );
	mpShadersManager->AddShaderPass( "LightmapProjectionShader", shader_names, shader_files, 
		shader_main_routine_names );

	shader_files.clear();
	shader_files.push_back( "Data/Shaders/sky_dome.hlsl" );
	shader_files.push_back( "Data/Shaders/sky_dome.hlsl" );
	mpShadersManager->AddShaderPass( "CubeShader", shader_names, shader_files, 
		shader_main_routine_names );

	shader_files.clear();
	shader_files.push_back( "Data/Shaders/reflection.hlsl" );
	shader_files.push_back( "Data/Shaders/reflection.hlsl" );
	mpShadersManager->AddShaderPass( "ReflectionShader", shader_names, shader_files, 
		shader_main_routine_names );

	shader_names.push_back( "GS" );
	shader_files.clear();
	shader_files.push_back( "Data/Shaders/particles.hlsl" );
	shader_files.push_back( "Data/Shaders/particles.hlsl" );
	shader_files.push_back( "Data/Shaders/particles.hlsl" );
	shader_main_routine_names.push_back( "GS" );
	mpShadersManager->AddShaderPass( "ParticlesShader", shader_names, shader_files, 
		shader_main_routine_names );
	mpShadersManager->EnableAlpha();

	// Load textures.
	mpTexturesManager = new GTexturesManager( mpDevice, mpContext,
		mpWindowRenderTargetView, mpWindowDepthStencilView );
	mpTexturesManager->LoadAddTexturesFromFile( "Data/Textures/scene_textures.textures" );
	mpTexturesManager->LoadAddTexturesFromMtlFile( "Data/Materials/scene_materials.mtl" );
	// Shadow map is 192 MBytes in size.
	mpTexturesManager->AddRenderedTexture( "SceneReflection", 
		mpWindowRenderTargetView, mpWindowDepthStencilView, 
		mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight, 
		mSampleCount, mSampleQuality );

	// Load materials.
	mpMaterialsManager = new GMaterialsManager();
	mpMaterialsManager->LoadAddMaterialsFromFile( "Data/Materials/scene_materials.mtl", 
		mpTexturesManager );
	GMaterial material;
	material.mName = "ReflectiveFloor";
	material.map_Kd = "Water";
	material.map_Ka = "Water";
	material.map_Ks = "SceneReflection";
	// add material, where diffuse and ambient maps use DepthTexture texture
	mpMaterialsManager->AddMaterial( material );

	// Load light.
	mLight.mType = GLIGHT_TYPE_PARALLEL;
	mLight.SetDirection( XMFLOAT3( 0.0f, -1.0f, -1.0f ) );
	mLight.SetPos( 0.0f, 1000.0f, 0.0f );
	mLight.mDiffuseColor = XMFLOAT4( 0.7f, 0.7f, 0.7f, 1.0f );
	mLight.mAmbientColor = XMFLOAT4( 0.7f, 0.7f, 0.7f, 1.0f );
	mLight.mSpecularColor = XMFLOAT4( 0.7f, 0.7f, 0.7f, 1.0f );
	mLight.mAttenuationParameters = XMFLOAT3( 0.0f, 0.01f, 0.0f );
	mLight.mSpotPower = 10.0f;
	mLight.mRange = 100.0f;

	// Load text.
	mText.push_back( new GText( mpDevice, mpContext, 
		mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight,
		"Data/Fonts/courier_new_12_dark_green.font", 
		"" ) );
	mText.back()->Position( XMINT2( 4, 4 ) );
	mText.back()->SetColor( GColor::WHITE );

	// Load models.
	GObjFile obj_file;

	//obj_file.Load( "Data/Models/lp670.obj" );
	//mpTexturesManager->LoadAddTexturesFromObjFile( obj_file );
	//mpMaterialsManager->LoadAddMaterialsFromObjFile( obj_file, mpTexturesManager );
	//for ( uint i = 0; i < obj_file.GetGroups().size(); ++i )
	//{
	//	mModels.push_back( new GModel( obj_file.GetGroups()[i].mName, 
	//		mpDevice, mpContext ) );
	//	mModels.back()->LoadFromObj( obj_file, i, *mpMaterialsManager );
	//	mModels.back()->MergeMeshes();
	//	mModels.back()->Scale( 20.0f );
	//	mModels.back()->RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), 90.0f );
	//}

	obj_file.Load( "Data/Models/multitextured.obj" );
	mpTexturesManager->LoadAddTexturesFromObjFile( obj_file );
	mpMaterialsManager->LoadAddMaterialsFromObjFile( obj_file, mpTexturesManager );
	mModels.push_back( new GModel( "MultitexturedCube0", mpDevice, mpContext ) );
	mModels.back()->LoadFromObj( obj_file, 0, *mpMaterialsManager );
	mModels.back()->MergeMeshes();
	mModels.back()->Scale( 2.0f );

	mModels.push_back( new GModel( "MultitexturedTeapot0", mpDevice, mpContext ) );
	mModels.back()->LoadFromObj( obj_file, 1, *mpMaterialsManager );
	mModels.back()->MergeMeshes();
	mModels.back()->Scale( 2.0f );
	mModels.back()->Move( 20.0f, 0.0f, -100.0f );

	// Load sky dome.
	mSkyDome = new GSkyDome( mpDevice, mpContext, "Data/Models/sky_dome.obj",
		100.0f, "Sky Dome Texture" );

	obj_file.Load( "Data/Models/plane_l100.obj" );
	mFloor = new GModel( "ReflectiveFloor", mpDevice, mpContext );
	mFloor->LoadFromObj( obj_file, 0, *mpMaterialsManager );
	mFloor->MergeMeshes();
	mFloor->Scale( 5.0f );
	mFloor->SetMaterial( mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ) );
	mFloor->Move( XMFLOAT3( -30.0f, -10.0f, 0.0f ) );

	mpNonReflectiveFloor = new GModel( "NonReflectiveFloor", mpDevice, mpContext );
	mpNonReflectiveFloor->LoadFromObj( obj_file, 0, *mpMaterialsManager );
	mpNonReflectiveFloor->MergeMeshes();
	mpNonReflectiveFloor->Scale( 5.0f );
	mpNonReflectiveFloor->SetMaterial( mpMaterialsManager->GetMaterialByName( "Sand" ) );
	mpNonReflectiveFloor->Move( XMFLOAT3( -30.0f, -5.0f, 0.0f ) );

	//obj_file.Load( "Data/Models/plane_l100.obj" );
	//mDebugWindow = new GMesh( "DebugWindow", obj_file, 0, 
	//	mpMaterialsManager, mpDevice, mpContext );
	//mDebugWindow->SetMaterial( mpMaterialsManager->GetMaterialByName( "DepthTexture" ) );
	//mDebugWindow->Scale( 2.0f *  
	//	mApplicationDesc.mClientWidth / mApplicationDesc.mClientHeight, 2.0f, 2.0f );
	//mDebugWindow->Move( 100.0f, -1.0f * ( mApplicationDesc.mClientHeight - 100.0f ), 0.0f );
	//mDebugWindow->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );

	obj_file.Load( "Data/Models/plane_l100.obj" );
	mCursor = new GMesh( "Cursor", obj_file, 0, 
		*mpMaterialsManager, mpDevice, mpContext );
	mCursor->SetMaterial( mpMaterialsManager->GetMaterialByName( "Cursor" ) );
	mCursor->Scale( 0.32f, 1.0f, 0.32f );
	mCursor->RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -90.0f );
	mCursor->Move( 16.0f, -16.0f, 0.0f );

	mSliders.append( "gShadowMapSize", new GUISlider() );
	mSliders["gShadowMapSize"]->Initialize( "gShadowMapSize", XMFLOAT2( 4.0f, 90.0f ), 200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1024.0f, 4096.0f, 1024.0f, 1024.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	mSliders.append( "gSunWidth", new GUISlider() );
	mSliders["gSunWidth"]->Initialize( "gSunWidth", XMFLOAT2( 4.0f, 150.0f ), 200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1.0f, 100.0f, 1.0f, 2.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	mSliders.append( "gParticleSize", new GUISlider() );
	mSliders["gParticleSize"]->Initialize( "gParticleSize", XMFLOAT2( 4.0f, 210.0f ), 200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 0.1f, 100.0f, 0.1f, 3.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	mSliders.append( "ParticlesPerSecond", new GUISlider() );
	mSliders["ParticlesPerSecond"]->Initialize( "ParticlesPerSecond", XMFLOAT2( 4.0f, 270.0f ), 
		200.0f, 20.0f, GColor::WHITE,
		GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 100.0f, 10000.0f, 1.0f, 1000.0f, mpDevice, mpContext,
		mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gShadowFactor", new GUISlider() );
	//mSliders["gShadowFactor"]->Initialize( "gShadowFactor", XMFLOAT2( 4.0f, 330.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 0.0f, 1.0f, 0.1f, 0.01f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	//mSliders.append( "gFilterSamples", std::make_shared<GUISlider>( GUISlider() ) );
	//mSliders["gFilterSamples"]->Initialize( "gFilterSamples", XMFLOAT2( 4.0f, 390.0f ), 200.0f, 20.0f, GColor::WHITE,
	//	GColor::LIGHT_YELLOW_GREEN, GColor::GREEN, 1.0f, 32.0f, 1.0f, 8.0f, mpDevice, mpContext,
	//	mApplicationDesc, 0.0f, 10.0f );

	mpParticles = new GParticles( mpDevice, mpContext, 2.0f, 
		XMFLOAT3( 40.0f, 40.0f, 0.0f ), 5.0f, 1000.0f, "Snowflake" );

	// Call this at the end on initApp()
	onResize();
}
//------------------------------------------------------------------------------------
void ParticlesDemo::onResize()
{
    GApplication::onResize();
	mCamera.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	//mPlayer.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	// Update text position when resizing window
	mText[0]->OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	// Resize render texture
	mpTexturesManager->OnWindowResize( mpWindowRenderTargetView, mpWindowDepthStencilView );
	//mpTexturesManager->ResizeTexture( "SceneReflection", );
	mLight.OnResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );

	for (auto &slider : mSliders)
	{
		slider.second->OnWindowResize( mApplicationDesc.mClientWidth, mApplicationDesc.mClientHeight );
	}
}
//------------------------------------------------------------------------------------
void ParticlesDemo::updateScene( float dSeconds )
{
    GApplication::updateScene( dSeconds );
	processInput( dSeconds );

	mpParticles->Update( dSeconds );

	for ( auto &model : mModels )
	{		
		model->RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), 0.3f );
	}

	//mPlayer.Update( dSeconds );

	std::ostringstream stream;
	stream << "Video card: " << mApplicationDesc.mVideoCardName << 
		", " << mApplicationDesc.mVideoCardFreeMemory << " MB" <<
		"\nFPS: " << mApplicationDesc.mFramesPerSecond <<
		"\nMilliseconds per frame: " << mApplicationDesc.mMillisecondsPerFrame <<
		"\nMouse X: " << mInput.MousePos().x << " Y: " << mInput.MousePos().y <<
		"\nParticles rendered: " << mpParticles->GetCount();
	mText[0]->SetText( stream.str() );

	//static float milliseconds_per_capture_screen = 0.0f;
	//if ( mInput.IsKeyPressed( '5' ) )
	//{
	//	milliseconds_per_capture_screen = mTimer.getGameTime();
	//	GApplication::GetScreen( L"image1.jpg", 75 );
	//	milliseconds_per_capture_screen = ( mTimer.getGameTime() - milliseconds_per_capture_screen ) * 1000.0f;
	//}

	//stream << "\nMs per screen capture:" << milliseconds_per_capture_screen;
}
//------------------------------------------------------------------------------------
void ParticlesDemo::processInput( float dSeconds )
{
	GApplication::processInput( dSeconds );
	
	// Light controls:
	if ( mInput.IsKeyDown( VK_LEFT ) )
	{
		//mLight.SetDirection( GMathVF( XMVector3Rotate( GMathFV( mLight.GetDirection() ), 
		//	XMQuaternionRotationAxis( GMathFV( XMFLOAT3( 0.0f, 0.0f, -1.0f ) ), -0.01f ) ) ) );
		//mLight.SetPos( mLight.GetPos().x - 3.0f, mLight.GetPos().y, mLight.GetPos().z );
		
		// Move particles.
		XMFLOAT3 new_emitter_position = mpParticles->GetEmitterPosition();
		new_emitter_position.x -= 100.0f * dSeconds;
		mpParticles->SetEmitterPosition( new_emitter_position );
	}
	if ( mInput.IsKeyDown( VK_RIGHT ) )
	{
		//mLight.SetDirection( GMathVF( XMVector3Rotate( GMathFV( mLight.GetDirection() ), 
		//	XMQuaternionRotationAxis( GMathFV( XMFLOAT3( 0.0f, 0.0f, -1.0f ) ), 0.01f ) ) ) );
		//mLight.SetPos( mLight.GetPos().x + 3.0f, mLight.GetPos().y, mLight.GetPos().z );

		// Move particles.
		XMFLOAT3 new_emitter_position = mpParticles->GetEmitterPosition();
		new_emitter_position.x += 100.0f * dSeconds;
		mpParticles->SetEmitterPosition( new_emitter_position );
	}
	if ( mInput.IsKeyDown( VK_UP ) )
	{
		// Move particles.
		XMFLOAT3 new_emitter_position = mpParticles->GetEmitterPosition();
		new_emitter_position.z += 100.0f * dSeconds;
		mpParticles->SetEmitterPosition( new_emitter_position );
	}
	if ( mInput.IsKeyDown( VK_DOWN ) )
	{
		// Move particles.
		XMFLOAT3 new_emitter_position = mpParticles->GetEmitterPosition();
		new_emitter_position.z -= 100.0f * dSeconds;
		mpParticles->SetEmitterPosition( new_emitter_position );
	}
	if ( mInput.IsKeyDown( 'A' ) ) mCamera.RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), -1.0f );
	if ( mInput.IsKeyDown( 'D' ) ) mCamera.RotateSelf( XMFLOAT3( 0.0f, 1.0f, 0.0f ), +1.0f );
	if ( mInput.IsKeyDown( 'W' ) ) mCamera.RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), -1.0f );
	if ( mInput.IsKeyDown( 'S' ) ) mCamera.RotateSelf( XMFLOAT3( 1.0f, 0.0f, 0.0f ), +1.0f );

	// Control shader parameters through sliders:
	for (auto &slider : mSliders)
	{
		slider.second->OnInput( mInput );
	}

	// Change shadow map size based on corresponding slider current value.
	if ( mSliders["gShadowMapSize"]->GetCurrentValue() != 
		mpTexturesManager->GetShadowMapSize() )
	{
		mpTexturesManager->ResizeDepthTexture(  
			static_cast<uint>( mSliders["gShadowMapSize"]->GetCurrentValue() ) );
	}

	// Change particles size.
	if ( mSliders["gParticleSize"]->GetCurrentValue() != 
		mpParticles->GetParticleSize() )
	{
		mpParticles->SetParticleSize( mSliders["gParticleSize"]->GetCurrentValue() );
	}

	// Change particles size
	if ( mSliders["ParticlesPerSecond"]->GetCurrentValue() != 
		mpParticles->GetParticlesPerSecond() )
	{
		mpParticles->SetParticlesPerSecond( 
			static_cast<uint>( mSliders["ParticlesPerSecond"]->GetCurrentValue() ) );
	}

	// Clear input when done processing it.
	mInput.Clear();
}
//------------------------------------------------------------------------------------
void ParticlesDemo::drawScene()
{
	GApplication::drawScene();
	//GApplication::BackfaceCullingNone();

	//------------------------------------------------------------------------------------
	// Render scene's depth.
	mpTexturesManager->RenderToDepth();
	GApplication::TurnZBufferOn();

	//------------------------------------------------------------------------------------
	// Render scene's depth values into depth texture.
	mpShadersManager->at( "DepthShader" )->ClearConstantBuffers();
	mpShadersManager->at( "DepthShader" )->SetShader();
	mpShadersManager->DisableAlpha();
	mpShadersManager->at( "DepthShader" )->SetConstant( "PerFrame", "View", 
		mLight.mCamera.View() );
	mpShadersManager->at( "DepthShader" )->SetConstant( "PerFrame", "Projection", 
		mLight.mCamera.Ortho() );
	mpShadersManager->at( "DepthShader" )->UpdateConstantBuffer( "PerFrame" );
	for ( const auto &model : mModels)
	{
		for ( const auto &mesh : model->AccessMeshes() )
		{
			mpShadersManager->at( "DepthShader" )->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpShadersManager->at( "DepthShader" )->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
	}

	//------------------------------------------------------------------------------------
	// Render particles into depth texture.
	mpShadersManager->at( "ParticlesShader" )->ClearConstantBuffers();
	GApplication::BackfaceCullingNone();
	mpShadersManager->at( "ParticlesShader" )->mTopology = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
	mpShadersManager->at( "ParticlesShader" )->DisablePixelShader();
	mpShadersManager->at( "ParticlesShader" )->SetShader();
	mpShadersManager->DisableAlpha();
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "View", 
		mLight.mCamera.View() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "Projection", 
		mLight.mCamera.Ortho() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "gParticleSize", 
		mpParticles->GetParticleSize() );
	mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerFrame" );
	mpTexturesManager->SetTexture( "DiffuseMap", mpParticles->GetTextureID() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerObject", "World", GMathMF( XMMatrixIdentity() ) );
	mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerObject" );
	mpParticles->Render();
	mpShadersManager->at( "ParticlesShader" )->EnablePixelShader();
	GApplication::BackfaceCullingOn();

	mpTexturesManager->RenderToScreen( mApplicationDesc.mClientWidth, 
		mApplicationDesc.mClientHeight, mApplicationDesc.mScreenClearColor );
	GApplication::ClearScreen();
	GApplication::TurnZBufferOn();

	////------------------------------------------------------------------------------------
	//// Render scene to texture (for reflections usage).
	//mpTexturesManager->RenderToTexture( "SceneReflection", mApplicationDesc.mScreenClearColor );
	//
	////------------------------------------------------------------------------------------
	//// Render sky dome into reflection texture.
	//mpShadersManager->at( "CubeShader" )->ClearConstantBuffers();
	//mpShadersManager->at( "CubeShader" )->SetShader();
	//mpShadersManager->EnableAlpha();

	//mpShadersManager->at( "CubeShader" )->SetConstant( "PerFrame", "View", mCamera.ReflectedView() );
	//mpShadersManager->at( "CubeShader" )->SetConstant( "PerFrame", "Projection", 
	//	mCamera.Proj() );
	//mpShadersManager->at( "CubeShader" )->UpdateConstantBuffer( "PerFrame" );

	//mpTexturesManager->SetTexture( "gCubeMap", mSkyDome->GetTextureID() );
	//mSkyDome->Draw();

	////------------------------------------------------------------------------------------
	//// Render models to reflection texture.
	//mpShadersManager->at( "ShadowMultitextureShader" )->ClearConstantBuffers();
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetShader();
	//mpShadersManager->EnableAlpha();

	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "View", 
	//	mCamera.ReflectedView() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "Projection", 
	//	mCamera.Proj() );
	float one_div_sm_size = 1.0f / mSliders["gShadowMapSize"]->GetCurrentValue();	
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gShadowMapDimensions", 
	//	XMFLOAT4( mSliders["gShadowMapSize"]->GetCurrentValue(), 
	//	mSliders["gShadowMapSize"]->GetCurrentValue(), one_div_sm_size, one_div_sm_size ) );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gSunWidth", 
	//	mSliders["gSunWidth"]->GetCurrentValue() );
	////mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gShadowFactor", 
	////	mSliders["gShadowFactor"]->GetCurrentValue() );

	//mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerFrame" );

	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "CameraPosition", 
	//	mCamera.Position() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Direction", 
	//	mLight.GetDirection() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Position", 
	//	mLight.GetPos() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "DiffuseColor", 
	//	mLight.mDiffuseColor );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "AmbientColor", 
	//	mLight.mAmbientColor );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "SpecularColor", 
	//	mLight.mSpecularColor );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "AttenuationParameters", 
	//	mLight.mAttenuationParameters );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "SpotPower", 
	//	mLight.mSpotPower );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Range", 
	//	mLight.mRange );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightType", 
	//	mLight.mType );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightView", 
	//	mLight.mCamera.View() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightProjection", 
	//	mLight.mCamera.Ortho() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "Light" );

	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	SetConstant( "PerObjectGroup", "UseReflectionMap", 1 );
	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	SetConstant( "PerObjectGroup", "UseProjectionMap", 1 );
	//mpShadersManager->at( "ShadowMultitextureShader" )->
	//	UpdateConstantBuffer( "PerObjectGroup" );

	//mpTexturesManager->SetTexture( "NormalMap", "Brick Tiled Normals" );
	//mpTexturesManager->SetShadowMapTexture();
	//mpTexturesManager->SetTexture( "ReflectionMap", "Brick Reflections" );
	//mpTexturesManager->SetTexture( "gProjectionTexture", "Light Projection Grate" );

	//for ( const auto &model : mModels)
	//{
	//	for ( const auto &mesh : model->AccessMeshes() )
	//	{
	//		if ( mesh.GetMaterialID() == "" )
	//		{
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
	//		}
	//		else if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd == "" )
	//		{
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
	//		}
	//		else
	//		{
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );

	//			mpTexturesManager->SetTexture( "DiffuseMap", 
	//				mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd );
	//			mpTexturesManager->SetTexture( "AmbientMap", 
	//				mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Ka );
	//		}

	//		if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_bump == "" )
	//		{
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
	//		}
	//		else
	//		{
	//			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
	//		}

	//		mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObjectGroup" );
	//		mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObject", "World", 
	//			GMathMul( mesh.GetWorld(), model->GetWorld() ) );
	//		mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObject" );
	//		mesh.draw();
	//	}
	//}

	////------------------------------------------------------------------------------------
	//// Render particles into reflection.
	//mpShadersManager->at( "ParticlesShader" )->ClearConstantBuffers();
	//mpShadersManager->at( "ParticlesShader" )->mTopology = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
	//mpShadersManager->at( "ParticlesShader" )->SetShader();
	//mpShadersManager->EnableAlpha();
	//mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "View", 
	//	mCamera.View() );
	//mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "Projection", 
	//	mCamera.Proj() );
	//mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "gParticleSize", 
	//	mpParticles->GetParticleSize() );
	//mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerFrame" );
	//mpTexturesManager->SetTexture( "DiffuseMap", mpParticles->GetTextureID() );
	//mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerObject", "World", GMathMF( XMMatrixIdentity() ) );
	//mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerObject" );
	//mpParticles->Render();

	//------------------------------------------------------------------------------------
	// Render everything to the screen.
	mpTexturesManager->RenderToScreen( mApplicationDesc.mClientWidth, 
		mApplicationDesc.mClientHeight, mApplicationDesc.mScreenClearColor );
	GApplication::ClearScreen();
	GApplication::TurnZBufferOn();

	// Render sky dome.
	mpShadersManager->at( "CubeShader" )->ClearConstantBuffers();
	mpShadersManager->at( "CubeShader" )->SetShader();
	mpShadersManager->EnableAlpha();

	mpShadersManager->at( "CubeShader" )->SetConstant( "PerFrame", "View", mCamera.View() );
	mpShadersManager->at( "CubeShader" )->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	mpShadersManager->at( "CubeShader" )->UpdateConstantBuffer( "PerFrame" );

	mpTexturesManager->SetTexture( "gCubeMap", mSkyDome->GetTextureID() );
	mSkyDome->Draw();

	mpShadersManager->at( "ShadowMultitextureShader" )->ClearConstantBuffers();
	mpShadersManager->EnableAlpha();
	mpShadersManager->at( "ShadowMultitextureShader" )->SetShader();	

	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "View", mCamera.View() );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	one_div_sm_size = 1.0f / mSliders["gShadowMapSize"]->GetCurrentValue();	
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gShadowMapDimensions", 
		XMFLOAT4( mSliders["gShadowMapSize"]->GetCurrentValue(), 
		mSliders["gShadowMapSize"]->GetCurrentValue(), one_div_sm_size, one_div_sm_size ) );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gSunWidth", 
		mSliders["gSunWidth"]->GetCurrentValue() );
	//mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerFrame", "gShadowFactor", 
	//	mSliders["gShadowFactor"]->GetCurrentValue() );

	mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerFrame" );

	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "CameraPosition", mCamera.Position() );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Direction", mLight.GetDirection() );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Position", mLight.GetPos() );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "DiffuseColor", mLight.mDiffuseColor );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "AmbientColor", mLight.mAmbientColor );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "SpecularColor", mLight.mSpecularColor );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "AttenuationParameters", 
		mLight.mAttenuationParameters );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "SpotPower", mLight.mSpotPower );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "Range", mLight.mRange );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightType", mLight.mType );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightView", 
		mLight.mCamera.View() );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "Light", "LightProjection", 
		mLight.mCamera.Ortho() );
	mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "Light" );

	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObjectGroup" );

	mpTexturesManager->SetTexture( "NormalMap", "Brick Tiled Normals" );
	mpTexturesManager->SetShadowMapTexture();
	mpTexturesManager->SetTexture( "ReflectionMap", "Brick Reflections" );
	mpTexturesManager->SetTexture( "gProjectionTexture", "Light Projection Grate" );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseProjectionMap", 0 );

	for ( const auto &model : mModels)
	{
		for ( const auto &mesh : model->AccessMeshes() )
		{
			if ( mesh.GetMaterialID() == "" )
			{
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd == "" )
			{
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 0 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
			}
			else
			{
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );

				mpTexturesManager->SetTexture( "DiffuseMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Kd );
				mpTexturesManager->SetTexture( "AmbientMap", 
					mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_Ka );
			}

			if ( mpMaterialsManager->GetMaterialByName( mesh.GetMaterialID() ).map_bump == "" )
			{
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
			}
			else
			{
				mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 1 );
			}

			mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObjectGroup" );
			mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObject", "World", 
				GMathMul( mesh.GetWorld(), model->GetWorld() ) );
			mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObject" );
			mesh.draw();
		}
	}

	//------------------------------------------------------------------------------------
	// Render non-reflective floor to the screen.
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
	mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObjectGroup", "UseProjectionMap", 1 );
	mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObjectGroup" );
	mpTexturesManager->SetTexture( "DiffuseMap", "Sand" );
	mpTexturesManager->SetTexture( "gProjectionTexture", "Light Projection Grate" );
	for ( const auto &mesh : mpNonReflectiveFloor->AccessMeshes() )
	{
		mpShadersManager->at( "ShadowMultitextureShader" )->SetConstant( "PerObject", "World", 
			GMathMul( mesh.GetWorld(), mpNonReflectiveFloor->GetWorld() ) );
		mpShadersManager->at( "ShadowMultitextureShader" )->UpdateConstantBuffer( "PerObject" );
		mesh.draw();
	}

	//mpShadersManager->at( "LightmapProjectionShader" )->ClearConstantBuffers();
	//mpShadersManager->at( "LightmapProjectionShader" )->SetShader();
	//mpShadersManager->at( "LightmapProjectionShader" )->
	//	SetConstant( "PerFrame", "View", mCamera.View() );
	//mpShadersManager->at( "LightmapProjectionShader" )->
	//	SetConstant( "PerFrame", "Projection", mCamera.Proj() );
	//mpShadersManager->at( "LightmapProjectionShader" )->
	//	SetConstant( "PerFrame", "LightView", mLight.mCamera.View() );
	//mpShadersManager->at( "LightmapProjectionShader" )->
	//	SetConstant( "PerFrame", "LightProjection", mLight.mCamera.Ortho() );
	//mpShadersManager->at( "LightmapProjectionShader" )->UpdateConstantBuffer( "PerFrame" );
	//
	//mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObjectGroup", "UseDiffuseMap", 1 );
	//mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObjectGroup", "UseNormalMap", 0 );
	//mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObjectGroup", "UseShadowMap", 1 );
	//mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObjectGroup", "UseReflectionMap", 0 );
	//mpShadersManager->at( "LightmapProjectionShader" )->UpdateConstantBuffer( "PerObjectGroup" );
	//mpTexturesManager->SetTexture( "DiffuseMap", "Sand" );
	//mpTexturesManager->SetTexture( "gProjectionTexture", "Light Projection Grate" );
	//for ( const auto &mesh : mpNonReflectiveFloor->AccessMeshes() )
	//{
	//	mpShadersManager->at( "LightmapProjectionShader" )->SetConstant( "PerObject", "World", 
	//		GMathMul( mesh.GetWorld(), mpNonReflectiveFloor->GetWorld() ) );
	//	mpShadersManager->at( "LightmapProjectionShader" )->UpdateConstantBuffer( "PerObject" );
	//	mesh.draw();
	//}


	//------------------------------------------------------------------------------------
	// Render reflective floor to the screen.
	mpShadersManager->at( "ReflectionShader" )->ClearConstantBuffers();
	mpShadersManager->at( "ReflectionShader" )->SetShader();
	mpShadersManager->DisableAlpha();
	mpShadersManager->at( "ReflectionShader" )->SetConstant( "PerFrame", "View", 
		mCamera.View() );
	mpShadersManager->at( "ReflectionShader" )->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	// Reflect by Y plane.
	mCamera.InitReflectionMatrix( XMFLOAT4( 0.0f, -10.0f, 0.0f, 0.0f ) );
	mpShadersManager->at( "ReflectionShader" )->SetConstant( "PerFrame", "ReflectedView", 
		mCamera.ReflectedView() );
	mpShadersManager->at( "ReflectionShader" )->UpdateConstantBuffer( "PerFrame" );
	mpTexturesManager->SetTexture( "DiffuseMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Kd );
	mpTexturesManager->SetTexture( "AmbientMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Ka );
	mpTexturesManager->SetTexture( "ReflectionMap", 
		mpMaterialsManager->GetMaterialByName( "ReflectiveFloor" ).map_Ks );
	for ( const auto &mesh : mFloor->AccessMeshes() )
	{
		mpShadersManager->at( "ReflectionShader" )->SetConstant( "PerObject", "World", 
			GMathMul( mesh.GetWorld(), mFloor->GetWorld() ) );
		mpShadersManager->at( "ReflectionShader" )->UpdateConstantBuffer( "PerObject" );
		mesh.draw();
	}

	//------------------------------------------------------------------------------------
	// Render particles onto the screen.
	mpShadersManager->at( "ParticlesShader" )->ClearConstantBuffers();
	mpShadersManager->at( "ParticlesShader" )->mTopology = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
	mpShadersManager->at( "ParticlesShader" )->SetShader();
	mpShadersManager->EnableAlpha();
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "View", 
		mCamera.View() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "Projection", 
		mCamera.Proj() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerFrame", "gParticleSize", 
		mpParticles->GetParticleSize() );
	mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerFrame" );
	mpTexturesManager->SetTexture( "DiffuseMap", mpParticles->GetTextureID() );
	mpShadersManager->at( "ParticlesShader" )->SetConstant( "PerObject", "World", GMathMF( XMMatrixIdentity() ) );
	mpShadersManager->at( "ParticlesShader" )->UpdateConstantBuffer( "PerObject" );
	mpParticles->Render();

	//------------------------------------------------------------------------------------
	// Render 2D Text.
	GApplication::TurnZBufferOff();
	mpShadersManager->at( "FontShader" )->ClearConstantBuffers();
	mpShadersManager->at( "FontShader" )->SetShader();
	mpShadersManager->EnableAlpha();
	mpShadersManager->at( "FontShader" )->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	mpShadersManager->at( "FontShader" )->SetConstant( "PerFrame", "Projection", mCamera.Ortho() );
	mpShadersManager->at( "FontShader" )->UpdateConstantBuffer( "PerFrame" );

	for ( const auto &text : mText )
	{
		mpTexturesManager->SetTexture( "DiffuseMap", text->GetTextureName() );
		mpShadersManager->at( "FontShader" )->SetConstant( "PerObject", "World", text->GetWorld() );
		mpShadersManager->at( "FontShader" )->SetConstant( "PerObject", "FontColor", text->GetColor() );
		mpShadersManager->at( "FontShader" )->UpdateConstantBuffer( "PerObject" );
		text->draw();
	}

	// Draw 2d overlay window.
	//mpShadersManager->at( "OverlayShader" )->ClearConstantBuffers();
	//mpShadersManager->at( "OverlayShader" )->SetShader();
	//mpShadersManager->at( "OverlayShader" )->EnableAlpha();
	//mpShadersManager->at( "OverlayShader" )->SetConstant( "PerFrame", "View", GMathMF( XMMatrixIdentity() ) );
	//mpShadersManager->at( "OverlayShader" )->SetConstant( "PerFrame", "Projection", mCamera.Ortho() );
	//mpShadersManager->at( "OverlayShader" )->UpdateConstantBuffer( "PerFrame" );
	//
	//mpShadersManager->at( "OverlayShader" )->SetConstant( "PerObject", "World", 
	//	GMathMF( GMathFM( mDebugWindow->GetWorld() ) * XMMatrixTranslation( 
	//	static_cast<float>( -0.5f * mApplicationDesc.mClientWidth ) + 0,	
	//	static_cast<float>( 0.5f * mApplicationDesc.mClientHeight ) - 0, 
	//	0.0f ) ) );
	//mpShadersManager->at( "OverlayShader" )->SetConstant( "PerObject", "UseDiffuseMap", 1);
	//mpShadersManager->at( "OverlayShader" )->UpdateConstantBuffer( "PerObject" );
	//mpTexturesManager->SetTexture( "DiffuseMap", 
	//	mpMaterialsManager->GetMaterialByName( mDebugWindow->GetMaterialID() ).map_Kd );
	//mDebugWindow->draw();

	for (auto &slider : mSliders)
	{
		slider.second->Render( *mpShadersManager, "FontShader", 
			"OverlayShader", *mpTexturesManager );
	}

	// Draw cursor.
	mpShadersManager->at( "OverlayShader" )->ClearConstantBuffers();
	mpShadersManager->at( "OverlayShader" )->SetShader();
	mpShadersManager->EnableAlpha();
	mpShadersManager->at( "OverlayShader" )->SetConstant( "PerObject", "World", 
		GMathMF( GMathFM( mCursor->GetWorld() ) * XMMatrixTranslation( 
		static_cast<float>( -0.5f * mApplicationDesc.mClientWidth ) + mInput.MousePos().x,	
		static_cast<float>( 0.5f * mApplicationDesc.mClientHeight ) - mInput.MousePos().y, 
		0.0f ) ) );
	mpShadersManager->at( "OverlayShader" )->SetConstant( "PerObject", "UseDiffuseMap", 1);
	mpShadersManager->at( "OverlayShader" )->UpdateConstantBuffer( "PerObject" );	
	mpTexturesManager->SetTexture( "DiffuseMap",
		mpMaterialsManager->GetMaterialByName( mCursor->GetMaterialID() ).map_Kd );
	mCursor->draw();

    mpSwapChain->Present( 0, 0 );
}
//------------------------------------------------------------------------------------
ParticlesDemo::~ParticlesDemo( void )
{
	delete mpNonReflectiveFloor;
	delete mpParticles;
	delete mSkyDome;
	for ( auto &slider : mSliders ) delete slider.second;
	delete mFloor;
	for ( auto &model : mModels ) delete model;
	for ( auto &text : mText ) delete text;
	delete mCursor;

	delete mpShadersManager;
	delete mpMaterialsManager;
	delete mpTexturesManager;

	if( mpContext )
	{
		mpContext->Flush();
		mpContext->ClearState();
	}
}
//------------------------------------------------------------------------------------