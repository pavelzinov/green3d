#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GRandom.h" 
#include "GVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GTexturesManager.h"
#include "GMaterialsManager.h"
#include "GShaderPass.h"
#include "GText.h"
#include "GLight.h"
#include "GObjFile.h"
#include "GImage.h"
#include "GTerrain.h"
#include "GPlayer.h"
#include "GModel.h"
#include "GSceneManager.h"
#include "GSceneRenderer.h"
#include "GUISlider.h"
#include "GSkyDome.h"
#include "GParticles.h"
#include "GShadersManager.h"

using namespace Game;

// Class ParticlesDemo.
class ParticlesDemo : public Game::GApplication
{
public:
    ParticlesDemo(HINSTANCE hInstance);

    virtual ~ParticlesDemo(void);

	// At this point we initialize all resources.
    virtual void initApp();

private:
	// Called when application window is resized.
	// So manage all resources resize events here.
    virtual void onResize();

	// Update all scene objects.
    virtual void updateScene(float dSeconds);

	// Process user input.
	virtual void processInput(float dSeconds);

	// Draw scene.
    virtual void drawScene();

	GTexturesManager	*mpTexturesManager;
	GMaterialsManager	*mpMaterialsManager;
	GShadersManager		*mpShadersManager;

	//GShaderPass				*mpFontShader;
	//GShaderPass				*mpOverlayShader;
	//GShaderPass				*mpDepthShader;
	//GShaderPass				*mpTexturedShader;
	//GShaderPass				*mpCubeShader;
	//GShaderPass				*mpReflectionShader;
	//GShaderPass				*mpParticlesShader;

    GCamera					mCamera;	// Main camera.
	//GPlayer					mPlayer;	// Main player.
    float					mDistance;	// Camera position.
	GRandom					mRandom;	// Random generator.
	GMesh					*mDebugWindow;
	GMesh					*mCursor;
	std::vector<GText*>		mText;
	GLight					mLight;
	std::vector<GModel*>	mModels;
	GModel					*mFloor;
	GModel					*mpNonReflectiveFloor;
	// Sliders to control shader parameters.
	GFastMap<std::string, GUISlider*>	mSliders;
	GSkyDome				*mSkyDome;
	GParticles				*mpParticles;
};