#include "light_helper.hlsl"
#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register(b0)
{
	matrix	World;
	matrix	View;
	matrix	Projection;
	float4	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float4	AttenuationParameters_SpotPower; // w is spot_power
	float4	Range; // x - range
	int4	LightType; // x - type: 0 - parallel, 1 - point, 2 - spotlight
	float4	CameraPosition;
}
Texture2D		ShaderTexture;

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));

    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	float4 model_color = ShaderTexture.Sample(LinearSampler, ps_input.TexC);

	SurfaceInfo surface_info = {ps_input.Pos.xyz, normalize(ps_input.Normal), model_color, model_color};

	Light light;
	light.pos = Position.xyz;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters_SpotPower.xyz;
	light.spotPower = AttenuationParameters_SpotPower.w;
	light.range = Range.x;

	if (LightType.x == 0)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType.x == 1)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType.x == 2)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
    return model_color;
}