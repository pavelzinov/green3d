#include "common_header.hlsl"
#include "light_helper.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix World;
}

cbuffer PerObjectGroup : register(b1)
{
	// Those flags determines which texture map is defined
	int		UseDiffuseMap;
	int		UseNormalMap;
	int		UseShadowMap;
	int		UseReflectionMap;
}

cbuffer PerFrame : register(b2)
{
	matrix	View;
	matrix	Projection;
	matrix	LightView;
	matrix	LightProjection;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4	Pos : SV_POSITION;
	float4	PosW : POSITION;
	float2	TexC : TEXCOORD0;
	float3	Normal : NORMAL;
	float3	Tangent : TANGENT;
	float3	Binormal : BINORMAL;
	float4	DiffuseColor : COLOR0;
	float4	AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul( float4( vs_input.Pos.xyz, 1.0f ), World );	
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	output.PosW = mul( float4( vs_input.Pos.xyz, 1.0f ), World );
	output.TexC = vs_input.TexC;
	output.Normal = normalize( mul( vs_input.Normal, (float3x3)World ) );
	output.Tangent = normalize( mul( vs_input.Tangent, (float3x3)World ) );
	output.Binormal = normalize( mul( vs_input.Binormal, (float3x3)World ) );
	output.DiffuseColor = vs_input.DiffuseColor;
	output.AmbientColor = vs_input.AmbientColor;
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// Vertex position in light's coordinate system.
	float4 PosL = mul(ps_input.PosW, LightView);
	PosL = mul(PosL, LightProjection);

	// Projected coordinates in texture space.
	float2 projected_coordinates;
	projected_coordinates.x = 0.5f * PosL.x / PosL.w + 0.5f;
	projected_coordinates.y = -0.5f * PosL.y / PosL.w + 0.5f;

	// Calculate model diffuse color.
	float4 model_color = ps_input.DiffuseColor;
	if (UseDiffuseMap == USE_DIFFUSE_MAP)
	{
		model_color = DiffuseMap.Sample(LinearSampler, ps_input.TexC);
	}

	// Calculate model's normal.
	float3 bump_normal = ps_input.Normal;
	if (UseNormalMap == USE_NORMAL_MAP)
	{
		float4 bump_map = NormalMap.Sample(LinearSampler, ps_input.TexC);
		// Expand the range of the normal value from (0, +1) to (-1, +1).
		bump_map = (bump_map * 2.0f) - 1.0f;
	
		bump_normal = bump_map.z * ps_input.Normal + 
			bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
		bump_normal = normalize(bump_normal);
	}

	float4 reflection_color = model_color;
	if (UseReflectionMap == USE_REFLECTION_MAP)
	{
		reflection_color = reflection_color * ReflectionMap.Sample(LinearSampler, ps_input.TexC);
	}

	// If current pixel is inside projected area.
	if ( saturate( projected_coordinates.x ) == projected_coordinates.x && 
		saturate( projected_coordinates.y ) == projected_coordinates.y )
	{
		float4 projected_texture_color = gProjectionTexture.Sample( LinearSampler, projected_coordinates );
		model_color.rgb *= projected_texture_color.rgb;
	}

	//SurfaceInfo surface_info = {ps_input.PosW.xyz, bump_normal, model_color, reflection_color};

	//Light light;
	//light.pos = Position;
	//light.dir = Direction.xyz;
	//light.ambient = AmbientColor;
	//light.diffuse = DiffuseColor;
	//light.spec = SpecularColor;
	//light.att = AttenuationParameters;
	//light.spotPower = SpotPower;
	//light.range = Range;

	//if (LightType == LIGHT_TYPE_POINT)
	//{
	//	model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	//}
	//else if (LightType == LIGHT_TYPE_PARALLEL)
	//{
	//	model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
	//}
	//else if (LightType == LIGHT_TYPE_SPOT)
	//{
	//	model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	//}

    return model_color;
}