#include "common_header.hlsl"
#include "light_helper.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix World;
}

cbuffer PerObjectGroup : register(b1)
{
	// Those flags determines which texture map is defined
	int		UseDiffuseMap;
	int		UseNormalMap;
	int		UseShadowMap;
	int		UseReflectionMap;
	int		UseProjectionMap;
	int3	_padding0;
}

cbuffer PerFrame : register(b2)
{
	matrix	View;
	matrix	Projection;
	float4	gShadowMapDimensions; // x - width, y - height, z - 1/x, w - 1/y
	float	gSunWidth;
	//float	gShadowFactor;
	float3	_padding1;
}

cbuffer Light : register(b3)
{
	// Light description	
	int		LightType;	// 0 - parallel, 1 - point, 2 - spotlight
	float3	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float3	AttenuationParameters; // x - a0, y - a1, z - a2
	float	SpotPower;
	float3	CameraPosition;
	float	Range;
	// This is for shadow generation
	matrix	LightView;
	matrix	LightProjection;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Contact hardening shadows
//--------------------------------------------------------------------------------------
#define FILTER_SIZE    11
#define FS  FILTER_SIZE
#define FS2 ( FILTER_SIZE / 2 )

// 4 control matrices for a dynamic cubic bezier filter weights matrix

static const float C3[11][11] = 
                 { { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 }, 
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 },
                   };

static const float C2[11][11] = 
                 { { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 }, 
                   { 0.0,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.2,0.0 },
                   { 0.0,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   };

static const float C1[11][11] = 
                 { { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 }, 
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,1.0,1.0,1.0,1.0,1.0,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,1.0,1.0,1.0,1.0,1.0,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,1.0,1.0,1.0,1.0,1.0,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,1.0,1.0,1.0,1.0,1.0,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,1.0,1.0,1.0,1.0,1.0,0.2,0.0,0.0 },
                   { 0.0,0.0,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   };

static const float C0[11][11] = 
                 { { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 }, 
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.8,0.8,0.8,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.8,1.0,0.8,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.8,0.8,0.8,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   { 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0 },
                   };

// compute dynamic weight at a certain row, column of the matrix
float Fw( int r, int c, float fL )
{
    return (1.0-fL)*(1.0-fL)*(1.0-fL) * C0[r][c] +
           fL*fL*fL * C3[r][c] +
           3.0f * (1.0-fL)*(1.0-fL)*fL * C1[r][c]+
           3.0f * fL*fL*(1.0-fL) * C2[r][c];
} 

#define BLOCKER_FILTER_SIZE    11
#define BFS  BLOCKER_FILTER_SIZE
#define BFS2 ( BLOCKER_FILTER_SIZE / 2 )

#define SUN_WIDTH gSunWidth
   
//======================================================================================
// This shader computes the contact hardening shadow filter
//======================================================================================
float shadow( float3 tc )
{
    float  s   = 0.0f;
    float2 stc = ( gShadowMapDimensions.xy * tc.xy ) + float2( 0.5, 0.5 );
    float2 tcs = floor( stc );
    float2 fc;
    int    row;
    int    col;
    float  w = 0.0;
    float  avgBlockerDepth = 0;
    float  blockerCount = 0;
    float  fRatio;
    float4 v1[ FS2 + 1 ];
    float2 v0[ FS2 + 1 ];
    float2 off;

    fc     = stc - tcs;
    tc.xy  = tc - ( fc * gShadowMapDimensions.zw );

    // find number of blockers and sum up blocker depth
    for( row = -BFS2; row <= BFS2; row += 2 )
    {
        for( col = -BFS2; col <= BFS2; col += 2 )
        {
            float4 d4 = ShadowMap.GatherRed( PointSampler, tc.xy, int2( col, row ) );
            float4 b4  = ( tc.zzzz <= d4 ) ? (0.0).xxxx : (1.0).xxxx;   

            blockerCount += dot( b4, (1.0).xxxx );
            avgBlockerDepth += dot( d4, b4 );
        }
    }

    // compute ratio using formulas from PCSS
    if( blockerCount > 0.0 )
    {
        avgBlockerDepth /= blockerCount;
        fRatio = saturate( ( ( tc.z - avgBlockerDepth ) * SUN_WIDTH ) / avgBlockerDepth );
        fRatio *= fRatio;
    }
    else
    {
        fRatio = 0.0; 
    }

    // sum up weights of dynamic filter matrix
    for( row = 0; row < FS; ++row )
    {
       for( col = 0; col < FS; ++col )
       {
          w += Fw(row,col,fRatio);
       }
    }

    // filter shadow map samples using the dynamic weights
    [unroll(FILTER_SIZE)]for( row = -FS2; row <= FS2; row += 2 )
    {
        for( col = -FS2; col <= FS2; col += 2 )
        {
            v1[(col+FS2)/2] = ShadowMap.GatherCmpRed( gPointCmpSampler, tc.xy, tc.z, 
                                                          int2( col, row ) );
          
            if( col == -FS2 )
            {
                s += ( 1 - fc.y ) * ( v1[0].w * ( Fw(row+FS2,0,fRatio) - 
                                      Fw(row+FS2,0,fRatio) * fc.x ) + v1[0].z * 
                                    ( fc.x * ( Fw(row+FS2,0,fRatio) - 
                                      Fw(row+FS2,1,fRatio) ) +  
                                      Fw(row+FS2,1,fRatio) ) );
                s += (     fc.y ) * ( v1[0].x * ( Fw(row+FS2,0,fRatio) - 
                                      Fw(row+FS2,0,fRatio) * fc.x ) + 
                                      v1[0].y * ( fc.x * ( Fw(row+FS2,0,fRatio) - 
                                      Fw(row+FS2,1,fRatio) ) +  
                                      Fw(row+FS2,1,fRatio) ) );
                if( row > -FS2 )
                {
                    s += ( 1 - fc.y ) * ( v0[0].x * ( Fw(row+FS2-1,0,fRatio) - 
                                          Fw(row+FS2-1,0,fRatio) * fc.x ) + v0[0].y * 
                                        ( fc.x * ( Fw(row+FS2-1,0,fRatio) - 
                                          Fw(row+FS2-1,1,fRatio) ) +  
                                          Fw(row+FS2-1,1,fRatio) ) );
                    s += (     fc.y ) * ( v1[0].w * ( Fw(row+FS2-1,0,fRatio) - 
                                          Fw(row+FS2-1,0,fRatio) * fc.x ) + v1[0].z * 
                                        ( fc.x * ( Fw(row+FS2-1,0,fRatio) - 
                                          Fw(row+FS2-1,1,fRatio) ) +  
                                          Fw(row+FS2-1,1,fRatio) ) );
                }
            }
            else if( col == FS2 )
            {
                s += ( 1 - fc.y ) * ( v1[FS2].w * ( fc.x * ( Fw(row+FS2,FS-2,fRatio) - 
                                      Fw(row+FS2,FS-1,fRatio) ) + 
                                      Fw(row+FS2,FS-1,fRatio) ) + v1[FS2].z * fc.x * 
                                      Fw(row+FS2,FS-1,fRatio) );
                s += (     fc.y ) * ( v1[FS2].x * ( fc.x * ( Fw(row+FS2,FS-2,fRatio) - 
                                      Fw(row+FS2,FS-1,fRatio) ) + 
                                      Fw(row+FS2,FS-1,fRatio) ) + v1[FS2].y * fc.x * 
                                      Fw(row+FS2,FS-1,fRatio) );
                if( row > -FS2 )
                {
                    s += ( 1 - fc.y ) * ( v0[FS2].x * ( fc.x * 
                                        ( Fw(row+FS2-1,FS-2,fRatio) - 
                                          Fw(row+FS2-1,FS-1,fRatio) ) + 
                                          Fw(row+FS2-1,FS-1,fRatio) ) + 
                                          v0[FS2].y * fc.x * Fw(row+FS2-1,FS-1,fRatio) );
                    s += (     fc.y ) * ( v1[FS2].w * ( fc.x * 
                                        ( Fw(row+FS2-1,FS-2,fRatio) - 
                                          Fw(row+FS2-1,FS-1,fRatio) ) + 
                                          Fw(row+FS2-1,FS-1,fRatio) ) + 
                                          v1[FS2].z * fc.x * Fw(row+FS2-1,FS-1,fRatio) );
                }
            }
            else
            {
                s += ( 1 - fc.y ) * ( v1[(col+FS2)/2].w * ( fc.x * 
                                    ( Fw(row+FS2,col+FS2-1,fRatio) - 
                                      Fw(row+FS2,col+FS2+0,fRatio) ) + 
                                      Fw(row+FS2,col+FS2+0,fRatio) ) +
                                      v1[(col+FS2)/2].z * ( fc.x * 
                                    ( Fw(row+FS2,col+FS2-0,fRatio) - 
                                      Fw(row+FS2,col+FS2+1,fRatio) ) + 
                                      Fw(row+FS2,col+FS2+1,fRatio) ) );
                s += (     fc.y ) * ( v1[(col+FS2)/2].x * ( fc.x * 
                                    ( Fw(row+FS2,col+FS2-1,fRatio) - 
                                      Fw(row+FS2,col+FS2+0,fRatio) ) + 
                                      Fw(row+FS2,col+FS2+0,fRatio) ) +
                                      v1[(col+FS2)/2].y * ( fc.x * 
                                    ( Fw(row+FS2,col+FS2-0,fRatio) - 
                                      Fw(row+FS2,col+FS2+1,fRatio) ) + 
                                      Fw(row+FS2,col+FS2+1,fRatio) ) );
                if( row > -FS2 )
                {
                    s += ( 1 - fc.y ) * ( v0[(col+FS2)/2].x * ( fc.x * 
                                        ( Fw(row+FS2-1,col+FS2-1,fRatio) - 
                                          Fw(row+FS2-1,col+FS2+0,fRatio) ) + 
                                          Fw(row+FS2-1,col+FS2+0,fRatio) ) +
                                          v0[(col+FS2)/2].y * ( fc.x * 
                                        ( Fw(row+FS2-1,col+FS2-0,fRatio) - 
                                          Fw(row+FS2-1,col+FS2+1,fRatio) ) + 
                                          Fw(row+FS2-1,col+FS2+1,fRatio) ) );
                    s += (     fc.y ) * ( v1[(col+FS2)/2].w * ( fc.x * 
                                        ( Fw(row+FS2-1,col+FS2-1,fRatio) - 
                                          Fw(row+FS2-1,col+FS2+0,fRatio) ) + 
                                          Fw(row+FS2-1,col+FS2+0,fRatio) ) +
                                          v1[(col+FS2)/2].z * ( fc.x * 
                                        ( Fw(row+FS2-1,col+FS2-0,fRatio) - 
                                          Fw(row+FS2-1,col+FS2+1,fRatio) ) + 
                                          Fw(row+FS2-1,col+FS2+1,fRatio) ) );
                }
            }
            
            if( row != FS2 )
            {
                v0[(col+FS2)/2] = v1[(col+FS2)/2].xy;
            }
        }
    }

    return s/w;
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);	
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.PosW = mul(float4(vs_input.Pos.xyz, 1.0f), World);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
	output.DiffuseColor = vs_input.DiffuseColor;
	output.AmbientColor = vs_input.AmbientColor;
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// vertex position in light's coordinate system
	float4 PosL = mul(ps_input.PosW, LightView);
	PosL = mul(PosL, LightProjection);
	// Factor in shadow bias here.
	//PosL.z -= gShadowBias;

	// Projected coordinates in texture space.
	float2 projected_coordinates;
	projected_coordinates.x = 0.5f * PosL.x / PosL.w + 0.5f;
	projected_coordinates.y = -0.5f * PosL.y / PosL.w + 0.5f;

	// Calculate model diffuse color.
	float4 model_color = ps_input.DiffuseColor;
	if (UseDiffuseMap == USE_DIFFUSE_MAP)
	{
		model_color = DiffuseMap.Sample(LinearSampler, ps_input.TexC);
	}
	//return model_color; // Debug diffuse color.

	// Calculate model's normal.
	float3 bump_normal = ps_input.Normal;
	if (UseNormalMap == USE_NORMAL_MAP)
	{
		float4 bump_map = NormalMap.Sample(LinearSampler, ps_input.TexC);
		// Expand the range of the normal value from (0, +1) to (-1, +1).
		bump_map = (bump_map * 2.0f) - 1.0f;
	
		bump_normal = bump_map.z * ps_input.Normal + 
			bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
		bump_normal = normalize(bump_normal);
	}

	float4 reflection_color = model_color;
	if (UseReflectionMap == USE_REFLECTION_MAP)
	{
		reflection_color = reflection_color * ReflectionMap.Sample(LinearSampler, ps_input.TexC);
	}

	SurfaceInfo surface_info = {ps_input.PosW.xyz, bump_normal, model_color, reflection_color};

	Light light;
	light.pos = Position;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters;
	light.spotPower = SpotPower;
	light.range = Range;

	float4 old_model_color = model_color;

	if (LightType == LIGHT_TYPE_POINT)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType == LIGHT_TYPE_PARALLEL)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType == LIGHT_TYPE_SPOT)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}

	float3 f3TC = float3( projected_coordinates, PosL.z - 0.001f );
    float fShadow = shadow( f3TC );

	//model_color *= saturate( float4( gShadowFactor, gShadowFactor, gShadowFactor, 0.0 ) + fShadow ); // shadow factor was 0.3
	model_color.rgb *= saturate( float3( 0.5f, 0.5f, 0.5f ) + fShadow );
	//model_color.a = 1.0f;
	
	// If current pixel is inside projected area.
	if ( saturate( projected_coordinates.x ) == projected_coordinates.x && 
		saturate( projected_coordinates.y ) == projected_coordinates.y )
	{
		float4 projected_texture_color = gProjectionTexture.Sample( LinearSampler, projected_coordinates );
		model_color.rgb *= saturate( float3( 0.5f, 0.5f, 0.5f ) + projected_texture_color.rgb );
	}

    return model_color;
}