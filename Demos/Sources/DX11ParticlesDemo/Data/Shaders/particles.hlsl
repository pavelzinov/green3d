#include "common_header.hlsl"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer PerObject : register(b0)
{
	matrix World;
}

cbuffer PerFrame : register(b1)
{
	matrix	View;
	matrix	Projection;
	float	gParticleSize;
	float3	padding0;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
	float4 PosLoc : POSITION;
	float2 TexC : TEXCOORD0;
};

struct GS_OUT
{
	float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
	output.PosLoc.xyz = vs_input.Pos.xyz;
	output.PosLoc.w = 1.0f;
	output.TexC = vs_input.TexC;
    
    return output;
}

//--------------------------------------------------------------------------------------
// Geometry Shader. Constructs quad, lying in XY plane, based on the center coordinates of this quad.
//--------------------------------------------------------------------------------------
[maxvertexcount(6)]
void GS(point VS_OUT gs_input[1], inout TriangleStream<GS_OUT> tri_stream)
{
	GS_OUT current_vertex = (GS_OUT) 0;
	float half_particle = gParticleSize * 0.5f;
	matrix WVP = mul( mul( World, View ), Projection );
	float4 local_position = gs_input[0].PosLoc;

	// Top-left point.
	local_position += float4( -half_particle, +half_particle, 0.0f, 0.0f );
	current_vertex.Pos = mul( local_position, WVP );
	current_vertex.TexC = float2( 0.0f, 0.0f );
	tri_stream.Append( current_vertex );
	// Top-right.
	local_position += float4( +gParticleSize, 0.0f, 0.0f, 0.0f );
	current_vertex.Pos = mul( local_position, WVP );
	current_vertex.TexC = float2( 1.0f, 0.0f );
	tri_stream.Append( current_vertex );
	// Bottom-left.
	local_position += float4( -gParticleSize, -gParticleSize, 0.0f, 0.0f );
	current_vertex.Pos = mul( local_position, WVP );
	current_vertex.TexC = float2( 0.0f, 1.0f );
	tri_stream.Append( current_vertex );
	
	// Start bottom triangle.
	tri_stream.RestartStrip();

	tri_stream.Append( current_vertex );
	
	local_position += float4( +gParticleSize, +gParticleSize, 0.0f, 0.0f );
	current_vertex.Pos = mul( local_position, WVP );
	current_vertex.TexC = float2( 1.0f, 0.0f );
	tri_stream.Append( current_vertex );
	
	local_position += float4( 0.0f, -gParticleSize, 0.0f, 0.0f );
	current_vertex.Pos = mul( local_position, WVP );
	current_vertex.TexC = float2( 1.0f, 1.0f );
	tri_stream.Append( current_vertex );

	// Start next triangle.
	//tri_stream.RestartStrip();
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( GS_OUT ps_input ) : SV_Target
{
	//float color = noise( ps_input.Pos );
	//return float4( abs( noise( ps_input.Pos ) ), abs( noise( ps_input.Pos ) ), 
	//	abs( noise( ps_input.Pos ) ), abs( noise( ps_input.Pos ) ) );
    return DiffuseMap.Sample( LinearSampler, ps_input.TexC );
}