#pragma once
#include "GModel.h"
using namespace Game;

template <>
void GModel<GColoredVertex>::constructBuffers()
{
	UINT verticesNumber = mVertices.size();
	GSimpleVertexData *vertices = new GSimpleVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].pos;
		vertices[i].color = mVertices[i].color;
	}

	// Vertex Buffer
	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GColoredVertex::getDataSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));

	delete[] vertices;

	// Index Buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(UINT) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}

// Open mvci file.
// .mvci file structure:
/////////////////////////////
// VertexType
// VertexCount
// x y z r g b a
// ...
// triangle_vertex_index_1 triangle_vertex_index_2 triangle_vertex_index_3
// ...
// [this line can be empty]
/////////////////////////////
// vertex's indices start from 1. It means that the first line
// containing x y z r g b a has index 1 and so on.
template <>
void GModel<GColoredVertex>::loadFromFile(std::string filename)
{
	std::string line;	
	std::ifstream model;
	model.open(filename);	
	std::vector<std::string> file_bytes;

	if (model.is_open())
	{
		while (model.good())
		{
			getline(model, line);
			file_bytes.push_back(line);
		}
		model.close();

		// delete last empty character
		if (file_bytes[file_bytes.size() - 1] == "")
			file_bytes.pop_back();
	}
	else
	{
		return;
	}
	
	std::string vertex_type = file_bytes[0];
	int vertex_count = 0;
	std::stringstream line_as_stream;
	float x, y, z, r, g, b, a;

	line_as_stream << file_bytes[1];
	line_as_stream >> vertex_count;
	line_as_stream.clear();

	if (vertex_type == "GColoredVertex")
	{
		GColoredVertex vertex(XMFLOAT3(0, 0, 0), XMFLOAT4(0, 0, 0, 0));
		// reading vertices
		mVertices.clear();
		int length = mVertices.max_size();
		mVertices.reserve(vertex_count);
		for (int i=2; i < vertex_count + 2; i++)
		{
			line_as_stream << file_bytes[i];
			line_as_stream >> x >> y >> z >> r >> g >> b >> a;
			vertex.SetData(x, y, z, r, g, b, a);
			mVertices.push_back(vertex);
			line_as_stream.clear();
		}

		// reading indices
		mIndices.clear();
		mIndices.reserve((file_bytes.size() - vertex_count - 2) * 3);
		UINT i1 = 0, i2 = 0, i3 = 0;
		for (unsigned int i=vertex_count+2; i<file_bytes.size(); i++)
		{
			line_as_stream << file_bytes[i];
			line_as_stream >> i1 >> i2 >> i3;
			mIndices.push_back(i1 - 1);
			mIndices.push_back(i2 - 1);
			mIndices.push_back(i3 - 1);
			line_as_stream.clear();
		}

		this->constructBuffers();
	}
	else if (vertex_type == "GTexturedVertex")
	{

	}
}