#pragma once
#include <windows.h>
#include <d3d11.h>
#include <directxmath.h>
#include <d3dcompiler.h>

using namespace DirectX;

// ������ ��� ������� ������������ COM-�������
#define ReleaseCOM(x) { if(x){x->Release(); x=0;} }

// ������ ��� ������ ��������� �������� DX10
#if defined(DEBUG) | defined(_DEBUG)
    #ifndef HR
    #define HR(x) (x)
	/*
    {                                                            \
        HRESULT hr = (x);                                        \
        if(FAILED(hr))                                           \
        {                                                        \
            DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, true);   \
        }                                                        \
    }*/
    #endif
#else
    #ifndef HR
    #define HR(x) (x)
    #endif
#endif

// DX11 Colors
const XMFLOAT4 WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 RED(1.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);

const XMFLOAT4 BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
const XMFLOAT4 LIGHT_BLUE(0.2f, 0.8f, 1.0f, 1.0f);
const XMFLOAT4 LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
const XMFLOAT4 DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
const XMFLOAT4 DARKBROWN(0.45f, 0.39f, 0.34f, 1.0f);

// math constants
const float INFINITY = FLT_MAX;
const float MATH_PI  = 3.14159265358979323f;
const float MATH_PIDIV2 = 1.570796326794896615f;
const float MATH_EPS = 0.0001f;

// c�������� 2-� �������� � ������� MATH_EPS
template <class T>
inline bool IsEqual(T &x, T &y)
{ 
	if (fabs(x-y) <= MATH_EPS)
		return true;
	return false;
}