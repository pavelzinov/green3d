#pragma once
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

#include "GUtility.h"
#include "GTriangle.h"

namespace Game
{

template<class T>
class GModel
{
public:
	GModel(ID3D11Device *device, ID3D11DeviceContext *context);
	GModel(ID3D11Device *device, ID3D11DeviceContext *context, std::string filename);
	GModel(GModel<T> &model);
	~GModel(void);

public:
	void addTriangle(GTriangle<T> triangle);
	void copyTriangles(std::vector<GTriangle<T> > triangles);
	void loadFromFile(std::string filename);

public:
	void drawTriangles();
	void draw();

private:
	void constructBuffers();
	UINT GetVertexCount();

private:
	std::vector<GTriangle<T> >	mTriangles;
	std::vector<T>				mVertices;
	std::vector<UINT>			mIndices;

	ID3D11Device		*mDevice;
	ID3D11DeviceContext	*mContext;
	ID3D11Buffer		*mVB;
	ID3D11Buffer		*mIB;

	// Control model space
public:
	XMMATRIX GetWorld();
	XMMATRIX RotateY(float angle);
	XMMATRIX Scale(float dx, float dy, float dz);
	XMMATRIX Scale(float delta_size);
	XMMATRIX Move(XMFLOAT3 direction);
	XMMATRIX Move(float dir_x, float dir_y, float dir_z);

private:
	XMFLOAT4X4 mScaling;
	XMFLOAT4X4 mTranslation;
	XMFLOAT4X4 mRotation;
};

template<class T>
GModel<T>::GModel(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mDevice = device;
	mContext = context;
	mVB = nullptr;
	mIB = nullptr;
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mRotation = mTranslation = mScaling;
}

template<class T>
GModel<T>::GModel(ID3D11Device *device, ID3D11DeviceContext *context, std::string filename)
{
	mDevice = device;
	mContext = context;
	mVB = nullptr;
	mIB = nullptr;
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mRotation = mTranslation = mScaling;
	loadFromFile(filename);
}

template<class T>
GModel<T>::GModel(GModel<T> &model)
{
	this->mDevice = model.mDevice;
	this->mContext = model.mContext;

	this->mVertices = model.mVertices;
	this->mIndices = model.mIndices;

	this->mScaling = model.mScaling;
	this->mRotation = model.mRotation;
	this->mTranslation = model.mTranslation;

	this->constructBuffers();
}

template<class T>
GModel<T>::~GModel()
{
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);
}

template<class T>
void GModel<T>::addTriangle(GTriangle<T> triangle)
{
	mTriangles.push_back(triangle);
	mVertices.push_back(triangle.getVertices()[0]);
	mVertices.push_back(triangle.getVertices()[1]);
	mVertices.push_back(triangle.getVertices()[2]);
}

template<class T>
void GModel<T>::copyTriangles(std::vector<GTriangle<T> > triangles)
{
	mTriangles = triangles;
	for (int i=0; i<mTriangles.size(); i++)
	{
		mVertices.push_back(mTriangles.getVertices()[0]);
		mVertices.push_back(mTriangles.getVertices()[1]);
		mVertices.push_back(mTriangles.getVertices()[2]);
	}

	this->constructBuffers();
}

template<class T>
void GModel<T>::drawTriangles()
{
	for (int i=0; i<mTriangles.size(); i++)
	{
		mTriangles[i].draw();
	}
}

template<class T>
void GModel<T>::draw()
{
	UINT strides = T::getDataSize();
	UINT offset = 0;
	mContext->IASetVertexBuffers(0, 1, &mVB, &strides, &offset);
	mContext->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);
	mContext->DrawIndexed(mIndices.size(), 0, 0);
}

template<class T>
UINT GModel<T>::GetVertexCount()
{
	return mVertices.size();
}

template<class T>
XMMATRIX GModel<T>::GetWorld()
{
	return XMMatrixTranspose(XMLoadFloat4x4(&mScaling) * XMLoadFloat4x4(&mRotation) 
		* XMLoadFloat4x4(&mTranslation));
}

template<class T>
XMMATRIX GModel<T>::RotateY(float angle)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationY(XMConvertToRadians(angle)));
	return GetWorld();
}

template<class T>
XMMATRIX GModel<T>::Scale(float scale_x, float scale_y, float scale_z)
{	
	XMStoreFloat4x4(&mScaling, XMLoadFloat4x4(&mScaling) 
		* XMMatrixScaling(scale_x, scale_y, scale_z));
	return GetWorld();
}

template<class T>
XMMATRIX GModel<T>::Scale(float delta_size)
{	
	return Scale(delta_size, delta_size, delta_size);
}

template<class T>
XMMATRIX GModel<T>::Move(XMFLOAT3 direction)
{
	XMStoreFloat4x4(&mTranslation, XMLoadFloat4x4(&mTranslation) 
		* XMMatrixTranslation(direction.x, direction.y, direction.z));
	return GetWorld();
}

template<class T>
XMMATRIX GModel<T>::Move(float dir_x, float dir_y, float dir_z)
{
	return Move(XMFLOAT3(dir_x, dir_y, dir_z));
}

}