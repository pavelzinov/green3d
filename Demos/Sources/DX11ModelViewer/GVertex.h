#pragma once
#include "GUtility.h"
#include <vector>

namespace Game
{

class GVertex
{
public:
	GVertex(void);
	GVertex(XMFLOAT3 position);
	virtual ~GVertex(void);

	void SetData(float x, float y, float z);
	void SetData(XMFLOAT3 pos);

	static UINT getDataSize();

	XMFLOAT3 pos;
};

}

