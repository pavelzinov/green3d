#include "GVertex.h"
using namespace Game;

GVertex::GVertex(void)
{
	this->pos = XMFLOAT3(0, 0, 0);
}

GVertex::GVertex(XMFLOAT3 position)
{
	this->pos = position;
}

GVertex::~GVertex(void)
{
}

void GVertex::SetData(float x, float y, float z)
{
	pos.x = x;
	pos.y = y;
	pos.z = z;
}

void GVertex::SetData(XMFLOAT3 pos)
{
	this->pos = pos;
}

UINT GVertex::getDataSize()
{
	return sizeof(XMFLOAT3);
}
