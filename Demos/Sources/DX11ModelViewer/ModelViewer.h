#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GColoredVertex.h"
#include "GModel.h"

using namespace Game;

struct ConstantBuffer
{
	XMMATRIX World;
	XMMATRIX View;
	XMMATRIX Projection;
};

// Class ModelViewer.
// Does: view .mvci files
class ModelViewer : public GApplication
{
    // Public constructor and destructor
public:
    ModelViewer(HINSTANCE hInstance);
    ~ModelViewer(void);

public:
    void initApp();

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
    // Create input layout and load vertex and pixel shaders
    HRESULT CreateShaders(); 
	// Set vertex and pixel shaders before drawing
    void SetShaders();
	// Shader file entry point. Must be .fx file
	std::wstring mShaderFileName;
	// Compile shaders from file
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

private:
    ID3D11InputLayout*	mpVertexLayout; // input vertex layout
	ID3D11PixelShader*	mpPixelShader;	// pixel shader pointer
	ID3D11VertexShader*	mpVertexShader; // vertex shader pointer
	ID3D11Buffer*		mpShaderConstantBuffer; // variable to access constant buffer in shaders

private:    
    GCamera	mCamera; // main camera
	float	mdx;
	float	mdy;
	float	mdz;
	float	mdMove;       // �������� �������� ���������������� ������ �� ����
    float	mdAngle;      // ������� �������� �������� ������
	float	mAngle;
    float	mDistance;    // ������ �������� ������

	GRandom	mRandom; // random generator

	std::vector<GModel<GColoredVertex>* >	models; // models
	std::vector<float>						mdAngles;
	std::vector<XMFLOAT3>					mDirections;
	std::vector<float>						mSpeeds;
};