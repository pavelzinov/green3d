#pragma once
#include "GUtility.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores View and Projection matrices used by shaders
// to translate 3D world into 2D screen surface
// Camera can be moved
////////////////////////////////////////////////////////
class GCamera
{
public:

    GCamera(void);
	GCamera(const GCamera& camera);
    GCamera& operator=(const GCamera& camera);
    ~GCamera(void);

	// Initialize camera's View matrix
	void initViewMatrix(XMFLOAT3 pos, XMFLOAT3 target, XMFLOAT3 up);

	// Initialize camera's Projection matrix
	void initProjMatrix(const float angle, const float clientWidth, const float clientHeight, 
								const float nearest, const float farthest);
	
	///////////////////////////////////////////////
	/*** View matrix transformation interfaces ***/

	// Set camera position coordinates
    void SetPosition(XMFLOAT3 pos);
	// Set camera position coordinates
    void SetPosition(float x, float y, float z);
	// Get camera position coordinates
    XMFLOAT3 GetPosition();

	// Move camera and it's target position
	void Move(XMFLOAT3 direction);
	// Move camera and it's target position
	void Move(float dx, float dy, float dz);

	// Set camera's target position
    void SetTarget(XMFLOAT3 target);
	// Set camera's target position
    void SetTarget(float x, float y, float z);
	// Get camera's target position
    XMFLOAT3 GetTarget();

	// Set camera's up vector
    void SetUp(XMFLOAT3 up);
	// Set camera's up vector
    void SetUp(float x, float y, float z);
	// Get camera's up vector
    XMFLOAT3 GetUp();
	
	// Returns transposed camera's View matrix	
    XMMATRIX GetView();

	/////////////////////////////////////////////////////
	/*** Projection matrix transformation interfaces ***/

	// Set view frustum's angle
	void SetAngle(float angle);
	// Get view frustum's angle
	float GetAngle();

	// Set camera projection field width
	void SetClientWidth(float clientWidth);
	// Set camera projection field height
	void SetClientHeight(float clientHeigth);

	// Set nearest culling plane distance from view frustum's projection plane
	void SetNearestPlane(float nearest);
	// Set farthest culling plane distance from view frustum's projection plane
	void SetFarthestPlane(float farthest);

	// Returns transposed camera's Projection matrix
	XMMATRIX GetProj();

private:
    /*** Camera parameters ***/
    XMFLOAT3 mPosition;		// Camera's coordinates
    XMFLOAT3 mTarget;		// View target's coordinates
    XMFLOAT3 mUp;			// Camera's up vector, allowing to determine camera's up/down orientation

	/*** Projection parameters ***/
	float mAngle;			// Angle of view frustum
	float mClientWidth;		// Window's width
	float mClientHeight;	// Window's height
	float mNearest;			// Nearest view frustum plane
	float mFarthest;		// Farthest view frustum plane

    XMFLOAT4X4  mView;		// View matrix
	XMFLOAT4X4	mProj;		// Projection matrix
};

} // namespace Game