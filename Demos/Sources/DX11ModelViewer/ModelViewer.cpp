#include "ModelViewer.h"

ModelViewer::ModelViewer(HINSTANCE hInstance) : GApplication(hInstance)
{
	// entry point in shader file system
	mShaderFileName = L"EmptyShader.fx";

    // init background (space) color
    GApplication::mClearColor = BLUE;

	mdx = mdy = mdz = 0.0f;
	mdMove = 60.0f;
    mdAngle = 240.0f;
	mAngle = 90.0f;
    mDistance = 100.0f;

	mCamera.initViewMatrix(XMFLOAT3(0.0f, mDistance, -mDistance), 
		XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
	mCamera.initProjMatrix(MATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void ModelViewer::initApp()
{
    GApplication::initApp();

	models.push_back(new GModel<GColoredVertex>(md3dDevice, md3dContext));
	models[0]->loadFromFile("cube.mvci");
	
	float scale = 10.0f;
	float move = 100.0f;
	for (int i=1; i<3000; i++)
	{
		models.push_back(new GModel<GColoredVertex>(*models[0]));
	}

	for (int i=0; i<models.size(); i++)
	{
		models[i]->Scale(5.0f);
	}
	//models.push_back(new GModel<GColoredVertex>(md3dDevice, md3dContext, "plane.mvci"));
	//models[models.size() - 1]->Scale(1000.0f, 1.0f, 1000.0f);

	for (int i=0; i<models.size(); i++)
	{
		mdAngles.push_back(mRandom.Gen(90.0f, -90.0f));
	}

	XMFLOAT3 temp;
	for (int i=0; i<models.size(); i++)
	{
		XMStoreFloat3(&temp, XMVector3Normalize(XMLoadFloat3(&XMFLOAT3(mRandom.Gen(1.0f, -1.0f), 
			mRandom.Gen(1.0f, -1.0f), mRandom.Gen(1.0f, -1.0f)))));
		mDirections.push_back(temp);
	}

	for (int i=0; i<models.size(); i++)
	{
		mSpeeds.push_back(mRandom.Gen(20.0f, 1.0f));
	}

	CreateShaders();
}

void ModelViewer::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(MATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void ModelViewer::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	for (int i=0; i<models.size(); i++)
	{
		if (static_cast<int>(mTimer.getGameTime()) % 40 >= 40/2)
		{
			models[i]->Move(-mDirections[i].x * dSeconds * mSpeeds[i], 
				-mDirections[i].y * dSeconds * mSpeeds[i], -mDirections[i].z * dSeconds * mSpeeds[i]);
		}
		else
		{
			models[i]->Move(mDirections[i].x * dSeconds * mSpeeds[i], 
				mDirections[i].y * dSeconds * mSpeeds[i], mDirections[i].z * dSeconds * mSpeeds[i]);
		}
		
		models[i]->RotateY(dSeconds * mdAngles[i]);
	}

	GApplication::mInput.UpdateMouseMove();
}

void ModelViewer::drawScene()
{
    GApplication::drawScene();

    // set shader variables from cpp code	
	ConstantBuffer cb;

	for (auto it = models.begin(); it < models.end(); it++)
	{
		ZeroMemory(&cb, sizeof(cb));
		cb.View = mCamera.GetView();
		cb.Projection = mCamera.GetProj();
		cb.World = (*it)->GetWorld();
		md3dContext->UpdateSubresource(mpShaderConstantBuffer, 0, nullptr, &cb, 0, 0);
		(*it)->draw();
	}

    mSwapChain->Present(0, 0);
}

HRESULT ModelViewer::CreateShaders()
{
	// Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile(mShaderFileName.c_str(), "VS", "vs_4_0", &pVSBlob);
    if(FAILED(hr))
    {
        MessageBox(NULL, 
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK);
        return hr;
    }

	// Create the vertex shader
	hr = md3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader);
	if(FAILED(hr))
	{	
		pVSBlob->Release();
        return hr;
	}

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE( layout );

    // Create the input layout
	hr = md3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if(FAILED(hr))
        return hr;

    // Set the input layout
    md3dContext->IASetInputLayout( mpVertexLayout );

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( mShaderFileName.c_str(), "PS", "ps_4_0", &pPSBlob );
    if(FAILED(hr))
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
					L"Error", 
					MB_OK );
        return hr;
    }

	// Create the pixel shader
	hr = md3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
    if(FAILED(hr))
        return hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
    hr = md3dDevice->CreateBuffer( &bd, NULL, &mpShaderConstantBuffer );
    if(FAILED(hr))
        return hr;
	
	SetShaders();
	return S_OK;
}

void ModelViewer::SetShaders()
{	
	md3dContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	md3dContext->VSSetShader(mpVertexShader, nullptr, 0);
	md3dContext->VSSetConstantBuffers(0, 1, &mpShaderConstantBuffer);
	md3dContext->PSSetShader(mpPixelShader, nullptr, 0);
}

HRESULT ModelViewer::CompileShaderFromFile( LPCWSTR szFileName, LPCSTR szEntryPoint, 
										   LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA(static_cast<char*>(pErrorBlob->GetBufferPointer()));
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

ModelViewer::~ModelViewer(void)
{
	for (auto it = models.begin(); it < models.end(); it++)
	{
		delete (*it);
	}

    if( md3dContext )
        md3dContext->ClearState();

	ReleaseCOM(mpShaderConstantBuffer);
	ReleaseCOM(mpVertexShader);
	ReleaseCOM(mpPixelShader);
    ReleaseCOM(mpVertexLayout);
}