#include "GColoredVertex.h"
using namespace Game;

GColoredVertex::GColoredVertex(const GColoredVertex &vertex)
{
	this->pos = vertex.pos;
	this->color = vertex.color;
}

GColoredVertex::GColoredVertex(XMFLOAT3 position, XMFLOAT4 color) : GVertex(position)
{
	this->color = color;
}

GColoredVertex::~GColoredVertex(void)
{

}

void GColoredVertex::SetData(float x, float y, float z, float r, float g, float b, float a)
{
	pos.x = x;
	pos.y = y;
	pos.z = z;

	color.x = r;
	color.y = g;
	color.z = b;
	color.w = a;	
}

void GColoredVertex::SetData(XMFLOAT3 position, XMFLOAT4 color)
{
	pos = position;
	this->color = color;
}

UINT GColoredVertex::getDataSize()
{
	return sizeof(GSimpleVertexData);
}
