#include "BumpMapping.h"
#include "DDSTextureLoader.h"

BumpMapping::BumpMapping(HINSTANCE hInstance) : GApplication(hInstance)
{
    // init background color
    GApplication::mClearColor = GCOLOR_BEACH_SAND;

    mDistance = 100.0f;

	mCamera.SetPosition(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.SetTarget(XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 100.0f);	
}

void BumpMapping::initApp()
{ 
	GApplication::initApp();

	// Load shaders
	mpColoredShader = new GShader<GColoredVertex, GSHADER_TYPE_DIR_LIGHT>(md3dDevice, md3dContext);
	mpColoredShader->LoadShader("Data/Shaders/colored_dir_light.fx");
	mpFontShader = new GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpFontShader->LoadShader("Data/Shaders/textured.fx");
	mpTexturedShader = new GShader<GTexturedVertex, GSHADER_TYPE_DIR_LIGHT>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("Data/Shaders/tex_bump_dir_light.fx");
	mpLightMappedShader = new GShader<GTexturedVertex, GSHADER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpLightMappedShader->LoadShader("Data/Shaders/tex_light_mapping.fx");

	// Load materials
	mpMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mpMaterialsManager->AddMaterial("Space", "Data/Textures/space.dds");
	mpMaterialsManager->AddMaterial("Water", "Data/Textures/water.dds");
	mpMaterialsManager->AddMaterial("VS2012", "Data/Textures/visual_studio_2012.dds");
	mpMaterialsManager->AddMaterial("Courier New 12pt Dark Green", "Data/Fonts/courier_new_12_dark_green.dds");
	mpMaterialsManager->AddMaterial("Segoe UI Semilight 10pt Yellow", "Data/Fonts/segoe_semilight_10_yellow.dds");
	mpMaterialsManager->AddMaterial("Brick", "Data/Textures/brick.dds");
	mpMaterialsManager->AddMaterial("Brick Normal Map", "Data/Textures/brick_bump.dds");
	mpMaterialsManager->AddMaterial("Brick Light Map", "Data/Textures/brick_light_map.dds");

	// Load light
	mDirLight.mDirection = XMFLOAT3(1.0f, 0.0f, 0.0f);
	mDirLight.mColor = GCOLOR_WHITE;
	// Load light's representation
	mLightVector = new GMesh<GColoredVertex>(md3dDevice, md3dContext);
	mLightVector->LoadFromObj("Data/Models/arrow.obj");

	// Load text
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		""));
	mText.back()->SetPosition(XMUINT2(4, 4), mClientWidth, mClientHeight);
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		"Use RIGHT and LEFT arrow keys to rotate light's direction"));
	mText.back()->SetPosition(XMUINT2(4, mClientHeight - 
		2 * mText.back()->GetSymbolHeight()), mClientWidth, mClientHeight);

	// Load models
	mColoredModels.push_back(new GMesh<GColoredVertex>(md3dDevice, md3dContext));
	mColoredModels.back()->LoadFromObj("Data/Models/cube.obj");

	mTexturedModels.push_back(new GMesh<GTexturedVertex>(md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/plane.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Scale(3.0f);

	mTexturedModels.push_back(new GMesh<GTexturedVertex>(md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/cube.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Move(50.0f, 0.0f, 0.0f);
}

void BumpMapping::onResize()
{
    GApplication::onResize();
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
}

void BumpMapping::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "UPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mCamera.GetPosition().x << " " << 
		mCamera.GetPosition().y << " " << mCamera.GetPosition().z << 
		"\nCamera target position: " << mCamera.GetTarget().x << " " <<
		mCamera.GetTarget().y << " " << mCamera.GetTarget().z <<
		"\nCamera Up vector: " << mCamera.GetUp().x << " " <<
		mCamera.GetUp().y << " " << mCamera.GetUp().z <<
		"\nLight direction: " << mDirLight.mDirection.x << " " <<
		mDirLight.mDirection.y << " " << mDirLight.mDirection.z <<
		"\nMouse X: " << mInput.GetAbsoluteX() << " Y: " << mInput.GetAbsoluteY();
	mText[0]->SetText(stream.str());

	// Camera controls:
	if (mInput.IsKeyDown('W'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('S'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('D'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('A'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown(VK_SPACE))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('X'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}

	// Update billboards position based on new camera:
	//mSky->UpdatePosition(mCamera.GetPosition(), mCamera.GetTarget(), mCamera.GetUp());

	// Light controls:
	if (mInput.IsKeyDown(VK_LEFT))
	{
		mDirLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mDirLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), -0.1f)));
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(-0.1f));
	}
	if (mInput.IsKeyDown(VK_RIGHT))
	{
		mDirLight.mDirection = GMathVF(XMVector3Rotate(GMathFV(mDirLight.mDirection), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), 0.1f)));
		mLightVector->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(0.1f));
	}		

	GApplication::mInput.UpdateMouseMove();
}

void BumpMapping::drawScene()
{
    GApplication::drawScene();
	GApplication::TurnZBufferOn();

    // Reset shader matrices for this frame
	mpColoredShader->ClearConstantBuffer();
	mpFontShader->ClearConstantBuffer();
	mpTexturedShader->ClearConstantBuffer();
	mpLightMappedShader->ClearConstantBuffer();

	mpColoredShader->SetShader();
	mpColoredShader->DisableAlpha();
	mpColoredShader->mPOConstantBuffer.View = mCamera.GetView();
	mpColoredShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	mpColoredShader->mPOConstantBuffer.LightDirection = mDirLight.mDirection;
	mpColoredShader->mPOConstantBuffer.LightColor = mDirLight.mColor;
	for (auto model : mColoredModels)
	{
		mpColoredShader->mPOConstantBuffer.World = model->GetWorld();
		mpColoredShader->UpdateConstantBuffer();
		model->draw();
	}
	mpColoredShader->mPOConstantBuffer.World = mLightVector->GetWorld();
	mpColoredShader->UpdateConstantBuffer();
	mLightVector->draw();

	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();
	mpTexturedShader->mPOConstantBuffer.View = mCamera.GetView();
	mpTexturedShader->mPOConstantBuffer.Projection = mCamera.GetProj();
	mpTexturedShader->mPOConstantBuffer.LightDirection = mDirLight.mDirection;
	mpTexturedShader->mPOConstantBuffer.LightColor = mDirLight.mColor;
	std::vector<std::string> materials;
	materials.push_back("Brick"); materials.push_back("Brick Normal Map");
	mpMaterialsManager->SetCurrentMaterials(materials);
	for (auto model : mTexturedModels)
	{
		//mpMaterialsManager->SetCurrentMaterials(materials);
		//mpMaterialsManager->SetCurrentMaterial(model->GetMaterialName());
		mpTexturedShader->mPOConstantBuffer.World = model->GetWorld();
		mpTexturedShader->UpdateConstantBuffer();
		model->draw();
	}

	// Draw 2D Text
	GApplication::TurnZBufferOff();
	mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->mPOConstantBuffer.View = GMathMF(XMMatrixIdentity());
	mpFontShader->mPOConstantBuffer.Projection = mCamera.GetOrtho();
	
	for (auto text : mText)
	{
		mpMaterialsManager->SetCurrentMaterial(text->GetMaterialName());
		mpFontShader->mPOConstantBuffer.World = text->GetWorld();
		mpFontShader->UpdateConstantBuffer();
		text->draw();
	}

    mSwapChain->Present(0, 0);
}

BumpMapping::~BumpMapping(void)
{
	delete mLightVector;

	std::for_each(mText.begin(), mText.end(), [](GText* item) { delete item; });
	std::for_each(mColoredModels.begin(), mColoredModels.end(), 
		[](GMesh<GColoredVertex>* item) { delete item; });
	std::for_each(mTexturedModels.begin(), mTexturedModels.end(), 
		[](GMesh<GTexturedVertex>* item) { delete item; });

	delete mpMaterialsManager;
	
	delete mpLightMappedShader;
	delete mpTexturedShader;
	delete mpFontShader;
	delete mpColoredShader;

    if( md3dContext )
        md3dContext->ClearState();
}