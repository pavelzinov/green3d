#include "GTexturedVertex.h"
using namespace Game;

GTexturedVertex::GTexturedVertex(void)
{
	this->mPosition = XMFLOAT3(0.0f, 0.0f, 0.0f);
	this->mTexturedCoordinates = XMFLOAT2(0.0f, 0.0f);
	this->mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	this->mTangent = XMFLOAT3(0.0f, 0.0f, 0.0f);
	this->mBinormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
}

GTexturedVertex::GTexturedVertex(const GTexturedVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mTexturedCoordinates = vertex.mTexturedCoordinates;
	this->mNormal = vertex.mNormal;	
	this->mTangent = vertex.mTangent;
	this->mBinormal = vertex.mBinormal;
}
