#pragma once
#include "GVertex.h"

namespace Game
{

// aligned textured vertex data storage
struct GTexturedVertexData
{
	XMFLOAT3 pos;
	XMFLOAT2 texC;
	XMFLOAT3 normal;
	XMFLOAT3 tangent;
	XMFLOAT3 binormal;
};

class GTexturedVertex :	public GVertex
{
public:
	GTexturedVertex(void);
	GTexturedVertex(const GTexturedVertex &vertex);
	~GTexturedVertex(void) {}

	static uint32_t getDataSize() { return sizeof(GTexturedVertexData); }

public:
	XMFLOAT2 mTexturedCoordinates;
	XMFLOAT3 mTangent;
	XMFLOAT3 mBinormal;
};

}