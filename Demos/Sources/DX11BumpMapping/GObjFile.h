#pragma once
#include "GUtility.h"

namespace Game
{

class GObjFile
{
private:
	struct VertexPosition
	{
		float x, y, z;
	};
	struct VertexNormal
	{
		float nx, ny, nz;
	};
	struct TextureCoordinate
	{
		float u, v;
	};
	struct FaceVertex
	{
		VertexPosition		mVertex;
		TextureCoordinate	mTexCoords;
		VertexNormal		mNormal;		
	};
	struct Face
	{
		FaceVertex mVertices[3];
	};
public:
	GObjFile(void) {}
	~GObjFile(void) {}

public:
	bool Load(std::string filename);
	bool Save(std::string filename);

public:
	std::vector<VertexPosition>		mVertices;
	std::vector<VertexNormal>		mNormals;
	std::vector<TextureCoordinate> mTexCoords;

	std::vector<Face>				mFaces;

private:
	// Read file line by line into `file_lines` vector
	std::vector<std::string>& GetFileLines(std::string filename, std::vector<std::string>& file_lines);

};

}