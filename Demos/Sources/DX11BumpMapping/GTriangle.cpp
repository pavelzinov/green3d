#pragma once
#include "GTriangle.h"
using namespace Game;

template<>
void GTriangle<GColoredVertex>::setVertices(GColoredVertex vertex1, 
	GColoredVertex vertex2, GColoredVertex vertex3)
{
	mVertices.clear();

	// Vertices
	mVertices.push_back(vertex1);
	mVertices.push_back(vertex2);
	mVertices.push_back(vertex3);

	// Normal
	XMStoreFloat3(&mNormal, XMVector3Cross(XMLoadFloat3(&vertex2.mPosition)-XMLoadFloat3(&vertex1.mPosition),
		XMLoadFloat3(&vertex3.mPosition)-XMLoadFloat3(&vertex2.mPosition)));
	XMStoreFloat3(&mNormal, XMVector3Normalize(XMLoadFloat3(&mNormal)));

	GColoredVertexData vertices[3];
	for (int i=0; i<3; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].color = mVertices[i].mColor;
		vertices[i].normal = mVertices[i].mNormal;
	}

	// Vertex Buffer
	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GColoredVertex::getDataSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

 	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));

	// Indices
	uint32_t indices[3] = {0, 1, 2};

	// Index Buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint32_t) * 3;
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = indices;
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}

template<>
void GTriangle<GTexturedVertex>::setVertices(GTexturedVertex vertex1, 
	GTexturedVertex vertex2, GTexturedVertex vertex3)
{
	mVertices.clear();

	// Vertices
	mVertices.push_back(vertex1);
	mVertices.push_back(vertex2);
	mVertices.push_back(vertex3);

	// Normal
	XMStoreFloat3(&mNormal, XMVector3Cross(XMLoadFloat3(&vertex2.mPosition)-XMLoadFloat3(&vertex1.mPosition),
		XMLoadFloat3(&vertex3.mPosition)-XMLoadFloat3(&vertex2.mPosition)));
	XMStoreFloat3(&mNormal, XMVector3Normalize(XMLoadFloat3(&mNormal)));

	GTexturedVertexData vertices[3];
	for (int i=0; i<3; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
		vertices[i].normal = mVertices[i].mNormal;
		vertices[i].tangent = mVertices[i].mTangent;
		vertices[i].binormal = mVertices[i].mBinormal;
	}

	// Vertex Buffer
	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GTexturedVertex::getDataSize() * mVertices.size();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

 	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));

	// Indices
	uint32_t indices[3] = {0, 1, 2};

	// Index Buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint32_t) * 3;
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = indices;
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}