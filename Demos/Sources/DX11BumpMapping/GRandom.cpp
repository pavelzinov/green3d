#include "GRandom.h"
#include <ctime>
#include <cstdlib>

namespace Game
{

GRandom::GRandom(void)
{
    srand(static_cast<unsigned int>(time(NULL)));
}


GRandom::~GRandom(void)
{
}

float GRandom::Gen(float end, float start)
{
    if (end <= start)
    {
        return -1.0f;
    }
    else
    {
        return (start + (end-start)*static_cast<float>(rand())/static_cast<float>(RAND_MAX));
    }
}

int GRandom::GenInt(int end, int start)
{
    if (end <= start)
    {
        return -1;
    }
    else
    {
        return (rand()%(end-start+1)+start);
    }
}

} // namespace Game
