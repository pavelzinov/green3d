#pragma once
#include "GTimer.h"

namespace Game
{
	//************************************
	// �������:				����������� �� ���������, �������� ��������� ���������� ������, �������� ��������,
	//						����������� ��� �������� ���������� ����� � �������� ��� �������� ����� - �������
	// ������� ������:		-
	// �������� ������:		mSecsPerCnt, mDeltaTime, mStartCnt, mTickPrevCnt, mTickCurrCnt, mTmpCnt
	//************************************
	GTimer::GTimer() : mSecsPerCnt(0.0), mDeltaTime(0.0), mGameTime(0.0), mStartCnt(0), mPauseCnt(0),
		mResumeCnt(0), mTickPrevCnt(0),	mTickCurrCnt(0), mTmpCnt(0), isPaused(true)
	{
		QueryPerformanceFrequency((LARGE_INTEGER*) &mTmpCnt);
		mSecsPerCnt = 1.0 / (double) mTmpCnt;
	}

	//************************************
	// �������:				��������� �������� ���������� mDeltaTime, �������� ���������� � ��������� ���������
	//						����� �������� ������� tick() � ��������
	// ������� ������:		-
	// �������� ������:		mDeltaTime
	//************************************
	double GTimer::getDeltaTime()
	{// ���������� �������, ���������� ����� ����� ����� ���������� �������� ������� tick()
		return mDeltaTime;
	}

	//************************************
	// �������:				��������� ������� � �������� ����� ��������� ������� ������� tick() 
	//						� ������� ������ �������
	// ������� ������:		-
	// �������� ������:		��������� �������� � ��������
	//************************************
	double GTimer::getDeltaTimeNow()
	{
		QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt); // ����������� ������� �������� �������� 
																//��� �������� �� ������
		return ((mTmpCnt - mTickCurrCnt) * mSecsPerCnt);
	}

	//************************************
	// �������:				��������� ������� ����, ���������� � ������� ������ start()
	// ������� ������:		-
	// �������� ������:		��������� �������� � ��������
	//			������:		-1 - �� ������� �������� ���������� � ����������
	//			������:		0 - ������ �� ��� �������
	//************************************
	double GTimer::getGameTime()
	{
		if (QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt))
		{
			if (isPaused)
			{
				return mGameTime;
			}
			else
			{
				return (mGameTime + (mTmpCnt - mResumeCnt) * mSecsPerCnt); // ��������� ����� ����� �������������
			}
		}
		else
		{
			return -1; // ������
		}
	}

	//************************************
	// �������:				������ ������� ���� ����� ������������� �������� ������ ������� mStartCnt
	// ������� ������:		-
	// �������� ������:		��������� mStartCnt, mResumeCnt, isPaused
	//************************************
	void GTimer::start()
	{
		if (!mStartCnt)
		{// ���� mStartCnt ������� ��� �� ��������������� (�.�. == 0)
			isPaused = false;
			if (QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt))
			{
				mStartCnt = mTmpCnt;
				mResumeCnt = mStartCnt;
			}			
		}		
	}

	//************************************
	// �������:				����� �������, ��������� ������� ���� ������ �� �����
	// ������� ������:		-
	// �������� ������:		��������� mPauseCnt, mGameTime, isPaused
	//************************************
	void GTimer::pause()
	{
		if (!isPaused && mStartCnt != 0)
		{
			isPaused = true;
			if (QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt))
			{
				mPauseCnt = mTmpCnt;
			}
			mGameTime += (mPauseCnt - mResumeCnt) * mSecsPerCnt;
		}
	}

	//************************************
	// �������:				������������� ������ �������
	// ������� ������:		-
	// �������� ������:		��������� mResumeCnt, isPaused
	//************************************
	void GTimer::resume()
	{
		if (isPaused && mStartCnt != 0)
		{// ����� ���������� �������� ������ ����� ������ �� ����� � ��� ������ start() �� ������ ���� �������
			isPaused = false;
			if (QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt))
			{
				mResumeCnt = mTmpCnt;
			}
		}
	}

	//************************************
	// �������:				����� ���������� ���������� ������� � 0, ��������������� ���������� mSecsPerCnt
	// ������� ������:		-
	// �������� ������:		mSecsPerCnt, mDeltaTime, mStartCnt, mTickPrevCnt, mTickCurrCnt, mTmpCnt	
	//************************************
	void GTimer::reset()
	{
		if (QueryPerformanceFrequency((LARGE_INTEGER*) &mTmpCnt))
			mSecsPerCnt = 1.0 / (double) mTmpCnt;
		mDeltaTime = 0.0;
		mGameTime = 0.0;

		mStartCnt = 0;
		mPauseCnt = 0;
		mResumeCnt = 0;
		mTickCurrCnt = 0;
		mTickPrevCnt = 0;
		mTmpCnt = 0;
		isPaused = true;
	}

	//************************************
	// �������:				��������� ���������� ����������, ���������� �� �������� ��������� ����������
	//						� ������ ������ �������
	// ������� ������:		-
	// �������� ������:		mDeltaTime
	//************************************
	void GTimer::tick()
	{		
		if (QueryPerformanceCounter((LARGE_INTEGER*) &mTmpCnt))
		{
			mTickPrevCnt = mTickCurrCnt; // ���������� � ���������� �������� ������ �������
			mTickCurrCnt = mTmpCnt; // ��������� ������� ��������
			mTickPrevCnt ? mDeltaTime = (mTickCurrCnt - mTickPrevCnt) * mSecsPerCnt : mDeltaTime = 0; // ��� ������ 
			// ���������� ���������� � ������� ����� ������, ���� ��� ������ ����� ���� ������ ���
			// � ���� ������ ������� ����� ������ ����� 0 ��
		}
	}

	GTimer::~GTimer()
	{

	}
}