#pragma once
#include "GMesh.h"
using namespace Game;

template <>
void* GMesh<GColoredVertex>::GetVertexData()
{
	uint32_t verticesNumber = GetVertexCount();
	GColoredVertexData *vertices = new GColoredVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].normal = mVertices[i].mNormal;
		vertices[i].color = mVertices[i].mColor;
	}
	return static_cast<void*>(vertices);
}

template <>
void* GMesh<GTexturedVertex>::GetVertexData()
{
	uint32_t verticesNumber = GetVertexCount();
	GTexturedVertexData *vertices = new GTexturedVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].pos = mVertices[i].mPosition;
		vertices[i].texC = mVertices[i].mTexturedCoordinates;
		vertices[i].normal = mVertices[i].mNormal;		
		vertices[i].tangent = mVertices[i].mTangent;
		vertices[i].binormal = mVertices[i].mBinormal;
	}
	return static_cast<void*>(vertices);
}

template <>
void GMesh<GColoredVertex>::FromFileData(std::vector<std::string> &file_data)
{
	std::string vertex_type = file_data[0];
	if (vertex_type != "GColoredVertex") return;

	unsigned int vertex_count = 0;
	std::stringstream line_as_stream;	

	line_as_stream << file_data[1];
	line_as_stream >> vertex_count;
	line_as_stream.clear();

	float x, y, z, r, g, b, a;
	GColoredVertex vertex;
	
	// reading vertices
	mVertices.clear();
	mVertices.reserve(vertex_count);
	auto index_data_start = vertex_count + 2;
	for (unsigned int i=2; i < index_data_start; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> x >> y >> z >> r >> g >> b >> a;
		vertex.mPosition.x = x;
		vertex.mPosition.y = y;
		vertex.mPosition.z = z;
		vertex.mColor.x = r;
		vertex.mColor.y = g;
		vertex.mColor.z = b;
		vertex.mColor.w = a;
		mVertices.push_back(vertex);
		line_as_stream.clear();
	}

	// reading indices
	mIndices.clear();
	mIndices.reserve((file_data.size() - vertex_count - 2) * 3);
	uint32_t i1 = 0, i2 = 0, i3 = 0;
	uint32_t file_size = file_data.size();
	for (auto i=index_data_start; i<file_size; i++)
	{
		line_as_stream << file_data[i];
		line_as_stream >> i1 >> i2 >> i3;
		mIndices.push_back(i1 - 1);
		mIndices.push_back(i2 - 1);
		mIndices.push_back(i3 - 1);
		line_as_stream.clear();
	}

	// constructing normals
	std::vector<std::vector<XMFLOAT3>> normals;
	normals.resize(vertex_count);
	// for each triangle
	uint32_t indices_count = mIndices.size();
	XMVECTOR vec1, vec2, normal;
	for (uint32_t i=0; i<indices_count; i+=3)
	{
		vec1 = GMathFV(mVertices[mIndices[i + 1]].mPosition) - 
			GMathFV(mVertices[mIndices[i]].mPosition);
		vec2 = GMathFV(mVertices[mIndices[i + 2]].mPosition) - 
			GMathFV(mVertices[mIndices[i]].mPosition);
		// determine normal for first triangle
		normal = XMVector3Normalize(XMVector3Cross(vec1, vec2));
		// add triangle's normal to all it's vertices's current normals
		normals[mIndices[i]].push_back(GMathVF(normal));
		normals[mIndices[i + 1]].push_back(GMathVF(normal));
		normals[mIndices[i + 2]].push_back(GMathVF(normal));
	}

	// make sure vertex's normals don't duplicate
	for (uint32_t i = 0; i < normals.size(); ++i)
	{
		for (uint32_t j = 0; j < normals[i].size(); ++j)
		{
			for (uint32_t k = j + 1; k < normals[i].size(); ++k)
			{
				if (normals[i][k].x == normals[i][j].x &&
					normals[i][k].y == normals[i][j].y &&
					normals[i][k].z == normals[i][j].z)
				{
					normals[i].erase(normals[i].begin() + k);
					k = j + 1;
				}
			}
		}
	}
	
	// summarize each vertex's normals to get single value per vertex
	normal = XMVectorZero();
	for (uint32_t i = 0; i < vertex_count; ++i)
	{
		// combine all normals for single vertex
		normal = XMVectorZero();
		for (auto it = normals[i].begin(); it != normals[i].end(); it++)
		{
			normal = normal + GMathFV(*it);
		}
		// normalize result and assign it to vertex's normal
		mVertices[i].mNormal = GMathVF(XMVector3Normalize(normal));
	}

	this->ConstructBuffers();
}

template<>
void GMesh<GColoredVertex>::LoadFromObj(const GObjFile& obj_file)
{
	GColoredVertex vertex;
	int faces_count = obj_file.mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.mFaces[i].mVertices[j].mVertex.x;
			vertex.mPosition.y = obj_file.mFaces[i].mVertices[j].mVertex.y;
			vertex.mPosition.z = obj_file.mFaces[i].mVertices[j].mVertex.z;

			vertex.mNormal.x = obj_file.mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.mFaces[i].mVertices[j].mNormal.nz;

			vertex.mColor = GCOLOR_CYAN;

			mVertices.push_back(vertex);
		}
	}

	uint32_t indices_count = faces_count * 3;
	mIndices.resize(indices_count);
	for (uint32_t i=0; i<indices_count; ++i)
	{
		mIndices[i] = i;
	}

	this->ConstructBuffers();
}

template<>
void GMesh<GTexturedVertex>::LoadFromObj(const GObjFile& obj_file)
{
	GTexturedVertex vertex;
	int faces_count = obj_file.mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.mFaces[i].mVertices[j].mVertex.x;
			vertex.mPosition.y = obj_file.mFaces[i].mVertices[j].mVertex.y;
			vertex.mPosition.z = obj_file.mFaces[i].mVertices[j].mVertex.z;

			vertex.mNormal.x = obj_file.mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.mFaces[i].mVertices[j].mNormal.nz;

			vertex.mTexturedCoordinates.x = obj_file.mFaces[i].mVertices[j].mTexCoords.u;
			vertex.mTexturedCoordinates.y = obj_file.mFaces[i].mVertices[j].mTexCoords.v;			

			mVertices.push_back(vertex);

			if (j == 2)
			{
				CalculateTangentAndBinormal(*(mVertices.end() - 1), *(mVertices.end() - 2), 
					*(mVertices.end() - 3));
			}
		}
	}

	uint32_t indices_count = faces_count * 3;
	mIndices.resize(indices_count);
	for (uint32_t i=0; i<indices_count; ++i)
	{
		mIndices[i] = i;
	}

	this->ConstructBuffers();
}

template<>
void GMesh<GTexturedVertex>::CalculateTangentAndBinormal(GTexturedVertex& v1, 
														 GTexturedVertex& v2, 
														 GTexturedVertex& v3)
{
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;


	// Calculate the two vectors for this face.
	vector1[0] = v2.mPosition.x - v1.mPosition.x;
	vector1[1] = v2.mPosition.y - v1.mPosition.y;
	vector1[2] = v2.mPosition.z - v1.mPosition.z;

	vector2[0] = v3.mPosition.x - v1.mPosition.x;
	vector2[1] = v3.mPosition.y - v1.mPosition.y;
	vector2[2] = v3.mPosition.z - v1.mPosition.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = v2.mTexturedCoordinates.x - v1.mTexturedCoordinates.x;
	tvVector[0] = v2.mTexturedCoordinates.y - v1.mTexturedCoordinates.y;

	tuVector[1] = v3.mTexturedCoordinates.x - v1.mTexturedCoordinates.x;
	tvVector[1] = v3.mTexturedCoordinates.y - v1.mTexturedCoordinates.y;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	XMFLOAT3 tangent;
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	XMFLOAT3 binormal;
	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of this normal.
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));

	// Normalize the normal and then store it
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of this normal.
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));

	// Normalize the normal and then store it
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	v1.mTangent = tangent;
	v1.mBinormal = binormal;
	v2.mTangent = tangent;
	v2.mBinormal = binormal;
	v3.mTangent = tangent;
	v3.mBinormal = binormal;
}