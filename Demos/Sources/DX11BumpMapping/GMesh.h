#pragma once
#include "GUtility.h"
#include "GTriangle.h"
#include "GMaterial.h"
#include "GObjFile.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores information about model represented by mesh. 
// It contains all vertices and indices of model, also
// handles all moving, scaling and rotating functions
// for mesh. This class is drawing mesh on the screen.
////////////////////////////////////////////////////////
template<class T>
class GMesh
{
public:
	// Construct mesh with null buffers
	GMesh(ID3D11Device *device, ID3D11DeviceContext *context);
	// Construct mesh from file with filename
	GMesh(ID3D11Device *device, ID3D11DeviceContext *context, std::string filename);
	// Construct mesh from argument
	GMesh(const GMesh<T>& mesh);
	virtual ~GMesh(void);

public:
	// Construct single mesh from number of different meshes
	void FromMeshes(std::vector<GMesh<T>>& meshes);
	// Construct single mesh from number of different meshes
	void FromMeshes(std::vector<GMesh<T>*>& meshes);
	// Construct mesh from set of triangles
	void FromTriangles(std::vector<GTriangle<T> > triangles);
	// Load mesh from .mvci file format
	void LoadFromFile(std::string filename);
	// Load mesh from OBJ file parser
	void LoadFromObj(const GObjFile& obj_file);
	// Load mesh from OBJ file
	void LoadFromObj(const std::string filename) { GObjFile file; file.Load(filename); LoadFromObj(file); }
	// Assign material name
	void SetMaterialName(std::string material_name);
	// Get material name
	std::string GetMaterialName();

public:
	// Draw mesh by calling draw for each triangle in set
	void drawTriangles();
	// Draw mesh by single draw call
	void draw();

public:
	std::string	mMaterialName;

protected:
	// Feed mVertices and mIndices from file data
	void FromFileData(std::vector<std::string> &file_data);
	// Read file line by line into `file_lines` vector
	void GetFileLines(std::string filename, vector<std::string>& file_lines);
	// (Re)constructs vertex and index buffers
	virtual void ConstructBuffers();
	// Returns the number of vertices that mesh consists of
	uint32_t GetVertexCount();
	// Returns pointer to dynamic array of vertex data. Remember to call delete[] on 
	// it's result once you have finished to work with it.
	void* GetVertexData();
	// Parse .OBJ file data line by line
	void ParseObjData(vector<std::string>& file_lines);
	void CalculateTangentAndBinormal(T& v1, T& v2, T& v3);

protected:
	std::vector<GTriangle<T> >	mTriangles;
	std::vector<T>				mVertices;
	std::vector<uint32_t>			mIndices;	

	ID3D11Device		*mDevice;
	ID3D11DeviceContext	*mContext;
	ID3D11Buffer		*mVB;
	ID3D11Buffer		*mIB;

/* Control model space */

public:
	// Returns mesh's world matrix
	XMFLOAT4X4 GetWorld();
	// Rotate mesh around X axis. Angle must be in degrees.
	XMFLOAT4X4 RotateX(float degrees);
	// Rotate mesh around Y axis. Angle must be in degrees.
	XMFLOAT4X4 RotateY(float degrees);
	// Rotate mesh around Z axis. Angle must be in degrees.
	XMFLOAT4X4 RotateZ(float degrees);
	// Rotate mesh around given axis. Angle must be in degrees.
	XMFLOAT4X4 Rotate(XMFLOAT3 axis, float degrees);
	// Scale mesh coordinates along each axis.
	XMFLOAT4X4 Scale(float dx, float dy, float dz);
	// Scale mesh coordinates along each axis by the same value.
	XMFLOAT4X4 Scale(float delta_size);
	// Move mesh by vector.
	XMFLOAT4X4 Move(XMFLOAT3 direction);
	// Move mesh by individual values along each axis.
	XMFLOAT4X4 Move(float dir_x, float dir_y, float dir_z);

protected:
	XMFLOAT4X4 mScaling;		// Scaling matrix
	XMFLOAT4X4 mTranslation;	// Translation matrix
	XMFLOAT4X4 mRotation;		// Rotation matrix
};

template<class T>
GMesh<T>::GMesh(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mDevice = device;
	mContext = context;
	mVB = nullptr;
	mIB = nullptr;
	mMaterialName = "";
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mRotation = mTranslation = mScaling;
}

template<class T>
GMesh<T>::GMesh(ID3D11Device *device, ID3D11DeviceContext *context, std::string filename)
{
	mDevice = device;
	mContext = context;
	mVB = nullptr;
	mIB = nullptr;
	mMaterialName = "";
	mRotation = mTranslation = mScaling = GMathMF(XMMatrixIdentity());
	LoadFromFile(filename);
}

template<class T>
GMesh<T>::GMesh(const GMesh<T>& mesh)
{
	this->mDevice = mesh.mDevice;
	this->mContext = mesh.mContext;
	this->mMaterialName = mesh.mMaterialName;

	this->mVertices = mesh.mVertices;
	this->mIndices = mesh.mIndices;

	this->mScaling = mesh.mScaling;
	this->mRotation = mesh.mRotation;
	this->mTranslation = mesh.mTranslation;

	this->mVB = nullptr;
	this->mIB = nullptr;

	this->ConstructBuffers();
}

template<class T>
GMesh<T>::~GMesh()
{
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);
}

template<class T>
void GMesh<T>::FromMeshes(std::vector<GMesh<T>>& meshes)
{
	mVertices.clear();
	mIndices.clear();

	uint32_t meshes_count = meshes.size();
	uint32_t vertices_count = 0;
	for (uint32_t i = 0; i < meshes_count; ++i)
	{
		vertices_count = meshes[i].mVertices.size();
		for (uint32_t j=0; j<vertices_count; ++j)
		{
			T vertex(meshes[i].mVertices[j]);
			vertex.mPosition = GMathVF(XMVector3Transform(GMathFV(vertex.mPosition), 
				XMMatrixTranspose(GMathFM(meshes[i].GetWorld()))));
			mVertices.push_back(vertex);
		}
	}

	uint32_t indices_offset = 0;
	uint32_t indices_count = 0;
	for (uint32_t i = 0; i < meshes_count; ++i)
	{
		indices_offset = 0;
		for (uint32_t j = 0; j < i; ++j)
		{
			indices_offset += meshes[j].mVertices.size();
		}
		indices_count = meshes[i].mIndices.size();
		for (uint32_t j = 0; j < indices_count; ++j)
		{
			mIndices.push_back(meshes[i].mIndices[j] + indices_offset);
		}
	}	

	ConstructBuffers();
}

template<class T>
void GMesh<T>::FromMeshes(std::vector<GMesh<T>*>& meshes)
{
	mVertices.clear();
	mIndices.clear();

	uint32_t meshes_count = meshes.size();
	uint32_t vertices_count = 0;
	for (uint32_t i = 0; i < meshes_count; ++i)
	{
		vertices_count = meshes[i]->mVertices.size();
		for (uint32_t j=0; j<vertices_count; ++j)
		{
			T vertex(meshes[i]->mVertices[j]);
			vertex.mPosition = GMathVF(XMVector3Transform(GMathFV(vertex.mPosition), 
				XMMatrixTranspose(GMathFM(meshes[i]->GetWorld()))));
			mVertices.push_back(vertex);
		}
	}

	uint32_t indices_offset = 0;
	uint32_t indices_count = 0;
	for (uint32_t i = 0; i < meshes_count; ++i)
	{
		indices_offset = 0;
		for (uint32_t j = 0; j < i; ++j)
		{
			indices_offset += meshes[j]->mVertices.size();
		}
		indices_count = meshes[i]->mIndices.size();
		for (uint32_t j = 0; j < indices_count; ++j)
		{
			mIndices.push_back(meshes[i]->mIndices[j] + indices_offset);
		}
	}

	ConstructBuffers();
}

template<class T>
void GMesh<T>::FromTriangles(std::vector<GTriangle<T> > triangles)
{
	mTriangles = triangles;
	for (int i=0; i<mTriangles.size(); i++)
	{
		mVertices.push_back(mTriangles.getVertices()[0]);
		mVertices.push_back(mTriangles.getVertices()[1]);
		mVertices.push_back(mTriangles.getVertices()[2]);
	}

	this->ConstructBuffers();
}

// Open mvci file.
// .mvci file structure:
/////////////////////////////
// VertexType
// VertexCount
// x y z r g b a
// ...
// triangle_vertex_index_1 triangle_vertex_index_2 triangle_vertex_index_3
// ...
// [this line can be empty]
/////////////////////////////
// vertex's indices start from 1. It means that the first line
// containing x y z r g b a has index 1 and so on.
template <class T>
void GMesh<T>::LoadFromFile(std::string filename)
{	
	std::vector<std::string> file_bytes;
	GetFileLines(filename, file_bytes);
	FromFileData(file_bytes);
}

template<class T>
void GMesh<T>::drawTriangles()
{
	for (int i=0; i<mTriangles.size(); i++)
	{
		mTriangles[i].draw();
	}
}

template<class T>
void GMesh<T>::draw()
{
	uint32_t strides = T::getDataSize();
	uint32_t offset = 0;
	mContext->IASetVertexBuffers(0, 1, &mVB, &strides, &offset);
	mContext->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);
	mContext->DrawIndexed(mIndices.size(), 0, 0);
}

template <class T>
void GMesh<T>::GetFileLines(std::string filename, vector<std::string>& file_lines)
{
	std::string line;	
	std::ifstream model;
	model.open(filename);	
	std::vector<std::string> file_bytes;

	if (model.is_open())
	{
		while (model.good())
		{
			getline(model, line);
			file_lines.push_back(line);
		}
		model.close();

		// delete last empty character
		if (file_lines.back() == "")
			file_lines.pop_back();
	}
	else
	{
		return;
	}
}

template <class T>
void GMesh<T>::ConstructBuffers()
{
	if (mVertices.size() <= 0)
		return;

	// Clear old buffers
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);	

	// Feed vertex data
	void *vertices = GetVertexData();

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = T::getDataSize() * GetVertexCount();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));
	delete[] vertices;

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint32_t) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}

template<class T>
uint32_t GMesh<T>::GetVertexCount()
{
	return mVertices.size();
}

template<class T>
XMFLOAT4X4 GMesh<T>::GetWorld()
{
	return GMathMF(XMMatrixTranspose(GMathFM(mScaling) * GMathFM(mRotation) 
		* GMathFM(mTranslation)));
}

template<class T>
XMFLOAT4X4 GMesh<T>::RotateX(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationX(XMConvertToRadians(degrees)));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::RotateY(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationY(XMConvertToRadians(degrees)));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::RotateZ(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationZ(XMConvertToRadians(degrees)));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::Rotate(XMFLOAT3 axis, float degrees)
{
	if (XMVector3Equal(GMathFV(axis), XMVectorZero()) ||
		degrees == 0.0f)
		return GetWorld();

	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationAxis(XMLoadFloat3(&axis), XMConvertToRadians(degrees)));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::Scale(float scale_x, float scale_y, float scale_z)
{	
	XMStoreFloat4x4(&mScaling, XMLoadFloat4x4(&mScaling) 
		* XMMatrixScaling(scale_x, scale_y, scale_z));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::Scale(float delta_size)
{	
	return Scale(delta_size, delta_size, delta_size);
}

template<class T>
XMFLOAT4X4 GMesh<T>::Move(XMFLOAT3 direction)
{
	XMStoreFloat4x4(&mTranslation, XMLoadFloat4x4(&mTranslation) 
		* XMMatrixTranslation(direction.x, direction.y, direction.z));
	return GetWorld();
}

template<class T>
XMFLOAT4X4 GMesh<T>::Move(float dir_x, float dir_y, float dir_z)
{
	return Move(XMFLOAT3(dir_x, dir_y, dir_z));
}

template<class T>
void GMesh<T>::SetMaterialName(std::string material_name)
{
	mMaterialName = material_name;
}

template<class T>
std::string GMesh<T>::GetMaterialName()
{
	return mMaterialName;
}

}