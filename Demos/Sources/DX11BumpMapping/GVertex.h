#pragma once
#include "GUtility.h"
#include <vector>

namespace Game
{
/////////////////////////////////////////////////
// Basic class for storing vertex data. Basic 
// vertex has only position in 3D space.
/////////////////////////////////////////////////
class GVertex
{
public:
	// Construct vertex with 0, 0, 0 position
	GVertex(void);
	// Construct vertex with given XMFLOAT3 position
	GVertex(XMFLOAT3 position, XMFLOAT3 normal);
	virtual ~GVertex(void) {}

	// Get vertex data component size in bytes. Useful when creating vertex buffers.
	static uint32_t getDataSize();

public:
	XMFLOAT3 mPosition;
	XMFLOAT3 mNormal;
};

}

