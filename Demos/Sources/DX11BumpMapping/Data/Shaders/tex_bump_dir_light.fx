//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 LightDirection;
	float4 LightColor;
}
Texture2D ShaderTexture[2];
SamplerState SampleType;

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	float4 model_color = ShaderTexture[0].Sample(SampleType, ps_input.TexC);
	float4 bump_map = ShaderTexture[1].Sample(SampleType, ps_input.TexC);
	// Expand the range of the normal value from (0, +1) to (-1, +1).
    bump_map = (bump_map * 2.0f) - 1.0f;
	float3 bump_normal = bump_map.z * ps_input.Normal + 
		bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
	bump_normal = normalize(bump_normal);
	float3 light_direction = -LightDirection.xyz;
	float light_intensity = max(dot(bump_normal, light_direction), 0.0f);
	model_color.xyz = model_color.xyz * 0.2f + model_color.xyz * light_intensity;
    return model_color;
}