//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 LightDirection;
	float4 LightColor;
}
Texture2D ShaderTexture;
SamplerState SampleType;

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
    output.TexC = vs_input.TexC;
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	float4 model_color = ShaderTexture[0].Sample(SampleType, ps_input.TexC);
	float3 light_direction = -LightDirection.xyz;
	float light_intensity = max(dot(ps_input.Normal, light_direction), 0.0f);
	model_color.xyz = model_color.xyz * 0.2f + model_color.xyz * light_intensity;
    return model_color;
}