//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
}
Texture2D ShaderTexture;
SamplerState SampleType;

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
    output.TexC = vs_input.TexC;
	output.Normal = vs_input.Normal;
	output.Tangent = vs_input.Tangent;
	output.Binormal = vs_input.Binormal;
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
    return ShaderTexture.Sample(SampleType, ps_input.TexC);
}
