#pragma once
#include "GVertex.h"

namespace Game
{

// aligned colored vertex data storage
struct GColoredVertexData
{
	XMFLOAT3 pos;
	XMFLOAT3 normal;
	XMFLOAT4 color;
};

// colored vertex
// contains position and color of vertex and
// some operations over it
class GColoredVertex : public GVertex
{
public:
	GColoredVertex(void);
	GColoredVertex(const GColoredVertex &vertex);
	~GColoredVertex(void) {}

	static uint32_t getDataSize();

	XMFLOAT4 mColor;
};

}

