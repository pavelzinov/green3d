#pragma once
#include <windows.h>
#include <sstream>
#include "GTimer.h"
#include "GUtility.h"
#include "GInput.h"

namespace Game
{
	class GApplication
	{
	public:
		GApplication(HINSTANCE hInstance);
		virtual ~GApplication(void);

		HINSTANCE	GetApplicationInstance();
		HWND		GetWindowHandle();

		// Initialize application.
		virtual void initApp();
		// Start application.
		int run();
		// Only for internal use. DONT CALL IT.
		virtual LRESULT msgProc(uint32_t msg, WPARAM wParam, LPARAM lParam);	
		virtual void SetUPS(int updates_per_second) { 
			mUpdatesPerSecond = updates_per_second;
			mSecondsBetweenUpdates = 1.0f/static_cast<float>(mUpdatesPerSecond);
		}

	protected:
		virtual void onResize();
		virtual void updateScene(float dSeconds);
		virtual void drawScene();

	protected:
		void TurnZBufferOn() { md3dContext->OMSetDepthStencilState(mpDepthEnabledStencil, 1); }
		void TurnZBufferOff() { md3dContext->OMSetDepthStencilState(mpDepthDisabledStencil, 1); }
		void BackfaceCullingOn() { md3dContext->RSSetState(mpRSBackfaceCullingSolid); }
		void BackfaceCullingNone() { md3dContext->RSSetState(mpRSNoCullingSolid); }
		void WireframeOn() { md3dContext->RSSetState(mpRSWireframe); }
		void WireframeOff() { md3dContext->RSSetState(mpRSBackfaceCullingSolid); }

	private:
		void initMainWindow();
		HRESULT initDirect3D();

	private:
		HINSTANCE	mhAppInst;  // application instance handle
		HWND		mhMainWnd;  // main window handle
		bool		mAppPaused; // is the application paused?
		bool		mMinimized; // is the application minimized?
		bool		mMaximized; // is the application maximized?
		bool		mResizing;  // are the resize bars being dragged?

		// How much world updates and input updates
		// must be done between two frames
		int			mUpdatesPerSecond;
		float		mSecondsBetweenUpdates;

	protected:
		// Used to keep track of the "delta-time" and game time
		GTimer mTimer;

        // Object which controls input from keyboard and mouse
        GInput mInput;

		// A string to store the frame statistics for output. We display
		// the average frames per second and the average time it takes
		// to render one frame.
		int mUPS;
		float mMsPerUpdate;
		int mFPS;
		float mMsPerFrame;

		// The D3D11 device, the swap chain for page flipping,
		// the 2D texture for the depth/stencil buffer,
		// and the render target and depth/stencil views. We
		// also store a font pointer so that we can render the
		// frame statistics to the screen.
		ID3D11Device*				md3dDevice;
		ID3D11DeviceContext*		md3dContext;
		IDXGISwapChain*				mSwapChain;

		ID3D11Texture2D*			mDepthStencilBuffer;
		ID3D11RenderTargetView*		mRenderTargetView;
		ID3D11DepthStencilView*		mDepthStencilView;

		ID3D11DepthStencilState*	mpDepthDisabledStencil;
		ID3D11DepthStencilState*	mpDepthEnabledStencil;

		ID3D11RasterizerState*		mpRSBackfaceCullingSolid;
		ID3D11RasterizerState*		mpRSNoCullingSolid;
		ID3D11RasterizerState*		mpRSWireframe;

		// The following variables are initialized in the GApplication constructor
		// to default values. However, you can override the values in the
		// derived class to pick different defaults.

		// Device type
		D3D_DRIVER_TYPE md3dDriverType;
		D3D_FEATURE_LEVEL md3dFeatureLevel;

		// Window title/caption. GApplication defaults to "D3D11 Application".
		std::wstring mMainWndCaption;

		// Color to clear the background. GApplication defaults to blue.
		XMFLOAT4 mClearColor;

		// Initial size of the window's client area. GApplication defaults to
		// 800x600.  Note, however, that these values change at run time
		// to reflect the current client area size as the window is resized.
		int mClientWidth;
		int mClientHeight;
        
        // AA
        uint32_t mSampleCount;
        uint32_t mSampleQuality;
	};
}