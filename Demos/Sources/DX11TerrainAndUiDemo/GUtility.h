#pragma once
#include <windows.h>
// DirectX includes
#include <d3d11.h>
#include <directxmath.h>
#include <d3dcompiler.h>
// STL includes
#include <string>
#include <vector>
#include <map>
#include <algorithm>
// STL streams
#include <sstream>
#include <fstream>
#include <iostream>
#include <istream>
// Memory
#include <memory>
// Include Game
#include "GLog.h"
#include "GMath.h"
#include "GColor.h"
#include "GString.h"
#include "GFileAscii.h"
#include "GTimer.h"
#include "GRandom.h"

using namespace Game;
using namespace DirectX;
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;

typedef unsigned int uint;

// Releasing COM-object
inline void ReleaseCOM(IUnknown *x) 
{ 
	if(x != nullptr)
	{
		x->Release(); 
		x=nullptr;
	} 
}

// Safe pointer release
template <class any_type>
inline void PtrRelease(any_type *x) 
{ 
	if(x != nullptr)
	{
		delete x; 
		x=nullptr;
	} 
}

// Output DX11 debug information
#if defined(DEBUG) | defined(_DEBUG)
inline void HR(HRESULT hresult)
{
	LPTSTR errorText = NULL;
	FormatMessage(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		|FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		|FORMAT_MESSAGE_IGNORE_INSERTS,  
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		hresult,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&errorText,  // output 
		0, // minimum size for output buffer
		NULL);   // arguments - see note 

	if ( NULL != errorText )
	{
		OutputDebugString(errorText);
		// release memory allocated by FormatMessage()
		LocalFree(errorText);
		errorText = NULL;
	}
}
#else
inline void HR(HRESULT hr)
{
	return;
}
#endif

// Return HRESULT statement
#define HRR(hr) { if (FAILED(hr)) return hr; }

// Comprising two values by using MATH_EPS
template <class T>
inline bool GMathEqual(T x, T y)
{ 
	if (fabs(static_cast<float>(x-y)) <= GMath::EPS)
		return true;
	return false;
}

inline XMVECTOR GMathFV(XMFLOAT3& val)
{
	return XMLoadFloat3(&val);	
}

inline XMFLOAT3 GMathVF(XMVECTOR& vec)
{
	XMFLOAT3 val;
	XMStoreFloat3(&val, vec);
	return val;
}

inline XMMATRIX GMathFM(XMFLOAT4X4& val)
{
	return XMLoadFloat4x4(&val);
}

inline XMFLOAT4X4 GMathMF(XMMATRIX& matrix)
{
	XMFLOAT4X4 val;
	XMStoreFloat4x4(&val, matrix);
	return val;
}

inline float GMathRound(float& val)
{
	return (val > 0.0f) ? floor(val + 0.5f) : ceil(val - 0.5f);
}

inline float GMathFloatRound(float val)
{
	if (GMathEqual<float>(val, GMathRound(val)))
		return GMathRound(val);
	return val;
}

// Output Debug message
#if defined(DEBUG) | defined(_DEBUG)
inline void Log(std::string &str)
{
	OutputDebugStringA(str.c_str());
}
#else
inline void Log(std::string &str)
{
	return;
}
#endif