#pragma once
#include "GMesh.h"
using namespace Game;
//-----------------------------------------------------------------------------------
GMesh::GMesh(const std::string& mesh_name, const GObjFile& obj_file, const uint primitive_index, 
	const GMaterialsManager& materials, ID3D11Device *device, ID3D11DeviceContext *context)
{
	mDevice = device;
	mContext = context;
	mName = mesh_name;
	mVB = nullptr;
	mIB = nullptr;
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mRotation = mTranslation = mScaling;
	LoadFromObj(obj_file, primitive_index, materials);
}
//------------------------------------------------------------------------------------
GMesh::GMesh(const std::vector<GMesh*> &meshes)
{
	this->mVB = nullptr;
	this->mIB = nullptr;
	XMStoreFloat4x4(&mScaling, XMMatrixIdentity());
	mRotation = mTranslation = mScaling;
	LoadFromMeshes(meshes);
}
//------------------------------------------------------------------------------------
GMesh::GMesh(const std::vector<GMesh*>::const_iterator &first, const std::vector<GMesh*>::const_iterator &last)
{
	this->mVB = nullptr;
	this->mIB = nullptr;
	*this = **first;
	for (auto it = first + 1; it <= last; ++it)
	{
		if (mMaterialID == (**it).mMaterialID)
		{
			AppendVertices((**it).mVertices, GMathMF(XMMatrixTranspose(GMathFM((**it).GetWorld()))));
			//mVertices.insert(mVertices.end(), (**it).mVertices.begin(), (**it).mVertices.end());
			uint old_indices_count = this->mIndices.size();
			mIndices.insert(mIndices.end(), (**it).mIndices.begin(), (**it).mIndices.end());
			for (auto it2 = mIndices.begin() + old_indices_count; it2 != mIndices.end(); ++it2)
			{
				*it2 += old_indices_count;
			}
		}
	}
	ConstructBuffers();
}
//------------------------------------------------------------------------------------
GMesh::GMesh(const GMesh& mesh)
{
	this->mVB = nullptr;
	this->mIB = nullptr;
	*this = mesh;
}
//------------------------------------------------------------------------------------
GMesh& GMesh::operator=(const GMesh &mesh)
{
	this->mDevice = mesh.mDevice;
	this->mContext = mesh.mContext;
	this->mMaterialID = mesh.mMaterialID;
	this->mName = mesh.mName;

	this->mVertices = mesh.mVertices;
	this->mIndices = mesh.mIndices;

	this->mScaling = mesh.mScaling;
	this->mRotation = mesh.mRotation;
	this->mTranslation = mesh.mTranslation;

	ReleaseCOM(mIB);
	ReleaseCOM(mVB);

	this->ConstructBuffers();

	return *this;
}
//------------------------------------------------------------------------------------
GMesh GMesh::operator+(const GMesh &mesh)
{
	if (mMaterialID == mesh.mMaterialID)
	{
		mVertices.insert(mVertices.end(), mesh.mVertices.begin(), mesh.mVertices.end());
		uint old_indices_count = this->mIndices.size();
		mIndices.insert(mIndices.end(), mesh.mIndices.begin(), mesh.mIndices.end());
		for (auto it = mIndices.begin() + old_indices_count; it != mIndices.end(); ++it)
		{
			*it += old_indices_count;
		}
		ConstructBuffers();
	}
	return *this;
}
//------------------------------------------------------------------------------------
GMesh::~GMesh()
{
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);
}
//------------------------------------------------------------------------------------
void GMesh::draw()
{
	uint strides = GVertex::GetSize();
	uint offset = 0;
	mContext->IASetVertexBuffers(0, 1, &mVB, &strides, &offset);
	mContext->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, 0);
	mContext->DrawIndexed(mIndices.size(), 0, 0);
}
//------------------------------------------------------------------------------------
void GMesh::AppendVertices(const std::vector<GVertex> &vertices, XMFLOAT4X4 world)
{
	// Reserve additional space for new vertices
	mVertices.reserve(mVertices.size() + vertices.size());
	for (const auto &vertex : vertices)
	{
		mVertices.push_back(vertex);
		mVertices.back().mPosition = GMathVF(XMVector3Transform(GMathFV(mVertices.back().mPosition), 
			GMathFM(world)));
		mVertices.back().mNormal = GMathVF(XMVector3TransformNormal(GMathFV(mVertices.back().mNormal), 
			GMathFM(world)));
		mVertices.back().mTangent = GMathVF(XMVector3TransformNormal(GMathFV(mVertices.back().mTangent), 
			GMathFM(world)));
		mVertices.back().mBinormal = GMathVF(XMVector3TransformNormal(GMathFV(mVertices.back().mBinormal), 
			GMathFM(world)));
	}
}
//------------------------------------------------------------------------------------
void GMesh::AppendVertices(const std::vector<GVertex> &vertices)
{
	// Reserve additional space for new vertices
	mVertices.reserve(mVertices.size() + vertices.size());
	for (const auto &vertex : vertices)
	{
		mVertices.push_back(vertex);
	}
}
//------------------------------------------------------------------------------------
void GMesh::ConstructBuffers()
{
	if (mVertices.size() <= 0)
		return;

	// Clear old buffers
	ReleaseCOM(mIB);
	ReleaseCOM(mVB);	

	// Feed vertex data
	void *vertices = GetVertexData();

	D3D11_BUFFER_DESC verticesDesc;
	verticesDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verticesDesc.ByteWidth = GVertex::GetSize() * GetVertexCount();
	verticesDesc.CPUAccessFlags = 0;
	verticesDesc.MiscFlags = 0;
	verticesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA verticesData;
	verticesData.pSysMem = vertices;
	verticesData.SysMemPitch = 0;
	verticesData.SysMemSlicePitch = 0;

	// Create vertex buffer
	HR(mDevice->CreateBuffer(&verticesDesc, &verticesData, &mVB));
	delete[] vertices;

	// Create index buffer
	D3D11_BUFFER_DESC indicesDesc;
	indicesDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indicesDesc.ByteWidth = sizeof(uint) * mIndices.size();
	indicesDesc.CPUAccessFlags = 0;
	indicesDesc.MiscFlags = 0;
	indicesDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indicesData;
	indicesData.pSysMem = &mIndices[0];
	indicesData.SysMemPitch = 0;
	indicesData.SysMemSlicePitch = 0;

	HR(mDevice->CreateBuffer(&indicesDesc, &indicesData, &mIB));
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::GetWorld()
{
	return GMathMF(XMMatrixTranspose(GMathFM(mScaling) * GMathFM(mRotation) 
		* GMathFM(mTranslation)));
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::RotateX(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationX(XMConvertToRadians(degrees)));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::RotateY(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationY(XMConvertToRadians(degrees)));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::RotateZ(float degrees)
{
	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationZ(XMConvertToRadians(degrees)));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::Rotate(XMFLOAT3 axis, float degrees)
{
	if (XMVector3Equal(GMathFV(axis), XMVectorZero()) ||
		degrees == 0.0f)
		return GetWorld();

	XMStoreFloat4x4(&mRotation, XMLoadFloat4x4(&mRotation) 
		* XMMatrixRotationAxis(XMLoadFloat3(&axis), XMConvertToRadians(degrees)));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::Scale(float scale_x, float scale_y, float scale_z)
{	
	XMStoreFloat4x4(&mScaling, XMLoadFloat4x4(&mScaling) 
		* XMMatrixScaling(scale_x, scale_y, scale_z));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::Scale(float delta_size)
{	
	return Scale(delta_size, delta_size, delta_size);
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::Move(XMFLOAT3 direction)
{
	XMStoreFloat4x4(&mTranslation, XMLoadFloat4x4(&mTranslation) 
		* XMMatrixTranslation(direction.x, direction.y, direction.z));
	return GetWorld();
}
//------------------------------------------------------------------------------------
XMFLOAT4X4 GMesh::Move(float dir_x, float dir_y, float dir_z)
{
	return Move(XMFLOAT3(dir_x, dir_y, dir_z));
}
//------------------------------------------------------------------------------------
void GMesh::SetMaterial(const GMaterial& material)
{
	mMaterialID = material.mName;
	for (GVertex &vertex : mVertices)
	{
		vertex.mDiffuseColor = material.GetDiffuseColor();
		vertex.mAmbientColor = material.GetAmbientColor();
	}
	// Reconstruct vertices
	ConstructBuffers();
}
//------------------------------------------------------------------------------------
void* GMesh::GetVertexData()
{
	uint verticesNumber = GetVertexCount();
	GVertexData *vertices = new GVertexData[verticesNumber];
	for (unsigned int i=0; i<verticesNumber; i++)
	{
		vertices[i].mPosition = mVertices[i].mPosition;
		vertices[i].mTextureCoordinates = mVertices[i].mTextureCoordinates;
		vertices[i].mNormal = mVertices[i].mNormal;
		vertices[i].mTangent = mVertices[i].mTangent;
		vertices[i].mBinormal = mVertices[i].mBinormal;
		vertices[i].mDiffuseColor = mVertices[i].mDiffuseColor;
		vertices[i].mAmbientColor = mVertices[i].mAmbientColor;
	}
	return static_cast<void*>(vertices);
}
//------------------------------------------------------------------------------------
void GMesh::LoadFromObj(const GObjFile& obj_file, const uint primitive_index, 
	const GMaterialsManager& materials)
{
	GVertex vertex;
	uint group_index = 0;
	uint mesh_index = 0;
	int count = primitive_index;
	for (uint i = 0; i < obj_file.mGroups.size(); ++i)
	{
		count -= obj_file.mGroups[i].mMeshes.size();
		if (count < 0)
		{
			count += obj_file.mGroups[i].mMeshes.size();
			group_index = i;
			mesh_index = count;
			i = obj_file.mGroups.size();
		}
	}
	int faces_count = obj_file.mGroups[group_index].mMeshes[mesh_index].mFaces.size();
	for (int i = 0; i < faces_count; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vertex.mPosition.x = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mVertex.x;
			vertex.mPosition.y = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mVertex.y;
			vertex.mPosition.z = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mVertex.z;

			vertex.mNormal.x = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mNormal.nx;
			vertex.mNormal.y = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mNormal.ny;
			vertex.mNormal.z = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mNormal.nz;

			vertex.mTextureCoordinates.x = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mTexCoords.u;
			vertex.mTextureCoordinates.y = obj_file.mGroups[group_index].mMeshes[mesh_index].
				mFaces[i].mVertices[j].mTexCoords.v;

			mVertices.push_back(vertex);

			if (j == 2)
			{
				CalculateTangentAndBinormal(*(mVertices.end() - 1), *(mVertices.end() - 2), 
					*(mVertices.end() - 3));
			}
		}
	}

	uint indices_count = mVertices.size();
	mIndices.resize(indices_count);
	for (uint i=0; i<indices_count; ++i)
	{
		mIndices[i] = i;
	}

	GMaterial material;
	if (obj_file.mGroups[group_index].mMeshes[mesh_index].mMaterial.mName == "")
	{
		material.d = 1.0f;
		material.Kd.r = GColor::LIGHT_YELLOW_GREEN.x;
		material.Kd.g = GColor::LIGHT_YELLOW_GREEN.y;
		material.Kd.b = GColor::LIGHT_YELLOW_GREEN.z;
		material.Ka.r = GColor::LIGHT_YELLOW_GREEN.x;
		material.Ka.g = GColor::LIGHT_YELLOW_GREEN.y;
		material.Ka.b = GColor::LIGHT_YELLOW_GREEN.z;
	}
	else
	{
		material = materials.GetMaterialByName(obj_file.mGroups[group_index].mMeshes[mesh_index].mMaterial.mName);
	}

	SetMaterial(material);	
}
//------------------------------------------------------------------------------------
void GMesh::LoadFromMeshes(const std::vector<GMesh*> &meshes)
{
	*this = *meshes[0];
	for (uint i = 1; i < meshes.size(); ++i)
	{
		if (mMaterialID == (*meshes[i]).mMaterialID)
		{
			AppendVertices((*meshes[i]).mVertices, GMathMF(XMMatrixTranspose(GMathFM((*meshes[i]).GetWorld()))));
			//mVertices.insert(mVertices.end(), (*meshes[i]).mVertices.begin(), (*meshes[i]).mVertices.end());
			uint old_indices_count = this->mIndices.size();
			mIndices.insert(mIndices.end(), (*meshes[i]).mIndices.begin(), (*meshes[i]).mIndices.end());
			for (auto it = mIndices.begin() + old_indices_count; it != mIndices.end(); ++it)
			{
				*it += old_indices_count;
			}
		}
	}
	ConstructBuffers();
}
//------------------------------------------------------------------------------------
void GMesh::CalculateTangentAndBinormal(GVertex& v1, GVertex& v2, GVertex& v3)
{
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;

	// Calculate the two vectors for this face.
	vector1[0] = v2.mPosition.x - v1.mPosition.x;
	vector1[1] = v2.mPosition.y - v1.mPosition.y;
	vector1[2] = v2.mPosition.z - v1.mPosition.z;

	vector2[0] = v3.mPosition.x - v1.mPosition.x;
	vector2[1] = v3.mPosition.y - v1.mPosition.y;
	vector2[2] = v3.mPosition.z - v1.mPosition.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = v2.mTextureCoordinates.x - v1.mTextureCoordinates.x;
	tvVector[0] = v2.mTextureCoordinates.y - v1.mTextureCoordinates.y;

	tuVector[1] = v3.mTextureCoordinates.x - v1.mTextureCoordinates.x;
	tvVector[1] = v3.mTextureCoordinates.y - v1.mTextureCoordinates.y;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	XMFLOAT3 tangent;
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	XMFLOAT3 binormal;
	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of this normal.
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));

	// Normalize the normal and then store it
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of this normal.
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));

	// Normalize the normal and then store it
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	v1.mTangent = tangent;
	v1.mBinormal = binormal;
	v2.mTangent = tangent;
	v2.mBinormal = binormal;
	v3.mTangent = tangent;
	v3.mBinormal = binormal;
}
//------------------------------------------------------------------------------------