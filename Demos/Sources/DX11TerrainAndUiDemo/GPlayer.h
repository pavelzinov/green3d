#pragma once
#include "GUtility.h"
#include "GCamera.h"

namespace Game
{

class GPlayer
{
public:
	GPlayer(void);
	void Initialize(float degrees, int screen_width, int screen_height, float near_z, float far_z);
	~GPlayer(void);

public:
	float mMovementVelocity;
	float mTurnVelocity;	
	float mTimeBufferMax; // max time to fade movement
	float mTimeBuffer[2];

	void Update(const float &dSecons);
	void OnResize(int new_width, int new_height) { mFirstPersonLook.OnResize(new_width, new_height); }

public:
	void MoveForward();
	void MoveBackward();
	void TurnLeft();
	void TurnRight();

private:
	GCamera mFirstPersonLook;
	bool mIsMovingForward;
	bool mIsMovingBackward;
	bool mIsTurningLeft;
	bool mIsTurningRight;

public:
	XMFLOAT4X4 View() { return mFirstPersonLook.View(); }
	XMFLOAT4X4 Proj() { return mFirstPersonLook.Proj(); }
	XMFLOAT4X4 Ortho() { return mFirstPersonLook.Ortho(); }
	XMFLOAT3 Position() { return mFirstPersonLook.Position(); }
	XMFLOAT3 Target() { return mFirstPersonLook.Target(); }
	XMFLOAT3 Up() { return mFirstPersonLook.Up(); }
};

}