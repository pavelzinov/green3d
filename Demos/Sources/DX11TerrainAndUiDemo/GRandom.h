#pragma once

namespace Game
{

/* 
Pseudo-random number generation class
*/
class GRandom
{
public:
	/* 
	Constructor initializes system rand() using current time	
	*/
    GRandom(void);
    ~GRandom(void) {}

public:
	/* 
	Function which generates random float

	if argument count is 0, then it returns float between 0.0 and 1.0
	if argument count is 1, then it returns float between 0.0 and argument passed
	if both arguments are set, it returns float between them
	*/
	float Gen(float end = 1.0f, float start = 0.0f);

	/* 
	Function which generates random integer

	if argument count is 0, then it returns 0 or 1
	if argument count is 1, then it returns integer between 0 and argument passed
	if both arguments are set, it returns float between them
	*/
    int GenInt(int end = 1, int start = 0);
};

} // namespace Game