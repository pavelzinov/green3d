#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GRandom.h" 
#include "GVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GTexturesManager.h"
#include "GMaterialsManager.h"
#include "GShader.h"
#include "GText.h"
#include "GLight.h"
#include "GObjFile.h"
#include "GImage.h"
#include "GTerrain.h"
#include "GPlayer.h"

using namespace Game;

// Class TerrainAndUiDemo.
class TerrainAndUiDemo : public GApplication
{
    // Public constructor and destructor
public:
    TerrainAndUiDemo(HINSTANCE hInstance);
    ~TerrainAndUiDemo(void);

public:
    void initApp(bool is_full_screen = false);

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
	GTexturesManager*						mpTexturesManager;
	GMaterialsManager*						mpMaterialsManager;

	GShader<GSHADER_BUFFER_TYPE_FONT>*		mpFontShader;
	GShader<GSHADER_BUFFER_TYPE_SIMPLE>*	mpOverlayShader;
	GShader<GSHADER_BUFFER_TYPE_SIMPLE>*	mpDepthShader;
	GShader<GSHADER_BUFFER_TYPE_SHADOWS>*	mpTexturedShader;
	GShader<GSHADER_BUFFER_TYPE_SHADOWS>*	mpTerrainShader;

private:
    GCamera	mCamera;	// main camera
	GPlayer mPlayer;
    float	mDistance;	// camera position
	GRandom	mRandom;	// random generator

	std::vector<GMesh*>	mTexturedModels;
	std::vector<GMesh*>	mColoredModels;
	GMesh*				mDebugWindow;
	GMesh*				mCursor;
	GTerrain*			mTerrain;
	
	std::vector<GText*>	mText;
	GLight				mLight;

	//XMFLOAT4 mResult;
	
};