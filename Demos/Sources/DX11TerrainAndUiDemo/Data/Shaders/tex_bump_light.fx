#include "light_helper.fx"
#include "common_header.fx"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix	World;
	matrix	View;
	matrix	Projection;
	float4	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float4	AttenuationParameters_SpotPower; // w is spot_power
	float4	Range; // x - range
	int4	LightType; // x - type: 0 - parallel, 1 - point, 2 - spotlight
	float4	CameraPosition;
}
//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);	
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.PosW = mul(vs_input.Pos.xyz, (float3x3)World);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	float4 model_color = DiffuseMap.Sample(SampleType, ps_input.TexC);
	
	float4 bump_map = NormalMap.Sample(SampleType, ps_input.TexC);
	// Expand the range of the normal value from (0, +1) to (-1, +1).
    bump_map = (bump_map * 2.0f) - 1.0f;
	
	float3 bump_normal = bump_map.z * ps_input.Normal + 
		bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
	bump_normal = normalize(bump_normal);

	SurfaceInfo surface_info = {ps_input.PosW, bump_normal, model_color, model_color};

	Light light;
	light.pos = Position.xyz;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters_SpotPower.xyz;
	light.spotPower = AttenuationParameters_SpotPower.w;
	light.range = Range.x;

	if (LightType.x == 0)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType.x == 1)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
			
	}
	else if (LightType.x == 2)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}

    return model_color;
}