// Defines:
// Object color type
#define OBJECT_COLOR_TYPE_COLORED 0
#define OBJECT_COLOR_TYPE_TEXTURED 1
#define USE_NORMAL_MAP 1
#define USE_SHADOW_MAP 1
#define USE_DIFFUSE_MAP 1
#define USE_AMBIENT_MAP 1
#define USE_LIGHT_MAP 1
// Textures:
Texture2D NormalMap : register(t0);
Texture2D ShadowMap : register(t1);
Texture2D DiffuseMap : register(t2);
Texture2D AmbientMap : register(t3);
Texture2D LightMap : register(t4);
// Sampler states:
SamplerState LinearSampler : register(s0);