//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix	World;
	matrix	View;
	matrix	Projection;
	float4	Color;
}

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float4 DiffuseColor : COLOR0;
	float4 AmbientColor : COLOR1;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(vs_input.Pos, World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));

    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
    return Color;
}