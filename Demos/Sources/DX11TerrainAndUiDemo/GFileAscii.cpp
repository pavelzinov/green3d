#include "GFileAscii.h"
using namespace Game;
//------------------------------------------------------------------------------------
bool GFileAscii::Open(const std::string filename)
{
	std::string full_name;

	// Determine if filename is not a full filename
	if (filename.find(":") == std::string::npos)
	{
		std::string home_directory = GFileAscii::CurrentDirectory();
		
		if (filename[0] == '/')
		{// determine if filename looks like "/data/text.txt"
			full_name = home_directory + filename.substr(1);
		}
		else
		{
			full_name = home_directory + filename;
		}
	}
	else
	{
		full_name = filename;
	}

	// Parse full file name, to get information about its name
	mNameInformation.mFullNameWithExtension = full_name;
	mNameInformation.mExtension = full_name.substr(full_name.find_last_of(".") + 1);
	mNameInformation.mExtensionWithDot = full_name.substr(full_name.find_last_of("."));
	int start, length;
	start = full_name.find_last_of("/") + 1;
	length = full_name.find_last_of(".") - full_name.find_last_of("/") - 1;
	mNameInformation.mShortName = full_name.substr(start, length);
	mNameInformation.mShortWithExtension = mNameInformation.mShortName + mNameInformation.mExtensionWithDot;
	mNameInformation.mHomeFolderFull = full_name.substr(0, full_name.find_last_of("/") + 1);
	start = mNameInformation.mHomeFolderFull.substr(0, mNameInformation.mHomeFolderFull.size() - 1).find_last_of("/") + 1;
	length = mNameInformation.mHomeFolderFull.find_last_of("/") - start;
	mNameInformation.mHomeFolderShort = mNameInformation.mHomeFolderFull.substr(start, length);

	// Read content
	std::fstream file;
	file.open(mNameInformation.mFullNameWithExtension, std::fstream::in);
	file.seekg(0, file.end);
	length = file.tellg();
	file.seekg(0, file.beg);
	mContent.resize(length, '\0');
	file.read(&mContent[0], length);
	file.close();

	// Parse file content
	ParseContent();

	return true;
}
//------------------------------------------------------------------------------------
std::string GFileAscii::CurrentDirectory()
{
	int length = GetCurrentDirectoryA(0, nullptr);
	std::string directory(length, '\0');
	GetCurrentDirectoryA(length, &directory[0]);	
	directory.back() = '\\';
	return directory;
}
//------------------------------------------------------------------------------------
void GFileAscii::ParseContent()
{
	std::stringstream ss(mContent);
	std::string line;
	mLines.reserve(line.size());
	while (std::getline(ss, line))
	{
		mLines.push_back(line);
	}
}
//------------------------------------------------------------------------------------
void GFileAscii::WriteLine(std::string content)
{
	mContent += content + "\n";
	mLines.push_back(content);
}
//-----------------------------------------------------------------------------------
void GFileAscii::Save(const std::string filename)
{
	std::fstream file(filename, std::fstream::trunc|std::fstream::out);
	for (std::string& line : mLines) {
		file << line << std::endl;
	}
}
//-----------------------------------------------------------------------------------