//#pragma once
//#include "GMesh.h"
//
//namespace Game
//{
//////////////////////////////////////////////////////////
//// Handling construction of plane mesh, it's scaling,
//// movement, rotation and drawing
//////////////////////////////////////////////////////////
//template <class T>
//class GPlane : public GMesh
//{
//public:
//	GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, uint width_segments,
//		float height, uint height_segments);
//	GPlane(GPlane& plane);
//	~GPlane(void) {}
//
//protected:
//	void GenerateVertices();
//	void GenerateNormals();
//
//protected:
//	float	mWidth;
//	float	mHeight;
//	uint	mWidthSegments;
//	uint	mHeightSegments;
//};
//
//template <class T>
//GPlane<T>::GPlane(ID3D11Device *device, ID3D11DeviceContext *context, float width, uint width_segments,
//		float height, uint height_segments) : GMesh(device, context)
//{
//	mWidth = width;
//	mHeight = height;
//	mWidthSegments = width_segments;
//	mHeightSegments = height_segments;
//
//	GenerateVertices();
//
//	// Create indices
//	uint faces_count = width_segments*height_segments*2;
//    // 2 triangles in every quad
//	uint k = 0; // quad number
//	int quad_top_left_point_count = GetVertexCount() - width_segments - 1;
//	mIndices.resize(faces_count*3);
//	for (int i=0; i<quad_top_left_point_count; ++i)
//	{
//		// adding quad to the index buffer            
//        if ((i+1) % (width_segments+1) != 0)
//        {// if point is not on the plane's right edge
//			// upper triangle
//			mIndices[k]		= i;
//			mIndices[k+1]	= i + 1;
//			mIndices[k+2]	= i + width_segments + 1;
//			// lower triangle
//			mIndices[k+3]	= mIndices[k+2];
//			mIndices[k+4]	= mIndices[k+1];
//			mIndices[k+5]	= mIndices[k+2] + 1;
//            k += 6;
//        } // end if
//	} // end for
//
//	GenerateNormals();
//	ConstructBuffers();
//}
//
//template <class T>
//GPlane<T>::GPlane(GPlane<T>& plane) : GMesh(dynamic_cast<GMesh&>(plane))
//{
//	mWidth = plane.mWidth;
//	mHeight = plane.mHeight;
//	mWidthSegments = plane.mWidthSegments;
//	mHeightSegments = plane.mHeightSegments;
//}
//
//template<class T>
//void GPlane<T>::GenerateNormals()
//{
//	if (mIndices.size() == 0 || mVertices.size() == 0)
//		return;
//
//	XMVECTOR vec1 = GMathFV(mVertices[mIndices[0]].mPosition) - 
//		GMathFV(mVertices[mIndices[1]].mPosition);
//	XMVECTOR vec2 = GMathFV(mVertices[mIndices[0]].mPosition) - 
//		GMathFV(mVertices[mIndices[2]].mPosition);
//	// determine normal for first triangle
//	XMFLOAT3 normal = GMathVF(XMVector3Normalize(XMVector3Cross(vec1, vec2)));
//	// all vertices will have the normal equal to normal vector of first triangle
//	for (auto it=mVertices.begin(); it != mVertices.end(); it++)
//	{
//		(*it).mNormal = normal;
//	}
//}
//
//}