#include "GImage.h"
using namespace Game;
//------------------------------------------------------------------------------------
bool GImage::LoadBitmap(std::string filename)
{
	mData.clear();

	// Initialize GDI+.
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	
	Gdiplus::Bitmap *bitmap = Gdiplus::Bitmap::FromFile(GString::ToUnicode(filename).c_str());
	if (bitmap == nullptr)
		return false;

	mWidth = bitmap->GetWidth();
	mHeight = bitmap->GetHeight();
	mData.resize(mWidth);
	Gdiplus::Color pixel_color(0, 0, 0, 0);
	Pixel pixel_to_store;
	for (uint i = 0; i < mWidth; ++i)
	{
		for (uint j = 0; j < mHeight; ++j)
		{
			bitmap->GetPixel(i, j, &pixel_color);
			pixel_to_store.r = static_cast<float>(pixel_color.GetRed()) / 255.0f;
			pixel_to_store.g = static_cast<float>(pixel_color.GetGreen()) / 255.0f;
			pixel_to_store.b = static_cast<float>(pixel_color.GetBlue()) / 255.0f;
			pixel_to_store.a = static_cast<float>(pixel_color.GetAlpha()) / 255.0f;
			mData[i].push_back(pixel_to_store);
		}
	}

	delete bitmap;
	Gdiplus::GdiplusShutdown(gdiplusToken);

	return true;
}
//------------------------------------------------------------------------------------