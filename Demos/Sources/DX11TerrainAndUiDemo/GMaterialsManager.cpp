#include "GMaterialsManager.h"
using namespace Game;
//------------------------------------------------------------------------------------
void GMaterialsManager::AddMaterial(const GMaterial &material)
{
	mMaterials.insert(std::pair<std::string, GMaterial>(material.mName, material));
}
//-----------------------------------------------------------------------------------
bool GMaterialsManager::LoadAddMaterialsFromFile(const std::string &filename, 
	const GTexturesManager &textures)
{
	// Load materials from .mtl file, by means of GMtlFile class object.
	GMtlFile mtl_file;
	mtl_file.Load(filename);
	auto materials = mtl_file.GetMaterials();
	// Clear empty materials, if any.
	materials.erase(std::remove_if(materials.begin(), materials.end(), [](GMaterial &mat)->bool
	{
		return mat.mName == "";
	}), materials.end());
	// Assign concrete texture from GTexturesManager
	// to every material's texture field.
	for (GMaterial& material : materials)
	{
		material.map_Ka = textures.GetTextureNameByFileName(material.map_Ka);
		material.map_Kd = textures.GetTextureNameByFileName(material.map_Kd);
		material.map_Ks = textures.GetTextureNameByFileName(material.map_Ks);
		material.map_Ns = textures.GetTextureNameByFileName(material.map_Ns);
		material.map_d = textures.GetTextureNameByFileName(material.map_d);
		material.map_bump = textures.GetTextureNameByFileName(material.map_bump);
		material.disp = textures.GetTextureNameByFileName(material.disp);
		material.decal = textures.GetTextureNameByFileName(material.decal);
		mMaterials.insert(std::pair<std::string, GMaterial>(material.mName, material));
	}
	return true;
}
//-----------------------------------------------------------------------------------
bool GMaterialsManager::LoadAddMaterialsFromFile(const std::string &filename)
{
	GMtlFile mtl_file;
	mtl_file.Load(filename);
	auto materials = mtl_file.GetMaterials();
	// Clear empty materials
	materials.erase(std::remove_if(materials.begin(), materials.end(), [](GMaterial &mat)->bool
	{
		return mat.mName == "";
	}), materials.end());
	// Append material from file to materials.
	// Material can not contain textures, so clear them 
	// before storing material in vector.
	for (GMaterial& material : materials)
	{
		// Clear all texture fields
		material.map_Ka = "";
		material.map_Kd = "";
		material.map_Ks = "";
		material.map_Ns = "";
		material.map_d = "";
		material.map_bump = "";
		material.disp = "";
		material.decal = "";
		mMaterials.insert(std::pair<std::string, GMaterial>(material.mName, material));
	}
	return true;
}
//-----------------------------------------------------------------------------------
bool GMaterialsManager::LoadAddMaterialsFromObjFile(const GObjFile &obj_file, 
	const GTexturesManager &textures)
{
	auto mtl_files = obj_file.mMaterialLibraries;
	for (const auto &mtl_file : mtl_files)
	{
		LoadAddMaterialsFromFile(mtl_file, textures);
	}

	return true;
}
//-----------------------------------------------------------------------------------