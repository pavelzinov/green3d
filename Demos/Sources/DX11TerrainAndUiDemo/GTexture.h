#pragma once
#include "GUtility.h"
#include "DDSTextureLoader.h"
//------------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// Stores View and Projection matrices used by shaders
	// to translate 3D world into 2D screen surface
	// Camera can be moved
	////////////////////////////////////////////////////////
	class GTexture
	{
	public:
		// Construct material from .dds file. if file name is empty, behaviour is undefined.
		GTexture(ID3D11Device *device, const std::string& texture_name, 
			const std::string& texture_file_name);
		// Copy constructor
		GTexture(const GTexture& texture);
		// Assignment constructor
		GTexture& operator=(const GTexture& texture);
		// Destructor
		~GTexture(void);
		// Load texture from .dds file
		HRESULT LoadFromFile(const std::string& textureFileName);
		// Get texture shader resource view
		ID3D11ShaderResourceView* GetView() const { return mpView; }
		// Get texture's file name. It can be a full path, or relative
		const std::string& GetFileName() const { return mFileName; }

	private:
		std::string					mFileName;
		ID3D11Device*				mpDevice;
		ID3D11Resource*				mpResource;
		ID3D11ShaderResourceView*   mpView;
	}; // class
} // namespace
//------------------------------------------------------------------------------------