#pragma once
#include <iostream>
#include <vector>
#include <sstream>
typedef unsigned int uint;
//--------------------------------------------------------------------------------------
namespace Game
{
	class GString
	{
	public:
		GString(void);
		~GString(void);

		static std::string ToAscii(std::wstring &wstr);
		static std::wstring ToUnicode(std::string &str);
		static std::vector<std::string>& Split(const std::string &str, const char delimiter,
			std::vector<std::string> &result);	
		static std::vector<std::string> Split(const std::string &str, const char delimiter);
		static std::vector<std::string>& Split(const std::string &str, const std::string delimiters,
			std::vector<std::string> &result);
		static std::vector<std::string> Split(const std::string &str, const std::string delimiters);
		static int ToInt(const std::string& str);
		static float ToFloat(const std::string& str);
	}; // class
} // namespace
//--------------------------------------------------------------------------------------