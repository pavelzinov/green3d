#include "GTexturesManager.h"
using namespace Game;
//------------------------------------------------------------------------------------
GTexturesManager::GTexturesManager(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mpDevice = device;
	mpContext = context;
	
	mpWindowRenderTargetView = nullptr;
	mpWindowDepthStencilView = nullptr;
	mpRenderTagetView = nullptr;
	mpDepthStencilBuffer = nullptr;
	mpDepthStencilView = nullptr;

	mShaderTexturesSemantics.push_back(std::pair<std::string, std::string>("NormalMap", ""));
	mShaderTexturesSemantics.push_back(std::pair<std::string, std::string>("ShadowMap", ""));
	mShaderTexturesSemantics.push_back(std::pair<std::string, std::string>("DiffuseMap", ""));
	mShaderTexturesSemantics.push_back(std::pair<std::string, std::string>("AmbientMap", ""));
	mShaderTexturesSemantics.push_back(std::pair<std::string, std::string>("LightMap", ""));

	GenerateSamplers();
	SetSamplers();
}
//------------------------------------------------------------------------------------
GTexturesManager::~GTexturesManager(void)
{
	ReleaseCOM(mpDepthStencilView);
	ReleaseCOM(mpDepthStencilBuffer);
	ReleaseCOM(mpRenderTagetView);
	// Release samplers
	std::for_each(mSamplers.begin(), mSamplers.end(), 
		[](std::pair<std::string, ID3D11SamplerState*> item) { ReleaseCOM(item.second); });
}
//------------------------------------------------------------------------------------
std::string GTexturesManager::AddTexture(const std::string &texture_name, GTexture& texture)
{
	auto names = GetTexturesNames();	
	auto found = std::find_if(mTextures.begin(), mTextures.end(), 
		[&](std::pair<std::string, GTexture> texture_pair)->bool
	{
		return texture_pair.second.GetFileName() == texture.GetFileName();	
	});
	if (found == mTextures.end())
	{// If .dds file is not in the dictionary, check it's name
		// If the name already exists, skip
		if (std::find(names.begin(), names.end(), texture_name) == names.end())
		{
			mTextures.insert(std::pair<std::string, GTexture>(texture_name, texture));
			return texture_name;
		}
		else
		{
			mTextures.insert(std::pair<std::string, GTexture>(texture_name + "_auto", texture));
			return texture_name + "_auto";
		}
	}
	else
	{// If .dds file already in dictionary, don't append another one
		return (*found).first;
	}
	
	return texture_name;
}
//------------------------------------------------------------------------------------
std::string GTexturesManager::AddTexture(const std::string &texture_name, const std::string &texture_file)
{
	GTexture texture(mpDevice, texture_name, texture_file);
	return AddTexture(texture_name, texture);
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromFile(const std::string &filename)
{
	GFileAscii file;
	file.Open(filename);
	auto lines = file.GetLines();
	// remove all empty strings
	lines.erase(std::remove_if(lines.begin(), lines.end(), [](std::string &str)->bool
	{
		return str == "" || str == "\n" || str == "\t";
	}), lines.end());
	std::string texture_name;
	std::string texture_file;
	for (uint i = 0; i < lines.size(); ++i)
	{
		if (lines[i][0] != '/' && lines[i][1] != '/' && lines[i] != "\n")
		{
			if (lines[i].find(".") == std::string::npos)
			{
				texture_name = lines[i];
			}
			else
			{
				texture_file = lines[i];
				AddTexture(texture_name, texture_file);
			}
		}		
	}
	return true;
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromMtlFile(const std::string &filename)
{
	GMtlFile file;
	file.Load(filename);
	auto materials = file.GetMaterials();
	for (GMaterial& material : materials)
	{
		if (material.map_Ka != "") {
			AddTexture(material.mName + ".map_Ka", material.map_Ka);
		}
		if (material.map_Kd != "") {
			AddTexture(material.mName + ".map_Kd", material.map_Kd);
		}
		if (material.map_Ks != "") {
			AddTexture(material.mName + ".map_Ka", material.map_Ks);
		}
		if (material.map_Ns != "") {
			AddTexture(material.mName + ".map_Ns", material.map_Ns);
		}
		if (material.map_d != "") {
			AddTexture(material.mName + ".map_d", material.map_d);
		}
		if (material.map_bump != "") {
			AddTexture(material.mName + ".map_bump", material.map_bump);
		}
		if (material.disp != "") {
			AddTexture(material.mName + ".map_disp", material.disp);
		}
		if (material.decal != "") {
			AddTexture(material.mName + ".map_decal", material.decal);
		}
	}
	return true;
}
//------------------------------------------------------------------------------------
bool GTexturesManager::LoadAddTexturesFromObjFile(const GObjFile &obj_file)
{
	auto mtl_files = obj_file.mMaterialLibraries;
	for (const auto &mtl_file : mtl_files)
	{
		LoadAddTexturesFromMtlFile(mtl_file);
	}

	return true;
}
//------------------------------------------------------------------------------------
GTexture& GTexturesManager::GetTexture(const std::string &texture_name)
{
	return mTextures.at(texture_name);
}
//------------------------------------------------------------------------------------
GRenderTargetTexture& GTexturesManager::GetRenderedTexture(const std::string &texture_name)
{
	return mRenderTargetTextures.at(texture_name);
}
//------------------------------------------------------------------------------------
const std::string GTexturesManager::GetTextureNameByFileName(const std::string &file_name) const
{
	for (auto &texture : mTextures)
	{
		if (texture.second.GetFileName() == file_name &&
			mRenderTargetTextures.find(texture.first) == mRenderTargetTextures.end())
		{
			return texture.first;
		}
	}
	return "";
}
//------------------------------------------------------------------------------------
std::vector<std::string> GTexturesManager::GetTexturesNames()
{
	std::vector<std::string> names;

	for (auto it = mTextures.begin(); it != mTextures.end(); it++)
	{
		names.push_back(it->first);
	}

	return names;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetTexture(const std::string &semantic_name, const std::string &texture_name)
{
	// Find slot number for texture shader resource
	int slot_number = 0;
	for (uint i = 0; i < mShaderTexturesSemantics.size(); ++i)
	{
		if (mShaderTexturesSemantics[i].first == semantic_name)
		{
			slot_number = i;
			i = mShaderTexturesSemantics.size();
		}
	}
	if (slot_number == mShaderTexturesSemantics.size() - 1)
		return;
	if (mShaderTexturesSemantics[slot_number].second == texture_name)
		return;

	// Set corresponding shader slot to use texture with desired `texture_name`
	ID3D11ShaderResourceView * resources[1];
	resources[0] = nullptr;
	// Unbind resource
	//mpContext->PSSetShaderResources(slot_number, 1, resources);
	// Find legit texture to bind to shader slot
	if (mRenderTargetTextures.find(texture_name) != mRenderTargetTextures.end())
	{// search for texture view in render textures first
		resources[0] = GetRenderedTexture(texture_name).GetView();
	}
	else if (mTextures.find(texture_name) != mTextures.end())
	{
		resources[0] = GetTexture(texture_name).GetView();
	}
	else
	{
		return;
	}
	
	mpContext->PSSetShaderResources(slot_number, 1, resources);
	mShaderTexturesSemantics[slot_number].second = texture_name;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetTextures(const std::vector<std::pair<std::string, std::string>> &shader_slot_pairs)
{
	for (auto &shader_slot_pair : shader_slot_pairs)
	{
		SetTexture(shader_slot_pair.first, shader_slot_pair.second);
	}
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetAllCurrentTextures()
{
	// Unbind all textures from pixel shader state
	std::vector<ID3D11ShaderResourceView*> srvs(mShaderTexturesSemantics.size(), nullptr);
	mpContext->PSSetShaderResources(0, mShaderTexturesSemantics.size(), srvs.data());
	srvs.clear();
	for (int i = 0; i < mShaderTexturesSemantics.size(); ++i)
	{
		if (mRenderTargetTextures.find(mShaderTexturesSemantics[i].second) != mRenderTargetTextures.end())
		{// search for texture view in render textures first
			srvs.push_back(GetRenderedTexture(mShaderTexturesSemantics[i].second).GetView());
		}
		else if (mTextures.find(mShaderTexturesSemantics[i].second) != mTextures.end())
		{
			srvs.push_back(GetTexture(mShaderTexturesSemantics[i].second).GetView());
		}
		else
		{
			srvs.push_back(nullptr);
		}
	}
	mpContext->PSSetShaderResources(0, mShaderTexturesSemantics.size(), srvs.data());
}
//------------------------------------------------------------------------------------
HRESULT GTexturesManager::AddRenderedTexture(const std::string &texture_name, 
	ID3D11RenderTargetView* window_render_target_view, ID3D11DepthStencilView* depth_stencil_view, 
	const uint texture_width, const uint texture_height, 
	const uint sample_count, const uint sample_quality)
{
	mSampleCount = sample_count;
	mSampleQuality = sample_quality;

	auto names = GetTexturesNames();
	if (std::find(names.begin(), names.end(), texture_name) != names.end())
		return E_FAIL;

	GRenderTargetTexture texture(mpDevice, texture_width, texture_height); 
	mRenderTargetTextures.insert(std::pair<std::string, GRenderTargetTexture>(texture_name, texture));

	OnResize(texture_name, window_render_target_view, depth_stencil_view, texture_width, 
		texture_height);

	return S_OK;
}
//------------------------------------------------------------------------------------
void GTexturesManager::RenderToTexture(XMFLOAT4& clear_color)
{
	// Unbind all textures from pixel shader state
	//std::vector<ID3D11ShaderResourceView*> srvs(mShaderTexturesSemantics.size(), nullptr);
	//mpContext->PSSetShaderResources(0, mShaderTexturesSemantics.size(), srvs.data());
	// Unbind shadow map resource
	ID3D11ShaderResourceView* resources[1];
	resources[0] = nullptr;
	// Find slot number for texture shader resource
	int slot_number = 0;
	for (uint i = 0; i < mShaderTexturesSemantics.size(); ++i)
	{
		if (mShaderTexturesSemantics[i].first == "ShadowMap")
		{
			slot_number = i;
			i = mShaderTexturesSemantics.size();
		}
	}
	mpContext->PSSetShaderResources(slot_number, 1, resources);
	// Set texture as render target
	mpContext->OMSetRenderTargets(1, &mpRenderTagetView, mpDepthStencilView);
	mpContext->ClearRenderTargetView(mpRenderTagetView, reinterpret_cast<float*>(&clear_color));
	mpContext->ClearDepthStencilView(mpDepthStencilView, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);
}
//------------------------------------------------------------------------------------
void GTexturesManager::RenderToScreen(const uint screen_width, const uint screen_height)
{
	// TODO: REDESIGN!

	mpContext->OMSetRenderTargets(1, &mpWindowRenderTargetView, mpWindowDepthStencilView);

	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>(screen_width);
	vp.Height = static_cast<float>(screen_height);
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	
	mpContext->RSSetViewports(1, &vp);
	// Restore shadow map resource
	ID3D11ShaderResourceView* resources[1];
	// Find slot number for texture shader resource
	int slot_number = 0;
	for (uint i = 0; i < mShaderTexturesSemantics.size(); ++i)
	{
		if (mShaderTexturesSemantics[i].first == "ShadowMap")
		{
			slot_number = i;
			i = mShaderTexturesSemantics.size();
		}
	}
	if (mShaderTexturesSemantics[slot_number].second != "")
	{
		resources[0] = GetRenderedTexture(mShaderTexturesSemantics[slot_number].second).GetView();
		mpContext->PSSetShaderResources(slot_number, 1, resources);
	}
}
//------------------------------------------------------------------------------------
void GTexturesManager::OnResize(const std::string &texture_name, 
	ID3D11RenderTargetView* window_render_target_view, ID3D11DepthStencilView* depth_stencil_view, 
	const uint texture_width, const uint texture_height)
{
	mpWindowRenderTargetView = window_render_target_view;
	mpWindowDepthStencilView = depth_stencil_view;

	ReleaseCOM(mpDepthStencilView);
	ReleaseCOM(mpDepthStencilBuffer);	
	ReleaseCOM(mpRenderTagetView);

	// Create the render target texture.
	this->GetRenderedTexture(texture_name).OnResize(texture_width, texture_height);
	ID3D11Texture2D *texture = this->GetRenderedTexture(texture_name).GetTexture2D();

	// Setup the description of the render target view.
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	// Create the render target view.
	HR(mpDevice->CreateRenderTargetView(texture, 
		&renderTargetViewDesc, &mpRenderTagetView));

	// Create the depth/stencil buffer and view.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc; // depth/stencil buffer is just a 2D texture,
	ZeroMemory(&depthStencilBufferDesc, sizeof(depthStencilBufferDesc));
	depthStencilBufferDesc.ArraySize			= 1;
	depthStencilBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags		= 0;
	depthStencilBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilBufferDesc.Width				= texture_width;
	depthStencilBufferDesc.Height				= texture_height;
	depthStencilBufferDesc.MipLevels			= 1;
	depthStencilBufferDesc.MiscFlags			= 0;
	depthStencilBufferDesc.SampleDesc.Count		= mSampleCount;
	depthStencilBufferDesc.SampleDesc.Quality	= mSampleQuality;
	depthStencilBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	HR(mpDevice->CreateTexture2D(&depthStencilBufferDesc, 0, &mpDepthStencilBuffer));
	HR(mpDevice->CreateDepthStencilView(mpDepthStencilBuffer, 0, &mpDepthStencilView));
}
//------------------------------------------------------------------------------------
void GTexturesManager::GenerateSamplers()
{
	// Generate default sampler state
	ID3D11SamplerState* default_sampler_state;
	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HR(mpDevice->CreateSamplerState(&samplerDesc, &default_sampler_state));

	mSamplers.insert(std::pair<std::string, ID3D11SamplerState*>("Default", default_sampler_state));

	default_sampler_state = 0;
}
//------------------------------------------------------------------------------------
void GTexturesManager::SetSamplers()
{
	ID3D11SamplerState * samplers[1];
	samplers[0] = mSamplers["Default"]; // TODO: redo this to accept array of sampler names
	mpContext->PSSetSamplers(0, ARRAYSIZE(samplers), samplers);
}
//------------------------------------------------------------------------------------
bool GTexturesManager::AddSampler(const std::string &sampler_name, D3D11_SAMPLER_DESC &sampler_description)
{
	ID3D11SamplerState* temp_sampler_state;
	HR(mpDevice->CreateSamplerState(&sampler_description, &temp_sampler_state));
	auto result = mSamplers.insert(std::pair<string, ID3D11SamplerState*>(sampler_name, temp_sampler_state));
	// return success or fail of insertion
	return result.second;
}