#pragma once
#include "GUtility.h"
#include "GMaterial.h"
#include "GObjFile.h"
#include "GVertex.h"
#include "GMaterialsManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// Stores information about model, represented by mesh. 
	// It contains all vertices and indices of model, also
	// handles all moving, scaling and rotating functions
	// for mesh. THis class stores the mesh's material. Additionally, 
	// this class is responsible for drawing the mesh on the screen.
	////////////////////////////////////////////////////////
	class GMesh
	{
	public:
		// Construct mesh with null buffers, and obj_file object, containing geometry and material
		// Since single OBJ file can contain lots of meshes, user have to specify primitive index
		// which starts from 0.
		GMesh(const std::string& mesh_name, const GObjFile& obj_file, const uint primitive_index, 
			const GMaterialsManager& materials, ID3D11Device *device, ID3D11DeviceContext *context);
		// Load from array of meshes with the same material as first mesh in a sequence.
		GMesh(const std::vector<GMesh*> &meshes);
		// Load from array of meshes with the same material as first mesh in a sequence.
		GMesh(const std::vector<GMesh*>::const_iterator &first, 
			const std::vector<GMesh*>::const_iterator &last);
		// Construct mesh from external mesh.
		GMesh(const GMesh &mesh);
		// Assign mesh to current one.
		GMesh& operator=(const GMesh &mesh);
		// Attach vertices if material is the same.
		GMesh operator+(const GMesh &mesh);
		virtual ~GMesh(void);
		// Load mesh from OBJ file parser.
		void LoadFromObj(const GObjFile& obj_file, const uint primitive_index, 
			const GMaterialsManager& materials);
		// Load from array of meshes with the same material as first mesh in a sequence.
		void LoadFromMeshes(const std::vector<GMesh*> &meshes);
		// Assign material.
		void SetMaterial(const GMaterial& material);
		// Retrieve mesh's current material.
		const std::string& GetMaterialID() const { return mMaterialID; }
		// Get mesh's name.
		const std::string& GetName() const { return mName; }
		// Draw mesh by single draw call.
		void draw();
		// Returns mesh's world matrix.
		XMFLOAT4X4 GetWorld();
		// Rotate mesh around X axis. Angle must be in degrees.
		XMFLOAT4X4 RotateX(float degrees);
		// Rotate mesh around Y axis. Angle must be in degrees.
		XMFLOAT4X4 RotateY(float degrees);
		// Rotate mesh around Z axis. Angle must be in degrees.
		XMFLOAT4X4 RotateZ(float degrees);
		// Rotate mesh around given axis. Angle must be in degrees.
		XMFLOAT4X4 Rotate(XMFLOAT3 axis, float degrees);
		// Scale mesh coordinates along each axis.
		XMFLOAT4X4 Scale(float dx, float dy, float dz);
		// Scale mesh coordinates along each axis by the same value.
		XMFLOAT4X4 Scale(float delta_size);
		// Move mesh by vector.
		XMFLOAT4X4 Move(XMFLOAT3 direction);
		// Move mesh by individual values along each axis.
		XMFLOAT4X4 Move(float dir_x, float dir_y, float dir_z);

	protected:
		// Append vertices, transformed by matrix `world`.
		// This action is not reconstructing buffers
		void AppendVertices(const std::vector<GVertex> &vertices, XMFLOAT4X4 world);
		// Append new vertices. This action is not reconstructing buffers.
		void AppendVertices(const std::vector<GVertex> &vertices);
		// (Re)constructs vertex and index buffers
		virtual void ConstructBuffers();
		// Returns the number of vertices that mesh consists of
		uint GetVertexCount() const { return mVertices.size(); }
		// Returns pointer to dynamic array of vertex data. Remember to call delete[] on 
		// its result once you have finished working with it.
		void* GetVertexData();
		// Parse .OBJ file data line by line.
		void ParseObjData(vector<std::string>& file_lines);
		// Calculate tangent and binormal for triangle, 
		// composed by v1, v2 and v3 vertices, passed in clockwise order.
		void CalculateTangentAndBinormal(GVertex& v1, GVertex& v2, GVertex& v3);

		std::string				mName;			// Mesh's unique name
		std::string				mMaterialID;	// Mesh's material name
		std::vector<GVertex>	mVertices;		// Vertices array
		std::vector<uint>		mIndices;		// Indices array

		XMFLOAT4X4				mScaling;		// Scaling matrix
		XMFLOAT4X4				mTranslation;	// Translation matrix
		XMFLOAT4X4				mRotation;		// Rotation matrix

		ID3D11Device			*mDevice;		// DirectX11 device
		ID3D11DeviceContext		*mContext;		// DirectX11 device context
		ID3D11Buffer			*mVB;			// Vertex buffer
		ID3D11Buffer			*mIB;			// Index buffer
	}; // class
} // namespace
//------------------------------------------------------------------------------------