#include "TerrainAndUiDemo.h"
#include "DDSTextureLoader.h"
//------------------------------------------------------------------------------------
TerrainAndUiDemo::TerrainAndUiDemo(HINSTANCE hInstance) : GApplication(hInstance)
{
    // init background color
    GApplication::mClearColor = GColor::BLACK;

    mDistance = 100.0f;

	//mResult = XMFLOAT4(0.0f, 30.0f, 0.0f, 1.0f);

	mPlayer.Initialize(60.0f, mClientWidth, mClientHeight, 0.01f, 1000.0f);

	mCamera.Position(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.Target(XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera.InitProjMatrix(GMath::PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 200.0f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 200.0f, 1000.0f);
}
//------------------------------------------------------------------------------------
void TerrainAndUiDemo::initApp(bool is_full_screen)
{ 
	GApplication::initApp(is_full_screen);
	SetUPS(100);

	// Load shaders
	mpFontShader = new GShader<GSHADER_BUFFER_TYPE_FONT>(md3dDevice, md3dContext);
	mpFontShader->LoadShader("Data/Shaders/font.fx");
	mpOverlayShader = new GShader<GSHADER_BUFFER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpOverlayShader->LoadShader("Data/Shaders/overlay.fx");
	mpDepthShader = new GShader<GSHADER_BUFFER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpDepthShader->LoadShader("Data/Shaders/depth.fx");
	mpTexturedShader = new GShader<GSHADER_BUFFER_TYPE_SHADOWS>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("Data/Shaders/tex_bump_light_shadow.fx");
	mpTerrainShader = new GShader<GSHADER_BUFFER_TYPE_SHADOWS>(md3dDevice, md3dContext);
	mpTerrainShader->LoadShader("Data/Shaders/terrain_shadow.fx");

	// Load textures
	mpTexturesManager = new GTexturesManager(md3dDevice, md3dContext);
	mpTexturesManager->LoadAddTexturesFromFile("Data/Textures/scene_textures.textures");
	mpTexturesManager->LoadAddTexturesFromMtlFile("Data/Materials/scene_materials.mtl");
	mpTexturesManager->AddRenderedTexture("RenderedTexture0", mpWindowRenderTargetView, mWindowDepthStencilView, 
		mClientWidth, mClientHeight, mSampleCount, mSampleQuality);

	// Load materials
	mpMaterialsManager = new GMaterialsManager();
	mpMaterialsManager->LoadAddMaterialsFromFile("Data/Materials/scene_materials.mtl", *mpTexturesManager);
	GMaterial material;
	material.mName = "RenderedTexture0";
	material.map_Kd = "RenderedTexture0";
	material.map_Ka = "RenderedTexture0";
	// add material, where diffuse and ambient maps are rendered texture 0
	mpMaterialsManager->AddMaterial(material);

	// Load light
	mLight.mType = GLIGHT_TYPE_PARALLEL;
	mLight.SetDirection(XMFLOAT3(1.0f, 0.0f, 0.0f));
	mLight.SetPos(0.0f, 1000.0f, 0.0f);
	mLight.mDiffuseColor = GColor::WHITE;
	mLight.mAmbientColor = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
	mLight.mSpecularColor = GColor::WHITE;
	mLight.mAttenuationParameters = XMFLOAT3(0.0f, 0.01f, 0.0f);
	mLight.mSpotPower = 1.0f;
	mLight.mRange = 100.0f;

	// Load text
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		"Data/Fonts/courier_new_12_dark_green.font", 
		""));
	mText.back()->Position(XMINT2(4, 4));

	//// Load models
	GObjFile obj_file;
	//obj_file.Load("Data/Models/cube_l40.obj");
	//mColoredModels.push_back(new GMesh("ColoredCube0", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext));

	//obj_file.Load("Data/Models/sphere_textured.obj");
	//mColoredModels.push_back(new GMesh("ColoredSphere0", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext));
	//mColoredModels.back()->Move(0.0f, 100.0f, 0.0f);
	//mColoredModels.back()->Scale(2.0f);

	//// Load light's representation
	//obj_file.Load("Data/Models/arrow.obj");
	//mColoredModels.push_back(new GMesh("LightDirection", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext));
	//mColoredModels.push_back(new GMesh("LightDirection", obj_file, 1, *mpMaterialsManager, md3dDevice, md3dContext));

	obj_file.Load("Data/Models/multitextured.obj");
	mpTexturesManager->LoadAddTexturesFromObjFile(obj_file);
	mpMaterialsManager->LoadAddMaterialsFromObjFile(obj_file, *mpTexturesManager);
	int primitive_index = -1;
	for (uint i = 0; i < obj_file.mGroups.size(); ++i)
	{
		for (uint j = 0; j < obj_file.mGroups[i].mMeshes.size(); ++j)
		{
			// Calculate primitive index for GMesh constructor
			primitive_index += 1;
			// Check if meshes has textured material or not
			if (mpMaterialsManager->GetMaterialByName(obj_file.mGroups[i].mMeshes[j].
				mMaterial.mName).UsesTextureMaps())
			{
				mTexturedModels.push_back(new GMesh("Castle", obj_file, primitive_index, 
					*mpMaterialsManager, md3dDevice, md3dContext));
				mTexturedModels.back()->Scale(2.0f);
			}
			else
			{
				mColoredModels.push_back(new GMesh("Castle", obj_file, primitive_index, 
					*mpMaterialsManager, md3dDevice, md3dContext));
				mColoredModels.back()->Scale(2.0f);
			}
		}
	}

	std::sort(mColoredModels.begin(), mColoredModels.end(), [](GMesh *mesh1, GMesh *mesh2)->bool
	{
		return mesh1->GetMaterialID() < mesh2->GetMaterialID();
	});

	std::sort(mTexturedModels.begin(), mTexturedModels.end(), [](GMesh *mesh1, GMesh *mesh2)->bool
	{
		return mesh1->GetMaterialID() < mesh2->GetMaterialID();
	});

	std::vector<GMesh*> meshes;
	std::vector<GMesh*> result_meshes;
	std::string current_material_name = mColoredModels[0]->GetMaterialID();
	for (const auto & mesh : mColoredModels)
	{
		if (mesh->GetMaterialID() == current_material_name)
		{
			meshes.push_back(mesh);
		}
		else
		{
			if (meshes.size() > 0)
			{
				result_meshes.push_back(new GMesh(meshes));
			}
			meshes.clear();
			current_material_name = mesh->GetMaterialID();
		}
	}
	result_meshes.push_back(new GMesh(meshes));
	meshes.clear();

	std::for_each(mColoredModels.begin(), mColoredModels.end(), 
		[](GMesh *item) { delete item; });
	mColoredModels.clear();

	mColoredModels = result_meshes;
	result_meshes.clear();

	current_material_name = mTexturedModels[0]->GetMaterialID();
	for (const auto & mesh : mTexturedModels)
	{
		if (mesh->GetMaterialID() == current_material_name)
		{
			meshes.push_back(mesh);
		}
		else
		{
			if (meshes.size() > 0)
			{
				result_meshes.push_back(new GMesh(meshes));				
			}
			meshes.clear();
			current_material_name = mesh->GetMaterialID();
		}
	}
	result_meshes.push_back(new GMesh(meshes));
	meshes.clear();

	std::for_each(mTexturedModels.begin(), mTexturedModels.end(), 
		[](GMesh *item) { delete item; });
	mTexturedModels.clear();

	mTexturedModels = result_meshes;
	result_meshes.clear();

	//obj_file.Load("Data/Models/cube_l40.obj");
	//mTexturedModels.push_back(new GMesh("TexturedCube0", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext));
	//mTexturedModels.back()->SetMaterial(mpMaterialsManager->GetMaterialByName("Brick"));
	//mTexturedModels.back()->Move(100.0f, 0.0f, -100.0f);

	//obj_file.Load("Data/Models/sphere_textured.obj");
	//mTexturedModels.push_back(new GMesh("TexturedSphere0", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext));
	//mTexturedModels.back()->SetMaterial(mpMaterialsManager->GetMaterialByName("Water"));
	//mTexturedModels.back()->Scale(2.0f);
	//mTexturedModels.back()->Move(100.0f, 0.0f, 0.0f);

	obj_file.Load("Data/Models/plane_l100.obj");
	mDebugWindow = new GMesh("DebugWindow", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext);
	mDebugWindow->SetMaterial(mpMaterialsManager->GetMaterialByName("RenderedTexture0"));
	mDebugWindow->Scale(2.0f * mClientWidth / mClientHeight, 2.0f, 2.0f);
	mDebugWindow->Move(100.0f, -(mClientHeight - 100.0f), 0.0f);
	mDebugWindow->RotateX(-90.0f);

	obj_file.Load("Data/Models/plane_l100.obj");
	mCursor = new GMesh("Cursor", obj_file, 0, *mpMaterialsManager, md3dDevice, md3dContext);
	mCursor->SetMaterial(mpMaterialsManager->GetMaterialByName("Cursor"));
	mCursor->Scale(0.32f, 1.0f, 0.32f);
	mCursor->RotateX(-90.0f);
	mCursor->Move(16.0f, -16.0f, 0.0f);

	mTerrain = new GTerrain(md3dDevice, md3dContext, 
		1000, 1000, 50, "Data/Textures/terrain_heightmap.bmp");

	// Call this at the end on initApp()
	this->onResize();
}
//------------------------------------------------------------------------------------
void TerrainAndUiDemo::onResize()
{
    GApplication::onResize();
	mCamera.OnResize(mClientWidth, mClientHeight);
	mPlayer.OnResize(mClientWidth, mClientHeight);
	// Update text position when resizing window
	mText[0]->OnResize(mClientWidth, mClientHeight);
	// Resize render texture
	mpTexturesManager->OnResize("RenderedTexture0", mpWindowRenderTargetView, mWindowDepthStencilView, 
		mClientWidth, mClientHeight);
	mLight.OnResize(mClientWidth, mClientHeight);	
}
//------------------------------------------------------------------------------------
void TerrainAndUiDemo::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "Video card: " << mVideoCard << 
		"\nVideo memory: " << mVideoMemory << " MB" <<
		"\nUPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mPlayer.Position().x << " " << 
		mPlayer.Position().y << " " << mPlayer.Position().z << 
		"\nCamera target position: " << mPlayer.Target().x << " " <<
		mPlayer.Target().y << " " << mPlayer.Target().z <<
		"\nCamera Up vector: " << mPlayer.Up().x << " " <<
		mPlayer.Up().y << " " << mPlayer.Up().z <<
		"\nLight direction: " << mLight.GetDirection().x << " " <<
		mLight.GetDirection().y << " " << mLight.GetDirection().z <<
		"\nMouse X: " << mInput.MousePos().x << " Y: " << mInput.MousePos().y;
	mText[0]->SetText(stream.str());

	// Camera controls:
	//if (mInput.IsKeyDown('W'))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.z += 1.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}
	//if (mInput.IsKeyDown('S'))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.z -= 1.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}
	//if (mInput.IsKeyDown('D'))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.x -= 1.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}
	//if (mInput.IsKeyDown('A'))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.x += 1.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}
	//if (mInput.IsKeyDown(VK_SPACE))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.y += 1.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}
	//if (mInput.IsKeyDown('X'))
	//{
	//	XMFLOAT3 new_camera_position = mCamera.GetPosition();
	//	new_camera_position.y -= 2.0f;
	//	mCamera.SetPosition(new_camera_position);
	//}

	// Light controls:
	if (mInput.IsKeyDown(VK_LEFT))
	{
		mLight.SetDirection(GMathVF(XMVector3Rotate(GMathFV(mLight.GetDirection()), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), -0.1f))));
		mLight.SetPos(mLight.GetPos().x - 3.0f, mLight.GetPos().y, mLight.GetPos().z);
		//for (auto &model : mColoredModels)
		//{
		//	if (model->GetName() == "LightDirection")
		//		model->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(-0.1f));
		//}
	}
	if (mInput.IsKeyDown(VK_RIGHT))
	{
		mLight.SetDirection(GMathVF(XMVector3Rotate(GMathFV(mLight.GetDirection()), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), 0.1f))));
		mLight.SetPos(mLight.GetPos().x + 3.0f, mLight.GetPos().y, mLight.GetPos().z);
		//for (auto &model : mColoredModels)
		//{
		//	if (model->GetName() == "LightDirection")
		//		model->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(0.1f));
		//}
	}

	if (mInput.IsKeyDown('A'))
	{
		mPlayer.TurnLeft();
	}
	if (mInput.IsKeyDown('D'))
	{
		mPlayer.TurnRight();
	}
	if (mInput.IsKeyDown('W'))
	{
		mPlayer.MoveForward();
	}
	if (mInput.IsKeyDown('S'))
	{
		mPlayer.MoveBackward();
	}
	mPlayer.Update(dSeconds);

	// Animate rotation of textured sphere
	//for (auto &model : mTexturedModels)
	//{
	//	if (model->GetName() == "TexturedSphere0")
	//		model->Rotate(XMFLOAT3(0.0f, 1.0f, 0.0f), XMConvertToDegrees(0.01f));
	//}

	//XMVECTOR vec = XMLoadFloat4(&mResult);
	//vec = XMVector4Transform(vec, XMMatrixTranspose(GMathFM(mPlayer.View())) * XMMatrixTranspose(GMathFM(mPlayer.Proj())));
	//vec = vec / XMVectorGetW(vec);
	//XMStoreFloat4(&mResult, vec);

	
	//GApplication::mInput.UpdateMouseMove();
}
//------------------------------------------------------------------------------------
void TerrainAndUiDemo::drawScene()
{
	GApplication::drawScene();

	mpTexturesManager->RenderToTexture(const_cast<XMFLOAT4&>(GColor::WHITE));

    // Reset shader matrices for this frame
	GApplication::TurnZBufferOn();
	mpDepthShader->ClearConstantBuffer();
	mpDepthShader->SetShader();
	mpDepthShader->DisableAlpha();
	mpDepthShader->mShaderConstBuffer0.View = mLight.mCamera.View();
	mpDepthShader->mShaderConstBuffer0.Projection = mLight.mCamera.Ortho();
	for (auto &model : mColoredModels)
	{
		mpDepthShader->mShaderConstBuffer0.World = model->GetWorld();
		mpDepthShader->UpdateConstantBuffer();
		model->draw();
	}
	for (auto &model : mTexturedModels)
	{
		mpDepthShader->mShaderConstBuffer0.World = model->GetWorld();
		mpDepthShader->UpdateConstantBuffer();
		model->draw();
	}
	mpDepthShader->mShaderConstBuffer0.World = mTerrain->GetWorld();
	mpDepthShader->UpdateConstantBuffer();
	mTerrain->draw();

	mpTexturesManager->RenderToScreen(mClientWidth, mClientHeight);
	GApplication::ClearScreen();

	GApplication::TurnZBufferOn();

	mpTexturedShader->ClearConstantBuffer();
	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();

	mpTexturedShader->mShaderConstBuffer0.View = mPlayer.View();
	mpTexturedShader->mShaderConstBuffer0.Projection = mPlayer.Proj();
	mpTexturedShader->mShaderConstBuffer0.CameraPosition = mPlayer.Position();

	mpTexturedShader->mShaderConstBuffer0.Light.Direction = mLight.GetDirection();
	mpTexturedShader->mShaderConstBuffer0.Light.Position = mLight.GetPos();
	mpTexturedShader->mShaderConstBuffer0.Light.DiffuseColor = mLight.mDiffuseColor;
	mpTexturedShader->mShaderConstBuffer0.Light.AmbientColor = mLight.mAmbientColor;
	mpTexturedShader->mShaderConstBuffer0.Light.SpecularColor = mLight.mSpecularColor;
	mpTexturedShader->mShaderConstBuffer0.Light.AttenuationParameters = mLight.mAttenuationParameters;
	mpTexturedShader->mShaderConstBuffer0.Light.SpotPower = mLight.mSpotPower;
	mpTexturedShader->mShaderConstBuffer0.Light.Range = mLight.mRange;
	mpTexturedShader->mShaderConstBuffer0.LightType = mLight.mType;
	mpTexturedShader->mShaderConstBuffer0.LightView = mLight.mCamera.View();
	mpTexturedShader->mShaderConstBuffer0.LightProjection = mLight.mCamera.Ortho();
	
	mpTexturesManager->SetTexture("NormalMap", "Brick Normal Map");
	mpTexturesManager->SetTexture("ShadowMap", "RenderedTexture0");
	for (auto &model : mColoredModels)
	{
		mpTexturedShader->mShaderConstBuffer0.UseNormalMap = 1;
		mpTexturedShader->mShaderConstBuffer0.UseShadowMap = 1;
		mpTexturedShader->mShaderConstBuffer0.UseDiffuseMap = 0;
		mpTexturedShader->mShaderConstBuffer0.World = model->GetWorld();
		mpTexturedShader->UpdateConstantBuffer();
		model->draw();
	}
	
	for (auto &model : mTexturedModels)
	{
		mpTexturesManager->SetTexture("NormalMap", "Brick Normal Map");
		mpTexturesManager->SetTexture("ShadowMap", "RenderedTexture0");
		mpTexturesManager->SetTexture("DiffuseMap", 
			mpMaterialsManager->GetMaterialByName(model->GetMaterialID()).map_Kd);
		mpTexturesManager->SetTexture("AmbientMap", 
			mpMaterialsManager->GetMaterialByName(model->GetMaterialID()).map_Ka);
		mpTexturedShader->mShaderConstBuffer0.UseNormalMap = 1;
		mpTexturedShader->mShaderConstBuffer0.UseShadowMap = 1;
		mpTexturedShader->mShaderConstBuffer0.UseDiffuseMap = 1;
		mpTexturedShader->mShaderConstBuffer0.World = model->GetWorld();
		mpTexturedShader->UpdateConstantBuffer();
		model->draw();
	}
	
	mpTerrainShader->ClearConstantBuffer();
	mpTerrainShader->SetShader();
	mpTerrainShader->EnableAlpha();
	mpTerrainShader->mShaderConstBuffer0.View = mPlayer.View();
	mpTerrainShader->mShaderConstBuffer0.Projection = mPlayer.Proj();
	mpTerrainShader->mShaderConstBuffer0.CameraPosition = mPlayer.Position();

	mpTerrainShader->mShaderConstBuffer0.Light.Direction = mLight.GetDirection();
	mpTerrainShader->mShaderConstBuffer0.Light.Position = mLight.GetPos();
	mpTerrainShader->mShaderConstBuffer0.Light.DiffuseColor = mLight.mDiffuseColor;
	mpTerrainShader->mShaderConstBuffer0.Light.AmbientColor = mLight.mAmbientColor;
	mpTerrainShader->mShaderConstBuffer0.Light.SpecularColor = mLight.mSpecularColor;
	mpTerrainShader->mShaderConstBuffer0.Light.AttenuationParameters = mLight.mAttenuationParameters;
	mpTerrainShader->mShaderConstBuffer0.Light.SpotPower = mLight.mSpotPower;
	mpTerrainShader->mShaderConstBuffer0.Light.Range = mLight.mRange;
	mpTerrainShader->mShaderConstBuffer0.LightType = mLight.mType;
	mpTerrainShader->mShaderConstBuffer0.LightView = mLight.mCamera.View();
	mpTerrainShader->mShaderConstBuffer0.LightProjection = mLight.mCamera.Ortho();

	mpTerrainShader->mShaderConstBuffer0.World = mTerrain->GetWorld();
	mpTexturesManager->SetTexture("ShadowMap", "RenderedTexture0");
	mpTexturesManager->SetTexture("DiffuseMap", "Sand");
	mpTexturesManager->SetTexture("AmbientMap", "Sand");
	mpTerrainShader->mShaderConstBuffer0.UseNormalMap = 0;
	mpTerrainShader->mShaderConstBuffer0.UseShadowMap = 1;
	mpTerrainShader->mShaderConstBuffer0.UseDiffuseMap = 1;
	mpTerrainShader->UpdateConstantBuffer();
	mTerrain->draw();

	// Draw 2D Text
	GApplication::TurnZBufferOff();
	mpFontShader->ClearConstantBuffer();
	mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->mShaderConstBuffer0.View = GMathMF(XMMatrixIdentity());
	mpFontShader->mShaderConstBuffer0.Projection = mPlayer.Ortho();
	mpFontShader->mShaderConstBuffer0.FontColor = GColor::WHITE;

	for (auto &text : mText)
	{
		mpTexturesManager->SetTexture("DiffuseMap", text->GetTextureName());
		mpFontShader->mShaderConstBuffer0.World = text->GetWorld();
		mpFontShader->UpdateConstantBuffer();
		text->draw();
	}

	// draw 2d overlay
	mpOverlayShader->ClearConstantBuffer();
	mpOverlayShader->SetShader();
	mpOverlayShader->EnableAlpha();
	mpOverlayShader->mShaderConstBuffer0.View = GMathMF(XMMatrixIdentity());
	mpOverlayShader->mShaderConstBuffer0.Projection = mPlayer.Ortho();
	mpTexturesManager->SetTexture("DiffuseMap", 
		mpMaterialsManager->GetMaterialByName(mDebugWindow->GetMaterialID()).map_Kd);
	mpOverlayShader->mShaderConstBuffer0.World = 
		GMathMF(XMMatrixTranspose(XMMatrixTranspose(GMathFM(mDebugWindow->GetWorld())) * XMMatrixTranslation(
		static_cast<float>(-mClientWidth)/2.0f + 0,	
		static_cast<float>(mClientHeight)/2.0f - 0, 
		0.0f)));
	mpOverlayShader->UpdateConstantBuffer();
	mDebugWindow->draw();

	// draw cursor
	mpTexturesManager->SetTexture("DiffuseMap",
		mpMaterialsManager->GetMaterialByName(mCursor->GetMaterialID()).map_Kd);
	mpOverlayShader->mShaderConstBuffer0.World = 
		GMathMF(XMMatrixTranspose(XMMatrixTranspose(GMathFM(mCursor->GetWorld())) * XMMatrixTranslation(
		static_cast<float>(-mClientWidth)/2.0f + mInput.MousePos().x,	
		static_cast<float>(mClientHeight)/2.0f - mInput.MousePos().y, 
		0.0f)));
	mpOverlayShader->UpdateConstantBuffer();
	mCursor->draw();

    mSwapChain->Present(0, 0);
}
//------------------------------------------------------------------------------------
TerrainAndUiDemo::~TerrainAndUiDemo(void)
{
	std::for_each(mText.begin(), mText.end(), 
		[](GText *item) { delete item; });
	std::for_each(mColoredModels.begin(), mColoredModels.end(), 
		[](GMesh *item) { delete item; });
	std::for_each(mTexturedModels.begin(), mTexturedModels.end(), 
		[](GMesh *item) { delete item; });

	delete mTerrain;
	delete mCursor;
	delete mDebugWindow;

	delete mpMaterialsManager;
	delete mpTexturesManager;
	
	delete mpTerrainShader;
	delete mpTexturedShader;
	delete mpDepthShader;
	delete mpOverlayShader;
	delete mpFontShader;

    if( md3dContext )
        md3dContext->ClearState();
}
//------------------------------------------------------------------------------------