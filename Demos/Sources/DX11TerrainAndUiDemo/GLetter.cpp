#include "GLetter.h"
using namespace Game;
//------------------------------------------------------------------------------------
GLetter::GLetter(char letter, uint width, uint height, float start, float end)
{
	mLetter = letter;
	mWidth = width;
	mHeight = height;
	mStartPosition = start;
	mEndPosition = end;
}
//------------------------------------------------------------------------------------
std::istream& Game::operator>>(std::istream& input_stream, GLetter& letter)
{
	input_stream >> letter.mLetter;
	input_stream >> letter.mWidth;
	input_stream >> letter.mHeight;
	input_stream >> letter.mStartPosition;
	input_stream >> letter.mEndPosition;
	return input_stream;
}
//------------------------------------------------------------------------------------