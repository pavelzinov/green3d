#pragma once
#include "GUtility.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GRenderTargetTexture
	{
	public:
		GRenderTargetTexture(ID3D11Device* device, const uint texture_width, const uint texture_height);
		GRenderTargetTexture(const GRenderTargetTexture& texture);
		GRenderTargetTexture& operator=(const GRenderTargetTexture& texture);
		~GRenderTargetTexture(void);
		// Call it to resize render-to-texture texture
		void OnResize(const uint texture_width, const uint texture_height);
		// Get texture shader resource view
		ID3D11ShaderResourceView* GetView() const { return mpView; }
		// Get texture 2D
		ID3D11Texture2D* GetTexture2D() const { return mpTexture; }
		const uint GetWidth() const { return mWidth; }
		const uint GetHeight() const { return mHeight; }

	private:
		void GenerateRenderTexture(uint texture_width, uint texture_height);
		uint						mWidth;
		uint						mHeight;
		ID3D11Device*				mpDevice;
		ID3D11Texture2D*			mpTexture;
		ID3D11ShaderResourceView*   mpView;
	}; // class
} // namespace
//------------------------------------------------------------------------------------