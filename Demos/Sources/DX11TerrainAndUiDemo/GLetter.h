#pragma once
#include "GUtility.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GLetter
	{
	public:
		GLetter() {}
		GLetter(char letter, uint width, uint height, float start, float end);
		~GLetter(void) {}

	public:
		char	mLetter;
		uint	mWidth;
		uint	mHeight;
		float	mStartPosition;
		float	mEndPosition;

	private:
		friend std::istream& operator>>(std::istream& input_stream, GLetter& letter);
	}; // class
	// friend operator>> overloading for istream
	std::istream& operator>>(std::istream& input_stream, GLetter& letter);
} // namespace
//------------------------------------------------------------------------------------