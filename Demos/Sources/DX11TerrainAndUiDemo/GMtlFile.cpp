#include "GMtlFile.h"
using namespace Game;
//------------------------------------------------------------------------------------
void GMtlFile::Load(std::string filename)
{
	GFileAscii file;
	file.Open(filename);
	auto file_lines = file.GetLines();
	std::vector<std::string> string_parts;
	GMaterial current_material;
	for (uint i = 0; i < file_lines.size(); ++i)
	{
		string_parts = GString::Split(file_lines[i], " \t");
		if (string_parts.size() <= 0)
			continue;

		if (string_parts[0] == "newmtl")
		{
			mMaterials.push_back(current_material);
			current_material.Clear();

			current_material.mName = string_parts[1];
		}
		else if (string_parts[0] == "Ka")
		{
			current_material.Ka.r = GString::ToFloat(string_parts[1]);
			current_material.Ka.g = GString::ToFloat(string_parts[2]);
			current_material.Ka.b = GString::ToFloat(string_parts[3]);
		}
		else if (string_parts[0] == "Kd")
		{
			current_material.Kd.r = GString::ToFloat(string_parts[1]);
			current_material.Kd.g = GString::ToFloat(string_parts[2]);
			current_material.Kd.b = GString::ToFloat(string_parts[3]);
		}
		else if (string_parts[0] == "Ks")
		{
			current_material.Ks.r = GString::ToFloat(string_parts[1]);
			current_material.Ks.g = GString::ToFloat(string_parts[2]);
			current_material.Ks.b = GString::ToFloat(string_parts[3]);
		}
		else if (string_parts[0] == "d")
		{
			current_material.d = GString::ToFloat(string_parts[1]);
		}
		else if (string_parts[0] == "illum")
		{
			current_material.illum = GString::ToInt(string_parts[1]);
		}
		else if (string_parts[0] == "map_Ka")
		{
			current_material.map_Ka = string_parts[1];
		}
		else if (string_parts[0] == "map_Kd")
		{
			current_material.map_Kd = string_parts[1];
		}
		else if (string_parts[0] == "map_Ks")
		{
			current_material.map_Ks = string_parts[1];
		}
		else if (string_parts[0] == "map_Ns")
		{
			current_material.map_Ns = string_parts[1];
		}
		else if (string_parts[0] == "map_d")
		{
			current_material.map_d = string_parts[1];
		}
		else if (string_parts[0] == "map_bump" || string_parts[0] == "bump")
		{
			current_material.map_bump = string_parts[1];
		}
		else if (string_parts[0] == "disp")
		{
			current_material.disp = string_parts[1];
		}
		else if (string_parts[0] == "decal")
		{
			current_material.decal = string_parts[1];
		}
	}
	mMaterials.push_back(current_material);
}
//------------------------------------------------------------------------------------