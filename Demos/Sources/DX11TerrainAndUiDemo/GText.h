#pragma once
#include "GUtility.h"
#include "GVertex.h"
#include "GLetter.h"
#include "GTexture.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GText
	{
	public:
		GText(ID3D11Device* device, ID3D11DeviceContext* context, 
			const int screen_width, const int screen_height, 
			const std::string& font_desc_filename, std::string text);
		GText(const GText &text);
		GText& operator=(const GText &text);
		~GText(void);

	public:
		void SetText(std::string text);
		std::string GetText() { return this->mText; }
		XMINT2 Position() { return XMINT2(mRealPosition.x, -mRealPosition.y); }
		void Position(XMINT2 position);
		XMFLOAT4X4 GetWorld() { return GMathMF(XMMatrixTranspose(GMathFM(mWorld))); }
		void SetFontFile(const std::string& font_desc_filename);
		const std::string& GetTextureName() const { return mTextureName; }
		uint GetSymbolHeight() { return mDictionary.size() > 0 ? mDictionary[0].mHeight : -1; }
		void OnResize(const uint &new_window_width, const uint &new_window_height);
		void SetColor(const XMFLOAT4 &color) { mColor = color; }
		const XMFLOAT4& GetColor() { return mColor; }

		void draw();

	private:
		// Load font description from .font file
		// .font file contains lines with following structure:
		// first line is always the name of the font texture as declared in GTexturesManager,
		// other lines are information about each symbol of alphabet
		// [ascii_symbol_code] [width_in_pixels] [height_in_pixels] [u_start_tex_coord] [u_end_tex_coord]
		void DictionaryFromFile(const std::string &font_filename);
		// Generate geometry based on mText and mDictionary members
		void GenerateGeometry();
		// Construct dynamic drawing buffers
		void ConstructBuffers();
		// Update dynamic drawing buffers
		void UpdateBuffers();

	protected:
		ID3D11Device*			mpDevice;
		ID3D11DeviceContext*	mpContext;
		ID3D11Buffer*			mVertexBuffer;
		ID3D11Buffer*			mIndexBuffer;

		std::vector<GVertex>	mVertices;
		std::vector<uint>		mIndices;
		uint					mCapacity;	// not resizing direct3d buffers until they exceed current capacity
	
		XMINT2					mRealPosition;		// top-left corner coordinates in real 3D world, not 2D
		std::string				mText;
		XMFLOAT4				mColor;
		std::string				mTextureName;
		std::string				mFontFile;
		XMFLOAT4X4				mWorld;
		std::map<char, GLetter>	mDictionary;
	}; // class
} // namespace
//------------------------------------------------------------------------------------