#pragma once
#include "GUtility.h"
#include "GMtlFile.h"
#include "GMaterial.h"
#include "GMath.h"
#include "GFileAscii.h"
//-------------------------------------------------------------------------------------
namespace Game
{
	class GObjFile
	{
	public:
		GObjFile(void) {}
		~GObjFile(void) {}
		bool Load(std::string filename);
		bool Save(std::string filename);
		int GetMeshesCount();

		struct VertexPosition
		{
			float x, y, z;
		};
		struct VertexNormal
		{
			float nx, ny, nz;
		};
		struct TextureCoordinate
		{
			float u, v;
		};
		struct FaceVertex
		{
			VertexPosition		mVertex;
			TextureCoordinate	mTexCoords;
			VertexNormal		mNormal;
		};
		struct Face
		{
			FaceVertex mVertices[3];
		};
		struct Mesh
		{
			GMaterial mMaterial;
			std::vector<Face> mFaces;
		};
		struct Group
		{
			std::string mName;
			std::vector<Mesh> mMeshes;
		};

		std::vector<VertexPosition>		mVertices;
		std::vector<VertexNormal>		mNormals;
		std::vector<TextureCoordinate>	mTexCoords;
		std::vector<GMaterial>			mMaterials;
		std::vector<std::string>		mMaterialLibraries;
		// This is a root node, which contains all of the information
		// about objects in .OBJ file
		std::vector<Group> mGroups;
	}; // class
} // namespace
//-------------------------------------------------------------------------------------