#pragma once
#include "GUtility.h"
#include "GMaterial.h"
#include "GFileAscii.h"
#include "GString.h"
#include "GMtlFile.h"
#include "GTexturesManager.h"
//------------------------------------------------------------------------------------
namespace Game
{
	class GMaterialsManager
	{
	public:
		GMaterialsManager(void) {}
		~GMaterialsManager(void) {}

		const GMaterial& GetMaterialByName(const std::string &material_name) const
			{ return mMaterials.at(material_name); }
		void AddMaterial(const GMaterial& material);
		// Load materials from .mtl file
		bool LoadAddMaterialsFromFile(const std::string &filename, 
			const GTexturesManager &textures);
		bool LoadAddMaterialsFromFile(const std::string &filename);
		bool LoadAddMaterialsFromObjFile(const GObjFile &obj_file, 
			const GTexturesManager &textures);

	private:
		std::map<std::string, GMaterial> mMaterials;
	};
}
//------------------------------------------------------------------------------------