#include "GLog.h"
using namespace Game;
//------------------------------------------------------------------------------------
void GLog::Debug(std::string &message)
{
#ifndef UBUNTU
	OutputDebugStringA(message.c_str());
#endif
}
//------------------------------------------------------------------------------------
static void MessageBox(std::string& message)
{
	MessageBoxA(nullptr, message.c_str(), "Message", MB_OK);
}
//------------------------------------------------------------------------------------
void GLog::WriteLine(std::string &message)
{
	// TODO: add time to log
	//auto temp = time(0);
	//auto now = localtime(&temp);
	//std::stringstream time_display;
	//time_display << "[" << now->tm_mday << "/" << 
	mLog.WriteLine(message);
}
//-------------------------------------------------------------------------------------
void GLog::SaveToFile(std::string &filename)
{
	mLog.Save(filename);
}
//------------------------------------------------------------------------------------
