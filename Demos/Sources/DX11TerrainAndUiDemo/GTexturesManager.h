#pragma once
#include "GUtility.h"
#include "GTexture.h"
#include "GRenderTargetTexture.h"
#include "GMtlFile.h"
#include "GObjFile.h"
//------------------------------------------------------------------------------------
namespace Game
{
	////////////////////////////////////////////////////////
	// Manager designed for storing all game textures in
	// single place, assigning special identifiers to them and
	// allow access to them. Unique identifier is created by
	// this manager when texture is added to collection.
	// Meshes and other primitives reference textures by those names.
	// Additionally, textures manager is responsible for
	// rendering to texture, instead of directly to the screen. To enable this functionality,
	// call AddTexture(...) - this will create texture and render target views, associated with it.
	// Don't forget to call OnResize, when window change it's size, so render target texture
	// have to be resized. Call RenderToTexture to enable rendering to texture and call
	// RenderToScreen to restore default rendering mechanism. Texture, that has been
	// made by rendering to texture can be set into shader via SetCurrentMaterial("RenderTexture0")
	////////////////////////////////////////////////////////
	class GTexturesManager
	{
	public:
		GTexturesManager(ID3D11Device* device, ID3D11DeviceContext* context);
		~GTexturesManager(void);
		// Add external texture into manager
		std::string AddTexture(const std::string &texture_name, GTexture& texture);
		// Create and add texture with given parameters
		std::string AddTexture(const std::string &texture_name, const std::string &texture_file);
		// Generate textures from .textures file
		// .textures file contains pairs of texture_name[new_line]texture_file
		// if .textures file line starts with // - it is a comment
		// this file can contain empty lines
		bool LoadAddTexturesFromFile(const std::string &filename);
		// Generate all textures contained in given .MTL file
		bool LoadAddTexturesFromMtlFile(const std::string &filename);
		// Generate textures from .mtl files, linked with given .obj file
		bool LoadAddTexturesFromObjFile(const GObjFile &obj_file);
		// Get texture's reference by it's name
		GTexture& GetTexture(const std::string &texture_name);
		// Get rendered texture's reference by it's name
		GRenderTargetTexture& GetRenderedTexture(const std::string &texture_name);
		// Get texture by file name
		const std::string GetTextureNameByFileName(const std::string &file_name) const;
		// Get all materials' names
		std::vector<std::string> GetTexturesNames();
		// Set material with name `material_name` as Textured2D resource in current shader
		void SetTexture(const std::string &semantic_name, const std::string &texture_name);
		// Set materials with given names as Textured2D array resource in current shader
		void SetTextures(const std::vector<std::pair<std::string, std::string>> &shader_slot_pairs);
		// Set all current textures
		void SetAllCurrentTextures();
		// Add render target as material. When resizing render target, OnResize() method must be called
		// with this material name as first parameter
		HRESULT AddRenderedTexture(const std::string &texture_name, 
			ID3D11RenderTargetView* window_render_target_view, ID3D11DepthStencilView* depth_stencil_view, 
			const uint texture_width, const uint texture_height, 
			const uint sample_count, const uint sample_quality);
		// From the point of this function call, rendering goes into texture
		void RenderToTexture(XMFLOAT4& clear_color);
		// From the point of this function call, rendering goes onto the screen
		void RenderToScreen(const uint screen_width, const uint screen_height);
		// Call this function, when window has been resized. It will properly resize render target texture
		void OnResize(const std::string &texture_name, ID3D11RenderTargetView *window_render_target_view, 
			ID3D11DepthStencilView *depth_stencil_view, const uint texture_width, const uint texture_height);
		// Generate several default samplers
		void GenerateSamplers();
		// Set all samplers for current shader. Shaders must include special file, which
		// contains all of possible sampler states, so this function can be called very rare,
		// since resources are not clearing from DirectX11 pipeline
		void SetSamplers();
		// Add specific sampler to inner collection of sampler states
		bool AddSampler(const std::string &sampler_name, D3D11_SAMPLER_DESC &sampler_description);
	
	private:
		// Associative container where each material can be referenced with it's string name
		std::map<std::string, GTexture>				mTextures;
		std::map<std::string, GRenderTargetTexture>	mRenderTargetTextures;
		ID3D11Device*				mpDevice;
		ID3D11DeviceContext*		mpContext;
		// Original window's render target. Needed for when restoring rendering to screen
		ID3D11RenderTargetView*		mpWindowRenderTargetView;
		// Original window's depth-stencil
		ID3D11DepthStencilView*		mpWindowDepthStencilView;
		// Render target view on 2D texture
		ID3D11RenderTargetView*		mpRenderTagetView;
		ID3D11Texture2D*			mpDepthStencilBuffer;
		ID3D11DepthStencilView*		mpDepthStencilView;
		uint						mSampleCount;
		uint						mSampleQuality;
		// Sampler states
		std::map<std::string, ID3D11SamplerState*>			mSamplers;
		std::vector<std::pair<std::string, std::string>>	mShaderTexturesSemantics;
	}; // class
} // namespace
//------------------------------------------------------------------------------------