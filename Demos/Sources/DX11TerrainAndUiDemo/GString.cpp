#include "GString.h"
using namespace Game;
//--------------------------------------------------------------------------------------
GString::GString(void)
{
}
//--------------------------------------------------------------------------------------
GString::~GString(void)
{
}
//--------------------------------------------------------------------------------------
std::string GString::ToAscii(std::wstring& wstr)
{
	// TODO: not working for russian symbols!!!
	std::string temp(wstr.length(), ' ');
	#pragma warning( disable : 4244 )
	std::copy(wstr.begin(), wstr.end(), temp.begin());
	return temp;
}
//--------------------------------------------------------------------------------------
std::wstring GString::ToUnicode(std::string& str)
{
	std::wstring temp(str.length(), L' ');
	std::copy(str.begin(), str.end(), temp.begin());
	return temp;
}
//--------------------------------------------------------------------------------------
std::vector<std::string>& GString::Split(const std::string &str, const char delimiter,
											 std::vector<std::string> &result)
{
	std::stringstream ss(str);
	std::string item;
	result.reserve(str.size());
	while(std::getline(ss, item, delimiter)) {
		if (item != "") {
			result.push_back(item);
		}
	}
	return result;
}
//--------------------------------------------------------------------------------------
std::vector<std::string> GString::Split(const std::string &str, const char delimiter)
{
	std::vector<std::string> result;
	Split(str, delimiter, result);
	return result;
}
//--------------------------------------------------------------------------------------
int GString::ToInt(const std::string& str)
{
	return std::stoi(str);
}
//--------------------------------------------------------------------------------------
float GString::ToFloat(const std::string& str)
{
	return std::stof(str);
}
//--------------------------------------------------------------------------------------
std::vector<std::string>& GString::Split(const std::string &str, const std::string delimiters,
											 std::vector<std::string> &result)
{
	std::string construction_string;
	for (uint i = 0; i < str.size(); ++i)
	{
		if (delimiters.find(str[i]) == std::string::npos)
		{
			construction_string.push_back(str[i]);
		}
		else
		{
			if (construction_string.size() > 0)
			{
				result.push_back(construction_string);
				construction_string.clear();
			}			
		}
	}
	if (construction_string.size() > 0)
	{
		result.push_back(construction_string);
		construction_string.clear();
	}

	return result;
}
//--------------------------------------------------------------------------------------
std::vector<std::string> GString::Split(const std::string &str, const std::string delimiters)
{
	std::vector<std::string> result;
	Split(str, delimiters, result);
	return result;
}
//--------------------------------------------------------------------------------------