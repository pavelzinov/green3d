#pragma once
#include "GUtility.h"
#include "GVertex.h"
#include "GShaderConstantBuffer.h"
//------------------------------------------------------------------------------------
namespace Game
{

template <uint shader_buffer_type>
class GShader
{
public:
	GShader(ID3D11Device* device, ID3D11DeviceContext* context);
	~GShader(void);

public:
	// Load pixel/vertex shader from .fx file
	HRESULT LoadShader(std::string filename);
	// Set all vertex and pixel shaders along with their parameters
	void SetShader();
	// Enable alpha-blending
	void EnableAlpha();
	// Disable alpha-blending
	void DisableAlpha();
	// Clear shader constant buffer
	void ClearConstantBuffer() { ZeroMemory(&mShaderConstBuffer0, sizeof(mShaderConstBuffer0)); }
	// Update shader constant buffer
	void UpdateConstantBuffer();
	// How to interpret input vertices
	D3D_PRIMITIVE_TOPOLOGY mTopology;
	// Shader constant buffer
	GShaderConstantBuffer<shader_buffer_type> mShaderConstBuffer0;

private:
	// Compile shader from .fx file. Vertex and pixel shaders must be compiled independently
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, 
		LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

private:
	ID3D11Device*			mpDevice;
	ID3D11DeviceContext*	mpContext;

	ID3D11PixelShader*		mpPixelShader;
	ID3D11VertexShader*		mpVertexShader;
	ID3D11InputLayout*		mpVertexLayout;

	ID3D11BlendState*		mpCurrentBlendState;
	ID3D11BlendState*		mpBlendStateEnableAlpha;
	ID3D11BlendState*		mpBlendStateDisableAlpha;

	ID3D11Buffer*			mpPOConstantBuffer;
};
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
GShader<shader_buffer_type>::GShader(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mTopology					= D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	mpDevice					= device;
	mpContext					= context;
	mpPixelShader				= nullptr;
	mpVertexShader				= nullptr;
	mpVertexLayout				= nullptr;
	mpCurrentBlendState			= nullptr;
	mpBlendStateEnableAlpha		= nullptr;
	mpBlendStateDisableAlpha	= nullptr;
	mpPOConstantBuffer			= nullptr;
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
GShader<shader_buffer_type>::~GShader(void)
{
	ReleaseCOM(mpPOConstantBuffer);
	mpCurrentBlendState	= nullptr;
	ReleaseCOM(mpBlendStateDisableAlpha);
	ReleaseCOM(mpBlendStateEnableAlpha);	
	ReleaseCOM(mpVertexLayout);
	ReleaseCOM(mpVertexShader);
	ReleaseCOM(mpPixelShader);
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
HRESULT GShader<shader_buffer_type>::LoadShader(std::string filename)
{
	// Compile the vertex shader
	ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile(GString::ToUnicode(filename).c_str(), "VS", "vs_4_0", &pVSBlob);
	if(FAILED(hr))
	{
		MessageBox(NULL, 
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = mpDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, 
		&mpVertexShader);
	if(FAILED(hr))
	{	
		pVSBlob->Release();
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	uint numElements = ARRAYSIZE( layout );

	// Create the input layout
	hr = mpDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &mpVertexLayout );
	pVSBlob->Release();
	if(FAILED(hr))
		return hr;    

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
	hr = CompileShaderFromFile(GString::ToUnicode(filename).c_str(), "PS", "ps_4_0", &pPSBlob );
	if(FAILED(hr))
	{
		MessageBox( NULL,
			L"The FX file cannot be compiled. Please run this executable from the directory that contains the FX file.", 
			L"Error", 
			MB_OK );
		return hr;
	}

	// Create the pixel shader
	hr = mpDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &mpPixelShader );
	pPSBlob->Release();
	if(FAILED(hr))
		return hr;

	// create blend state
	D3D11_BLEND_DESC blend_desc;
	ZeroMemory(&blend_desc, sizeof(blend_desc));
	blend_desc.AlphaToCoverageEnable = true;
	blend_desc.IndependentBlendEnable = true;
	blend_desc.RenderTarget[0].BlendEnable = TRUE;
	blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blend_desc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	HRR(mpDevice->CreateBlendState(&blend_desc, &mpBlendStateEnableAlpha));
	mpCurrentBlendState = mpBlendStateEnableAlpha;

	mpBlendStateDisableAlpha = nullptr;

	// Create constant buffer corresponding to
	// shader variables
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(mShaderConstBuffer0);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	HRR(mpDevice->CreateBuffer(&bd, NULL, &mpPOConstantBuffer));

	return S_OK;
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
HRESULT GShader<shader_buffer_type>::CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, 
													LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA(static_cast<char*>(pErrorBlob->GetBufferPointer()));
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
void GShader<shader_buffer_type>::EnableAlpha()
{
	mpCurrentBlendState = mpBlendStateEnableAlpha;
	mpContext->OMSetBlendState(mpBlendStateEnableAlpha, nullptr, 0xffffffff);
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
void GShader<shader_buffer_type>::DisableAlpha()
{
	mpCurrentBlendState = mpBlendStateDisableAlpha;
	mpContext->OMSetBlendState(mpBlendStateDisableAlpha, nullptr, 0xffffffff);
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
void GShader<shader_buffer_type>::UpdateConstantBuffer()
{
	mpContext->UpdateSubresource(mpPOConstantBuffer, 0, nullptr, &mShaderConstBuffer0, 0, 0);
}
//------------------------------------------------------------------------------------
template <uint shader_buffer_type>
void GShader<shader_buffer_type>::SetShader()
{
	mpContext->IASetInputLayout(mpVertexLayout);
	mpContext->IASetPrimitiveTopology(mTopology);

	mpContext->VSSetShader(mpVertexShader, nullptr, 0);
	mpContext->VSSetConstantBuffers(0, 1, &mpPOConstantBuffer);

	mpContext->PSSetShader(mpPixelShader, nullptr, 0);
	mpContext->PSSetConstantBuffers(0, 1, &mpPOConstantBuffer);

	// set alpha-blending for opacity
	mpContext->OMSetBlendState(mpCurrentBlendState, nullptr, 0xffffffff);
}

} // namespace