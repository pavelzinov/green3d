#pragma once
#include <float.h>
typedef unsigned int uint;
//------------------------------------------------------------------------------------
namespace Game
{
	class GMath
	{
	public:
		GMath(void);
		~GMath(void);

		static const float INFINITY;
		static const float PI;
		static const float PIDIV2;
		static const float PIDIV4;
		static const float EPS;

		static uint GetUniqueId() { return ++id; }

	private:
		static uint id;
	}; // class
} // namespace
//------------------------------------------------------------------------------------