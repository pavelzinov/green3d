#include "GRenderTargetTexture.h"
using namespace Game;
//------------------------------------------------------------------------------------
GRenderTargetTexture::GRenderTargetTexture(ID3D11Device* device, 
	const uint texture_width, const uint texture_height)
{
	mpDevice = device;
	mpTexture = nullptr;
	mpView = nullptr;
	mWidth = texture_width;
	mHeight = texture_height;
	GenerateRenderTexture(mWidth, mHeight);
}
//------------------------------------------------------------------------------------
GRenderTargetTexture::GRenderTargetTexture(const GRenderTargetTexture& texture)
{
	mpTexture = nullptr;
	mpView = nullptr;
	*this = texture;
}
//------------------------------------------------------------------------------------
GRenderTargetTexture& GRenderTargetTexture::operator=(const GRenderTargetTexture& texture)
{
	ReleaseCOM(mpView);
	ReleaseCOM(mpTexture);

	mpDevice = texture.mpDevice;
	mWidth = texture.mWidth;
	mHeight = texture.mHeight;	
	GenerateRenderTexture(mWidth, mHeight);

	return *this;
}
//------------------------------------------------------------------------------------
GRenderTargetTexture::~GRenderTargetTexture(void)
{
	ReleaseCOM(mpView);
	ReleaseCOM(mpTexture);	
}
//------------------------------------------------------------------------------------
void GRenderTargetTexture::OnResize(uint texture_width, uint texture_height)
{
	if (texture_width == 0 || texture_height == 0/* ||
		(texture_width == mTextureWidth && texture_height == mTextureHeight)*/)
		return;

	ReleaseCOM(mpView);
	ReleaseCOM(mpTexture);

	GenerateRenderTexture(texture_width, texture_height);
}
//------------------------------------------------------------------------------------
void GRenderTargetTexture::GenerateRenderTexture(uint texture_width, uint texture_height)
{
	if (texture_width == 0 || texture_height == 0)
		return;

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.Width = texture_width;
	textureDesc.Height = texture_height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	// Create the render target texture.
	HR(mpDevice->CreateTexture2D(&textureDesc, NULL, &mpTexture));

	// Setup the description of the shader resource view.
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	HR(mpDevice->CreateShaderResourceView(mpTexture, 
		&shaderResourceViewDesc, &mpView));
}
//------------------------------------------------------------------------------------