#pragma once

#ifndef UBUNTU
#include <windows.h>
#endif

#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <fstream>
#include "GFileAscii.h"
//---------------------------------------------------------
namespace Game
{

class GLog
{
public:
	GLog(void) {}
	~GLog(void) {}

	static void Debug(std::string &message);
	static void MessageBox(std::string& message);
	void WriteLine(std::string &message);
	void SaveToFile(std::string &filename);

private:
	GFileAscii mLog;
};

} // namespace