#include "GTexture.h"
using namespace Game;
//------------------------------------------------------------------------------------
GTexture::GTexture(ID3D11Device* device, const std::string& texture_name, 
				   const std::string& texture_file_name)
{
	mFileName = texture_file_name;
	mpDevice = device;
	mpResource = nullptr;
	mpView = nullptr;
	LoadFromFile(mFileName);
}
//------------------------------------------------------------------------------------
GTexture::GTexture(const GTexture& texture)
{
	mpResource = nullptr;
	mpView = nullptr;
	mpDevice = texture.mpDevice;
	mFileName = texture.mFileName;
	LoadFromFile(mFileName);
}
//------------------------------------------------------------------------------------
GTexture::~GTexture(void)
{
	ReleaseCOM(mpResource);
	ReleaseCOM(mpView);
}
//------------------------------------------------------------------------------------
GTexture& GTexture::operator=(const GTexture& texture)
{
	ReleaseCOM(mpResource);
	ReleaseCOM(mpView);

	mpDevice = texture.mpDevice;
	//mName = texture.mName;
	mFileName = texture.mFileName;
	LoadFromFile(mFileName);	

	return *this;
}
//------------------------------------------------------------------------------------
HRESULT GTexture::LoadFromFile(const std::string& textureFileName)
{
	if (textureFileName == "")
		return E_FAIL;

	mFileName = textureFileName;

	ReleaseCOM(mpResource);
	ReleaseCOM(mpView);

	HRR(CreateDDSTextureFromFile(mpDevice, GString::ToUnicode(mFileName).c_str(), 
		&mpResource, &mpView));

	return S_OK;
}
//------------------------------------------------------------------------------------