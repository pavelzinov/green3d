#pragma once
#include "GUtility.h"
#include "GTexturedVertex.h"
#include "GLetter.h"
#include "GMaterial.h"

namespace Game
{

class GText
{
public:
	GText(ID3D11Device* device, ID3D11DeviceContext* context, 
		int screen_width, int screen_height, 
		const GMaterial& material, std::string text);
	~GText(void);

public:
	void SetText(std::string text);
	std::string GetText() { return this->mText; }
	XMINT2 GetPosition() { return XMINT2(mRealPosition.x, -mRealPosition.y); }
	void SetPosition(XMUINT2 position, int screen_width, int screen_height);
	XMFLOAT4X4 GetWorld() { return GMathMF(XMMatrixTranspose(GMathFM(mWorld))); }
	void SetMaterial(const GMaterial& material);
	std::string GetMaterialName() { return mMaterialName; }
	uint32_t GetSymbolHeight() { return mDictionary.size() > 0 ? mDictionary[0].mHeight : -1; }

	void draw();

private:
	// Load font description from .fntdesc file
	void DictionaryFromFile(std::string font_filename);
	void GenerateGeometry();
	void ConstructBuffers();
	void UpdateBuffers();
	GLetter GetLetter(char letter);

protected:
	ID3D11Device*					mpDevice;
	ID3D11DeviceContext*			mpContext;
	ID3D11Buffer*					mVertexBuffer;
	ID3D11Buffer*					mIndexBuffer;

	std::vector<GTexturedVertex>	mVertices;
	std::vector<uint32_t>			mIndices;
	uint32_t						mCapacity;

	XMINT2					mRealPosition;
	std::string				mText;
	XMFLOAT4X4				mWorld;
	std::string				mMaterialName;
	std::vector<GLetter>	mDictionary;
};

}