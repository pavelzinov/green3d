-- First convert object to Editable Mesh
out_name = getSaveFileName caption:"Export to Text File"\
	types:"Text Files(*.txt)|*.txt"\
	initialDir:(getDir #export)\
	historyCategory:"ExportTextFiles"
if out_name != undefined then 
(
	tmesh = snapshotAsMesh selection[1]
	out_file = createfile out_name
	num_verts = tmesh.numverts 
	num_tverts = tmesh.numtverts 
	num_faces = tmesh.numfaces
	format "GColoredVertex\n%\n" num_verts to:out_file
 
	for v = 1 to num_verts do
	(
		vert = getVert tmesh v
		format "% % % " vert.x vert.y vert.z to:out_file
		--normal = getNormal  tmesh v
		format "% % % 1.0\n" (random 0.0 1.0) (random 0.0 1.0) (random 0.0 1.0) to:out_file
	)
	
	for f = 1 to num_faces do
	(
		face = getFace tmesh f
		format "% % %\n" (face.x as Integer) \
			(face.y as Integer) \
			(face.z as Integer) to:out_file
	)
	close out_file
	delete tmesh
)