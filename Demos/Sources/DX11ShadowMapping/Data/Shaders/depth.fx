//#include "light_helper.fx"
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix	World;
	matrix	View;
	matrix	Projection;
}

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));

    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	float depthValue = ps_input.Pos.z / ps_input.Pos.w;

	return float4(depthValue, depthValue, depthValue, 1.0f);
}