//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix	World;
	matrix	View;
	matrix	Projection;
	// Light description
	float4	Position;
	float4	Direction;
	float4	AmbientColor;
	float4	DiffuseColor;
	float4	SpecularColor;
	float4	AttenuationParameters_SpotPower;	// x - a0, y - a1, z - a2, w - spot_power
	float4	Range;								// x - range
	int4	LightType;							// x - type: 0 - parallel, 1 - point, 2 - spotlight
	float4	CameraPosition;
	// This is for shadow generation
	matrix	LightView;
	matrix	LightProjection;
	// This determines if object diffuse color is uniform
	// or texture as diffuse color
	int4	ObjectColorType;	// x - type; 0 - colored, 1 - textured
	float4	ObjectDiffuseColor;
}

Texture2D		NormalMap;
Texture2D		ShadowMap;
Texture2D		DiffuseMap;

SamplerState	SampleWrap;

//--------------------------------------------------------------------------------------
// Light helpers
//-------------------------------------------------------------------------------------
struct Light
{
	float3 pos;
	float3 dir;
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float3 att;
	float  spotPower;
	float  range;
};

struct SurfaceInfo
{
	float3 pos;
    float3 normal;
    float4 diffuse;
    float4 spec;
};

float3 ParallelLight(SurfaceInfo v, Light L, float3 eyePos)
{
	float3 litColor = float3(0.0f, 0.0f, 0.0f);
 
	// The light vector aims opposite the direction the light rays travel.
	float3 lightVec = -L.dir;
	
	// Add the ambient term.
	litColor += v.diffuse * L.ambient;	
	
	// Add diffuse and specular term, provided the surface is in 
	// the line of site of the light.
	
	float diffuseFactor = dot(lightVec, v.normal);
	[branch]
	if( diffuseFactor > 0.0f )
	{
		float specPower  = max(v.spec.a, 1.0f);
		float3 toEye     = normalize(eyePos - v.pos);
		float3 R         = reflect(-lightVec, v.normal);
		float specFactor = pow(max(dot(R, toEye), 0.0f), specPower);
					
		// diffuse and specular terms
		litColor += diffuseFactor * v.diffuse * L.diffuse;
		litColor += specFactor * v.spec * L.spec;
	}
	
	return litColor;
}

float3 PointLight(SurfaceInfo v, Light L, float3 eyePos)
{
	float3 litColor = float3(0.0f, 0.0f, 0.0f);
	
	// The vector from the surface to the light.
	float3 lightVec = L.pos - v.pos;
		
	// The distance from surface to light.
	float d = length(lightVec);
	
	if( d > L.range )
		return float3(0.0f, 0.0f, 0.0f);	
		
	// Normalize the light vector.
	lightVec /= d;
	
	// Add the ambient light term.
	litColor += v.diffuse * L.ambient;
		
	// Add diffuse and specular term, provided the surface is in 
	// the line of site of the light.
	
	float diffuseFactor = dot(lightVec, v.normal);
	[branch]
	if( diffuseFactor > 0.0f )
	{
		float specPower  = max(v.spec.a, 1.0f);
		float3 toEye     = normalize(eyePos - v.pos);
		float3 R         = reflect(-lightVec, v.normal);
		float specFactor = pow(max(dot(R, toEye), 0.0f), specPower);
	
		// diffuse and specular terms
		litColor += diffuseFactor * v.diffuse * L.diffuse;		
		litColor += specFactor * v.spec * L.spec;		
	}

	// attenuate
	return litColor / dot(L.att, float3(1.0f, d, d*d));
}

float3 SpotLight(SurfaceInfo v, Light L, float3 eyePos)
{
	float3 litColor = PointLight(v, L, eyePos);
	
	// The vector from the surface to the light.
	float3 lightVec = normalize(L.pos - v.pos);
	
	float s = pow(max(dot(-lightVec, L.dir), 0.0f), L.spotPower);
	
	// Scale color by spotlight factor.
	return litColor*s;
}

//-------------------------------------------------------------------------------------
struct VS_IN
{
	float4 Pos : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct VS_OUT
{
    float4 Pos : SV_POSITION;
	float4 PosW : POSITION;
	float2 TexC : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS(VS_IN vs_input)
{
    VS_OUT output = (VS_OUT)0;
    output.Pos = mul(float4(vs_input.Pos.xyz, 1.0f), World);	
    output.Pos = mul(output.Pos, View);
    output.Pos = mul(output.Pos, Projection);
	output.PosW = mul(float4(vs_input.Pos.xyz, 1.0f), World);
	output.TexC = vs_input.TexC;
	output.Normal = normalize(mul(vs_input.Normal, (float3x3)World));
	output.Tangent = normalize(mul(vs_input.Tangent, (float3x3)World));
	output.Binormal = normalize(mul(vs_input.Binormal, (float3x3)World));
    
    return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT ps_input) : SV_Target
{
	// vertex position in light's coordinate system
	float4 PosL = mul(ps_input.PosW, LightView);
	PosL = mul(PosL, LightProjection);

	float bias = 0.001f;

	float2 projected_coordinates;
	projected_coordinates.x = PosL.x / PosL.w / 2.0f + 0.5f;
	projected_coordinates.y = -PosL.y / PosL.w / 2.0f + 0.5f;

	float4 model_color;
	if (ObjectColorType.x == 0)
	{
		model_color = ObjectDiffuseColor;
	}
	else if (ObjectColorType.x == 1)
	{
		model_color = DiffuseMap.Sample(SampleWrap, ps_input.TexC);
	}

	// Check if projected vertex coordinates are in range of [0, 1] 
	// so they are projected to the screen texture, not outside it
	// else, return ambient color
	if (saturate(projected_coordinates.x) != projected_coordinates.x ||
		saturate(projected_coordinates.y) != projected_coordinates.y)
		return model_color * AmbientColor;

	float depthL = PosL.z / PosL.w - bias;
	float pixel_current_depth = ShadowMap.Sample(SampleWrap, projected_coordinates);

	// Check if pixel's current depth from light's point of view
	// is greater than depth of current vertex in light's point of view.
	// If so, the vertex should be shadowed, thus returning ambient light
	// else light calculations involved
	if (depthL > pixel_current_depth)
		return model_color * AmbientColor;

	float4 bump_map = NormalMap.Sample(SampleWrap, ps_input.TexC);
	// Expand the range of the normal value from (0, +1) to (-1, +1).
	bump_map = (bump_map * 2.0f) - 1.0f;
	
	float3 bump_normal = bump_map.z * ps_input.Normal + 
		bump_map.x * ps_input.Tangent + bump_map.y * ps_input.Binormal;
	bump_normal = normalize(bump_normal);

	SurfaceInfo surface_info = {ps_input.PosW.xyz, bump_normal, model_color, model_color};

	Light light;
	light.pos = Position.xyz;
	light.dir = Direction.xyz;
	light.ambient = AmbientColor;
	light.diffuse = DiffuseColor;
	light.spec = SpecularColor;
	light.att = AttenuationParameters_SpotPower.xyz;
	light.spotPower = AttenuationParameters_SpotPower.w;
	light.range = Range.x;

	if (LightType.x == 0)
	{
		model_color = float4(ParallelLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}
	else if (LightType.x == 1)
	{
		model_color = float4(PointLight(surface_info, light, CameraPosition.xyz), model_color.a);
			
	}
	else if (LightType.x == 2)
	{
		model_color = float4(SpotLight(surface_info, light, CameraPosition.xyz), model_color.a);
	}	

    return model_color;
}