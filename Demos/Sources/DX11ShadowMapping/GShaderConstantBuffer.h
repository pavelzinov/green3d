#pragma once
#include "GUtility.h"

namespace Game
{

const uint32_t GSHADER_BUFFER_TYPE_SIMPLE = 0;
const uint32_t GSHADER_BUFFER_TYPE_LIGHT = 1;
const uint32_t GSHADER_BUFFER_TYPE_SHADOWS = 2;

// Remember that Shader Constant Buffers must be 16-byte aligned
// hence paddingX parameters present.
template <uint32_t shader_type>
struct GShaderConstantBuffer
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
};

template <>
struct GShaderConstantBuffer<GSHADER_BUFFER_TYPE_SIMPLE>
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;
};

template <>
struct GShaderConstantBuffer<GSHADER_BUFFER_TYPE_LIGHT>
{
	XMFLOAT4X4 World;
	XMFLOAT4X4 View;
	XMFLOAT4X4 Projection;

	struct LightParameters
	{
		XMFLOAT3 Position;
		float padding0;
		XMFLOAT3 Direction;
		float padding1;
		XMFLOAT4 AmbientColor;
		XMFLOAT4 DiffuseColor;
		XMFLOAT4 SpecularColor;
		XMFLOAT3 AttenuationParameters;
		float SpotPower;
		float Range;
		float padding2[3];
	} Light;
	int LightType; // 0 - parallel, 1 - point, 2 - spotlight
	int padding3[3];
	XMFLOAT3 CameraPosition;
	float padding4;
};

template <>
struct GShaderConstantBuffer<GSHADER_BUFFER_TYPE_SHADOWS>
{
	XMFLOAT4X4		World;
	XMFLOAT4X4		View;
	XMFLOAT4X4		Projection;

	struct LightParameters
	{
		XMFLOAT3	Position;
		float		padding0;
		XMFLOAT3	Direction;
		float		padding1;
		XMFLOAT4	AmbientColor;
		XMFLOAT4	DiffuseColor;
		XMFLOAT4	SpecularColor;
		XMFLOAT3	AttenuationParameters;
		float		SpotPower;
		float		Range;
		float		padding2[3];
	} Light;
	int				LightType; // 0 - parallel, 1 - point, 2 - spotlight
	int				padding3[3];
	XMFLOAT3		CameraPosition;
	float			padding4;
	XMFLOAT4X4		LightView;
	XMFLOAT4X4		LightProjection;
	int				ObjectColorType;
	int				padding5[3];
	XMFLOAT4		ObjectDiffuseColor;
};

}