#pragma once
#include "GUtility.h"
#include "GCamera.h"

namespace Game
{

enum GLIGHT_TYPE
{
	GLIGHT_TYPE_PARALLEL = 0,
	GLIGHT_TYPE_POINT = 1,
	GLIGHT_TYPE_SPOT = 2
};

//-----------------------------------------------------
// * Class GLight *
//-----------------------------------------------------
// Holds parameters for parallel, point and spot lights.
// * Parallel light *:
// - mDirection - light direction
// - All colors - light ray colors
// * Point light *:
// - mPosition - position of light
// - All colors - light ray colors
// - mAttenuationParameters - defines light fading ~ I/(a0 + a1 * d + a2 * d2). 
// So, for example, if a0 and a2 equals to 0, fading will be linear.
// - mRange - this is a d parameter for light equation. It defines light sphere around mPosition
// * Spot light *:
// - mPosition - spot light position
// - mDirection - where spot light is looking at
// - All colors - the same as previous
// - mAttenuationParameters - defines spot light volume fading
// - mSpotPower - implicitly defines spot light cone volume maximum angle
// - mRange - the length of light volume
class GLight
{
public:
	GLight(void);
	~GLight(void) {}

private:
	XMFLOAT3 mPosition;
	XMFLOAT3 mDirection;
public:
	void SetPos(float x, float y, float z);
	XMFLOAT3 GetPos() { return mPosition; }
	void SetDirection(XMFLOAT3 direction);
	XMFLOAT3 GetDirection() { return mDirection; }
	
	XMFLOAT4 mAmbientColor;
	XMFLOAT4 mDiffuseColor;
	XMFLOAT4 mSpecularColor;
	XMFLOAT3 mAttenuationParameters;
	float mSpotPower;
	float mRange;
	int mType; // 0 - parallel, 1 - point, 2 - spotlight

public:
	void OnResize(uint32_t new_width, uint32_t new_height) { mCamera.OnResize(new_width, new_height); }
	GCamera mCamera;	
};

}