#include "GPlane.h"
using namespace Game;

template<>
void GPlane<GColoredVertex>::GenerateVertices()
{
	float width_segment_length = mWidth/mWidthSegments;
	float height_segment_length = mHeight/mHeightSegments;
	XMFLOAT3 point(0.0f, 0.0f, 0.0f);
	XMFLOAT3 translate_coords(-mWidth/2.0f, 0.0f, mHeight/2.0f);
	uint32_t vertices_count = (mWidthSegments+1)*(mHeightSegments+1);

	mVertices.resize(vertices_count);
	mVertices[0].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
	for (uint32_t i=1; i<vertices_count; ++i)
	{
		// if point is first in line
		if (i % (mWidthSegments + 1) == 0)
		{// reset it's coordinates
			point.x = 0.0f;
			point.z -= height_segment_length;
		}
		else
		{
			point.x += width_segment_length;
		}

		mVertices[i].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
		mVertices[i].mNormal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		mVertices[i].mColor = GCOLOR_DARK_YELLOW_GREEN;
	}
}

template<>
void GPlane<GTexturedVertex>::GenerateVertices()
{
	float width_segment_length = mWidth/mWidthSegments;
	float height_segment_length = mHeight/mHeightSegments;
	float width_texel_length = 1.0f/mWidthSegments;
	float height_texel_length = 1.0f/mHeightSegments;
	XMFLOAT3 point(0.0f, 0.0f, 0.0f);
	XMFLOAT2 point_tex_coords(0.0f, 0.0f);
	XMFLOAT3 translate_coords(-mWidth/2.0f, 0.0f, mHeight/2.0f);
	uint32_t vertices_count = (mWidthSegments+1)*(mHeightSegments+1);

	mVertices.resize(vertices_count);
	mVertices[0].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
	mVertices[0].mTexturedCoordinates = point_tex_coords;
	for (uint32_t i=1; i<vertices_count; ++i)
	{
		// if point is first in line
		if (i % (mWidthSegments + 1) == 0)
		{// reset it's coordinates
			point.x = 0.0f;
			point.z -= height_segment_length;
			point_tex_coords.x = 0;
			point_tex_coords.y += height_texel_length;
		}
		else
		{
			point.x += width_segment_length;
			point_tex_coords.x += width_texel_length;
		}

		mVertices[i].mPosition = GMathVF(GMathFV(point) + GMathFV(translate_coords));
		mVertices[i].mTexturedCoordinates = point_tex_coords;

		// TODO: Tangent, binormal
	}
}
