#pragma once
#include "GUtility.h"

namespace Game
{
////////////////////////////////////////////////////////
// Stores View and Projection matrices used by shaders
// to translate 3D world into 2D screen surface
// Camera can be moved
////////////////////////////////////////////////////////
class GCamera
{
public:
	// Constructs default camera looking at 0,0,0
	// placed at 0,0,-1 with up vector 0,1,0 (note that mUp is NOT a vector - it's vector's end)
    GCamera(void);
	// Create camera, based on another one
	GCamera(const GCamera& camera);
	// Copy all camera's parameters
    GCamera& operator=(const GCamera& camera);
    ~GCamera(void);

private:
	// Initialize camera's View matrix from mPosition, mTarget and mUp coordinates
	void initViewMatrix();
public:
	// Initialize camera's Projection matrix
	void initProjMatrix(const float angle, const float clientWidth, const float clientHeight, 
								const float nearest, const float farthest);

	void InitOrthoMatrix(const float clientWidth, const float clientHeight,
		const float nearZ, const float fartherZ);

	// Resize matrices when window size changes
	void OnResize(uint32_t new_width, uint32_t new_height);

	///////////////////////////////////////////////
	/*** View matrix transformation interfaces ***/
	///////////////////////////////////////////////

	// Move camera
	void Move(XMFLOAT3 direction);
	// Rotate camera around `axis` by `degrees`. Camera's position is a 
	// pivot point of rotation, so it doesn't change
	void Rotate(XMFLOAT3 axis, float degrees);
	// Set camera position coordinates
    void SetPosition(XMFLOAT3& new_position);
	// Get camera position coordinates
	XMFLOAT3 GetPosition() { return mPosition; }
	// Change camera target position
	void SetTarget(XMFLOAT3 new_target);
	// Get camera's target position coordinates
	XMFLOAT3 GetTarget() { return mTarget; }
	// Get camera's up vector
	XMFLOAT3 GetUp() { return GMathVF(GMathFV(mUp) - GMathFV(mPosition)); }
	// Get camera's look at target vector
	XMFLOAT3 GetLookAtTarget() { return GMathVF(GMathFV(mTarget) - GMathFV(mPosition)); }	
	// Returns transposed camera's View matrix	
    XMFLOAT4X4 GetView() { return GMathMF(XMMatrixTranspose(GMathFM(mView))); }

	/////////////////////////////////////////////////////
	/*** Projection matrix transformation interfaces ***/
	/////////////////////////////////////////////////////

	// Set view frustum's angle
	void SetAngle(float angle);
	// Get view frustum's angle
	float GetAngle() { return mAngle; }

	// Set camera projection field width
	void SetClientWidth(float clientWidth);
	// Set camera projection field height
	void SetClientHeight(float clientHeigth);

	// Set nearest culling plane distance from view frustum's projection plane
	void SetNearestPlane(float nearest);
	// Set farthest culling plane distance from view frustum's projection plane
	void SetFarthestPlane(float farthest);

	// Returns transposed camera's Projection matrix
	XMFLOAT4X4 GetProj() { return GMathMF(XMMatrixTranspose(GMathFM(mProj))); }
	// Returns transposed orthogonal camera matrix
	XMFLOAT4X4 GetOrtho() { return GMathMF(XMMatrixTranspose(GMathFM(mOrtho))); }

private:
    /*** Camera parameters ***/
    XMFLOAT3 mPosition;		// Camera's coordinates
    XMFLOAT3 mTarget;		// View target's coordinates
    XMFLOAT3 mUp;			// Camera's up coordinates

	/*** Projection parameters ***/
	float mAngle;			// Angle of view frustum
	float mClientWidth;		// Window's width
	float mClientHeight;	// Window's height
	float mNearest;			// Nearest view frustum plane
	float mFarthest;		// Farthest view frustum plane

    XMFLOAT4X4  mView;		// View matrix
	XMFLOAT4X4	mProj;		// Projection matrix
	XMFLOAT4X4	mOrtho;		// Ortho matrix for drawing without tranformation
};

} // namespace Game