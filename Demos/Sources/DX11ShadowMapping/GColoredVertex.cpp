#include "GColoredVertex.h"
using namespace Game;

GColoredVertex::GColoredVertex(void)
{
	this->mPosition = XMFLOAT3(0.0f, 0.0f, 0.0f);
	this->mNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	this->mColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);	
}

GColoredVertex::GColoredVertex(const GColoredVertex &vertex)
{
	this->mPosition = vertex.mPosition;
	this->mNormal = vertex.mNormal;
	this->mColor = vertex.mColor;	
}

uint32_t GColoredVertex::getDataSize()
{
	return sizeof(GColoredVertexData);
}
