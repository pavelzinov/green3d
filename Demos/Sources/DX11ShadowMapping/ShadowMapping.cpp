#include "ShadowMapping.h"
#include "DDSTextureLoader.h"

ShadowMapping::ShadowMapping(HINSTANCE hInstance) : GApplication(hInstance)
{
    // init background color
    GApplication::mClearColor = GCOLOR_BEACH_SAND;

    mDistance = 100.0f;

	XMFLOAT3 pos(1, 2, -100);
	XMFLOAT3 target(3, 4, 5);
	XMFLOAT3 up(0, 1, 0);

	XMFLOAT4X4 view = GMathMF(XMMatrixTranspose(XMMatrixLookAtLH(GMathFV(pos), GMathFV(target), GMathFV(up))));

	mCamera.SetPosition(XMFLOAT3(0.0f, mDistance, -mDistance));
	mCamera.SetTarget(XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera.initProjMatrix(GMATH_PIDIV2, static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.01f, 1000.0f);
	mCamera.InitOrthoMatrix(static_cast<const float>(mClientWidth),
		static_cast<const float>(mClientHeight), 0.0f, 1000.0f);	
}

void ShadowMapping::initApp()
{ 
	GApplication::initApp();

	// Load shaders
	mpColoredShader = new GShader<GColoredVertex, GSHADER_BUFFER_TYPE_LIGHT>(md3dDevice, md3dContext);
	mpColoredShader->LoadShader("Data/Shaders/colored_light.fx");
	mpFontShader = new GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpFontShader->LoadShader("Data/Shaders/tex_orthogonal.fx");
	mpDepthShader = new GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpDepthShader->LoadShader("Data/Shaders/depth.fx");
	mpTexturedShader = new GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SHADOWS>(md3dDevice, md3dContext);
	mpTexturedShader->LoadShader("Data/Shaders/tex_bump_light_shadow.fx");
	mpLightMappedShader = new GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>(md3dDevice, md3dContext);
	mpLightMappedShader->LoadShader("Data/Shaders/tex_light_mapping.fx");

	// Load materials
	mpMaterialsManager = new GMaterialsManager(md3dDevice, md3dContext);
	mpMaterialsManager->AddMaterial("Space", "Data/Textures/space.dds");
	mpMaterialsManager->AddMaterial("Water", "Data/Textures/water.dds");
	mpMaterialsManager->AddMaterial("VS2012", "Data/Textures/visual_studio_2012.dds");
	mpMaterialsManager->AddMaterial("Courier New 12pt Dark Green", "Data/Fonts/courier_new_12_dark_green.dds");
	mpMaterialsManager->AddMaterial("Segoe UI Semilight 10pt Yellow", "Data/Fonts/segoe_semilight_10_yellow.dds");
	mpMaterialsManager->AddMaterial("Brick", "Data/Textures/brick.dds");
	mpMaterialsManager->AddMaterial("Brick Normal Map", "Data/Textures/brick_bump.dds");
	mpMaterialsManager->AddMaterial("Circle Light Map", "Data/Textures/brick_light_map.dds");
	mpMaterialsManager->AddMaterial("Cursor", "Data/Textures/cursor.dds");
	mpMaterialsManager->AddMaterial("RenderedTexture0", mpWindowRenderTargetView, mDepthStencilView, 
		mClientWidth, mClientHeight);

	// Load light
	mLight.mType = GLIGHT_TYPE_PARALLEL;
	mLight.SetDirection(XMFLOAT3(1.0f, 0.0f, 0.0f));
	mLight.SetPos(0.0f, 1000.0f, 0.0f);
	mLight.mDiffuseColor = GCOLOR_WHITE;
	mLight.mAmbientColor = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	mLight.mSpecularColor = GCOLOR_BLACK;
	mLight.mAttenuationParameters = XMFLOAT3(0.0f, 0.01f, 0.0f);
	mLight.mSpotPower = 20.0f;
	mLight.mRange = 100.0f;

	// Load text
	mText.push_back(new GText(md3dDevice, md3dContext, mClientWidth, mClientHeight,
		mpMaterialsManager->GetMaterial("Courier New 12pt Dark Green"), 
		""));
	mText.back()->SetPosition(XMUINT2(4, 4), mClientWidth, mClientHeight);

	// Load models
	mColoredModels.push_back(new GMesh<GTexturedVertex>("ColoredCube0", md3dDevice, md3dContext));
	mColoredModels.back()->LoadFromObj("Data/Models/cube_l40.obj");
	mColoredModels.back()->mDiffuseColor = GCOLOR_YELLOW;

	mColoredModels.push_back(new GMesh<GTexturedVertex>("ColoredSphere0", md3dDevice, md3dContext));
	mColoredModels.back()->LoadFromObj("Data/Models/sphere_textured.obj");
	mColoredModels.back()->Move(0.0f, 100.0f, 0.0f);
	mColoredModels.back()->Scale(2.0f);
	mColoredModels.back()->mDiffuseColor = GCOLOR_LIGHT_YELLOW_GREEN;

	// Load light's representation
	mColoredModels.push_back(new GMesh<GTexturedVertex>("LightDirection", md3dDevice, md3dContext));
	mColoredModels.back()->LoadFromObj("Data/Models/arrow.obj");
	mColoredModels.back()->mDiffuseColor = GCOLOR_BEACH_SAND;

	mTexturedModels.push_back(new GMesh<GTexturedVertex>("Terrain", md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/plane_l100.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Scale(3.0f);

	mTexturedModels.push_back(new GMesh<GTexturedVertex>("TexturedCube0", md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/cube_l40.obj");
	mTexturedModels.back()->SetMaterialName("Brick");
	mTexturedModels.back()->Move(100.0f, 0.0f, -100.0f);

	mTexturedModels.push_back(new GMesh<GTexturedVertex>("TexturedSphere0", md3dDevice, md3dContext));
	mTexturedModels.back()->LoadFromObj("Data/Models/sphere_textured.obj");
	mTexturedModels.back()->SetMaterialName("Water");
	mTexturedModels.back()->Scale(2.0f);
	mTexturedModels.back()->Move(100.0f, 0.0f, 0.0f);

	mDebugWindow = new GMesh<GTexturedVertex>("DebugWindow", md3dDevice, md3dContext);
	mDebugWindow->LoadFromObj("Data/Models/plane_l100.obj");
	mDebugWindow->SetMaterialName("RenderedTexture0");
	mDebugWindow->Scale(2.0f * mClientWidth / mClientHeight, 2.0f, 2.0f);
	mDebugWindow->Move(100.0f, -100.0f, 0.0f);
	mDebugWindow->RotateX(-90.0f);

	mCursor = new GMesh<GTexturedVertex>("Cursor", md3dDevice, md3dContext);
	mCursor->LoadFromObj("Data/Models/plane_l100.obj");
	mCursor->SetMaterialName("Cursor");
	mCursor->Scale(0.32f, 1.0f, 0.32f);
	mCursor->RotateX(-90.0f);
	mCursor->Move(16.0f, -16.0f, 0.0f);
}

void ShadowMapping::onResize()
{
    GApplication::onResize();
	mCamera.OnResize(mClientWidth, mClientHeight);
	// Update text position when resizing window
	mText[0]->SetPosition(XMUINT2(mText[0]->GetPosition().x, mText[0]->GetPosition().y), 
		mClientWidth, mClientHeight);
	// Resize render texture
	mpMaterialsManager->OnResize("RenderedTexture0", mpWindowRenderTargetView, mDepthStencilView, 
		mClientWidth, mClientHeight);
	mLight.OnResize(mClientWidth, mClientHeight);	
}

void ShadowMapping::updateScene(float dSeconds)
{
    GApplication::updateScene(dSeconds);

	std::ostringstream stream;
	stream << "UPS: " << mUPS <<
		"\nMilliseconds per update: " << mMsPerUpdate <<
		"\nFPS: " << mFPS <<
		"\nMilliseconds per frame: " << mMsPerFrame <<
		"\nCamera position: " << mCamera.GetPosition().x << " " << 
		mCamera.GetPosition().y << " " << mCamera.GetPosition().z << 
		"\nCamera target position: " << mCamera.GetTarget().x << " " <<
		mCamera.GetTarget().y << " " << mCamera.GetTarget().z <<
		"\nCamera Up vector: " << mCamera.GetUp().x << " " <<
		mCamera.GetUp().y << " " << mCamera.GetUp().z <<
		"\nLight direction: " << mLight.GetDirection().x << " " <<
		mLight.GetDirection().y << " " << mLight.GetDirection().z <<
		"\nMouse X: " << mInput.MousePos().x << " Y: " << mInput.MousePos().y;
	mText[0]->SetText(stream.str());

	// Camera controls:
	if (mInput.IsKeyDown('W'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('S'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.z -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('D'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x -= 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('A'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.x += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown(VK_SPACE))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y += 1.0f;
		mCamera.SetPosition(new_camera_position);
	}
	if (mInput.IsKeyDown('X'))
	{
		XMFLOAT3 new_camera_position = mCamera.GetPosition();
		new_camera_position.y -= 2.0f;
		mCamera.SetPosition(new_camera_position);
	}

	// Light controls:
	if (mInput.IsKeyDown(VK_LEFT))
	{
		mLight.SetDirection(GMathVF(XMVector3Rotate(GMathFV(mLight.GetDirection()), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), -0.1f))));
		mLight.SetPos(mLight.GetPos().x - 3.0f, mLight.GetPos().y, mLight.GetPos().z);
		for (auto model : mColoredModels)
		{
			if (model->GetName() == "LightDirection")
				model->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(-0.1f));
		}
	}
	if (mInput.IsKeyDown(VK_RIGHT))
	{
		mLight.SetDirection(GMathVF(XMVector3Rotate(GMathFV(mLight.GetDirection()), 
			XMQuaternionRotationAxis(GMathFV(XMFLOAT3(0.0f, 0.0f, -1.0f)), 0.1f))));
		mLight.SetPos(mLight.GetPos().x + 3.0f, mLight.GetPos().y, mLight.GetPos().z);
		for (auto model : mColoredModels)
		{
			if (model->GetName() == "LightDirection")
				model->Rotate(XMFLOAT3(0.0f, 0.0f, -1.0f), XMConvertToDegrees(0.1f));
		}
	}

	for (auto model : mTexturedModels)
	{
		if (model->GetName() == "TexturedSphere0")
			model->Rotate(XMFLOAT3(0.0f, 1.0f, 0.0f), XMConvertToDegrees(0.01f));
	}

	GApplication::mInput.UpdateMouseMove();
}

void ShadowMapping::drawScene()
{
	GApplication::drawScene();

	mpMaterialsManager->RenderToTexture(const_cast<XMFLOAT4&>(GCOLOR_WHITE));

    // Reset shader matrices for this frame
	GApplication::TurnZBufferOn();
	mpDepthShader->ClearConstantBuffer();
	mpDepthShader->SetShader();
	mpDepthShader->DisableAlpha();
	mpDepthShader->mShaderConstBuffer0.View = mLight.mCamera.GetView();
	mpDepthShader->mShaderConstBuffer0.Projection = mLight.mCamera.GetOrtho();
	for (auto model : mColoredModels)
	{
		mpDepthShader->mShaderConstBuffer0.World = model->GetWorld();
		mpDepthShader->UpdateConstantBuffer();
		model->draw();
	}
	for (auto model : mTexturedModels)
	{
		mpDepthShader->mShaderConstBuffer0.World = model->GetWorld();
		mpDepthShader->UpdateConstantBuffer();
		model->draw();
	}

	mpMaterialsManager->RenderToScreen();
	GApplication::ClearScreen();

	GApplication::TurnZBufferOn();

	// Reset shader matrices for this frame
	mpColoredShader->ClearConstantBuffer();
	mpFontShader->ClearConstantBuffer();
	mpTexturedShader->ClearConstantBuffer();

	//mpColoredShader->SetShader();
	//mpColoredShader->DisableAlpha();

	//mpColoredShader->mShaderConstBuffer0.View = mCamera.GetView();
	//mpColoredShader->mShaderConstBuffer0.Projection = mCamera.GetProj();
	//mpColoredShader->mShaderConstBuffer0.CameraPosition = mCamera.GetPosition();

	//mpColoredShader->mShaderConstBuffer0.Light.Direction = mLight.mDirection;
	//mpColoredShader->mShaderConstBuffer0.Light.Position = mLight.mPosition;
	//mpColoredShader->mShaderConstBuffer0.Light.DiffuseColor = mLight.mDiffuseColor;
	//mpColoredShader->mShaderConstBuffer0.Light.AmbientColor = mLight.mAmbientColor;
	//mpColoredShader->mShaderConstBuffer0.Light.SpecularColor = mLight.mSpecularColor;
	//mpColoredShader->mShaderConstBuffer0.Light.AttenuationParameters = mLight.mAttenuationParameters;
	//mpColoredShader->mShaderConstBuffer0.Light.SpotPower = mLight.mSpotPower;
	//mpColoredShader->mShaderConstBuffer0.Light.Range = mLight.mRange;
	//mpColoredShader->mShaderConstBuffer0.LightType = mLight.mType;

	//for (auto model : mColoredModels)
	//{
	//	mpColoredShader->mShaderConstBuffer0.World = model->GetWorld();
	//	mpColoredShader->UpdateConstantBuffer();
	//	model->draw();
	//}

	mpTexturedShader->SetShader();
	mpTexturedShader->EnableAlpha();

	mpTexturedShader->mShaderConstBuffer0.View = mCamera.GetView();
	mpTexturedShader->mShaderConstBuffer0.Projection = mCamera.GetProj();
	mpTexturedShader->mShaderConstBuffer0.CameraPosition = mCamera.GetPosition();

	mpTexturedShader->mShaderConstBuffer0.Light.Direction = mLight.GetDirection();
	mpTexturedShader->mShaderConstBuffer0.Light.Position = mLight.GetPos();
	mpTexturedShader->mShaderConstBuffer0.Light.DiffuseColor = mLight.mDiffuseColor;
	mpTexturedShader->mShaderConstBuffer0.Light.AmbientColor = mLight.mAmbientColor;
	mpTexturedShader->mShaderConstBuffer0.Light.SpecularColor = mLight.mSpecularColor;
	mpTexturedShader->mShaderConstBuffer0.Light.AttenuationParameters = mLight.mAttenuationParameters;
	mpTexturedShader->mShaderConstBuffer0.Light.SpotPower = mLight.mSpotPower;
	mpTexturedShader->mShaderConstBuffer0.Light.Range = mLight.mRange;
	mpTexturedShader->mShaderConstBuffer0.LightType = mLight.mType;
	mpTexturedShader->mShaderConstBuffer0.LightView = mLight.mCamera.GetView();
	mpTexturedShader->mShaderConstBuffer0.LightProjection = mLight.mCamera.GetOrtho();

	std::vector<std::string> materials;
	for (auto model : mColoredModels)
	{
		materials.clear();
		materials.push_back("Brick Normal Map");
		materials.push_back("RenderedTexture0");
		mpMaterialsManager->SetCurrentMaterials(materials);
		mpTexturedShader->mShaderConstBuffer0.World = model->GetWorld();
		mpTexturedShader->mShaderConstBuffer0.ObjectColorType = 0;
		mpTexturedShader->mShaderConstBuffer0.ObjectDiffuseColor = model->mDiffuseColor;
		mpTexturedShader->UpdateConstantBuffer();
		model->draw();
	}
	for (auto model : mTexturedModels)
	{
		materials.clear();		
		materials.push_back("Brick Normal Map");
		materials.push_back("RenderedTexture0");
		materials.push_back(model->mMaterialName);
		mpMaterialsManager->SetCurrentMaterials(materials);
		mpTexturedShader->mShaderConstBuffer0.World = model->GetWorld();
		mpTexturedShader->mShaderConstBuffer0.ObjectColorType = 1;
		mpTexturedShader->UpdateConstantBuffer();
		model->draw();
	}

	// Draw 2D Text
	GApplication::TurnZBufferOff();
	mpFontShader->SetShader();
	mpFontShader->EnableAlpha();
	mpFontShader->mShaderConstBuffer0.View = GMathMF(XMMatrixIdentity());
	mpFontShader->mShaderConstBuffer0.Projection = mCamera.GetOrtho();

	mpMaterialsManager->SetCurrentMaterial(mDebugWindow->mMaterialName);
	mpFontShader->mShaderConstBuffer0.World = 
		GMathMF(XMMatrixTranspose(XMMatrixTranspose(GMathFM(mDebugWindow->GetWorld())) * XMMatrixTranslation(
		static_cast<float>(-mClientWidth)/2.0f + 0,	
		static_cast<float>(mClientHeight)/2.0f - 0, 
		0.0f)));
	mpFontShader->UpdateConstantBuffer();
	mDebugWindow->draw();
	
	for (auto text : mText)
	{
		mpMaterialsManager->SetCurrentMaterial(text->GetMaterialName());
		mpFontShader->mShaderConstBuffer0.World = text->GetWorld();
		mpFontShader->UpdateConstantBuffer();
		text->draw();
	}

	// draw cursor
	mpMaterialsManager->SetCurrentMaterial(mCursor->mMaterialName);
	mpFontShader->mShaderConstBuffer0.World = 
		GMathMF(XMMatrixTranspose(XMMatrixTranspose(GMathFM(mCursor->GetWorld())) * XMMatrixTranslation(
		static_cast<float>(-mClientWidth)/2.0f + mInput.MousePos().x,	
		static_cast<float>(mClientHeight)/2.0f - mInput.MousePos().y, 
		0.0f)));
	mpFontShader->UpdateConstantBuffer();
	mCursor->draw();

    mSwapChain->Present(0, 0);
}

ShadowMapping::~ShadowMapping(void)
{
	std::for_each(mText.begin(), mText.end(), 
		[](GText *item) { delete item; });
	std::for_each(mColoredModels.begin(), mColoredModels.end(), 
		[](GMesh<GTexturedVertex> *item) { delete item; });
	std::for_each(mTexturedModels.begin(), mTexturedModels.end(), 
		[](GMesh<GTexturedVertex> *item) { delete item; });

	delete mpMaterialsManager;

	delete mCursor;
	delete mDebugWindow;
	
	delete mpLightMappedShader;
	delete mpTexturedShader;
	delete mpDepthShader;
	delete mpFontShader;
	delete mpColoredShader;

    if( md3dContext )
        md3dContext->ClearState();
}