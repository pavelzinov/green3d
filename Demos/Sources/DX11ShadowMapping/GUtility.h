#pragma once
#include <windows.h>

#include <d3d11.h>
#include <directxmath.h>
#include <d3dcompiler.h>

#include <vector>
#include <map>
#include <algorithm>

#include <sstream>
#include <fstream>
#include <iostream>

#include <memory>

using namespace DirectX;
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;

typedef unsigned int uint;

// Releasing COM-object
inline void ReleaseCOM(IUnknown *x) 
{ 
	if(x != nullptr)
	{
		x->Release(); 
		x=nullptr;
	} 
}

// Safe pointer release
template <class any_type>
inline void PtrRelease(any_type *x) 
{ 
	if(x != nullptr)
	{
		delete x; 
		x=nullptr;
	} 
}

// Output DX11 debug information
#if defined(DEBUG) | defined(_DEBUG)
inline void HR(HRESULT hresult)
{
	LPTSTR errorText = NULL;
	FormatMessage(
		// use system message tables to retrieve error text
		FORMAT_MESSAGE_FROM_SYSTEM
		// allocate buffer on local heap for error text
		|FORMAT_MESSAGE_ALLOCATE_BUFFER
		// Important! will fail otherwise, since we're not 
		// (and CANNOT) pass insertion parameters
		|FORMAT_MESSAGE_IGNORE_INSERTS,  
		NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
		hresult,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&errorText,  // output 
		0, // minimum size for output buffer
		NULL);   // arguments - see note 

	if ( NULL != errorText )
	{
		OutputDebugString(errorText);
		// release memory allocated by FormatMessage()
		LocalFree(errorText);
		errorText = NULL;
	}
}
#elif
inline void HR(HRESULT hr)
{
	return;
}
#endif

// Return HRESULT statement
#define HRR(hr) { if (FAILED(hr)) return hr; }

// DX11 Colors
const XMFLOAT4 GCOLOR_WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_RED(1.0f, 0.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const XMFLOAT4 GCOLOR_CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
const XMFLOAT4 GCOLOR_LIGHT_BLUE(0.2f, 0.8f, 1.0f, 1.0f);
const XMFLOAT4 GCOLOR_LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
const XMFLOAT4 GCOLOR_DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
const XMFLOAT4 GCOLOR_DARKBROWN(0.45f, 0.39f, 0.34f, 1.0f);

// Math constants
const float GMATH_INFINITY = FLT_MAX;
const float GMATH_PI  = 3.14159265358979323f;
const float GMATH_PIDIV2 = 1.570796326794896615f;
const float GMATH_PIDIV4 = 0.7853981633974483075f;
const float GMATH_EPS = 0.0001f;

// Comprising two values by using MATH_EPS
template <class T>
inline bool GMathEqual(T x, T y)
{ 
	if (fabs(static_cast<float>(x-y)) <= GMATH_EPS)
		return true;
	return false;
}

inline std::wstring StringToWstring(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}

inline std::string WstringToString(const std::wstring& wide)
{
	std::string result = "";
	for (uint32_t i=0; i<wide.size(); i++)
	{
		result.push_back(static_cast<char>(wide[i]));
	}
	return result;
}

inline XMVECTOR GMathFV(XMFLOAT3& val)
{
	return XMLoadFloat3(&val);	
}

inline XMFLOAT3 GMathVF(XMVECTOR& vec)
{
	XMFLOAT3 val;
	XMStoreFloat3(&val, vec);
	return val;
}

inline XMMATRIX GMathFM(XMFLOAT4X4& val)
{
	return XMLoadFloat4x4(&val);
}

inline XMFLOAT4X4 GMathMF(XMMATRIX& matrix)
{
	XMFLOAT4X4 val;
	XMStoreFloat4x4(&val, matrix);
	return val;
}

inline float GMathRound(float& val)
{
	return (val > 0.0f) ? floor(val + 0.5f) : ceil(val - 0.5f);
}

inline float GMathFloatRound(float val)
{
	if (GMathEqual<float>(val, GMathRound(val)))
		return GMathRound(val);
	return val;
}

// Output Debug message
#if defined(DEBUG) | defined(_DEBUG)
inline void Log(std::string str)
{	
	OutputDebugString(StringToWstring(str).c_str());
}
#elif
inline void Log(std::string str)
{
	return;
}
#endif

// Split string using delimiters
inline std::vector<std::string> &SplitString(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) {
		if (item != "")
			elems.push_back(item);
	}
	return elems;
}

// Split string using delimiters
inline std::vector<std::string> SplitString(const std::string &s, char delim) {
	std::vector<std::string> elems;
	return SplitString(s, delim, elems);
}

// Get float from string
inline float FloatFromString(const string&s)
{
	return static_cast<float>(atof(s.c_str()));
}

// Get int from string
inline int IntFromString(const string&s)
{
	return static_cast<int>(atoi(s.c_str()));
}