#pragma once
#include "GUtility.h"
#include "GTexturedVertex.h"
#include "GColoredVertex.h"
#include "GShaderConstantBuffer.h"

namespace Game
{

template <typename vertex_type, uint32_t shader_buffer_type>
class GShader
{
public:
	GShader(ID3D11Device* device, ID3D11DeviceContext* context);
	~GShader(void);

public:
	// Load pixel/vertex shader from .fx file
	HRESULT LoadShader(std::string filename);
	// Set all vertex and pixel shaders along with their parameters
	void SetShader();
	// Enable alpha-blending
	void EnableAlpha();
	// Disable alpha-blending
	void DisableAlpha();
	// Clear shader constant buffer
	void ClearConstantBuffer() { ZeroMemory(&mShaderConstBuffer0, sizeof(mShaderConstBuffer0)); }
	// Update shader constant buffer
	void UpdateConstantBuffer();
	// How to interpret input vertices
	D3D_PRIMITIVE_TOPOLOGY mTopology;
	// Shader constant buffer
	GShaderConstantBuffer<shader_buffer_type> mShaderConstBuffer0;

private:
	// Compile shader from .fx file. Vertex and pixel shaders must be compiled independently
	HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, 
		LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

private:
	ID3D11Device*			mpDevice;
	ID3D11DeviceContext*	mpContext;

	ID3D11PixelShader*		mpPixelShader;
	ID3D11VertexShader*		mpVertexShader;
	ID3D11InputLayout*		mpVertexLayout;

	ID3D11BlendState*		mpCurrentBlendState;
	ID3D11BlendState*		mpBlendStateEnableAlpha;
	ID3D11BlendState*		mpBlendStateDisableAlpha;

	ID3D11Buffer*			mpPOConstantBuffer;
};

template <typename vertex_type, uint32_t shader_buffer_type>
GShader<vertex_type, shader_buffer_type>::GShader(ID3D11Device* device, ID3D11DeviceContext* context)
{
	mTopology					= D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	mpDevice					= device;
	mpContext					= context;
	mpPixelShader				= nullptr;
	mpVertexShader				= nullptr;
	mpVertexLayout				= nullptr;
	mpCurrentBlendState			= nullptr;
	mpBlendStateEnableAlpha		= nullptr;
	mpBlendStateDisableAlpha	= nullptr;
	mpPOConstantBuffer			= nullptr;
}

template <typename vertex_type, uint32_t shader_buffer_type>
GShader<vertex_type, shader_buffer_type>::~GShader(void)
{
	ReleaseCOM(mpPOConstantBuffer);
	mpBlendStateDisableAlpha = nullptr;
	ReleaseCOM(mpBlendStateEnableAlpha);
	mpCurrentBlendState	= nullptr;
	ReleaseCOM(mpVertexLayout);
	ReleaseCOM(mpVertexShader);
	ReleaseCOM(mpPixelShader);
}

template <typename vertex_type, uint32_t shader_buffer_type>
HRESULT GShader<vertex_type, shader_buffer_type>::CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, 
													LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DCompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA(static_cast<char*>(pErrorBlob->GetBufferPointer()));
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

template <typename vertex_type, uint32_t shader_buffer_type>
void GShader<vertex_type, shader_buffer_type>::EnableAlpha()
{
	mpCurrentBlendState = mpBlendStateEnableAlpha;
	mpContext->OMSetBlendState(mpBlendStateEnableAlpha, nullptr, 0xffffffff);
}

template <typename vertex_type, uint32_t shader_buffer_type>
void GShader<vertex_type, shader_buffer_type>::DisableAlpha()
{
	mpCurrentBlendState = mpBlendStateDisableAlpha;
	mpContext->OMSetBlendState(mpBlendStateDisableAlpha, nullptr, 0xffffffff);
}

template <typename vertex_type, uint32_t shader_buffer_type>
void GShader<vertex_type, shader_buffer_type>::UpdateConstantBuffer()
{
	mpContext->UpdateSubresource(mpPOConstantBuffer, 0, nullptr, &mShaderConstBuffer0, 0, 0);
}

template <typename vertex_type, uint32_t shader_buffer_type>
void GShader<vertex_type, shader_buffer_type>::SetShader()
{
	mpContext->IASetInputLayout(mpVertexLayout);
	mpContext->IASetPrimitiveTopology(mTopology);

	mpContext->VSSetShader(mpVertexShader, nullptr, 0);
	mpContext->VSSetConstantBuffers(0, 1, &mpPOConstantBuffer);

	mpContext->PSSetShader(mpPixelShader, nullptr, 0);
	mpContext->PSSetConstantBuffers(0, 1, &mpPOConstantBuffer);

	// set alpha-blending for opacity
	mpContext->OMSetBlendState(mpCurrentBlendState, nullptr, 0xffffffff);
}

}