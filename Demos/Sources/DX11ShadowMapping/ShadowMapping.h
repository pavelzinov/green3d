#pragma once
#include "GApplication.h"
#include "GCamera.h"
#include "GTriangle.h"
#include "GRandom.h" 
#include "GColoredVertex.h"
#include "GMesh.h"
#include "GPlane.h"
#include "GMaterialsManager.h"
#include "GBillboard.h"
#include "GShader.h"
#include "GText.h"
#include "GLight.h"
#include "GObjFile.h"
#include "GImage.h"

using namespace Game;

// Class ShadowMapping.
class ShadowMapping : public GApplication
{
    // Public constructor and destructor
public:
    ShadowMapping(HINSTANCE hInstance);
    ~ShadowMapping(void);

public:
    void initApp();

private:
    void onResize();
    void updateScene(float dSeconds);
    void drawScene();

private:
	GMaterialsManager*											mpMaterialsManager;
	GShader<GColoredVertex, GSHADER_BUFFER_TYPE_LIGHT>*			mpColoredShader;
	GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>*		mpFontShader;
	GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>*		mpDepthShader;
	GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SHADOWS>*		mpTexturedShader;
	GShader<GTexturedVertex, GSHADER_BUFFER_TYPE_SIMPLE>*		mpLightMappedShader;

private:
    GCamera	mCamera;	// main camera
    float	mDistance;	// camera position
	GRandom	mRandom;	// random generator

	std::vector<GMesh<GTexturedVertex>*>	mTexturedModels;
	std::vector<GMesh<GTexturedVertex>*>		mColoredModels;
	GMesh<GTexturedVertex>*					mDebugWindow;
	GMesh<GTexturedVertex>*					mCursor;
	
	std::vector<GText*>						mText;
	GLight									mLight;
	
};