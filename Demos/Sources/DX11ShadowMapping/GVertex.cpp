#include "GVertex.h"
using namespace Game;

GVertex::GVertex(void)
{
	this->mPosition = XMFLOAT3(0, 0, 0);
	this->mNormal = XMFLOAT3(0, 0, 0);
}

GVertex::GVertex(XMFLOAT3 position, XMFLOAT3 normal)
{
	this->mPosition = position;
	this->mNormal = normal;
}

uint32_t GVertex::getDataSize()
{
	return 2*sizeof(XMFLOAT3);
}
