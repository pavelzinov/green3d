#pragma once
#include "GUtility.h"
#include "GMaterial.h"

namespace Game
{
////////////////////////////////////////////////////////
// Manager designed for storing all game materials in
// single place, calling them when needed through special
// string identifiers. Those identifiers are created by
// this manager when material is added to collection.
// Meshes and other primitives are storing those
// identifiers instead of Material itself.
////////////////////////////////////////////////////////
// Additionaly, materials manager is responsible for
// rendering to texture, instead of screen. To enable this functionality,
// call InitRenderingToTexture - this will create texture and render target views.
// Don't forget to call OnResize, when window change it's size, so render target texture
// have to be resized. Call RenderToTexture to enable rendering to texture and call
// RenderToScreen to restore default rendering mechanism. Texture, that has been
// made by rendering to texture can be set into shader via SetCurrentMaterial("RenderTexture0")
////////////////////////////////////////////////////////
class GMaterialsManager
{
public:
	GMaterialsManager(ID3D11Device* device, ID3D11DeviceContext* context);
	~GMaterialsManager(void);

public:
	// Add external material into manager
	bool AddMaterial(GMaterial& material);
	// Create and add material with given parameters
	bool AddMaterial(std::string material_name, std::string texture_file);
	// Get material's reference by it's name
	GMaterial& GetMaterial(std::string material_name) { return mMaterials.at(material_name); }
	// Get all materials' names
	std::vector<std::string> GetMaterialNames();
	// Set material with name `material_name` as Textured2D resource in current shader
	void SetCurrentMaterial(std::string material_name);
	// Set materials with given names as Textured2D array resource in current shader
	void SetCurrentMaterials(std::vector<std::string> material_names);
	
	// Add render target as material. When resizing render target, OnResize() method must be called
	// with this material name as first parameter
	HRESULT AddMaterial(std::string material_name, ID3D11RenderTargetView* window_render_target_view, 
		ID3D11DepthStencilView* depth_stencil_view, uint32_t texture_width, uint32_t texture_height);
	// From the point of this function call, rendering goes into texture
	void RenderToTexture(XMFLOAT4& clear_color);
	// From the point of this function call, rendering goes onto the screen
	void RenderToScreen();
	// Call this function, when window has been resized. It will properly resize render target texture
	void OnResize(std::string material_name, ID3D11RenderTargetView* window_render_target_view, 
		ID3D11DepthStencilView* depth_stencil_view, uint32_t texture_width, uint32_t texture_height);

private:
	// Associative container where each material can be referenced with it's string name
	std::map<std::string, GMaterial>	mMaterials;
	// The name of material, currently set in shader
	std::string							mCurrentMaterialName;
	// The names of materials, currently set in shader
	std::vector<std::string>			mCurrentMaterialNames;

private:
	ID3D11Device*			mpDevice;
	ID3D11DeviceContext*	mpContext;

	// Original window's render target. Needed for when restoring rendering to screen
	ID3D11RenderTargetView*		mpWindowRenderTargetView;
	// Original window's depth-stencil
	ID3D11DepthStencilView*		mpDepthStencilView;
	// Render target view on 2D texture
	ID3D11RenderTargetView*		mpRenderTagetView;
};

}

