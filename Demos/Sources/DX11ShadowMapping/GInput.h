#pragma once
#include "GUtility.h"
#include "GTimer.h"
#include <sstream>

/////////////////////////////////////////////////////
// Class GInput
//---------------------------------------------------
// - Based on Raw Input
// - Updating states only when some input came to application from device
// - How to make this class work:
// --- Init Raw Input somewhere
// --- Place Update() in WM_INPUT messge
// --- Place UpdateMouseMove() at the end of every frame update
// - Allows:
// --- Check if mouse buttons are down, up or clicked
// --- Check keyboard for key pressed, down or up
/////////////////////////////////////////////////////
namespace Game
{

class GInput
{
public:
    GInput(void) {}
	~GInput(void) {}

    void Initialize(HWND handle);           // init Raw Input  
    void Update(HRAWINPUT hRawInput, uint client_width, uint client_height);   // call from WM_INPUT message
    void UpdateMouseMove();             // call this at the end of every frame update!

	const POINT& MousePos() const { return mSimpleCursorPosition; }

    long GetAbsoluteX(void);
    long GetAbsoluteY(void);
    long GetRelativeX(void);
    long GetRelativeY(void);
    float GetMouseSensitivity();
    void SetMouseSensitivity(float val);
    bool IsMouseLeftClicked(void);
    bool IsMouseRightClicked(void);
    bool IsMouseLeftDown(void);
    bool IsMouseRightDown(void);
    bool IsKeyDown(USHORT symbol);
    bool IsKeyUp(USHORT symbol);
    bool IsKeyPressed(USHORT symbol);
    bool IsScrollDown(void);
    bool IsScrollUp(void);

private:
    // Inner timer
    GTimer mScrollTimer;
    // Mouse cursor position
    long mCurrentX;
    long mCurrentY;
    long mPreviousX;
    long mPreviousY;
    // Mouse states
    bool mMouseLeftButtonDown;
    bool mMouseLeftButtonUp;
    bool mMouseRightButtonDown;
    bool mMouseRightButtonUp;
    bool mMouseMiddleButtonDown;
    bool mMouseMiddleButtonUp;
    // Mouse scrolling
    bool mScrollDown;
    bool mScrollUp;    
    // Mouse property
    float mScrollDelay; // how long to scroll
    float mMouseSensitivity; // by how much to move mouse per input
    // Keyboard
    bool mKeysDown[1000];
    bool mKeysUp[1000];

	// Simple screen coordinates
	POINT mSimpleCursorPosition;

    // Screen resolution
    int mScreenWidth;
    int mScreenHeight;

    // Temp variables
    float mTmpDX;
    float mTmpDY;

	HWND mWindowHandle;
};

}